import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';

InputDecoration kTxtFieldDecoration = InputDecoration(
  hoverColor: servicecardfillcolor,
  enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Color(0xFFD3D3D3), width: 1.0),
    borderRadius: BorderRadius.all(
      Radius.circular(5.0),
    ),
  ),
  focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Color(0xFFD3D3D3), width: 1.0),
    borderRadius: BorderRadius.all(
      Radius.circular(5.0),
    ),
  ),
  errorBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.red, width: 1.0),
    borderRadius: BorderRadius.all(
      Radius.circular(5.0),
    ),
  ),
  focusedErrorBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Color(0xFF5C75B3), width: 1.0),
    borderRadius: BorderRadius.all(
      Radius.circular(5.0),
    ),
  ),
);

BoxDecoration kBorderDecoration = BoxDecoration(
  border: Border.all(color: txtborderbluecolor),
  borderRadius: BorderRadius.all(Radius.circular(15.0)),
);

InputDecoration kOTPDecoration = InputDecoration(
  contentPadding: EdgeInsets.all(15.0),
  enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Color(0xFF5C75B3)),
    borderRadius: BorderRadius.all(
      Radius.circular(10.0),
    ),
  ),
  focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Color(0xFF5C75B3)),
    borderRadius: BorderRadius.all(
      Radius.circular(10.0),
    ),
  ),
);

InputDecoration kSearchTxtFieldDecoration = InputDecoration(
  // contentPadding: EdgeInsets.all(15.0),
  prefixIcon: Image.asset(
    "assets/icons/ic_search.png",
    color: greytxtcolor,
  ),
  hintText: "Search your best deal",
  hintStyle: TextStyle(color: greytxtcolor),
  enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Color(0xFF5C75B3)),
    borderRadius: BorderRadius.all(
      Radius.circular(10.0),
    ),
  ),
  focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Color(0xFF5C75B3)),
    borderRadius: BorderRadius.all(
      Radius.circular(10.0),
    ),
  ),
);

TextStyle kTxtStyle = TextStyle(
  color: Colors.white,
);

TextStyle kHeadText = TextStyle(
  fontSize: 25.0,
  fontWeight: FontWeight.bold,
  color: Colors.white,
);

TextStyle kOTPStyle = TextStyle(
  color: Colors.white,
  fontSize: 28.0,
  fontWeight: FontWeight.bold,
);
