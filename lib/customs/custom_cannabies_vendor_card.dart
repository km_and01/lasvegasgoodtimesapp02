import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_hotel_display_card.dart';
import 'package:lasvegas_gts_app/screens/services/businessService/cannabies_product_listing_scr.dart';
import 'package:lasvegas_gts_app/screens/services/fashion/fashon_product_listing_scr.dart';

// String amount = '';
String vendorFashionID = '';
// String serviceEventSpaceID = '';
// List<String> foodNameListCatering = [];

String sDate = '', sType = '', sCity = '', noPerson = '';

class CustomCanneabisVendorCard extends StatefulWidget {
  final String shortDisc;
  final String vendorID,add1,add2,vendrDetail;
  // final String sDate, sType, sCity;
  final String bname, price, priceUnit,CannabisVendorLogo;
  final List CannabisVendorIMG;
  final List photoVendorIMG;

  const CustomCanneabisVendorCard({
    Key? key,
    required this.shortDisc,
    required this.vendorID,
    // required this.sDate,
    // required this.sType,
    // required this.sCity,

    required this.CannabisVendorIMG,
    required this.bname,
    required this.price,
    // required this.eventspsService,
    required this.priceUnit,
    required this.CannabisVendorLogo,
    required this.photoVendorIMG,
    required this.add1, required this.add2, required this.vendrDetail,
  }) : super(key: key);

  State<CustomCanneabisVendorCard> createState() =>
      _CustomCanneabisVendorCardState();
}

class _CustomCanneabisVendorCardState extends State<CustomCanneabisVendorCard> {
  @override
  void initState() {
    print("Vendor fashion id though widget ========" + widget.vendorID);
    print("Vendor fashion Name though widget ========" + widget.bname);
    // amount = widget.price;
    vendorFashionID = widget.vendorID;
    // serviceEventSpaceID = widget.eventspsService;
    // foodNameListCatering = widget.foodName;
    setState(() {
      // sDate = widget.sDate;
      // sCity = widget.sCity;
      // sType = widget.sType;
      // noPerson = widget.noPerson;
    });
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      margin: EdgeInsets.symmetric(vertical: 7.5),
      decoration: BoxDecoration(
        // color: servicecardfillcolor,
        border: Border.all(color: txtborderbluecolor),
        borderRadius: BorderRadius.all(Radius.circular(7.0)),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(7.0),
              bottomLeft: Radius.circular(7.0),
            ),
            child: Image.network(
              bLogoUrl + widget.CannabisVendorLogo,
              fit: BoxFit.fill,
              height: 150,
              width: 150,
            ),
            //     Image.asset(
            //   "assets/images/restaurent1.png",
            // ),
          ),
          SizedBox(
            width: 15.0,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              // SizedBox(
              //   height: 10.0,
              // ),
              Text(
                widget.bname,
                // "Event Name",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 18.0,
                    fontFamily: 'Raleway-Medium'),
              ),
              RatingBar.builder(
                glow: false,
                itemSize: 15.0,
                initialRating: 3,
                minRating: 1,
                direction: Axis.horizontal,
                allowHalfRating: true,
                itemCount: 5,
                //itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
                itemBuilder: (context, _) => Icon(
                  Icons.star,
                  color: Colors.amber,
                  //   Image.asset(
                  // "assets/icons/rating_star.png",
                  // color: Colors.amber,
                ),
                // ),
                unratedColor: Colors.grey,
                onRatingUpdate: (rating) {
                  print(rating);
                },
              ),

              SizedBox(
                width: 200.0,
                child: Text(
                  widget.shortDisc,
                  // "Event Name",
                  maxLines: 2,
                  style: TextStyle(
                      color: greytxtcolor,
                      fontSize: 13.0,
                      fontFamily: 'Manrope'),
                ),
              ),
              SizedBox(
                height: 3.0,
              ),
              Text(
                "Price Starts from " + widget.priceUnit + " " + widget.price,
                // "456 - Lorem ipsum dolor sit amet,\nconsectetur adipiscing elit.",
                style: TextStyle(
                    fontSize: 16.0,
                    fontFamily: 'Manrope-SemiBold',
                    color: Colors.white
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  print("Vendor fashion id though Btn ========" +
                      widget.vendorID);
                  print(
                      "Vendor fashion name though Btn ========" + widget.bname);
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => CannabiesProductListingScr(
                        fashionVendorID: widget.vendorID,
                        pkgIMGList: widget.CannabisVendorIMG,
                        bname: widget.bname,
                        add1: widget.add1,
                        add2: widget.add2,
                        vendrDetail: widget.vendrDetail,
                        photoVendorIMG: widget.photoVendorIMG,
                      ),
                    ),
                  );
                },
                child: Text(
                  "Shop Now",
                  style: TextStyle(
                      fontFamily: 'Helvetica',
                      color: Color(0xff121A31),
                      letterSpacing: 0.7),
                ),
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.amber)),
              ),
            ],
          )
        ],
      ),
    );
  }
}
