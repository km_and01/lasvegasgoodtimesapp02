// ignore_for_file: avoid_print

import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/screens/services/eat/restaurent/table_listing_scr.dart';

String vendorIDRestaurent = '';
String profile_img = 'http://koolmindapps.com/lasvegas/public/images/profile_photo/';

class CustomRestaurentCard extends StatefulWidget {
  final String add1, restaurentName, add2, shortDisc;
  final List<String> tableCapacity;
  final List<String> tableQuantity;
  final List<String> typeOfFood;
  final List<String> tablePrice;
  final List<String> tablePhoto;
  final String sDate, sTime, vendorID, logo;
  const CustomRestaurentCard({
    Key? key,
    required this.add1,
    required this.restaurentName,
    required this.tableCapacity,
    required this.tableQuantity,
    required this.typeOfFood,
    required this.tablePrice,
    required this.tablePhoto,
    required this.sDate,
    required this.sTime,
    required this.add2,
    required this.shortDisc,
    required this.vendorID,
    required this.logo,
  }) : super(key: key);

  @override
  State<CustomRestaurentCard> createState() => _CustomRestaurentCardState();
}

class _CustomRestaurentCardState extends State<CustomRestaurentCard> {
  @override
  void initState() {
    vendorIDRestaurent = widget.vendorID;
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (_) => RestaurentTables(
                      tableCapacity: widget.tableCapacity,
                      tablePhoto: widget.tablePhoto,
                      tablePrice: widget.tablePrice,
                      tableQuantity: widget.tableQuantity,
                      typeOfFood: widget.typeOfFood,
                      selectedDate: widget.sDate,
                      selectedTime: widget.sTime,
                      add1: widget.add1,
                      add2: widget.add2,
                      restauName: widget.restaurentName,
                      shortDisc: widget.shortDisc,
                      vendorID: widget.vendorID,
                    )));
      },
      child: Badge(
        position: BadgePosition.topEnd(top: 14, end: 14),
        badgeContent: Padding(
          padding: const EdgeInsets.only(left: 8.0, right: 8.0),
          child: Text(
            "Open",
            style: TextStyle(
              fontFamily: 'Raleway-Medium',
              fontSize: 12.0,
              color: Colors.white
            ),
          ),
        ),
        badgeColor: Colors.green,
        shape: BadgeShape.square,
        borderRadius: BorderRadius.all(Radius.circular(7.0)),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 7.5),
          decoration: BoxDecoration(
            // color: servicecardfillcolor,
            border: Border.all(color: txtborderbluecolor),
            borderRadius: BorderRadius.all(Radius.circular(7.0)),
          ),
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(7.0),
                  bottomLeft: Radius.circular(7.0),
                ),
                child: Image.network(
                  '$profile_img${widget.logo}',
                  fit: BoxFit.fitHeight,
                  width: 100,
                  height: 100,
                ),
              ),
              SizedBox(
                width: 15.0,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    widget.restaurentName,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18.0,
                        fontFamily: 'Raleway-Medium',),
                  ),
                  Text(
                    widget.add1,
                    // "456 - Lorem ipsum dolor sit amet,\nconsectetur adipiscing elit.",
                    style: kTxtStyle,
                  ),
                  RatingBar.builder(
                    glow: false,
                    itemSize: 15.0,
                    initialRating: 3,
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 5,
                    //itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: Colors.amber,
                      //   Image.asset(
                      // "assets/icons/rating_star.png",
                      // color: Colors.amber,
                    ),
                    // ),
                    unratedColor: Colors.grey,
                    onRatingUpdate: (rating) {
                      print(rating);
                    },
                  ),
                  SizedBox(
                    width: 200.0,
                    child: Text(
                      widget.shortDisc,
                      maxLines: 2,
                      // "456 - Lorem ipsum dolor sit amet,\nconsectetur adipiscing elit.",
                      style: TextStyle(
                        fontSize: 11.0,
                        fontFamily: 'Manrope',
                        color: Colors.white
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
