import 'package:badges/badges.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/screens/cart/cart_list.dart';

class CustomAppBarCart extends StatelessWidget implements PreferredSizeWidget {
  final Widget lead;
  final Widget title;

  CustomAppBarCart({required this.lead, required this.title});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0.0,
      leading: Padding(
        padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
        child: lead,
      ),
      leadingWidth: 40.0,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          title,
          Badge(
            position: BadgePosition.topEnd(top: 8, end: 10),
            child: IconButton(
              iconSize: 30,
              padding: EdgeInsets.all(0),
              splashRadius: 0.1,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (_) => CartListScr(),
                  ),
                );
              },
              icon: Icon(Icons.shopping_cart_rounded),
            ),
          ),
        ],
      ),
      bottom: PreferredSize(
        child: Column(
          children: const [
            Divider(
              thickness: 2,
              height: 2,
              color: Colors.amber,
            ),
            SizedBox(
              height: 3.0,
            ),
            DottedLine(
              lineThickness: 4.5,
              dashRadius: 7,
              dashLength: 4.0,
              // lineLength:  MediaQuery.of(context).size.width,
              dashColor: Colors.amber,
            ),
          ],
        ),
        preferredSize: Size.fromHeight(1),
      ),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(80);
}
