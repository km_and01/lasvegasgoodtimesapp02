import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_hotel_display_card.dart';
import 'package:lasvegas_gts_app/screens/services/eat/catering/catering_overview.dart';

String amount = '';
String vendorCateringID = '';
List<String> foodNameListCatering = [];

String sDate = '', sType = '', sCity = '', noPerson = '';

class CustomCateringCard extends StatefulWidget {
  final String shortDisc, add1, add2;
  final String vendorID;
  final String sDate, sType, sCity, noPerson;
  final String cateIMG, bname, price;
  final List<String> foodName, experties;
  const CustomCateringCard({
    Key? key,
    required this.shortDisc,
    required this.add1,
    required this.add2,
    required this.vendorID,
    required this.sDate,
    required this.sType,
    required this.sCity,
    required this.noPerson,
    required this.cateIMG,
    required this.bname,
    required this.price,
    required this.foodName,
    required this.experties,
  }) : super(key: key);

  @override
  State<CustomCateringCard> createState() => _CustomCateringCardState();
}

class _CustomCateringCardState extends State<CustomCateringCard> {
  @override
  void initState() {
    amount = widget.price;
    vendorCateringID = widget.vendorID;
    foodNameListCatering = widget.foodName;
    setState(() {
      sDate = widget.sDate;
      sCity = widget.sCity;
      sType = widget.sType;
      noPerson = widget.noPerson;
    });
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (_) => CateringOverview(
                      shoertDiscrption: widget.shortDisc,
                      experties: widget.experties,
                      add1: widget.add1,
                      add2: widget.add2,
                      bnamwe: widget.bname,
                      chefIMG: widget.cateIMG,
                    )));
      },
      child: Badge(
        position: BadgePosition.topEnd(top: 14, end: 14),
        badgeContent: Text(
          "Open",
          style: kTxtStyle,
        ),
        badgeColor: Colors.green,
        shape: BadgeShape.square,
        borderRadius: BorderRadius.all(Radius.circular(20.0)),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 7.5),
          decoration: BoxDecoration(
            // color: servicecardfillcolor,
            border: Border.all(color: txtborderbluecolor),
            borderRadius: BorderRadius.all(Radius.circular(15.0)),
          ),
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(15.0),
                  bottomLeft: Radius.circular(15.0),
                ),
                child: Image.network(
                  bLogoUrl + widget.cateIMG,
                  fit: BoxFit.fill,
                  height: 110,
                  width: 130,
                ),
                // Image.asset(
                //   "assets/images/restaurent1.png",

                // ),
              ),
              SizedBox(
                width: 15.0,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.bname,
                    // "Catering Name",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold),
                  ),
                  RatingBar.builder(
                    glow: false,
                    itemSize: 20.0,
                    initialRating: 3,
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 5,
                    itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: Colors.amber,
                      //   Image.asset(
                      // "assets/icons/rating_star.png",
                      // color: Colors.amber,
                    ),
                    // ),
                    unratedColor: Colors.grey,
                    onRatingUpdate: (rating) {
                      print(rating);
                    },
                  ),
                  Text(
                    "Price Starts from \$" + widget.price,
                    // "456 - Lorem ipsum dolor sit amet,\nconsectetur adipiscing elit.",
                    style: kTxtStyle,
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
