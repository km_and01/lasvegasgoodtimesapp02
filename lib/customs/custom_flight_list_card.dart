import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_hotel_display_card.dart';
import 'package:lasvegas_gts_app/screens/services/air/air_checkout.dart';
import 'package:lasvegas_gts_app/screens/services/air/air_select_room.dart';
import 'package:lasvegas_gts_app/screens/services/air/airlines_details_scr.dart';
import 'package:lasvegas_gts_app/screens/services/air/vendor_service_details.dart';
import 'package:lasvegas_gts_app/screens/services/stay/hotel_room_details.dart';

class CustomFlightListingCard extends StatefulWidget {
  final String businessName,
      logo,
      address1,
      address2,
      discPrice,
      totalGuest,
      flightName,
      flightDetails,
      DepartureDate,
      returnDate;
  final List<String> imagesk, amaities;
    final String fromAP,toAP,toAPC,fromAPC,vendorId,helijet;
  const CustomFlightListingCard(
      {Key? key,
      required this.businessName,
      required this.address1,
      required this.address2,
      required this.discPrice,
      required this.totalGuest,
      required this.flightName,
      required this.flightDetails,
      required this.DepartureDate,
      required this.returnDate,
      required this.imagesk,
      required this.amaities,
      required this.logo,
        required this.fromAP, required this.toAP, required this.toAPC, required this.fromAPC, required this.vendorId, required this.helijet})
      : super(key: key);

  @override
  _CustomFlightListingCardState createState() =>
      _CustomFlightListingCardState();
}

bool isFav = false;

class _CustomFlightListingCardState extends State<CustomFlightListingCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: txtborderbluecolor),
        borderRadius: BorderRadius.all(Radius.circular(15.0)),
      ),
      // padding: EdgeInsets.all(10.0),
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          GestureDetector(
            onTap: () {
              setState(() {
                isFav == false ? isFav = true : isFav = false;
              });
            },
            child: Badge(
              position: BadgePosition.topEnd(top: 15, end: 10),
              badgeColor: isFav ? btngoldcolor : greytxtcolor,
              elevation: 0,
              badgeContent: Image.asset(
                "assets/icons/ic_fav.png",
                height: 17.0,
                width: 17.0,
                color: isFav ? favcolor : Colors.white,
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(13),
                    topRight: Radius.circular(13)),
                child: Image.network(
                  bLogoUrl + widget.logo,
                  width: 380.0,
                  height: 150.0,
                  fit: BoxFit.fitWidth,
                ),
              ),
            ),
          ),
          SizedBox(
            height: 12.0,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.businessName,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16.0,
                      fontFamily: 'Manrope_Medium'
                    ),
                  ),
                  Text(
                    widget.address1,
                    style: TextStyle(
                        color: greytxtcolor,
                        fontSize: 14.0,
                        fontFamily: 'Raleway-Regular'
                    ),
                  ),
                  RatingBar.builder(
                    glow: false,
                    itemSize: 15.0,
                    initialRating: 3,
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 5,
                    /*itemPadding: EdgeInsets.symmetric(horizontal: 4.0),*/
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: Colors.amber,
                      //   Image.asset(
                      // "assets/icons/rating_star.png",
                      // color: Colors.amber,
                    ),
                    // ),
                    unratedColor: Colors.grey,
                    onRatingUpdate: (rating) {
                      print(rating);
                    },
                  ),
                  Row(
                    children: [
                      // Text(
                      //   "P\$ " + widget.discPrice, //'widget.basePrice',
                      //   style: TextStyle(
                      //     fontSize: 16.0,
                      //     decoration: TextDecoration.lineThrough,
                      //     color: greytxtcolor,
                      //   ),
                      // ),
                      // SizedBox(
                      //   width: 10.0,
                      // ),
                      Text(
                        "Price starts from \$ " +
                            widget.discPrice, // 'widget.discPrice',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18.0,
                          fontFamily: 'Manrope_Medium'
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              ElevatedButton(
                onPressed: () async {
                  /*Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) => AirLineDetailsScreen(
                              businessName: widget.businessName,
                              address1: widget.address1,
                              address2: widget.address2,
                              imagesk: widget.imagesk,
                              discPrice: widget.discPrice,
                              totalGuest: widget.totalGuest,
                              amaities: widget.amaities,
                              flightName: widget.flightName,
                              flightDetails: widget.flightDetails,
                              DepartureDate: widget.DepartureDate,
                              returnDate: widget.returnDate, 
                              fromAP: widget.fromAP, 
                              fromAPC:widget.fromAPC, 
                              toAP: widget.toAP, 
                              toAPC: widget.toAPC, 
                              helijet: widget.helijet, 
                              vendorID: widget.vendorId,),
                          ));*/


                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => AirLineVendorDetailsScreen(
                          businessName: widget.businessName,
                          address1: widget.address1,
                          address2: widget.address2,
                          imagesk: widget.imagesk,
                          discPrice: widget.discPrice,
                          totalGuest: widget.totalGuest,
                          amaities: widget.amaities,
                          flightName: widget.flightName,
                          flightDetails: widget.flightDetails,
                          DepartureDate: widget.DepartureDate,
                          returnDate: widget.returnDate,
                          fromAP: widget.fromAP,
                          fromAPC:widget.fromAPC,
                          toAP: widget.toAP,
                          toAPC: widget.toAPC,
                          helijet: widget.helijet,
                          vendorID: widget.vendorId,),
                      ));

                },
                child: Text(
                  "Book Now",
                  style: TextStyle(
                      fontFamily: 'Raleway-Medium',
                      fontSize: 14.0,
                      color: Color(0xff121A31)),
                ),
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  fixedSize: Size(108.0, 40.0),
                  primary: Color(0xFFFFC71E),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 15.0,
          ),
        ],
      ),
    );
  }
}
