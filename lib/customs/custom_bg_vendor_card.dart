import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lasvegas_gts_app/screens/services/bodyguard/bodyguard_pkg_listing_scr.dart';
import 'package:lasvegas_gts_app/screens/services/eventSpace/event_space_package_listing_scr.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_hotel_display_card.dart';
import 'package:lasvegas_gts_app/screens/services/eat/catering/catering_overview.dart';
import 'package:lasvegas_gts_app/screens/services/fun/fun_pkg_listing_scr.dart';
import 'package:lasvegas_gts_app/screens/services/wedding/wedding_pkg_listing_scr.dart';

String vendorBGID = '';

class CustomBGVendorCard extends StatefulWidget {
  final String vendorID, secType;
  final String weddVendorIMG, bname, minPrice, priceUnit;
  final String short_des;

  const CustomBGVendorCard({
    Key? key,
    required this.vendorID,
    required this.weddVendorIMG,
    required this.bname,
    required this.minPrice,
    required this.priceUnit,
    required this.secType,
    required this.short_des,
  }) : super(key: key);

  @override
  State<CustomBGVendorCard> createState() => _CustomBGVendorCardState();
}

class _CustomBGVendorCardState extends State<CustomBGVendorCard> {
  @override
  void initState() {
    vendorBGID = widget.vendorID;

    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => BGPkgListingScr(
              bgvendorID: vendorBGID,
              secType: widget.secType,
              bName: widget.bname,
            ),
          ),
        );
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 7.5),
        decoration: BoxDecoration(
          // color: servicecardfillcolor,
          border: Border.all(color: txtborderbluecolor),
          borderRadius: BorderRadius.all(Radius.circular(7.0)),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(7.0),
                bottomLeft: Radius.circular(7.0),
              ),
              child: Image.network(
                bLogoUrl + widget.weddVendorIMG,
                fit: BoxFit.fill,
                height: 110,
                width: 130,
              ),
              //     Image.asset(
              //   "assets/images/restaurent1.png",
              // ),
            ),
            SizedBox(
              width: 15.0,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  widget.bname,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18.0,
                      fontFamily: 'Raleway-Medium'),
                ),
                /*RatingBar.builder(
                  glow: false,
                  itemSize: 20.0,
                  initialRating: 3,
                  minRating: 1,
                  direction: Axis.horizontal,
                  allowHalfRating: true,
                  itemCount: 5,
                  itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
                  itemBuilder: (context, _) => Icon(
                    Icons.star,
                    color: Colors.amber,
                    //   Image.asset(
                    // "assets/icons/rating_star.png",
                    // color: Colors.amber,
                  ),
                  // ),
                  unratedColor: Colors.grey,
                  onRatingUpdate: (rating) {
                    print(rating);
                  },
                ),*/
                SizedBox(
                  width: 200.0,
                  child: Text(
                    widget.short_des,
                    // "Event Name",
                    maxLines: 2,
                    style: TextStyle(
                        color: greytxtcolor,
                        fontSize: 13.0,
                        fontFamily: 'Manrope'),
                  ),
                ),
                Text(
                  "Price Starts from " +
                      widget.priceUnit +
                      " " +
                      widget.minPrice,
                  style: TextStyle(
                      fontSize: 16.0,
                      fontFamily: 'Manrope-SemiBold',
                      color: Colors.white
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
