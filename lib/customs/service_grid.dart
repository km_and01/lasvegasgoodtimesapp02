// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/customs/custom_card.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart' as hmscr;
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';
import 'package:lasvegas_gts_app/screens/services/air/air_main_src.dart';
import 'package:lasvegas_gts_app/screens/services/bodyguard/bodtguard_main_scr.dart';
import 'package:lasvegas_gts_app/screens/services/businessService/business_service_main_scr.dart';
import 'package:lasvegas_gts_app/screens/services/celebAppearance/celeb_main_scr.dart';
import 'package:lasvegas_gts_app/screens/services/eat/restaurent/restaurent_listing_scr.dart';
import 'package:lasvegas_gts_app/screens/services/eat/eat_opening_scr.dart';
import 'package:lasvegas_gts_app/screens/services/eventSpace/event_space_main_scr.dart';
import 'package:lasvegas_gts_app/screens/services/evets/event_main_src.dart';
import 'package:lasvegas_gts_app/screens/services/fashion/fashion_main_scr.dart';
import 'package:lasvegas_gts_app/screens/services/fun/fun_main_scr.dart';
import 'package:lasvegas_gts_app/screens/services/party/party_main_scr.dart';
import 'package:lasvegas_gts_app/screens/services/rental/rental_main_scr.dart';
import 'package:lasvegas_gts_app/screens/services/stay/stay_search_filter_scr.dart';
import 'package:lasvegas_gts_app/screens/services/transport/transport_main_scr.dart';
import 'package:lasvegas_gts_app/screens/services/wedding/wedding_main_scr.dart';

class ServiceGrid extends StatefulWidget {
  final String userToken;

  // String userTokenS = user_token;
  static var serviceList = [
    CoustomCard(
      icon: "ic_stay",
      title: "Stay",
      DestinationWidget: StaySearchFilter(
        utoken: homeToken,
      ),
    ),
    CoustomCard(
      icon: "ic_air_service",
      title: "Air Service",
      DestinationWidget: AirHome(
        uToken: '',
      ),
    ),
    CoustomCard(
      icon: "ic_eat",
      title: "Eat",
      DestinationWidget: EatHome(),
    ),
    CoustomCard(
      icon: "ic_event",
      title: "Event",
      DestinationWidget: EventHome(),
    ),
    CoustomCard(
      icon: "ic_event_space",
      title: "Event Space",
      DestinationWidget: EventSpaceHome(),
    ),
    CoustomCard(
      icon: "ic_transportation",
      title: "Transport",
      DestinationWidget: TransportHome(),
    ),
    CoustomCard(
      icon: "ic_bodyguard",
      title: "Bodyguards",
      DestinationWidget: BodyguardHome(),
    ),
    CoustomCard(
      icon: "ic_celeb_appearence",
      title: "Celebrity Appearance",
      DestinationWidget: CelebAppearanceHome(),
    ),
    CoustomCard(
      icon: "ic_rental",
      title: "Rental",
      DestinationWidget: RentalHome(),
    ),
    CoustomCard(
      icon: "ic_party",
      title: "Party",
      DestinationWidget: PartyHome(),
    ),
    CoustomCard(
      icon: "ic_fasion_retail",
      title: "Fashion / Retails",
      DestinationWidget: FashionHome(),
    ),
    CoustomCard(
      icon: "ic_wedding_service",
      title: "Wedding Service",
      DestinationWidget: WeddingHome(),
    ),
    CoustomCard(
      icon: "ic_business_services",
      title: "Business Service",
      DestinationWidget: BusinessServiceHome(),
    ),
    CoustomCard(
      icon: "ic_fun",
      title: "Fun",
      DestinationWidget: FunHome(),
    ),
  ];

  const ServiceGrid({Key? key, required this.userToken}) : super(key: key);

  @override
  State<ServiceGrid> createState() => _ServiceGridState();
}

class _ServiceGridState extends State<ServiceGrid> {
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 3, crossAxisSpacing: 5.0, mainAxisSpacing: 5.0),
        itemCount: ServiceGrid.serviceList.length,
        itemBuilder: (_, index) {
          return ServiceGrid.serviceList[index];
        });
  }
}
