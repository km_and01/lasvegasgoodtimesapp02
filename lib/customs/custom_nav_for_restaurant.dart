import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';

class CustomNavBarForRestaurents extends StatefulWidget {
  final int defaultSelectedIndex;
  final Function(int) onChange;
  // ignore: use_key_in_widget_constructors
  const CustomNavBarForRestaurents({
    this.defaultSelectedIndex = 0,
    required this.onChange,
  });

  @override
  State<CustomNavBarForRestaurents> createState() =>
      _CustomNavBarForRestaurentsState();
}

class _CustomNavBarForRestaurentsState
    extends State<CustomNavBarForRestaurents> {
  int _selectedIndex = 0;

  @override
  void initState() {
    // TODO: implement initState
    _selectedIndex = widget.defaultSelectedIndex;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        border: Border.all(color: txtborderbluecolor, width: 1),
        color: Color(0xFF1A3A89),
      ),
      child: Row(
        children: [
          buildNavBarItem(0, context, "Overview"),
          buildNavBarItem(1, context, "Booking"),
          buildNavBarItem(2, context, "Review"),
        ],
      ),
    );
  }

  Widget buildNavBarItem(
    int index,
    BuildContext context,
    String title,
  ) {
    return Container(
      padding: EdgeInsets.fromLTRB(0, 0, 0, 20.0),
      height: 55.0,
      width: MediaQuery.of(context).size.width / 3.02,
      child: InkWell(
        onTap: () {
          widget.onChange(index);
          setState(() {
            _selectedIndex = index;
          });
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Divider(
              height: 2,
              thickness: 2,
              color:
                  index == _selectedIndex ? btngoldcolor : Colors.transparent,
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              title,
              style: TextStyle(
                color: index == _selectedIndex ? btngoldcolor : Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
