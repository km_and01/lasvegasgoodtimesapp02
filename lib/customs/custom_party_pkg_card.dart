import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:intl/intl.dart';
import 'package:lasvegas_gts_app/screens/services/party/part_pkg_details_scr.dart';

String partyLogoUrl = 'http://koolmindapps.com/lasvegas/public/images/party/';

class CustomPartyPackageCard extends StatefulWidget {
  final String packageIMG,
      pkgVendorID,
      packageName,
      priceUnit,
      partyID,
      packagePrice,
      packageDiscPrice,
      eventName,
      date,
   pkgShortDetails,
      capacity;
  final String add1, add2, fullDetails, serviceID;
  final List<String> pkgIMGList; 
  CustomPartyPackageCard(
      {Key? key,
      required this.packageIMG,
      required this.packageName,
      required this.priceUnit,
      required this.packagePrice,
      required this.packageDiscPrice,
      required this.capacity,
      required this.add1,
      required this.add2,
      required this.fullDetails,
      required this.pkgIMGList,
      required this.pkgVendorID,
      required this.eventName,
      required this.serviceID, 
      required this.date, required this.pkgShortDetails, required this.partyID})
      : super(key: key);

  @override
  _CustomPartyPackageCardState createState() => _CustomPartyPackageCardState();
}

class _CustomPartyPackageCardState extends State<CustomPartyPackageCard> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) {
              return PackageDetailsParty(
                pkgName: widget.packageName, 
                priceUnit: widget.priceUnit, 
                discPrice: widget.packagePrice, 
                pkgDetails: widget.fullDetails, 
                address1: widget.add1, 
                address2: widget.add2, 
                pkgIMGList: widget.pkgIMGList, 
                pkgShortDetails: widget.pkgShortDetails, 
                pkgVendorID: widget.pkgVendorID, 
                pkgIMG: widget.packageIMG, 
                partyType: widget.eventName, 
                date: widget.date, 
                partyID: widget.partyID,
                );
            },
          ),
        );
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 7.5),
        decoration: BoxDecoration(
          // color: servicecardfillcolor,
          border: Border.all(color: txtborderbluecolor),
          borderRadius: BorderRadius.all(Radius.circular(7.0)),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(7.0),
                bottomLeft: Radius.circular(7.0),
              ),
              child: Image.network(
                partyLogoUrl + widget.packageIMG,
                fit: BoxFit.fill,
                height: 120,
                width: 130,
              ),
              //     Image.asset(
              //   "assets/images/restaurent1.png",
              // ),
            ),
            SizedBox(
              width: 15.0,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    SizedBox(
                      height: 5.0,
                    ),
                    Text(
                      widget.packageName + "/",
                      // "Event Name",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 14.0,
                          fontFamily: 'Raleway-Medium'),
                    ),
                    Text(
                      widget.eventName,
                      // "Event Name",
                      style: TextStyle(
                          color: greytxtcolor,
                          fontSize: 12.0,
                          fontFamily: 'Raleway-Medium'),
                    ),
                  ],
                ),
               SizedBox(
                 height: 2.0,
               ),

               widget.date == null || widget.date == '' || widget.date.isEmpty || widget.date.contains('null') ? SizedBox(height: 0.0,) : Text(
                 DateFormat.yMMMEd('en_US').add_jm().format(DateTime.tryParse(widget.date)!) 
                      ,
                      // "Event Name",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 14.0,
                          fontFamily: 'Raleway-Medium'),
                    ),
                SizedBox(
                  height: 2.0,
                ),
                Text(
                  'Max ${widget.capacity} People Are Allowed In Show',
                  style: TextStyle(
                    fontSize: 13.0,
                    overflow: TextOverflow.ellipsis,
                    color: greytxtcolor,
                      fontFamily: 'Raleway-Medium'
                  ),
                ),
                SizedBox(
                  height: 5.0,
                ),
                RatingBar.builder(
                  glow: false,
                  itemSize: 15.0,
                  initialRating: 3,
                  minRating: 1,
                  direction: Axis.horizontal,
                  allowHalfRating: true,
                  itemCount: 5,
                  //itemPadding: EdgeInsets.symmetric(horizontal: 0.5),
                  itemBuilder: (context, _) => Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  unratedColor: Colors.grey,
                  onRatingUpdate: (rating) {
                    print(rating);
                  },
                ),
                SizedBox(
                  height: 5.0,
                ),
                Text(
                  "Price " + widget.priceUnit + widget.packagePrice,
                  style: TextStyle(
                      fontSize: 16.0,
                      fontFamily: 'Manrope-SemiBold',
                      color: Colors.white
                  ),
                ),
                SizedBox(
                  height: 5.0,
                ),
                // Text(
                //   "Around " + widget.totalStall + " people are gathered",
                //   style: TextStyle(
                //     fontSize: 14.0,
                //     overflow: TextOverflow.ellipsis,
                //     color: greytxtcolor,
                //   ),
                // ),
                // Row(
                //   children: [
                //     // Text(
                //     //   widget.priceUnit + " " + widget.packagePrice + "/Day ",
                //     //   style: TextStyle(
                //     //     fontSize: 12.0,
                //     //     decoration: TextDecoration.lineThrough,
                //     //     color: greytxtcolor,
                //     //   ),
                //     // ),
                //     // SizedBox(
                //     //   width: 5.0,
                //     // ),
                //     Text(
                //       widget.priceUnit + " " + widget.packageDiscPrice,
                //       //  + "/Day ",
                //       style: TextStyle(
                //         color: Colors.white,
                //         fontSize: 15.0,
                //       ),
                //     ),
                //   ],
                // ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
