import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';

String amiImgURL =
    "http://koolmindapps.com/lasvegas/public/images/category/amenities/";

class AmenitiesCard extends StatelessWidget {
  final String icon;
  final String title;

   const AmenitiesCard({Key? key, required this.icon, required this.title}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Container(
        alignment: Alignment.center,
        height: 100.0,
        width: 120.0,
        padding: EdgeInsets.all(5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.network(
              amiImgURL + icon,
              width: 30.0,
              height: 30.0,
              color: Colors.white,
            ),
            SizedBox(
              height: 8.0,
            ),
            Text(
              title,
              textAlign: TextAlign.center,
              style: kTxtStyle,
            )
          ],
        ),
        decoration: BoxDecoration(
          color: servicecardfillcolor,
          border: Border.all(color: txtborderbluecolor),
          borderRadius: BorderRadius.all(Radius.circular(15.0)),
        ),
      ),
    );
  }
}
