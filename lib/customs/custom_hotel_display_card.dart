import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lasvegas_gts_app/Networking/StayAPI/stay_search_specil_suits_api.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/screens/services/stay/hotel_room_details.dart';
import 'package:lasvegas_gts_app/screens/services/stay/sta_search_main.dart';
import 'package:lasvegas_gts_app/screens/services/stay/stay_select_room.dart';

String bLogoUrl =
    'http://koolmindapps.com/lasvegas/public/images/profile_photo/';

class CustomHotelDisplayCard extends StatefulWidget {
  final String hotelName;
  // final String hotelRating;
  final String basePrice;
  final String discPrice;
  final String vendorID;
  final String subCatName;
  final String roomName;
  final String addLine2;
  final String addLine1;
  final String roomDetails;
  final List<String> imagesk;
  final String bLogo;
  final List<String> stayPhotos;
 final List<String> amanities;
  final String totalRooms;
  final String totalGuest;
  final String checkInDate;
  final String checkOutDate;

  final String roomQuantity;

  const CustomHotelDisplayCard(
      {Key? key,
      required this.hotelName,
      required this.basePrice,
      required this.discPrice,
      required this.vendorID,
      required this.subCatName,
      required this.roomName,
      required this.addLine2,
      required this.addLine1,
      required this.roomDetails,
      required this.imagesk,
      required this.totalRooms,
      required this.totalGuest,
      required this.checkInDate,
      required this.checkOutDate,
      required this.bLogo,
      required this.stayPhotos, 
      required this.roomQuantity, 
      required this.amanities})
      : super(key: key);

  @override
  State<CustomHotelDisplayCard> createState() => _CustomHotelDisplayCardState();
}

class _CustomHotelDisplayCardState extends State<CustomHotelDisplayCard> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0),
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          GestureDetector(
            onTap: () {
              setState(() {
                isFav == false ? isFav = true : isFav = false;
              });
            },
            child: Badge(
              position: BadgePosition.topEnd(top: 15, end: 10),
              badgeColor: isFav ? btngoldcolor : greytxtcolor,
              elevation: 0,
              badgeContent: Image.asset(
                "assets/icons/ic_fav.png",
                height: 17.0,
                width: 17.0,
                color: isFav ? favcolor : Colors.white,
              ),
              child: Image.network(
                bLogoUrl + widget.bLogo,
                // "assets/images/hotel_banner_hd.png",
                width: 380.0,
                height: 150.0,
                fit: BoxFit.fitWidth,
                // color: Colors.amber,
              ),
            ),
          ),
          SizedBox(
            height: 15.0,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.hotelName,
                    style: TextStyle(
                      fontFamily: 'Manrope-Medium',
                      color: Colors.white,
                      fontSize: 18.0,
                    ),
                  ),
                  RatingBar.builder(
                    glow: false,
                    itemSize: 20.0,
                    initialRating: 3,
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 5,
                    itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: Colors.amber,
                      //   Image.asset(
                      // "assets/icons/rating_star.png",
                      // color: Colors.amber,
                    ),
                    // ),
                    unratedColor: Colors.grey,
                    onRatingUpdate: (rating) {
                      print(rating);
                    },
                  ),
                  Row(
                    children: [
                      Text(
                        "\$ " + widget.basePrice,
                        style: TextStyle(
                          fontFamily: 'Manrope-Medium',
                          fontSize: 16.0,
                          decoration: TextDecoration.lineThrough,
                          color: greytxtcolor,
                        ),
                      ),
                      SizedBox(
                        width: 10.0,
                      ),
                      Text(
                        "\$ " + widget.discPrice,
                        style: TextStyle(
                          fontFamily: 'Manrope-Medium',
                          color: Colors.white,
                          fontSize: 20.0,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              ElevatedButton(
                onPressed: () async {
                  print('this id is of VENDOR : ${widget.vendorID}');
                  print('this id is of VENDOR : ${widget.subCatName}');
                  // await RoomAndOtherDetails(widget.vendorID, widget.subCatName);
                  // GetDetails(roomCapacity, roomtype);
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => SelectRoom(
                          roomCapacity: '',
                          roomSize: '',
                          roomType: '',
                          roomName: widget.roomName,
                          discPrice: widget.discPrice,
                          addLine1: widget.addLine1,
                          addLine2: widget.addLine2,
                          roomDetails: widget.roomDetails,
                          businessName: widget.hotelName,
                          imagesk: widget.imagesk,
                          checkInDate: widget.checkInDate,
                          checkOutDate: widget.checkOutDate,
                          totalGuest: widget.totalGuest,
                          totalRooms: widget.totalRooms,
                          bLogo: widget.bLogo,
                          stayRoomPhoto: widget.stayPhotos, 
                          noofRooms: widget.roomQuantity,
                           amani: widget.amanities, vendorID: widget.vendorID,
                          sub_category: widget.subCatName,
                        ),
                      ));
                },
                child: Text(
                  "Book Now",
                  style: TextStyle(
                      fontFamily: 'Raleway-Medium',
                      color: Color(0xff121A31),
                      fontSize: 14.0),
                ),
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  fixedSize: Size(108.0, 40.0),
                  primary: Color(0xFFFFC71E),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 15.0,
          ),
        ],
      ),
    );
  }
}

class GetDetails {
  final String roomCapacity;
  final String roomtype;

  GetDetails(this.roomCapacity, this.roomtype);
}
