import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// class AppColors {
MaterialColor primebgcolor = MaterialColor(
  0xFF121A31,
  const <int, Color>{
    50: Color(0xFF121A31), //10%
    100: Color(0xFF121A31), //20%
    200: Color(0xFF121A31), //30%
    300: Color(0xFF121A31), //40%
    400: Color(0xFF121A31), //50%
    500: Color(0xFF121A31), //60%
    600: Color(0xFF121A31), //70%
    700: Color(0xFF121A31), //80%
    800: Color(0xFF121A31), //90%
    900: Color(0xFF121A31), //100%
  },
);

MaterialColor gold = MaterialColor(
   0xFFFFC71E,
  const <int, Color>{
    50: Color(0xFFFFC71E), //10%
    100: Color(0xFFFFC71E), //20%
    200: Color(0xFFFFC71E), //30%
    300: Color(0xFFFFC71E), //40%
    400: Color(0xFFFFC71E), //50%
    500: Color(0xFFFFC71E), //60%
    600: Color(0xFFFFC71E), //70%
    700: Color(0xFFFFC71E), //80%
    800: Color(0xFFFFC71E), //90%
    900: Color(0xFFFFC71E), //100%
  },
);

const bgcolor = Color(0xFF121A31);
const greytxtcolor = Color(0xFF95A0AE);
const txtborderbluecolor = Color(0xFF5C75B3);
const btngoldcolor = Color(0xFFFFC71E);
const servicecardfillcolor = Color(0xFF1A3A89);
const buttonBackColor = Color(0xff198754);
const favcolor = Color(0xFF2F0C26);
const bgGradientBlack = Color(0xFF010103);
const bgGradientGrey = Color(0xFF222432);

//  LinearGradient(
//           begin: Alignment.topCenter,
//           end:Alignment.bottomCenter, // 10% of the width, so there are ten blinds.
//           colors: <Color>[
//             Color(0xFF121A31),
//             Color(0xFF1A3A89),
//           ], // red to yellow;
// // }
