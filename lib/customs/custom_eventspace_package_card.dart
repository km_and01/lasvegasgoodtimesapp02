import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/screens/services/eventSpace/package_details_scr.dart';

String pLogoUrl =
    'http://lasvegas.koolmindapps.com/images/package/logo/eventspace/';

class CustomEventSpacePackageCard extends StatefulWidget {
  final String packageIMG,pkgVendorID,
      packageName,
      priceUnit,
      packagePrice,
      packageDiscPrice,
      capacity;
      final String add1,add2,shortDiscp;
      final String pkgService,possibleEvents; 
      final List<String> pkgIMGList,amaities;
  const CustomEventSpacePackageCard(
      {Key? key,
      required this.packageIMG,
      required this.packageName,
      required this.priceUnit,
      required this.packagePrice,
      required this.packageDiscPrice,
      required this.capacity, 
      required this.add1, 
      required this.add2, 
      required this.shortDiscp, 
      required this.pkgIMGList, 
      required this.amaities,  required this.pkgService, required this.possibleEvents,
       required this.pkgVendorID})
      : super(key: key);

  @override
  _CustomEventSpacePackageCardState createState() =>
      _CustomEventSpacePackageCardState();
}

class _CustomEventSpacePackageCardState
    extends State<CustomEventSpacePackageCard> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => PackageDetailsEventSpace(
              address1:widget.add2, 
              address2: widget.add2, 
              discPrice: widget.packageDiscPrice, 
              pkgDetails: widget.shortDiscp, 
              pkgIMGList: widget.pkgIMGList, 
              pkgName: widget.packageName, 
              priceUnit: widget.priceUnit, amaities:widget.amaities, 
              pkgCapacity: widget.capacity, 
              pkgService: widget.pkgService,
              possibleEvents: widget.possibleEvents, 
              pkgVendorID: widget.pkgVendorID, pkgIMG: widget.packageIMG,

            ),
          ),
        );
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 7.5),
        decoration: BoxDecoration(
          // color: servicecardfillcolor,
          border: Border.all(color: txtborderbluecolor),
          borderRadius: BorderRadius.all(Radius.circular(7.0)),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(7.0),
                bottomLeft: Radius.circular(7.0),
              ),
              child: Image.network(
                pLogoUrl + widget.packageIMG,
                fit: BoxFit.fill,
                height: 110,
                width: 130,
              ),
              //     Image.asset(
              //   "assets/images/restaurent1.png",
              // ),
            ),
            SizedBox(
              width: 15.0,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  widget.packageName,
                  // "Event Name",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18.0,
                      fontFamily: 'Raleway-Medium'),
                ),
                Text(
                  "Upto " + widget.capacity + ' People',
                  style: TextStyle(
                      color: greytxtcolor,
                      fontSize: 14.0,
                      fontFamily: 'Manrope'),
                ),
                RatingBar.builder(
                  glow: false,
                  itemSize: 15.0,
                  initialRating: 3,
                  minRating: 1,
                  direction: Axis.horizontal,
                  allowHalfRating: true,
                  itemCount: 5,
                  itemPadding: EdgeInsets.symmetric(horizontal: 0.5),
                  itemBuilder: (context, _) => Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  unratedColor: Colors.grey,
                  onRatingUpdate: (rating) {
                    print(rating);
                  },
                ),
                SizedBox(
                  height: 5.0,
                ),
                Row(
                  children: [
                    Text(
                      widget.priceUnit + " " + widget.packagePrice + "/Day ",
                      style: TextStyle(
                        fontSize: 14.0,
                        decoration: TextDecoration.lineThrough,
                        color: greytxtcolor,
                        fontFamily: 'Manrope-SemiBold',
                      ),
                    ),
                    SizedBox(
                      width: 5.0,
                    ),
                    Text(
                      widget.priceUnit +
                          " " +
                          widget.packageDiscPrice +
                          "/Day ",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                        fontFamily: 'Manrope-SemiBold',
                      ),
                    ),
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
