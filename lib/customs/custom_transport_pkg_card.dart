import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/screens/services/transport/transport_stall_listing_scr.dart';

String eventLogoUrl = 'http://koolmindapps.com/lasvegas/public/images/event/';

class CustomTransportPackageCard extends StatefulWidget {
  final String packageIMG,
      pkgVendorID,
      packageName,
      priceUnit,
      packagePrice,
      packageDiscPrice,
      eventName,
      totalStall,
      gatherPeople,
      location;
  final List<String> stallLoc;
  final List<String> stallfees;
  final List<String> stallFor;
  final List<String> stallArea;
  final List<String> stallFeeUnit;
  // capacity;
  final String add1, add2, fullDetails,serviceID;
  // final String pkgService,possibleEvents;
  final List<String> pkgIMGList; //,amaities;
  const CustomTransportPackageCard(
      {Key? key,
      required this.packageIMG,
      required this.packageName,
      required this.priceUnit,
      required this.packagePrice,
      required this.packageDiscPrice,
      // required this.capacity,
      required this.add1,
      required this.add2,
      required this.fullDetails,
      required this.pkgIMGList,
      // required this.amaities,  required this.pkgService, required this.possibleEvents,
      required this.pkgVendorID,
      required this.eventName,
      required this.location,
      required this.totalStall,
      required this.gatherPeople,
      required this.stallLoc,
      required this.stallfees,
      required this.stallFor,
      required this.stallArea, required this.stallFeeUnit, required this.serviceID})
      : super(key: key);

  @override
  _CustomTransportPackageCardState createState() => _CustomTransportPackageCardState();
}

class _CustomTransportPackageCardState extends State<CustomTransportPackageCard> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) {
              return TransportStallListingScr(
                stallArea: widget.stallArea,
                stallfees: widget.stallfees,
                stallFor: widget.stallFor,
                stallLoc: widget.stallLoc,
                stallFeeUnit: widget.stallFeeUnit, 
                stallImg: widget.packageIMG, eventName: widget.packageName, serviceID: widget.serviceID,
              );
            },
          ),
        );
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 7.5),
        decoration: BoxDecoration(
          border: Border.all(color: txtborderbluecolor),
          borderRadius: BorderRadius.all(Radius.circular(7.0)),
        ),
        child: Row(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(7.0),
                bottomLeft: Radius.circular(7.0),
              ),
              child: Image.network(
                eventLogoUrl + widget.packageIMG,
                fit: BoxFit.fill,
                height: 110,
                width: 130,
              ),
              //     Image.asset(
              //   "assets/images/restaurent1.png",
              // ),
            ),
            SizedBox(
              width: 15.0,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      widget.eventName + "/",
                      // "Event Name",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18.0,
                        fontFamily: 'Manrope-SemiBold'),
                    ),
                    Text(
                      widget.packageName,
                      // "Event Name",
                      style: TextStyle(
                          color: greytxtcolor,
                          fontSize: 14.0,
                        fontFamily: 'Manrope-SemiBold',),
                    ),
                  ],
                ),
                // RatingBar.builder(
                //   glow: false,
                //   itemSize: 20.0,
                //   initialRating: 3,
                //   minRating: 1,
                //   direction: Axis.horizontal,
                //   allowHalfRating: true,
                //   itemCount: 5,
                //   itemPadding: EdgeInsets.symmetric(horizontal: 0.5),
                //   itemBuilder: (context, _) => Icon(
                //     Icons.star,
                //     color: Colors.amber,
                //   ),
                //   unratedColor: Colors.grey,
                //   onRatingUpdate: (rating) {
                //     print(rating);
                //   },
                // ),
                SizedBox(
                  height: 5.0,
                ),
                Text(
                  widget.location,
                  style: TextStyle(
                    fontSize: 14.0,
                    overflow: TextOverflow.ellipsis,
                    color: greytxtcolor,
                    fontFamily: 'Manrope_Medium'
                  ),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Text(
                  "Total " + widget.totalStall + " Stall",
                  style: TextStyle(
                    fontSize: 14.0,
                    overflow: TextOverflow.ellipsis,
                    color: greytxtcolor,
                      fontFamily: 'Manrope_Medium'
                  ),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Text(
                  "Around " + widget.totalStall + " people are gathered",
                  style: TextStyle(
                    fontSize: 14.0,
                    overflow: TextOverflow.ellipsis,
                    color: greytxtcolor,
                      fontFamily: 'Manrope_Medium'
                  ),
                ),
                // Row(
                //   children: [
                //     Text(
                //       widget.priceUnit + " " + widget.packageDiscPrice,
                //       style: TextStyle(
                //         color: Colors.white,
                //         fontSize: 15.0,
                //       ),
                //     ),
                //   ],
                // ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
