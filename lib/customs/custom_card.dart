import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';

class CoustomCard extends StatelessWidget {
  final String icon;
  final Widget? DestinationWidget;
  final String title;

  const CoustomCard(
      {required this.icon, required this.title,  this.DestinationWidget});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GestureDetector(
        onTap: () {
          
          Navigator.push(
              context, MaterialPageRoute(builder: (_) => DestinationWidget!));
        },
        child: Container(
            alignment: Alignment.center,
            height: 100.0,
            width: 100.0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image(
                  image: AssetImage("assets/icons/$icon.png"),
                  width: 30.0,
                  height: 30.0,
                  color: Colors.white,
                ),
                SizedBox(
                  height: 8.0,
                ),
                Text(
                  title,
                  textAlign: TextAlign.center,
                  style: kTxtStyle,
                )
              ],
            ),
            decoration: BoxDecoration(
              color: servicecardfillcolor,
              border: Border.all(color: txtborderbluecolor),
              borderRadius: BorderRadius.all(Radius.circular(15.0)),
            ),
            ),
      ),
    );
  }
}
