import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';

class CustomLoadingScr extends StatelessWidget {
  const CustomLoadingScr({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
       child: Center(
         child: Column(
           mainAxisAlignment: MainAxisAlignment.center,
           children:const [
             
             CircularProgressIndicator(
               backgroundColor: Colors.transparent,
                               color: btngoldcolor,
             ),
             SizedBox(height: 2.0),
             Text(
               "Please wait...",
               style: TextStyle(
                   color: btngoldcolor,
                   fontSize: 11.0),
             )
           ],
         ),
       ),
     );
  }
}
