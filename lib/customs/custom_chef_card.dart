import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_hotel_display_card.dart';
import 'package:lasvegas_gts_app/screens/services/eat/chef/chef_overview.dart';

String amount = '';
String vendorChefID = '';
List<String> foodNameList = [];

String sDate = '', sType = '', sCity = '', noPerson = '';

class CustomChefCard extends StatefulWidget {
  final String chefIMG, bname, price;
  final String shortDisc, add1, add2;
  final String vendorID;
  final String sDate, sType, sCity, noPerson;
  final List<String> foodName, experties;
  const CustomChefCard({
    Key? key,
    required this.chefIMG,
    required this.bname,
    required this.price,
    required this.shortDisc,
    required this.experties,
    required this.add1,
    required this.add2,
    required this.foodName,
    required this.vendorID,
    required this.sDate,
    required this.sType,
    required this.sCity,
    required this.noPerson,
  }) : super(key: key);

  @override
  State<CustomChefCard> createState() => _CustomChefCardState();
}

class _CustomChefCardState extends State<CustomChefCard> {
  @override
  void initState() {
    // TODO: implement initState
    amount = widget.price;
    vendorChefID = widget.vendorID;
    foodNameList = widget.foodName;
    setState(() {
      sDate = widget.sDate;
      sCity = widget.sCity;
      sType = widget.sType;
      noPerson = widget.noPerson;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => ChefOverview(
                shoertDiscrption: widget.shortDisc,
                experties: widget.experties,
                add1: widget.add1,
                add2: widget.add2,
                bnamwe: widget.bname,
                chefIMG: widget.chefIMG,
              ),
            ));
      },
      child: Badge(
        padding: EdgeInsets.all(8),
        position: BadgePosition.topEnd(top: 14, end: 14),
        badgeContent: Text(
          "Available",
          style: kTxtStyle,
        ),
        badgeColor: Colors.green,
        shape: BadgeShape.square,
        borderRadius: BorderRadius.all(Radius.circular(20.0)),
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 7.5),
          decoration: BoxDecoration(
            // color: servicecardfillcolor,
            border: Border.all(color: txtborderbluecolor),
            borderRadius: BorderRadius.all(Radius.circular(15.0)),
          ),
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(15.0),
                  bottomLeft: Radius.circular(15.0),
                ),
                child: Image.network(
                  bLogoUrl + widget.chefIMG,
                  fit: BoxFit.fill,
                  height: 110,
                  width: 130,
                ),
              ),
              SizedBox(
                width: 15.0,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.bname,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  RatingBar.builder(
                    glow: false,
                    itemSize: 20.0,
                    initialRating: 3,
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 5,
                    itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: Colors.amber,
                      //   Image.asset(
                      // "assets/icons/rating_star.png",
                      // color: Colors.amber,
                    ),
                    // ),
                    unratedColor: Colors.grey,
                    onRatingUpdate: (rating) {
                      print(rating);
                    },
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Text(
                    "Price Starts from \$" + widget.price,
                    style: kTxtStyle,
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
