import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_event_pkg_card.dart';
import 'package:lasvegas_gts_app/customs/custom_hotel_display_card.dart';
import 'package:lasvegas_gts_app/screens/services/evets/event_pkg_listing_scr.dart';
import 'package:lasvegas_gts_app/screens/services/evets/event_stall_booking_scr.dart';

String vendorEventID = '';
String serviceEventID = '';
String stallLogoUrl =
    'http://koolmindapps.com/images/event/';
class CustomEventStallCard extends StatefulWidget {
  // final String shortDisc;
  final String vendorID,area,stallFor, serviceID;
  final String EventVendorIMG, stallname, price, priceUnit,eventName;


  const CustomEventStallCard({
    Key? key,
    required this.vendorID,
    required this.EventVendorIMG,
    required this.stallname,
    required this.price,
    // required this.eventspsService,
    required this.priceUnit,
    required this.area, required this.stallFor, required this.eventName, required this.serviceID,
  }) : super(key: key);

  @override
  State<CustomEventStallCard> createState() => _CustomEventStallCardState();
}

class _CustomEventStallCardState extends State<CustomEventStallCard> {
  @override
  void initState() {
    setState(() {
      vendorEventID = widget.vendorID;
    });
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => EventStallBookings(
              vendorID: widget.vendorID, 
              pkgIMG: widget.EventVendorIMG, 
              stallPrice: widget.price, 
              eventName: widget.eventName, serviceID: widget.serviceID,
            )
              ), 
           
          
        );
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 7.5),
        decoration: BoxDecoration(
          // color: servicecardfillcolor,
          border: Border.all(color: txtborderbluecolor),
          borderRadius: BorderRadius.all(Radius.circular(7.0)),
        ),
        child: Row(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(7.0),
                bottomLeft: Radius.circular(7.0),
              ),
              child: Image.network(
                eventLogoUrl + widget.EventVendorIMG,
                fit: BoxFit.fill,
                height: 130,
                width: 130,
              ),
            ),
            SizedBox(
              width: 15.0,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                 "Stall Location is "+ widget.stallname,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'Manrope-SemiBold'),
                ),
                  SizedBox(
                  height: 8.0,
                ),
                 Text(
                "Area is "+  widget.area,
                  style: TextStyle(
                    fontFamily: 'Manrope_Medium',
                    color: Colors.white
                  ),
                ),
                  SizedBox(
                  height: 3.0,
                ),
                  Text(
                "Stall For "+  widget.stallFor,
                  style: TextStyle(
                      fontFamily: 'Manrope_Medium',
                      color: Colors.white
                  ),
                ),
                RatingBar.builder(
                  glow: false,
                  itemSize: 15.0,
                  initialRating: 3,
                  minRating: 1,
                  direction: Axis.horizontal,
                  allowHalfRating: true,
                  itemCount: 5,
                  //itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
                  itemBuilder: (context, _) => Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  unratedColor: Colors.grey,
                  onRatingUpdate: (rating) {
                    print(rating);
                  },
                ),
                  SizedBox(
                  height: 3.0,
                ),
                Text(
                  widget.priceUnit + " " + widget.price,
                  style: TextStyle(
                      fontSize: 16.0,
                      fontFamily: 'Manrope-SemiBold',
                      color: Colors.white
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
