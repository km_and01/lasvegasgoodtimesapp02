import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/screens/bookings/bookings.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';
import 'package:lasvegas_gts_app/screens/profile/profile.dart';
import 'package:lasvegas_gts_app/screens/vendor/vendor_service_selection_scr.dart';
import 'package:lasvegas_gts_app/screens/services/services.dart';

class CustomNavBar extends StatefulWidget {
  final int defaultSelectedIndex;
  final Function(int) onChange;
  // ignore: use_key_in_widget_constructors
  const CustomNavBar({
    this.defaultSelectedIndex = 0,
    required this.onChange,
  });

  @override
  State<CustomNavBar> createState() => _CustomNavBarState();
}

class _CustomNavBarState extends State<CustomNavBar> {
  int _selectedIndex = 0;
  
  @override
  void initState() {
    // TODO: implement initState
    _selectedIndex = widget.defaultSelectedIndex;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        border: Border.all(color: txtborderbluecolor, width: 1),
        borderRadius: BorderRadius.only(
            topRight: Radius.circular(28.0), topLeft: Radius.circular(28.0)),
        color: Color(0xFF1A3A89),
      ),
      child: Row(
        children: [
          buildNavBarItem(0, context, "ic_home.png", "Home"),
          buildNavBarItem(1, context, "ic_services.png", "Services"),
          buildNavBarItem(2, context, "booking_ic.png", "Bookings"),
          buildNavBarItem(3, context, "ic_profile.png", "Profile"),
        ],
      ),
    );
  }

  Widget buildNavBarItem(
    int index,
    BuildContext context,
    String icon,
    String title,
  ) {
    return Container(
      padding: EdgeInsets.fromLTRB(0, 12.0, 0, 0),
      height: 75.0,
      width: MediaQuery.of(context).size.width / 4.2,
      // decoration: BoxDecoration(
      //   color: index == _selectedItemIndex ? Colors.white : greytxtcolor,
      // ),
      child: InkWell(
        onTap: () { 
        widget.onChange(index);
        setState(() {
          _selectedIndex = index;
        });
      },
        child: Column(
          children: [
            Image.asset(
              "assets/icons/$icon",
              height: 24,
              width: 24,
              color: index == _selectedIndex ? Colors.white : greytxtcolor,
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              title,
              style: TextStyle(
                color: index == _selectedIndex ? Colors.white : greytxtcolor,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
