import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final Widget lead;
  final Widget title;

  CustomAppBar({required this.lead, required this.title});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0.0,
      leading: Padding(
        padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
        child: lead,
      ),
      leadingWidth: 40.0,
      title: title,
      bottom: PreferredSize(
        child: Column(
          children: const [
            Divider(
              thickness: 2,
              height: 2,
              color: Colors.amber,
            ),
            SizedBox(
              height: 3.0,
            ),
            DottedLine(
              lineThickness: 4.5,
              dashRadius: 7,
              dashLength: 4.0,
              // lineLength:  MediaQuery.of(context).size.width,
              dashColor: Colors.amber,
            ),
          ],
        ),
        preferredSize: Size.fromHeight(1),
      ),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(80);
}
