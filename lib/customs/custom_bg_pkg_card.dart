import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/screens/services/bodyguard/bodyguard_details_scr.dart';
import 'package:lasvegas_gts_app/screens/services/eventSpace/package_details_scr.dart';
import 'package:lasvegas_gts_app/screens/services/fun/fun_pkg_detail_scr.dart';
import 'package:lasvegas_gts_app/screens/services/wedding/wedd_pkg_detail_scr.dart';

String bgLogoUrl =
    'http://koolmindapps.com/lasvegas/public/images/executiveprotection/';

class CustomBGPackageCard extends StatefulWidget {
  final String packageIMG,
      pkgVendorID,
      packageName,
      priceUnit,
      packagePrice,
      packageDiscPrice;
  // shortDetails;
  // capacity;
  final String add1, add2, fullDetails;
  // final String pkgService,possibleEvents;
  final List<String> pkgIMGList; //,amaities;
  const CustomBGPackageCard({
    Key? key,
    required this.packageIMG,
    required this.packageName,
    required this.priceUnit,
    required this.packagePrice,
    required this.packageDiscPrice,
    // required this.capacity,
    required this.add1,
    required this.add2,
    required this.fullDetails,
    required this.pkgIMGList,
    // required this.amaities,  required this.pkgService, required this.possibleEvents,
    required this.pkgVendorID,
    // required this.shortDetails,
    // required this.pkgInclude
  }) : super(key: key);

  @override
  _CustomBGPackageCardState createState() => _CustomBGPackageCardState();
}

class _CustomBGPackageCardState extends State<CustomBGPackageCard> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) {
              return PackageDetailsBG(
                pkgName: widget.packageName,
                priceUnit: widget.priceUnit,
                // discPrice: widget.packageDiscPrice,
                pkgDetails: widget.fullDetails,
                address1: widget.add1,
                address2: widget.add2,
                pkgIMGList: widget.pkgIMGList,
                // pkgShortDetails: widget.shortDetails,
                pkgVendorID: widget.pkgVendorID,
                pkgIMG: widget.packageIMG,
                // pkgInclude: widget.pkgInclude,
                depositPrice: widget.packageDiscPrice,
                pkgPrice: widget.packagePrice, secType: widget.packageName,
              );
            },
          ),
        );
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 7.5),
        decoration: BoxDecoration(
          // color: servicecardfillcolor,
          border: Border.all(color: txtborderbluecolor),
          borderRadius: BorderRadius.all(Radius.circular(7.0)),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(7.0),
                bottomLeft: Radius.circular(7.0),
              ),
              child: Image.network(
                bgLogoUrl + widget.packageIMG,
                fit: BoxFit.fill,
                height: 110,
                width: 130,
              ),
              //     Image.asset(
              //   "assets/images/restaurent1.png",
              // ),
            ),
            SizedBox(
              width: 15.0,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  widget.packageName,
                  // "Event Name",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18.0,
                      fontFamily: 'Raleway-Medium'),
                ),
                /*RatingBar.builder(
                  glow: false,
                  itemSize: 20.0,
                  initialRating: 3,
                  minRating: 1,
                  direction: Axis.horizontal,
                  allowHalfRating: true,
                  itemCount: 5,
                  itemPadding: EdgeInsets.symmetric(horizontal: 0.5),
                  itemBuilder: (context, _) => Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  unratedColor: Colors.grey,
                  onRatingUpdate: (rating) {
                    print(rating);
                  },
                ),*/
                SizedBox(
                  height: 5.0,
                ),
                Text(
                  'City were i can provide service',
                  style: TextStyle(
                    fontSize: 15.0,
                    overflow: TextOverflow.ellipsis,
                    color: greytxtcolor,
                      fontFamily: 'Raleway-Medium'
                  ),
                ),
                Text(
                  '1. ${widget.add1}',
                  maxLines: 1,
                  style: TextStyle(
                      fontSize: 13.0,
                      overflow: TextOverflow.ellipsis,
                      color: greytxtcolor,
                      fontFamily: 'Raleway-Medium'
                  ),
                ),
                Text(
                  '2. ${widget.add2}',
                  maxLines: 1,
                  style: TextStyle(
                      fontSize: 13.0,
                      overflow: TextOverflow.ellipsis,
                      color: greytxtcolor,
                      fontFamily: 'Raleway-Medium'
                  ),
                ),
                SizedBox(
                  width: 5.0,
                ),
                Text(
                  "Price " + widget.priceUnit + " " + widget.packagePrice,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16.0,
                    fontFamily: 'Manrope-SemiBold',
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
