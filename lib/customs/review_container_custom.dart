import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';

class ReviewCustomContainer extends StatelessWidget {
  const ReviewCustomContainer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 3.0),
      padding: EdgeInsets.fromLTRB(15, 10.0, 15, 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              CircleAvatar(
                radius: 25,
                backgroundColor: greytxtcolor,
                backgroundImage: AssetImage("assets/images/chef5.png"),
              ),
              SizedBox(
                width: 15.0,
              ),
              Text(
                "Reviewer Name",
                style: kTxtStyle.copyWith(fontSize: 16.0),
              ),
            ],
          ),
          SizedBox(
            height: 10.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Cause of visiting",
                style: kTxtStyle,
              ),
              Text(
                "dd MON yyyy",
                style: kTxtStyle,
              ),
            ],
          ),
          SizedBox(
            height: 15.0,
          ),
          Text(
            "Review.... Review.... Review.... Review.... Review....",
            style: kHeadText.copyWith(fontSize: 18),
          ),
        ],
      ),
      decoration: BoxDecoration(
        // color: servicecardfillcolor,
        border: Border.all(color: txtborderbluecolor),
      ),
    );
  }
}
