import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lasvegas_gts_app/screens/services/eventSpace/event_space_package_listing_scr.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_hotel_display_card.dart';
import 'package:lasvegas_gts_app/screens/services/eat/catering/catering_overview.dart';
import 'package:lasvegas_gts_app/screens/services/fun/fun_pkg_listing_scr.dart';

// String amount = '';
String vendorEventSpaceID = '';
String serviceEventSpaceID = '';
// List<String> foodNameListCatering = [];

String sDate = '', sType = '', sCity = '', noPerson = '';

class CustomFunVendorCard extends StatefulWidget {
  final String shortDisc; 
  final String vendorID;
  // final String sDate, sType, sCity;
  final String FunVendorIMG, bname, price, priceUnit;
  
  const CustomFunVendorCard({
    Key? key,
    required this.shortDisc,
   
    required this.vendorID,
    // required this.sDate,
    // required this.sType,
    // required this.sCity,
 
    required this.FunVendorIMG,
    required this.bname,
    required this.price,
    // required this.eventspsService,
    required this.priceUnit,
 
  }) : super(key: key);

  @override
  State<CustomFunVendorCard> createState() =>
      _CustomFunVendorCardState();
}

class _CustomFunVendorCardState
    extends State<CustomFunVendorCard> {
  @override
  void initState() {
    // amount = widget.price;
    vendorEventSpaceID = widget.vendorID;
    // serviceEventSpaceID = widget.eventspsService;
    // foodNameListCatering = widget.foodName;
    setState(() {
      // sDate = widget.sDate;
      // sCity = widget.sCity;
      // sType = widget.sType;
      // noPerson = widget.noPerson;
    });
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => FunPkgListingScr(
                funvendorID: vendorEventSpaceID,
                bname: widget.bname,
                ),
          ),
        );
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 7.5),
        decoration: BoxDecoration(
          // color: servicecardfillcolor,
          border: Border.all(color: txtborderbluecolor),
          borderRadius: BorderRadius.all(Radius.circular(7.0)),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(7.0),
                bottomLeft: Radius.circular(7.0),
              ),
              child: Image.network(
                bLogoUrl + widget.FunVendorIMG,
                fit: BoxFit.fill,
                height: 110,
                width: 130,
              ),
              //     Image.asset(
              //   "assets/images/restaurent1.png",
              // ),
            ),
            SizedBox(
              width: 15.0,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.bname,
                  // "Event Name",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18.0,
                      fontFamily: 'Raleway-Medium'),
                ),
                RatingBar.builder(
                  glow: false,
                  itemSize: 15.0,
                  initialRating: 3,
                  minRating: 1,
                  direction: Axis.horizontal,
                  allowHalfRating: true,
                  itemCount: 5,
                  //itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
                  itemBuilder: (context, _) => Icon(
                    Icons.star,
                    color: Colors.amber,
                    //   Image.asset(
                    // "assets/icons/rating_star.png",
                    // color: Colors.amber,
                  ),
                  // ),
                  unratedColor: Colors.grey,
                  onRatingUpdate: (rating) {
                    print(rating);
                  },
                ),
                SizedBox(
                  width: 200.0,
                  child: Text(
                    widget.shortDisc,
                    // "Event Name",
                    maxLines: 2,
                    style: TextStyle(
                        color: greytxtcolor,
                        fontSize: 13.0,
                        fontFamily: 'Manrope'),
                  ),
                ),
                SizedBox(
                  height: 3.0,
                ),
                Text(
                  "Price Starts from " + widget.priceUnit + " " + widget.price,
                  // "456 - Lorem ipsum dolor sit amet,\nconsectetur adipiscing elit.",
                  style: TextStyle(
                      fontSize: 16.0,
                      fontFamily: 'Manrope-SemiBold',
                      color: Colors.white
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
