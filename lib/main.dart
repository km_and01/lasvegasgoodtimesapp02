import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';
import 'package:lasvegas_gts_app/screens/login/login_src.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  // SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersiveSticky,);
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
      statusBarColor: Colors.transparent
    )
  );

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return MyAppState();
  }

  // This widget is the root of your application.

}

class MyAppState extends State<MyApp> {

  bool is_login = false;

  @override
  void initState() {
    // TODO: implement initState
    _configureApp();
    super.initState();

  }
  _configureApp() async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();

    setState(() {
      is_login = preferences.getBool('IsLogin')!;
    });
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Las Vegas',
      theme: ThemeData(
        fontFamily: 'Raleway',
        primarySwatch: primebgcolor,
        unselectedWidgetColor: Colors.white,

        // accentColor: Colors.white,
      ),
      home: is_login ? HomeScreen() : LoginScreen(),
    );
  }
}
