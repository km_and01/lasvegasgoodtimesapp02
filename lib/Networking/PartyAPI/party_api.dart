import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:lasvegas_gts_app/Networking/PartyAPI/party_booking_model.dart';
import 'package:lasvegas_gts_app/Networking/PartyAPI/party_pkg_model.dart';
import 'package:lasvegas_gts_app/Networking/PartyAPI/party_vendor_model.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';

class PartyAPIs {

  Future getPartyVendorData(String city_id, String date) async {
    var res = await http.post(
        Uri.parse('http://koolmindapps.com/lasvegas/public/api/v1/partyvendors'),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "city": city_id,
          "date" : date,
        });
    print(res.body);

    if (res.statusCode == 200) {
      print(res.body);

      final responseJson = json.decode(res.body);
      print(responseJson);
      var data = PartyVendorModel.fromJson(responseJson);
      print(data.message);
      return data.data!;
    } else {
      print(res.statusCode);
    }
  }

  Future bookEvent(String vendorId, imagePath, partyID, name, amount,date) async {
    try {
      var res = await http.post(
          Uri.parse(
              'http://koolmindapps.com/lasvegas/public/api/v1/bookpartyservice'),
          headers: {
            'Authorization': "Bearer " + homeToken,
            'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
          },
          body: {
            "vendor_id": vendorId,
            "party_id": partyID,
            "img1": imagePath,
            "amount": amount,
            "person2": name,
            "date": date,
          });
       if (res.statusCode == 200) {
      print(res.body);

      final responseJson = json.decode(res.body);
      print(responseJson);
      var data = PartyBookingModel.fromJson(responseJson);
      print(data.message);
     
      return data.message;
    } else {
      print(res.statusCode);
    }
    } catch (e) {
      print(e);
      return e;
    }
  }

  Future getPartyPkgData(String vendor_id) async {
    var res = await http.post(
        Uri.parse(
            'http://koolmindapps.com/lasvegas/public/api/v1/partyservices'),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "vendor_id": vendor_id,
        });
    print(res.body);

    if (res.statusCode == 200) {
      print(res.body);

      final responseJson = json.decode(res.body);
      print(responseJson);
      var data = PartyPkgModel.fromJson(responseJson);
      print(data.message);
      print("Restaurent DATA in APIs =====" +
          data.data![0].bussinessDetails!.shortDescription.toString());

      return data.data!;
    } else {
      print(res.statusCode);
    }
  }


}
