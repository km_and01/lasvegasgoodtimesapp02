// To parse this JSON data, do
//
//     final partyBookingModel = partyBookingModelFromJson(jsonString);

import 'dart:convert';

PartyBookingModel partyBookingModelFromJson(String str) => PartyBookingModel.fromJson(json.decode(str));

String partyBookingModelToJson(PartyBookingModel data) => json.encode(data.toJson());

class PartyBookingModel {
    PartyBookingModel({
        this.message,
        this.statusCode,
        this.data,
    });

    String? message;
    int? statusCode;
    PBook? data;

    factory PartyBookingModel.fromJson(Map<String, dynamic> json) => PartyBookingModel(
        message: json["message"],
        statusCode: json["status_code"],
        data: PBook.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "status_code": statusCode,
        "data": data!.toJson(),
    };
}

class PBook {
    PBook({
        this.bookingNumber,
        this.userId,
        this.vendorId,
        this.name,
        this.categoryId,
        this.person,
        this.categoryName,
        this.imagePath,
        this.amount,
        this.checkIn,
        this.updatedAt,
        this.createdAt,
        this.id,
    });

    String? bookingNumber;
    int? userId;
    String? vendorId;
    String? name;
    String? categoryId;
    String? person;
    String? categoryName;
    String? imagePath;
    String? amount;
    String? checkIn;
    DateTime? updatedAt;
    DateTime? createdAt;
    int? id;

    factory PBook.fromJson(Map<String, dynamic> json) => PBook(
        bookingNumber: json["booking_number"],
        userId: json["user_id"],
        vendorId: json["vendor_id"],
        name: json["name"],
        categoryId: json["category_id"],
        person: json["person"],
        categoryName: json["category_name"],
        imagePath: json["image_path"],
        amount: json["amount"],
        checkIn: json["check_in"],
        updatedAt: DateTime.parse(json["updated_at"]),
        createdAt: DateTime.parse(json["created_at"]),
        id: json["id"],
    );

    Map<String, dynamic> toJson() => {
        "booking_number": bookingNumber,
        "user_id": userId,
        "vendor_id": vendorId,
        "name": name,
        "category_id": categoryId,
        "person": person,
        "category_name": categoryName,
        "image_path": imagePath,
        "amount": amount,
        "check_in": checkIn,
        "updated_at": updatedAt!.toIso8601String(),
        "created_at": createdAt!.toIso8601String(),
        "id": id,
    };
}
