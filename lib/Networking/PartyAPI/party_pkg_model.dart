// To parse this JSON data, do
//
//     final partyPkgModel = partyPkgModelFromJson(jsonString);

import 'dart:convert';

PartyPkgModel partyPkgModelFromJson(String str) => PartyPkgModel.fromJson(json.decode(str));

String partyPkgModelToJson(PartyPkgModel data) => json.encode(data.toJson());

class PartyPkgModel {
    PartyPkgModel({
        this.message,
        this.statusCode,
        this.data,
    });

    String? message;
    int? statusCode;
    List<PartPkg>? data;

    factory PartyPkgModel.fromJson(Map<String, dynamic> json) => PartyPkgModel(
        message: json["message"],
        statusCode: json["status_code"],
        data: List<PartPkg>.from(json["data"].map((x) => PartPkg.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "status_code": statusCode,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class PartPkg {
    PartPkg({
        this.id,
        this.vendorId,
        this.serviceType,
        this.packageFor,
        this.name,
        this.forAdultsOnly,
        this.price,
        this.details,
        this.priceUnit,
        this.maxPersonAllowed,
        this.images,
        this.showDate,
        this.amenities,
        this.complimentaryItem,
        this.status,
        this.createdAt,
        this.updatedAt,
        this.bussinessDetails,
    });

    int? id;
    String? vendorId;
    String? serviceType;
    String? packageFor;
    String? name;
    String? forAdultsOnly;
    String? price;
    String? details;
    String? priceUnit;
    String? maxPersonAllowed;
    List<String>? images;
    String? showDate;
    List<String>? amenities;
    List<String>? complimentaryItem;
    int? status;
    DateTime? createdAt;
    DateTime? updatedAt;
    BussinessDetails? bussinessDetails;

    factory PartPkg.fromJson(Map<String, dynamic> json) => PartPkg(
        id: json["id"],
        vendorId: json["vendor_id"],
        serviceType: json["service_type"],
        packageFor: json["package_for"],
        name: json["name"],
        forAdultsOnly: json["for_adults_only"] == null ? 'null' : json["for_adults_only"],
        price: json["price"],
        details: json["details"],
        priceUnit: json["price_unit"],
        maxPersonAllowed: json["max_person_allowed"],
        images: List<String>.from(json["images"].map((x) => x)),
        showDate: json["show_date"] == null ? 'null' : json["show_date"],
        amenities: List<String>.from(json["amenities"].map((x) => x)),
        complimentaryItem: json["complimentary_item"] == null ? ["null"] : List<String>.from(json["complimentary_item"].map((x) => x)),
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        bussinessDetails: BussinessDetails.fromJson(json["bussiness_details"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "vendor_id": vendorId,
        "service_type": serviceType,
        "package_for": packageFor,
        "name": name,
        "for_adults_only": forAdultsOnly == null ? 'null' : forAdultsOnly,
        "price": price,
        "details": details,
        "price_unit": priceUnit,
        "max_person_allowed": maxPersonAllowed,
        "images": List<dynamic>.from(images!.map((x) => x)),
        "show_date": showDate == null ? 'null' : showDate,
        "amenities": List<dynamic>.from(amenities!.map((x) => x)),
        "complimentary_item": complimentaryItem == null ? 'null' : List<dynamic>.from(complimentaryItem!.map((x) => x)),
        "status": status,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
        "bussiness_details": bussinessDetails!.toJson(),
    };
}

class BussinessDetails {
    BussinessDetails({
        this.id,
        this.userId,
        this.categoryId,
        this.bussinessType,
        this.bussinessName,
        this.addressLine1,
        this.addressLine2,
        this.landmark,
        this.pincode,
        this.city,
        this.state,
        this.country,
        this.shortDescription,
        this.latitude,
        this.longitude,
        this.location,
        this.bussinessLogo,
        this.imagesk,
        this.timezone,
        this.doc,
        this.openTime,
        this.closeTime,
        this.createdAt,
        this.updatedAt,
    });

    int? id;
    String? userId;
    List<String>? categoryId;
    dynamic bussinessType;
    String? bussinessName;
    String? addressLine1;
    String? addressLine2;
    dynamic landmark;
    String? pincode;
    String? city;
    String? state;
    String? country;
    String? shortDescription;
    dynamic latitude;
    dynamic longitude;
    dynamic location;
    String? bussinessLogo;
    List<String>? imagesk;
    String? timezone;
    List<String>? doc;
    List<String>? openTime;
    List<String>? closeTime;
    DateTime? createdAt;
    DateTime? updatedAt;

    factory BussinessDetails.fromJson(Map<String, dynamic> json) => BussinessDetails(
        id: json["id"],
        userId: json["user_id"],
        categoryId: List<String>.from(json["category_id"].map((x) => x)),
        bussinessType: json["bussiness_type"],
        bussinessName: json["bussiness_name"],
        addressLine1: json["address_line1"],
        addressLine2: json["address_line2"],
        landmark: json["landmark"],
        pincode: json["pincode"],
        city: json["city"],
        state: json["state"],
        country: json["country"],
        shortDescription: json["short_description"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        location: json["location"],
        bussinessLogo: json["bussiness_logo"],
        imagesk: List<String>.from(json["imagesk"].map((x) => x)),
        timezone: json["timezone"],
        doc: List<String>.from(json["doc"].map((x) => x)),
        openTime: List<String>.from(json["open_time"].map((x) => x == null ? 'null' : x)),
        closeTime: List<String>.from(json["close_time"].map((x) => x == null ? 'null' : x)),
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "category_id": List<dynamic>.from(categoryId!.map((x) => x)),
        "bussiness_type": bussinessType,
        "bussiness_name": bussinessName,
        "address_line1": addressLine1,
        "address_line2": addressLine2,
        "landmark": landmark,
        "pincode": pincode,
        "city": city,
        "state": state,
        "country": country,
        "short_description": shortDescription,
        "latitude": latitude,
        "longitude": longitude,
        "location": location,
        "bussiness_logo": bussinessLogo,
        "imagesk": List<dynamic>.from(imagesk!.map((x) => x)),
        "timezone": timezone,
        "doc": List<dynamic>.from(doc!.map((x) => x)),
        "open_time": List<dynamic>.from(openTime!.map((x) => x == null ? 'null' : x)),
        "close_time": List<dynamic>.from(closeTime!.map((x) => x == null ? 'null' : x)),
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
    };
}
