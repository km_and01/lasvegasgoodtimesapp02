// import 'dart:convert';

import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:lasvegas_gts_app/Networking/StayAPI/stay_details_model.dart';
import 'package:lasvegas_gts_app/Networking/StayAPI/stay_search_pent_house_api.dart';
import 'package:lasvegas_gts_app/screens/services/stay/sta_search_main.dart';
//  var res;

Uri url = Uri.parse("http://lasvegas.koolmindapps.com/api/v1/stayvendor");
String? token = 'GetDataPvtBnb.userToken';
Future<void> StaySearchPrivateBnB(String check_in, String check_out,
    String no_of_person, String no_of_rooms) async {
  var res = await http.post(url, headers: {
    'API_KEY': APIkey,
    'Authorization': "Bearer " + token!
  }, body: {
    // "category_id": '1',
    "check_in": check_in,
    "check_out": check_out,
    "no_of_person": no_of_person,
    "no_rooms": no_of_rooms,
    "service_name": "Private BnB"
  });
  print(res.statusCode);
  print(res.body);

  var data = jsonDecode(res.body);
  print(data.toString());
  try {
    if (res.statusCode == 200) {
      var refinedData = StayDetailsModel.fromJson(data);
      // print(refinedData.services.roomDescription);

      var roomDiscription = refinedData.services.privateBnb.roomDescription;
      var businessName =
          refinedData.services.privateBnb.bussinessDetails.bussinessName;
      var basePrice = refinedData.services.privateBnb.basePrice;
      var discountedPrice = refinedData.services.privateBnb.disPrice;
      var thumbnailHotel =
          refinedData.services.privateBnb.bussinessDetails.imagesk;
      var vendorID = refinedData.services.privateBnb.vendorId;
      var subCat = refinedData.services.privateBnb.subServiceId;
      var roomName = refinedData.services.privateBnb.roomName;
      var businessLogo =
          refinedData.services.privateBnb.bussinessDetails.bussinessLogo;

      var add1 = refinedData.services.privateBnb.bussinessDetails.addressLine1;
      var add2 = refinedData.services.privateBnb.bussinessDetails.addressLine2;
      print(roomDiscription);
      print(businessName);

      // GetDataPvtBnb.hotelName = businessName!;
      // GetDataPvtBnb.basePrice = basePrice;
      // GetDataPvtBnb.discountPrice = discountedPrice;
      // GetDataPvtBnb.roomddDiscription = roomDiscription;
      // GetDataPvtBnb.vendorID = vendorID;
      // GetDataPvtBnb.suCategory = subCat;
      // GetDataPvtBnb.roomName = roomName;

      // GetDataPvtBnb.addLine1 = add1;
      // GetDataPvtBnb.addLine2 = add2;
      // GetDataPvtBnb.images = thumbnailHotel;
      // GetDataPvtBnb.logo = businessLogo;
    } else {
      throw Exception('Failed to load ');
    }
  } catch (e) {
    print(e);
  }
}
