// import 'dart:convert';

import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:lasvegas_gts_app/Networking/StayAPI/stay_details_model.dart';
import 'package:lasvegas_gts_app/screens/services/stay/sta_search_main.dart';
//  var res;

Uri url =
    Uri.parse("http://koolmindapps.com/lasvegas/public/api/v1/stayvendor");
String APIkey = 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs';
Uri url2 =
    Uri.parse("http://koolmindapps.com/lasvegas/public/api/v1/stayservice");
String? token ='uToken';
Future<void> StaySearchPentHouse(
  String checkIn, 
  String checkOut,
    String no_of_person,
     String no_of_rooms
     ) async {
  var res = await http.post(url, headers: {
    'API_KEY': APIkey,
    'Authorization': "Bearer " + token!
  }, body: {
    // "category_id": '1',
    "check_in": checkIn,
    "check_out": checkOut,
    "no_of_person": no_of_person,
    "no_rooms": no_of_rooms,
    "service_name": "Pent House"
  });
  print(res.statusCode);
  print(res.body);

  var data = jsonDecode(res.body);
  print(data.toString());
  try {
    if (res.statusCode == 200) {
      var refinedData = StayDetailsModel.fromJson(data);
      print(refinedData.services.pentHouses.roomDescription);

      var roomDiscription = refinedData.services.pentHouses.roomDescription;
      var businessName =
          refinedData.services.pentHouses.bussinessDetails.bussinessName;
      var basePrice = refinedData.services.pentHouses.basePrice;
      var discountedPrice = refinedData.services.pentHouses.disPrice;
      var thumbnailHotel =
          refinedData.services.pentHouses.bussinessDetails.imagesk;
      var vendorID = refinedData.services.pentHouses.vendorId;
      var subCat = refinedData.services.pentHouses.subServiceId;
      var roomName = refinedData.services.pentHouses.roomName;
      var add1 = refinedData.services.pentHouses.bussinessDetails.addressLine1;
      var add2 = refinedData.services.pentHouses.bussinessDetails.addressLine2;
      var businessLogo =
          refinedData.services.pentHouses.bussinessDetails.bussinessLogo;

      print("this ids Business in PENT HOUSE ===" + businessName!);
      print("this ids Vensor  in PENT HOUSE ===" + vendorID);
      // SpecialSuitView.getData();

      // GetDataPentHouse.hotelName = businessName;
      // GetDataPentHouse.basePrice = basePrice;
      // GetDataPentHouse.discountPrice = discountedPrice;
      // GetDataPentHouse.roomddDiscription = roomDiscription;
      // GetDataPentHouse.suCategory = subCat;
      // GetDataPentHouse.vendorID = vendorID;
      // GetDataPentHouse.roomName = roomName;
      // GetDataPentHouse.addLine1 = add1;
      // GetDataPentHouse.addLine2 = add2;
      // GetDataPentHouse.images = thumbnailHotel;
      // GetDataPentHouse.logo = businessLogo;
    } else {
      throw Exception('Failed to load ');
    }
  } catch (e) {
    print("This is ERROR =====" + e.toString());
  }
}
