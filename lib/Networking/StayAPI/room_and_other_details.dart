import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:lasvegas_gts_app/Networking/StayAPI/stay_details_model.dart';
import 'package:lasvegas_gts_app/customs/custom_hotel_display_card.dart';
import 'package:lasvegas_gts_app/screens/services/stay/sta_search_main.dart';

Uri url = Uri.parse("http://lasvegas.koolmindapps.com/api/v1/stayvservice");

Future<void> RoomAndOtherDetails(
    String vendorID, String subCategorytype) async {
  var details = await http.post(url, headers: {
    'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
    'Authorization':
        'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNTE0YzJkM2U2NGRkMTE0YTNmYzdlNzQzZmFmOGM1N2U4MmY0ZTlhNTFjMzAzMDZmMzAzODE3NmU3NDBhMzIwNmIxZDlmMGEzZmFhY2M2YzEiLCJpYXQiOjE2NDA2ODI0NDAuODk3NTQ3LCJuYmYiOjE2NDA2ODI0NDAuODk3NTUxLCJleHAiOjE2NzIyMTg0NDAuODk2MjI5LCJzdWIiOiI2NSIsInNjb3BlcyI6W119.hrRRM_JZ0B8IU5ChF55F4P50-iU3Gxg06eUn7KYgwG5xKmFvOu1bb9sCLs_e1Z4gUJAhwgKiAsk2M2eAkRz4_nz0Z_wqBmTCBOw5-oijZxVnHmguXvVzrMPvBWrJxYNHCrirkA4UwjgY38Sw0QErgFwhC8r5FqYcJ_gvpbI9NFujgkHjotDiBUhfcX12IaGDRbDVx_jDHHyx78xNQSHSd_qN8Aim2piJBdv460PLrPp_uEk5bGcPZFqXnqfEkFOZYANZNTixug3wU0PrfRZg46i7aU-DMQUofT9P_SRt3LqYmEHQamoT4mMSFF_OCVYsmygSqThV-Qsfg6NhfSfGA6ATWHkoIn4yqo_QARJCHj-H7ocZIB2vAp8ThbTq0Yc9IyVppHA8L1B5EpA8HYHVK1vjiohrTn3laVE02lmRMB-_FyTZGAQd__sMT6NpOiSUj41gSaidv59y680sGt1E2_FM7k2AuV-cYdywPNNBNjLpG6eeNnsGaQSxz39NFuJo8ofa_iW1E_XMldjBhCSwRqNPDROUgVGZrRWul2J3TV_xXSAxXZxgOwpvwLvreZuqsWWfZ26xiT7dvLvFrVf-nQvZeaGmuD960lnkJ7BCu8KeQ4US9Pn1MXndligLL6sLVUXJg_HDQONC3QSJfrUgOJB7GrcH8Jm20f_tpZ4Vslo'
  }, body: {
    "vendor_id": vendorID,
    "sub_service_id": subCategorytype
  });
  print(details.statusCode);
  print(details.body);
  var data = jsonDecode(details.body);
  // GetDetails(roomCapacity, roomtype);
}
