// To parse this JSON data, do
//
//     final stayVendorModel = stayVendorModelFromJson(jsonString);

import 'dart:convert';

StayVendorModel stayVendorModelFromJson(String str) => StayVendorModel.fromJson(json.decode(str));

String stayVendorModelToJson(StayVendorModel data) => json.encode(data.toJson());

class StayVendorModel {
    StayVendorModel({
        this.message,
        this.statusCode,
        this.services,
    });

    String ?message;
    int? statusCode;
    List<Service>? services;

    factory StayVendorModel.fromJson(Map<String, dynamic> json) => StayVendorModel(
        message: json["message"],
        statusCode: json["status_code"],
        services: List<Service>.from(json["services"].map((x) => Service.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "status_code": statusCode,
        "services": List<dynamic>.from(services!.map((x) => x.toJson())),
    };
}

class Service {
    Service({
        this.id,
        this.vendorId,
        this.subServiceId,
        this.roomName,
        this.basePrice,
        this.priceUnit,
        this.disPrice,
        this.roomDescription,
        this.stayPhotos,
        this.amenities,
        this.status,
        this.quantity,
        this.createdAt,
        this.updatedAt,
        this.minPrice,
        this.bussinessDetails,
    });

    int? id;
    String? vendorId;
    String? subServiceId;
    String? roomName;
    String? basePrice;
    String? priceUnit;
    String? disPrice;
    String? roomDescription;
    List<String>? stayPhotos;
    List<String>? amenities;
    int? status;
    String ?quantity;
    DateTime? createdAt;
    DateTime? updatedAt;
    String? minPrice;
    BussinessDetails? bussinessDetails;

    factory Service.fromJson(Map<String, dynamic> json) => Service(
        id: json["id"],
        vendorId: json["vendor_id"],
        subServiceId: json["sub_service_id"],
        roomName: json["room_name"],
        basePrice: json["base_price"],
        priceUnit: json["price_unit"],
        disPrice: json["dis_price"],
        roomDescription: json["room_description"],
        stayPhotos: json["stay_photos"] == null ? [] : List<String>.from(json["stay_photos"].map((x) => x)),
        amenities:json["amenities"]== null ?[]: List<String>.from(json["amenities"].map((x) => x)),
        status: json["status"],
        quantity: json["quantity"],
        createdAt: json["created_at"] == null ? DateTime.now() :  DateTime.parse(json["created_at"]),
        updatedAt: json["created_at"] == null ? DateTime.now() : DateTime.parse(json["updated_at"]),
        minPrice: json["min_price"],
        bussinessDetails:json["bussiness_details"]== null ? null  : BussinessDetails?.fromJson(json["bussiness_details"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "vendor_id": vendorId,
        "sub_service_id": subServiceId,
        "room_name": roomName,
        "base_price": basePrice,
        "price_unit": priceUnit,
        "dis_price": disPrice,
        "room_description": roomDescription,
        "stay_photos": List<dynamic>.from(stayPhotos!.map((x) => x)),
        "amenities": List<dynamic>.from(amenities!.map((x) => x)),
        "status": status,
        "quantity": quantity,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
        "min_price": minPrice,
        "bussiness_details": bussinessDetails!.toJson(),
    };
}

class BussinessDetails {
    BussinessDetails({
        this.id,
        this.userId,
        this.categoryId,
        this.bussinessType,
        required this.bussinessName,
        this.addressLine1,
        this.addressLine2,
        this.landmark,
        this.pincode,
        this.city,
        this.state,
        this.country,
        this.shortDescription,
        this.latitude,
        this.longitude,
        this.location,
        this.bussinessLogo,
        this.imagesk,
        this.timezone,
        this.doc,
        this.openTime,
        this.closeTime,
        this.createdAt,
        this.updatedAt,
    });

    int? id;
    String? userId;
    List<String>? categoryId;
    dynamic? bussinessType;
    String bussinessName;
    String ?addressLine1;
    String? addressLine2;
    dynamic landmark;
    String ?pincode;
    String? city;
    String ?state;
    String ?country;
    String? shortDescription;
    dynamic latitude;
    dynamic longitude;
    dynamic location;
    String? bussinessLogo;
    List<String>? imagesk;
    String ?timezone;
    List<String>? doc;
    List<String>? openTime;
    List<String>? closeTime;
    DateTime? createdAt;
    DateTime? updatedAt;

    factory BussinessDetails.fromJson(Map<String, dynamic> json) => BussinessDetails(
        id: json["id"],
        userId: json["user_id"],
        categoryId: List<String>.from(json["category_id"].map((x) => x)),
        bussinessType: json["bussiness_type"],
        bussinessName: json["bussiness_name"],
        addressLine1: json["address_line1"],
        addressLine2: json["address_line2"],
        landmark: json["landmark"],
        pincode: json["pincode"],
        city: json["city"],
        state: json["state"],
        country: json["country"],
        shortDescription: json["short_description"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        location: json["location"],
        bussinessLogo: json["bussiness_logo"],
        imagesk: List<String>.from(json["imagesk"].map((x) => x)),
        timezone: json["timezone"],
        doc: List<String>.from(json["doc"].map((x) => x)),
        openTime: List<String>.from(json["open_time"].map((x) => x == null ? "null" : x)),
        closeTime: List<String>.from(json["close_time"].map((x) => x == null ? "null" : x)),
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "category_id": List<dynamic>.from(categoryId!.map((x) => x)),
        "bussiness_type": bussinessType,
        "bussiness_name": bussinessName,
        "address_line1": addressLine1,
        "address_line2": addressLine2,
        "landmark": landmark,
        "pincode": pincode,
        "city": city,
        "state": state,
        "country": country,
        "short_description": shortDescription,
        "latitude": latitude,
        "longitude": longitude,
        "location": location,
        "bussiness_logo": bussinessLogo,
        "imagesk": List<dynamic>.from(imagesk!.map((x) => x)),
        "timezone": timezone,
        "doc": List<dynamic>.from(doc!.map((x) => x)),
        "open_time": List<dynamic>.from(openTime!.map((x) => x == null ? null : x)),
        "close_time": List<dynamic>.from(closeTime!.map((x) => x == null ? null : x)),
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
    };
}
