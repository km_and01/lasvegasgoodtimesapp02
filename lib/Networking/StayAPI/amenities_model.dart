// To parse this JSON data, do
//
//     final amenitiesListModel = amenitiesListModelFromJson(jsonString);

import 'dart:convert';

AmenitiesListModel amenitiesListModelFromJson(String str) => AmenitiesListModel.fromJson(json.decode(str));

String amenitiesListModelToJson(AmenitiesListModel data) => json.encode(data.toJson());

class AmenitiesListModel {
    AmenitiesListModel({
        this.message,
        this.statusCode,
        this.services,
        required this.ami,
    });

    String? message;
    int? statusCode;
    List<dynamic>? services;
    List<Ami> ami;

    factory AmenitiesListModel.fromJson(Map<String, dynamic> json) => AmenitiesListModel(
        message: json["message"],
        statusCode: json["status_code"],
        services: List<dynamic>.from(json["services"].map((x) => x)),
        ami: List<Ami>.from(json["ami"].map((x) => Ami.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "status_code": statusCode,
        "services": List<dynamic>.from(services!.map((x) => x)),
        "ami": List<dynamic>.from(ami.map((x) => x.toJson())),
    };
}

class Ami {
    Ami({
        required this.id,
        required this.categoryId,
        required this.vendorSubServiceId,
        required this.amenitiesName,
        required this.amenitiesDescription,
        required this.amenitiesImage,
        required this.createdAt,
        required this.updatedAt,
        required this.status,
    });

    int id;
    String categoryId;
    String vendorSubServiceId;
    String amenitiesName;
    String amenitiesDescription;
    String amenitiesImage;
    DateTime createdAt;
    DateTime updatedAt;
    String status;

    factory Ami.fromJson(Map<String, dynamic> json) => Ami(
        id: json["id"],
        categoryId: json["category_id"],
        vendorSubServiceId: json["vendor_sub_service_id"],
        amenitiesName: json["amenities_name"],
        amenitiesDescription: json["amenities_description"],
        amenitiesImage: json["amenities_image"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        status: json["status"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "category_id": categoryId,
        "vendor_sub_service_id": vendorSubServiceId,
        "amenities_name": amenitiesName,
        "amenities_description": amenitiesDescription,
        "amenities_image": amenitiesImage,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "status": status,
    };
}
