// To parse this JSON data, do
//
//     final flightListModel = flightListModelFromJson(jsonString);

import 'dart:convert';

FlightListModel flightListModelFromJson(String str) => FlightListModel.fromJson(json.decode(str));

String flightListModelToJson(FlightListModel data) => json.encode(data.toJson());

class FlightListModel {
    FlightListModel({
        this.message,
        this.statusCode,
        required this.services,
    });

    String? message;
    int? statusCode;
    List<ServiceAir> services;

    factory FlightListModel.fromJson(Map<String, dynamic> json) => FlightListModel(
        message: json["message"],
        statusCode: json["status_code"],
        services: List<ServiceAir>.from(json["services"].map((x) => ServiceAir.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "status_code": statusCode,
        "services": List<dynamic>.from(services.map((x) => x.toJson())),
    };
}

class ServiceAir {
    ServiceAir({
     
        this.id,
        this.vendorId,
        this.airServiceType,
        this.noOfPaxs,
        this.price,
        this.priceUnit,
        this.status,
        this.descr,
        this.regions,
        this.pjSname,
        this.priceForPj,
        this.personPj,
        this.createdAt,
        this.updatedAt,
        this.fillHerat,
        this.bussinessDetails, 
        this.minPrice,
    });

    int? id;
    String? vendorId;
    String? airServiceType;
    String? noOfPaxs;
    String? price;
    String? priceUnit;
    int? status;
    String? descr;
    List<String>? regions;
    dynamic pjSname;
    dynamic priceForPj;
    dynamic personPj;
    DateTime? createdAt;
    DateTime? updatedAt;
    int? fillHerat;
    BussinessDetails? bussinessDetails;
    String? minPrice;

    factory ServiceAir.fromJson(Map<String, dynamic> json) => ServiceAir(
        id: json["id"],
        vendorId: json["vendor_id"],
        airServiceType: json["air_service_type"],
        noOfPaxs: json["no__of_paxs"],
        price: json["price"],
        priceUnit: json["price_unit"],
        status: json["status"],
        descr: json["descr"],
        regions: List<String>.from(json["regions"].map((x) => x)),
        pjSname: json["pj_sname"],
        priceForPj: json["price_for_pj"],
        personPj: json["person_pj"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        fillHerat: json["fill_herat"],
        minPrice: json["min_price"],
        bussinessDetails: BussinessDetails.fromJson(json["bussiness_details"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "min_price" : minPrice,
        "vendor_id": vendorId,
        "air_service_type": airServiceType,
        "no__of_paxs": noOfPaxs,
        "price": price,
        "price_unit": priceUnit,
        "status": status,
        "descr": descr,
        "regions": List<dynamic>.from(regions!.map((x) => x)),
        "pj_sname": pjSname,
        "price_for_pj": priceForPj,
        "person_pj": personPj,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
        "fill_herat": fillHerat,
        "bussiness_details": bussinessDetails!.toJson(),
    };
}

class BussinessDetails {
    BussinessDetails({
        this.id,
        this.userId,
        this.categoryId,
        this.bussinessType,
        this.bussinessName,
        this.addressLine1,
        this.addressLine2,
        this.landmark,
        this.pincode,
        this.city,
        this.state,
        this.country,
        this.shortDescription,
        this.latitude,
        this.longitude,
        this.location,
        this.bussinessLogo,
        this.imagesk,
        this.timezone,
        this.doc,
        this.openTime,
        this.closeTime,
        this.createdAt,
        this.updatedAt,
    });

    int? id;
    String? userId;
    List<String>? categoryId;
    dynamic bussinessType;
    String? bussinessName;
    String? addressLine1;
    String? addressLine2;
    dynamic landmark;
    String? pincode;
    String? city;
    String? state;
    String? country;
    String? shortDescription;
    dynamic latitude;
    dynamic longitude;
    dynamic location;
    String? bussinessLogo;
    List<String>? imagesk;
    String? timezone;
    List<String>? doc;
    List<String>? openTime;
    List<String>? closeTime;
    DateTime? createdAt;
    DateTime? updatedAt;

    factory BussinessDetails.fromJson(Map<String, dynamic> json) => BussinessDetails(
        id: json["id"],
        userId: json["user_id"],
        categoryId: List<String>.from(json["category_id"].map((x) => x)),
        bussinessType: json["bussiness_type"],
        bussinessName: json["bussiness_name"],
        addressLine1: json["address_line1"],
        addressLine2: json["address_line2"],
        landmark: json["landmark"],
        pincode: json["pincode"],
        city: json["city"],
        state: json["state"],
        country: json["country"],
        shortDescription: json["short_description"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        location: json["location"],
        bussinessLogo: json["bussiness_logo"],
        imagesk: List<String>.from(json["imagesk"].map((x) => x)),
        timezone: json["timezone"],
        doc: List<String>.from(json["doc"].map((x) => x)),
      openTime: List<String>.from(json["open_time"].map((x) => x == null ? "null" : x)),
        closeTime: List<String>.from(json["close_time"].map((x) => x == null ? "null" : x)),
         createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "category_id": List<dynamic>.from(categoryId!.map((x) => x)),
        "bussiness_type": bussinessType,
        "bussiness_name": bussinessName,
        "address_line1": addressLine1,
        "address_line2": addressLine2,
        "landmark": landmark,
        "pincode": pincode,
        "city": city,
        "state": state,
        "country": country,
        "short_description": shortDescription,
        "latitude": latitude,
        "longitude": longitude,
        "location": location,
        "bussiness_logo": bussinessLogo,
        "imagesk": List<dynamic>.from(imagesk!.map((x) => x)),
        "timezone": timezone,
        "doc": List<dynamic>.from(doc!.map((x) => x)),
        "open_time": List<dynamic>.from(openTime!.map((x) => x == null ? null : x)),
        "close_time": List<dynamic>.from(closeTime!.map((x) => x == null ? null : x)),
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
    };
}
