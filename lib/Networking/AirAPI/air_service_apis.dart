import 'dart:convert';

import 'package:lasvegas_gts_app/Networking/AirAPI/airport_list_model.dart';
import 'package:http/http.dart' as http;
import 'package:lasvegas_gts_app/Networking/AirAPI/flight_list_model.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';

class AirServiceApis {

  Uri url2 =
  Uri.parse("http://koolmindapps.com/lasvegas/public/api/v1/airservice");

  Future<List<Airport>?> getAirportList(String uToken) async {
    print(uToken);
    var res = await http.get(
        Uri.parse('http://koolmindapps.com/lasvegas/public/api/v1/airport'),
        headers: {
          'Authorization': "Bearer " + uToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        });

    print(res.body);

    if (res.statusCode == 200) {
      final responseJson = json.decode(res.body);
      print(res.body);
      var data = AirportListModel.fromJson(responseJson);
      print(data.airport![0].name);
      List<Airport>? airportList;

      print(airportList?[0].name);

      return data.airport;
    } else {
      print(res.statusCode);
    }
  }

  Future getFlightListing(
    String departureDate,
    String returnDate,
    String originPort,
    String destinationPort,
    String passengersCount,
    String heliJet,
  ) async {
    print(homeToken);
    var res = await http.post(
        Uri.parse('http://koolmindapps.com/lasvegas/public/api/v1/airvendor'),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "checkindate": departureDate,
          "checkoutdate": returnDate,
          "from": originPort,
          "to": destinationPort,
          "passenger": passengersCount,
          "services": heliJet,
        });

    print('PORT: $originPort');
    print('PORT: $destinationPort');
    if (res.statusCode == 200) {
      print(res.body);

      final responseJson = json.decode(res.body);
      print(res.body);
      var data = FlightListModel.fromJson(responseJson);
      print(data.services[0].bussinessDetails!.bussinessName);
      // List<Airport>? airportList;

      // print(airportList?[0].name);

      return data.services;
    } else {
      print(res.statusCode);
    }
  }


  Future<List<ServiceAir>> getSubFlightListing(String service_name, String vendor_id) async {
    print(homeToken);

    var details = await http.post(url2, headers: {
      'Authorization': "Bearer " + homeToken,
      'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
    }, body: {
      "service_name": service_name,
      "vendor_id": vendor_id,
    });

    /*var res = await http.post(
        Uri.parse('http://koolmindapps.com/lasvegas/public/api/v1/airservice'),
        headers: {
          'Authorization': 'Bearer ' + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "service_name": '',
          "vendor_id": vendor_id,
        });*/

    print('REQ $service_name');
    print('REQ $vendor_id');
    print('REQ ${details.request}');
    if (details.statusCode == 200) {
      print(details.body);

      final responseJson = json.decode(details.body);
      print(details.body);
      var data = FlightListModel.fromJson(responseJson);


      // List<Airport>? airportList;

      // print(airportList?[0].name);

      return data.services;
    } else {
      return [];
    }
  }

  Future airBooking(
    String departureDate,
    String returnDate,
    String originPort,
    String destinationPort,
    String passengersCount,
    String heliJet,
    String userID,
    String vendorID,
    String price,
    String pname,
    String pMob,
    String pEmail,
    String page,
    String plane_type,
  ) async {
    var res = await http.post(
        Uri.parse('http://koolmindapps.com/lasvegas/public/api/v1/bookair'),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "check_in": departureDate,
          "check_out": returnDate,
          "formair": originPort,
          "toair": destinationPort,
          "person": passengersCount,
          "sub_service_id": heliJet,
          "user_id": userID,
          "vendor_id": vendorID,
          "price": price,
          "pname": pname,
          "phone_number": pMob,
          "pemail": pEmail,
          "age": page,
          "plane_type": plane_type,
        });
    print(res.body);
  }
}
