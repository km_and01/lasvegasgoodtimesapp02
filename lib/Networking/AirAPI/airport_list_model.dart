// To parse this JSON data, do
//
//     final airportListModel = airportListModelFromJson(jsonString);

import 'dart:convert';

AirportListModel airportListModelFromJson(String str) => AirportListModel.fromJson(json.decode(str));

String airportListModelToJson(AirportListModel data) => json.encode(data.toJson());

class AirportListModel {
    AirportListModel({
        this.message,
        this.statusCode,
        this.airport,
    });

    String? message;
    int? statusCode;
    List<Airport>? airport;

    factory AirportListModel.fromJson(Map<String, dynamic> json) => AirportListModel(
        message: json["message"],
        statusCode: json["status_code"],
        airport: List<Airport>.from(json["airport"].map((x) => Airport.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "status_code": statusCode,
        "airport": List<dynamic>.from(airport!.map((x) => x.toJson())),
    };
}

class Airport {
    Airport({
        this.id,
        this.name,
        this.code,
        this.cityCode,
        this.countryCode,
        this.timeZone,
        this.iataType,
        this.status,
        this.createdAt,
        this.updatedAt,
    });

    int? id;
    String? name;
    String? code;
    String? cityCode;
    String? countryCode;
    String? timeZone;
    String? iataType;
    int? status;
    DateTime? createdAt;
    DateTime? updatedAt;

    factory Airport.fromJson(Map<String, dynamic> json) => Airport(
        id: json["id"],
        name: json["name"],
        code: json["code"],
        cityCode: json["city_code"],
        countryCode: json["country_code"],
        timeZone: json["time_zone"],
        iataType: json["iata_type"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "code": code,
        "city_code": cityCode,
        "country_code": countryCode,
        "time_zone": timeZone,
        "iata_type": iataType,
        "status": status,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
    };
}
