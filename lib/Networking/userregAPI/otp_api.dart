import 'dart:convert';
import 'package:http/http.dart' as http;
Uri url =  Uri.parse("http://lasvegas.koolmindapps.com/api/v1/verifyotp");
Future<void> verifyOtp(
  String uPhone, String uotp) async {
  print(uPhone);
  var res = await http.post(
     url,
      headers: {'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs'},
      body: {"mobile_number": uPhone, "otp": uotp
      });
      
  print(uotp);
  print(res.bodyBytes);
  //;
}

Otp otpFromJson(String str) => Otp.fromJson(json.decode(str));

String otpToJson(Otp data) => json.encode(data.toJson());

class Otp {
  Otp({
    required this.mobileNumber,
    required this.otp,
  });

  String mobileNumber;
  String otp;

  factory Otp.fromJson(Map<String, dynamic> json) => Otp(
        mobileNumber: json["mobile_number"],
        otp: json["otp"],
      );

  Map<String, dynamic> toJson() => {
        "mobile_number": mobileNumber,
        "otp": otp,
      };
}
