import 'dart:convert';
import 'package:http/http.dart' as http;
//  var res;

Uri url = Uri.parse("http://lasvegas.koolmindapps.com/api/v1/register");

Future<void> regUserData(
    String uName, String uEmail, String uPhnone, String uPassword) async {
  var res = await http.post(url, headers: {
    'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs'
  }, body: {
    "name": uName,
    "email": uEmail,
    "mobile_number": uPhnone,
    "password": uPassword,
    "user_type": "user"
  });
  print(res.body);

  Future<void> sendOtp(uPhnone) async {
    var resotp = await http.post(
        Uri.parse("http://lasvegas.koolmindapps.com/api/v1/resendotp"),
        headers: {'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs'},
        body: {"mobile_number": uPhnone, "user_type": "user"});

    var otpData = jsonDecode(resotp.body);
    print(otpData);
  }
}

// To parse this JSON data, do
//
//     final uReg = uRegFromJson(jsonString);

UReg uRegFromJson(String str) => UReg.fromJson(json.decode(str));

String uRegToJson(UReg data) => json.encode(data.toJson());

class UReg {
  UReg({
    required this.name,
    required this.email,
    required this.mobile_number,
    required this.password,
  });

  String name;
  String email;
  String mobile_number;
  String password;

  factory UReg.fromJson(Map<String, dynamic> json) => UReg(
        name: json["name"],
        email: json["email"],
        mobile_number: json["mobile_number"],
        password: json["password"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "email": email,
        "mobile_number": mobile_number,
        "password": password,
      };
}
