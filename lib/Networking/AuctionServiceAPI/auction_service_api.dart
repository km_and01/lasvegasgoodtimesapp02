// ignore_for_file: avoid_print

import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:lasvegas_gts_app/Networking/AuctionServiceAPI/auction_service_details_model.dart';
import 'package:lasvegas_gts_app/Networking/AuctionServiceAPI/auction_service_model.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';

class AuctionServiceAPIs {
  Future getAuctionServiceData(String auctionTitle) async {
    print('objectToken=============> $homeToken');
    var res = await http.post(
        Uri.parse('http://koolmindapps.com/lasvegas/public/api/v1/auctionlist'),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "category_name": auctionTitle,
        });
    print(res.body);

    if (res.statusCode == 200) {
      print(res.body);

      final responseJson = json.decode(res.body);
      print(responseJson);
      var data = AuctionServiceModel.fromJson(responseJson);
      print(data.message);
      return data.data;
    } else {
      print(res.statusCode);
    }
  }

  Future getAuctionServiceDetailData(String auctionId) async {
    var res = await http.post(
        Uri.parse(
            'http://koolmindapps.com/lasvegas/public/api/v1/auctiondetails'),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {    
          "auction_id": auctionId,
        });
    print(res.body);

    if (res.statusCode == 200) {
      print(res.body);

      final responseJson = json.decode(res.body);
      print(responseJson);
      var data = AuctionServiceDetailsModel.fromJson(responseJson);
      print(data.message);
      return data;
    } else {
      print(res.statusCode);
    }
  }
}
