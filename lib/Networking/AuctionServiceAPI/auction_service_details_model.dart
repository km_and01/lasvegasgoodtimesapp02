// To parse this JSON data, do
//
//     final auctionServiceDetailsModel = auctionServiceDetailsModelFromJson(jsonString);

import 'dart:convert';

AuctionServiceDetailsModel auctionServiceDetailsModelFromJson(String str) => AuctionServiceDetailsModel.fromJson(json.decode(str));

String auctionServiceDetailsModelToJson(AuctionServiceDetailsModel data) => json.encode(data.toJson());

class AuctionServiceDetailsModel {
    AuctionServiceDetailsModel({
        this.message,
        this.statusCode,
        this.data,
        this.demo,
    });

    String? message;
    int? statusCode;
    Data? data;
    List<Demo>? demo;

    factory AuctionServiceDetailsModel.fromJson(Map<String, dynamic> json) => AuctionServiceDetailsModel(
        message: json["message"],
        statusCode: json["status_code"],
        data: Data.fromJson(json["data"]),
        demo: List<Demo>.from(json["demo"].map((x) => Demo.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "status_code": statusCode,
        "data": data!.toJson(),
        "demo": List<dynamic>.from(demo!.map((x) => x.toJson())),
    };
}

class Data {
    Data({
        this.id,
        this.vendorId,
        this.name,
        this.categoryName,
        this.description,
        this.image,
        this.auctionIncludes,
        this.auctionExcludes,
        this.openingTime,
        this.openingTimeStatus,
        this.closingTime,
        this.closingTimeStatus,
        this.timezone,
        this.openingPrice,
        this.status,
        this.createdAt,
        this.updatedAt,
        this.bussinessDetails,
    });

    int? id;
    int? vendorId;
    String? name;
    String? categoryName;
    String? description;
    List<String>? image;
    List<String>? auctionIncludes;
    List<String>? auctionExcludes;
    String? openingTime;
    int? openingTimeStatus;
    String? closingTime;
    String? closingTimeStatus;
    String? timezone;
    String? openingPrice;
    int? status;
    DateTime? createdAt;
    DateTime? updatedAt;
    BussinessDetails? bussinessDetails;

    factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"],
        vendorId: json["vendor_id"],
        name: json["name"],
        categoryName: json["category_name"],
        description: json["description"],
        image: List<String>.from(json["image"].map((x) => x)),
        auctionIncludes: List<String>.from(json["auction_includes"].map((x) => x)),
        auctionExcludes: List<String>.from(json["auction_excludes"].map((x) => x)),
        openingTime: json["opening_time"],
        openingTimeStatus: json["opening_time_status"],
        closingTime: json["closing_time"],
        closingTimeStatus: json["closing_time_status"],
        timezone: json["timezone"],
        openingPrice: json["opening_price"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        bussinessDetails: BussinessDetails.fromJson(json["bussiness_details"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "vendor_id": vendorId,
        "name": name,
        "category_name": categoryName,
        "description": description,
        "image": List<dynamic>.from(image!.map((x) => x)),
        "auction_includes": List<dynamic>.from(auctionIncludes!.map((x) => x)),
        "auction_excludes": List<dynamic>.from(auctionExcludes!.map((x) => x)),
        "opening_time": openingTime,
        "opening_time_status": openingTimeStatus,
        "closing_time": closingTime,
        "closing_time_status": closingTimeStatus,
        "timezone": timezone,
        "opening_price": openingPrice,
        "status": status,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
        "bussiness_details": bussinessDetails!.toJson(),
    };
}

class BussinessDetails {
    BussinessDetails({
        this.id,
        this.userId,
        this.categoryId,
        this.bussinessType,
        this.bussinessName,
        this.addressLine1,
        this.addressLine2,
        this.landmark,
        this.pincode,
        this.city,
        this.state,
        this.country,
        this.shortDescription,
        this.latitude,
        this.longitude,
        this.location,
        this.bussinessLogo,
        this.imagesk,
        this.timezone,
        this.doc,
        this.openTime,
        this.closeTime,
        this.createdAt,
        this.updatedAt,
    });

    int? id;
    String? userId;
    List<String>? categoryId;
    dynamic bussinessType;
    String? bussinessName;
    String? addressLine1;
    String? addressLine2;
    dynamic landmark;
    String? pincode;
    String? city;
    String? state;
    String? country;
    String? shortDescription;
    dynamic latitude;
    dynamic longitude;
    dynamic location;
    String? bussinessLogo;
    List<String>? imagesk;
    String? timezone;
    List<String>? doc;
    List<String>? openTime;
    List<String>? closeTime;
    DateTime? createdAt;
    DateTime? updatedAt;

    factory BussinessDetails.fromJson(Map<String, dynamic> json) => BussinessDetails(
        id: json["id"],
        userId: json["user_id"],
        categoryId: List<String>.from(json["category_id"].map((x) => x)),
        bussinessType: json["bussiness_type"],
        bussinessName: json["bussiness_name"],
        addressLine1: json["address_line1"],
        addressLine2: json["address_line2"],
        landmark: json["landmark"],
        pincode: json["pincode"],
        city: json["city"],
        state: json["state"],
        country: json["country"],
        shortDescription: json["short_description"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        location: json["location"],
        bussinessLogo: json["bussiness_logo"],
        imagesk: List<String>.from(json["imagesk"].map((x) => x)),
        timezone: json["timezone"],
        doc: List<String>.from(json["doc"].map((x) => x)),
        openTime: List<String>.from(json["open_time"].map((x) => x)),
        closeTime: List<String>.from(json["close_time"].map((x) => x)),
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "category_id": List<dynamic>.from(categoryId!.map((x) => x)),
        "bussiness_type": bussinessType,
        "bussiness_name": bussinessName,
        "address_line1": addressLine1,
        "address_line2": addressLine2,
        "landmark": landmark,
        "pincode": pincode,
        "city": city,
        "state": state,
        "country": country,
        "short_description": shortDescription,
        "latitude": latitude,
        "longitude": longitude,
        "location": location,
        "bussiness_logo": bussinessLogo,
        "imagesk": List<dynamic>.from(imagesk!.map((x) => x)),
        "timezone": timezone,
        "doc": List<dynamic>.from(doc!.map((x) => x)),
        "open_time": List<dynamic>.from(openTime!.map((x) => x)),
        "close_time": List<dynamic>.from(closeTime!.map((x) => x)),
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
    };
}

class Demo {
    Demo({
        this.id,
        this.name,
        this.auctionId,
        this.auctionName,
        this.ammount,
        this.date,
        this.time,
        this.categoryId,
        this.categoryName,
        this.vendorId,
        this.userId,
        this.status,
        this.createdAt,
        this.updatedAt,
    });

    int? id;
    String? name;
    int? auctionId;
    String? auctionName;
    String? ammount;
    String? date;
    String? time;
    String? categoryId;
    String? categoryName;
    String? vendorId;
    String? userId;
    int? status;
    DateTime? createdAt;
    DateTime? updatedAt;

    factory Demo.fromJson(Map<String, dynamic> json) => Demo(
        id: json["id"],
        name: json["name"],
        auctionId: json["auction_id"],
        auctionName: json["auction_name"],
        ammount: json["ammount"],
        date: json["date"],
        time: json["time"],
        categoryId: json["category_id"],
        categoryName: json["category_name"],
        vendorId: json["vendor_id"],
        userId: json["user_id"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "auction_id": auctionId,
        "auction_name": auctionName,
        "ammount": ammount,
        "date": date,
        "time": time,
        "category_id": categoryId,
        "category_name": categoryName,
        "vendor_id": vendorId,
        "user_id": userId,
        "status": status,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
    };
}
