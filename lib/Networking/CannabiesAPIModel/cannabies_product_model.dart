// To parse this JSON data, do
//
//     final cannabiesProductModel = cannabiesProductModelFromJson(jsonString);

import 'dart:convert';

CannabiesProductModel cannabiesProductModelFromJson(String str) => CannabiesProductModel.fromJson(json.decode(str));

String cannabiesProductModelToJson(CannabiesProductModel data) => json.encode(data.toJson());

class CannabiesProductModel {
  CannabiesProductModel({
    this.message,
    this.statusCode,
    required this.data,
    this.review,
  });

  String? message;
  int? statusCode;
  List<CannabiesProducts> data;
  List<dynamic>? review;

  factory CannabiesProductModel.fromJson(Map<String, dynamic> json) => CannabiesProductModel(
    message: json["message"],
    statusCode: json["status_code"],
    data: List<CannabiesProducts>.from(json["data"].map((x) => CannabiesProducts.fromJson(x))),
    review: List<dynamic>.from(json["review"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "message": message,
    "status_code": statusCode,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
    "review": List<dynamic>.from(review!.map((x) => x)),
  };
}

class CannabiesProducts {
  CannabiesProducts({
    this.id,
    this.vendorId,
    this.serviceName,
    this.productName,
    this.productQty,
    this.productQtyUnit,
    this.productPrice,
    this.priceUnit,
    this.productDetails,
    this.photos,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.bussinessDetails,
  });

  int? id;
  String? vendorId;
  dynamic serviceName;
  String? productName;
  String? productQty;
  String? productQtyUnit;
  String? productPrice;
  String? priceUnit;
  String? productDetails;
  List<String>? photos;
  int? status;
  DateTime? createdAt;
  DateTime? updatedAt;
  BussinessDetails? bussinessDetails;

  factory CannabiesProducts.fromJson(Map<String, dynamic> json) => CannabiesProducts(
    id: json["id"],
    vendorId: json["vendor_id"],
    serviceName: json["service_name"],
    productName: json["product_name"],
    productQty: json["product_qty"],
    productQtyUnit: json["product_qty_unit"],
    productPrice: json["product_price"],
    priceUnit: json["price_unit"],
    productDetails: json["product_details"],
    photos: List<String>.from(json["photos"].map((x) => x)),
    status: json["status"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    bussinessDetails: BussinessDetails.fromJson(json["bussiness_details"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "vendor_id": vendorId,
    "service_name": serviceName,
    "product_name": productName,
    "product_qty": productQty,
    "product_qty_unit": productQtyUnit,
    "product_price": productPrice,
    "price_unit": priceUnit,
    "product_details": productDetails,
    "photos": List<dynamic>.from(photos!.map((x) => x)),
    "status": status,
    "created_at": createdAt!.toIso8601String(),
    "updated_at": updatedAt!.toIso8601String(),
    "bussiness_details": bussinessDetails!.toJson(),
  };
}

class BussinessDetails {
  BussinessDetails({
    this.id,
    this.userId,
    this.categoryId,
    this.bussinessType,
    this.bussinessName,
    this.addressLine1,
    this.addressLine2,
    this.landmark,
    this.pincode,
    this.city,
    this.state,
    this.country,
    this.shortDescription,
    this.latitude,
    this.longitude,
    this.location,
    this.bussinessLogo,
    this.imagesk,
    this.timezone,
    this.doc,
    this.openTime,
    this.closeTime,
    this.createdAt,
    this.updatedAt,
  });

  int? id;
  String? userId;
  List<String>? categoryId;
  dynamic bussinessType;
  String? bussinessName;
  String? addressLine1;
  String? addressLine2;
  dynamic landmark;
  String? pincode;
  String? city;
  String? state;
  String? country;
  String? shortDescription;
  dynamic latitude;
  dynamic longitude;
  dynamic location;
  String? bussinessLogo;
  List<String>? imagesk;
  String? timezone;
  List<String>? doc;
  List<String>? openTime;
  List<String>? closeTime;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory BussinessDetails.fromJson(Map<String, dynamic> json) => BussinessDetails(
    id: json["id"],
    userId: json["user_id"],
    categoryId: List<String>.from(json["category_id"].map((x) => x)),
    bussinessType: json["bussiness_type"],
    bussinessName: json["bussiness_name"],
    addressLine1: json["address_line1"],
    addressLine2: json["address_line2"],
    landmark: json["landmark"],
    pincode: json["pincode"],
    city: json["city"],
    state: json["state"],
    country: json["country"],
    shortDescription: json["short_description"],
    latitude: json["latitude"],
    longitude: json["longitude"],
    location: json["location"],
    bussinessLogo: json["bussiness_logo"],
    imagesk: List<String>.from(json["imagesk"].map((x) => x)),
    timezone: json["timezone"],
    doc: List<String>.from(json["doc"].map((x) => x)),
    openTime: List<String>.from(json["open_time"].map((x) => x == null ? 'null' : x)),
    closeTime: List<String>.from(json["close_time"].map((x) => x == null ? 'null' : x)),
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "user_id": userId,
    "category_id": List<dynamic>.from(categoryId!.map((x) => x)),
    "bussiness_type": bussinessType,
    "bussiness_name": bussinessName,
    "address_line1": addressLine1,
    "address_line2": addressLine2,
    "landmark": landmark,
    "pincode": pincode,
    "city": city,
    "state": state,
    "country": country,
    "short_description": shortDescription,
    "latitude": latitude,
    "longitude": longitude,
    "location": location,
    "bussiness_logo": bussinessLogo,
    "imagesk": List<dynamic>.from(imagesk!.map((x) => x)),
    "timezone": timezone,
    "doc": List<dynamic>.from(doc!.map((x) => x)),
    "open_time": List<dynamic>.from(openTime!.map((x) => x == null ? 'null' : x)),
    "close_time": List<dynamic>.from(closeTime!.map((x) => x == null ? 'null' : x)),
    "created_at": createdAt!.toIso8601String(),
    "updated_at": updatedAt!.toIso8601String(),
  };
}
