// ignore_for_file: avoid_print

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:lasvegas_gts_app/Networking/eventSpaceAPI/event_space_package_list_model.dart';
import 'package:lasvegas_gts_app/Networking/eventSpaceAPI/event_space_vendor_list_model.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';

class EventSpaceAPIs {
  Future getEventSpsVendorData(
      String event_space_date, event_space_service_id, event_space_city) async {
    var res = await http.post(
        Uri.parse(
            "http://koolmindapps.com/lasvegas/public/api/v1/eventspacevendor"),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "date": event_space_date,
          "services": event_space_service_id,
          "city2": event_space_city,
        });

    print('SPACE $event_space_date');
    print('SPACE $event_space_service_id');
    print('SPACE $event_space_city');
    print(res.body);

    if (res.statusCode == 200) {
      final responseJson = json.decode(res.body);
      print(responseJson);
      var data = EventSpaceVendorListModel.fromJson(responseJson);
      print("Event Space Vendor DATA in APIs =====" +
          data.services[0].bussinessDetails!.shortDescription.toString());
      return data.services;
    }
  }

  Future getEventSpsPackageData(
      String event_space_vendorID, event_space_service_id) async {
    var res = await http.post(
        Uri.parse(
            "http://koolmindapps.com/lasvegas/public/api/v1/eventspaceservice"),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "event_space_service_id": event_space_service_id,
          "vendor_id": event_space_vendorID,
        });
    print(res.body);
    if (res.statusCode == 200) {
      final responseJson = json.decode(res.body);
      print(responseJson);
      var data = EventSpacePackageListModel.fromJson(responseJson);
      print("Event Space Vendor DATA in APIs =====" +
          data.data[0].bussinessDetails!.shortDescription.toString());
      return data.data;
    }
  }

  Future<Map<String, dynamic>> bookEventSps(
      String vendor_id,
      image_path,
      event_space_name,
      amount,
      no_of_people_allowed,
      date,
      package_id,
      user_name,
      phone_number,
      email,
      no_guest,
      time) async {

    Map<String, dynamic>? map;

    var details = await http.post(
        Uri.parse(
            "http://koolmindapps.com/lasvegas/public/api/v1/bookeventspace"),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "vendor_id": vendor_id,
          "image_path": image_path,
          "event_space_name": event_space_name,
          "amount": amount,
          "no_of_people_allowed": no_of_people_allowed,
          "date": date,
          "package_id": package_id,
          "user_name": user_name,
          "phone_number": phone_number,
          "email": email,
          "no_guest": no_guest,
          "time": time,
        });
    if (details.statusCode == 200) {
      map = json.decode(details.body);


      return map!;
      // roomdata.services[index].roomName;
    } else {
      return map!;
    }
  }
}
