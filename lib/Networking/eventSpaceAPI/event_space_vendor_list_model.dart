// To parse this JSON data, do
//
//     final eventSpaceVendorListModel = eventSpaceVendorListModelFromJson(jsonString);

import 'dart:convert';

EventSpaceVendorListModel eventSpaceVendorListModelFromJson(String str) =>
    EventSpaceVendorListModel.fromJson(json.decode(str));

String eventSpaceVendorListModelToJson(EventSpaceVendorListModel data) =>
    json.encode(data.toJson());

class EventSpaceVendorListModel {
  EventSpaceVendorListModel({
    required this.message,
    required this.statusCode,
    required this.services,
  });

  String message;
  int statusCode;
  List<ServiceEventSpace> services;

  factory EventSpaceVendorListModel.fromJson(Map<String, dynamic> json) =>
      EventSpaceVendorListModel(
        message: json["message"],
        statusCode: json["status_code"],
        services: List<ServiceEventSpace>.from(
            json["services"].map((x) => ServiceEventSpace.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "status_code": statusCode,
        "services": List<dynamic>.from(services.map((x) => x.toJson())),
      };
}

class ServiceEventSpace {
  ServiceEventSpace({
    this.id,
    this.eventSpaceServiceId,
    this.name,
    this.venue,
    this.eventToDoPossbly,
    this.amenities,
    this.photos,
    this.noOfPeopleAllowed,
    this.price,
    this.priceUnit,
    this.shortDescription,
    this.disPrice,
    this.status,
    this.logo,
    this.createdAt,
    this.updatedAt,
    this.vendorId,
    this.categoryId,
    this.minPrice,
    this.bussinessDetails,
  });

  int? id;
  String? eventSpaceServiceId;
  String? name;
  String? venue;
  String? eventToDoPossbly;
  List<String>? amenities;
  List<String>? photos;
  String? noOfPeopleAllowed;
  String? price;
  String? priceUnit;
  String? shortDescription;
  String? disPrice;
  int? status;
  String? logo;
  DateTime? createdAt;
  DateTime? updatedAt;
  String? vendorId;
  String? categoryId;
  String? minPrice;
  BussinessDetails? bussinessDetails;

  factory ServiceEventSpace.fromJson(Map<String, dynamic> json) =>
      ServiceEventSpace(
        id: json["id"],
        eventSpaceServiceId: json["event_space_service_id"],
        name: json["name"],
        venue: json["venue"],
        eventToDoPossbly: json["event_to_do_possbly"],
        amenities: List<String>.from(json["amenities"].map((x) => x)),
        photos: List<String>.from(json["photos"].map((x) => x)),
        noOfPeopleAllowed: json["no_of_people_allowed"],
        price: json["price"],
        priceUnit: json["price_unit"],
        shortDescription: json["short_description"],
        disPrice: json["dis_price"],
        status: json["status"],
        logo: json["logo"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        vendorId: json["vendor_id"],
        categoryId: json["category_id"],
        minPrice: json["min_price"] == null ? json["price"] : json["min_price"],
        bussinessDetails: BussinessDetails.fromJson(json["bussiness_details"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "event_space_service_id": eventSpaceServiceId,
        "name": name,
        "venue": venue,
        "event_to_do_possbly": eventToDoPossbly,
        "amenities": List<dynamic>.from(amenities!.map((x) => x)),
        "photos": List<dynamic>.from(photos!.map((x) => x)),
        "no_of_people_allowed": noOfPeopleAllowed,
        "price": price,
        "price_unit": priceUnit,
        "short_description": shortDescription,
        "dis_price": disPrice,
        "status": status,
        "logo": logo,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
        "vendor_id": vendorId,
        "category_id": categoryId,
        "min_price": minPrice,
        "bussiness_details": bussinessDetails!.toJson(),
      };
}

class BussinessDetails {
  BussinessDetails({
    this.id,
    this.userId,
    this.categoryId,
    this.bussinessType,
    this.bussinessName,
    this.addressLine1,
    this.addressLine2,
    this.landmark,
    this.pincode,
    this.city,
    this.state,
    this.country,
    this.shortDescription,
    this.latitude,
    this.longitude,
    this.location,
    this.bussinessLogo,
    this.imagesk,
    this.timezone,
    this.doc,
    this.openTime,
    this.closeTime,
    this.createdAt,
    this.updatedAt,
  });

  int? id;
  String? userId;
  List<String>? categoryId;
  dynamic bussinessType;
  String? bussinessName;
  String? addressLine1;
  String? addressLine2;
  dynamic landmark;
  String? pincode;
  String? city;
  String? state;
  String? country;
  String? shortDescription;
  dynamic latitude;
  dynamic longitude;
  dynamic location;
  String? bussinessLogo;
  List<String>? imagesk;
  String? timezone;
  List<String>? doc;
  List<String>? openTime;
  List<String>? closeTime;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory BussinessDetails.fromJson(Map<String, dynamic> json) =>
      BussinessDetails(
        id: json["id"],
        userId: json["user_id"],
        categoryId: List<String>.from(json["category_id"].map((x) => x)),
        bussinessType: json["bussiness_type"],
        bussinessName: json["bussiness_name"],
        addressLine1: json["address_line1"],
        addressLine2: json["address_line2"],
        landmark: json["landmark"],
        pincode: json["pincode"],
        city: json["city"],
        state: json["state"],
        country: json["country"],
        shortDescription: json["short_description"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        location: json["location"],
        bussinessLogo: json["bussiness_logo"],
        imagesk: List<String>.from(json["imagesk"].map((x) => x)),
        timezone: json["timezone"],
        doc: List<String>.from(json["doc"].map((x) => x)),
        openTime: List<String>.from(
            json["open_time"].map((x) => x == null ? 'null' : x)),
        closeTime: List<String>.from(
            json["close_time"].map((x) => x == null ? 'null' : x)),
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "category_id": List<dynamic>.from(categoryId!.map((x) => x)),
        "bussiness_type": bussinessType,
        "bussiness_name": bussinessName,
        "address_line1": addressLine1,
        "address_line2": addressLine2,
        "landmark": landmark,
        "pincode": pincode,
        "city": city,
        "state": state,
        "country": country,
        "short_description": shortDescription,
        "latitude": latitude,
        "longitude": longitude,
        "location": location,
        "bussiness_logo": bussinessLogo,
        "imagesk": List<dynamic>.from(imagesk!.map((x) => x)),
        "timezone": timezone,
        "doc": List<dynamic>.from(doc!.map((x) => x)),
        "open_time":
            List<dynamic>.from(openTime!.map((x) => x == null ? 'null' : x)),
        "close_time":
            List<dynamic>.from(closeTime!.map((x) => x == null ? 'null' : x)),
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
      };
}
