// To parse this JSON data, do
//
//     final transportPKgModel = transportPKgModelFromJson(jsonString);

import 'dart:convert';

TransportPKgModel transportPKgModelFromJson(String str) => TransportPKgModel.fromJson(json.decode(str));

String transportPKgModelToJson(TransportPKgModel data) => json.encode(data.toJson());

class TransportPKgModel {
  TransportPKgModel({
    this.message,
    this.statusCode,
    this.data,
    this.review,
  });

  String? message;
  int? statusCode;
  List<TransportPkg>? data;
  List<Review>? review;

  factory TransportPKgModel.fromJson(Map<String, dynamic> json) => TransportPKgModel(
    message: json["message"],
    statusCode: json["status_code"],
    data: List<TransportPkg>.from(json["data"].map((x) => TransportPkg.fromJson(x))),
    review: List<Review>.from(json["review"].map((x) => Review.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "message": message,
    "status_code": statusCode,
    "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    "review": List<dynamic>.from(review!.map((x) => x.toJson())),
  };
}

class TransportPkg {
  TransportPkg({
    this.id,
    this.vendorId,
    this.subServiceId,
    this.price,
    this.priceUnit,
    this.priceOn,
    this.country,
    this.state,
    this.city,
    this.countryName,
    this.stateName,
    this.cityName,
    this.photos,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.bussinessDetails,
  });

  int? id;
  String? vendorId;
  String? subServiceId;
  String? price;
  String? priceUnit;
  String? priceOn;
  List<String>? country;
  List<String>? state;
  List<String>? city;
  List<String>? countryName;
  List<String>? stateName;
  List<String>? cityName;
  List<String>? photos;
  int? status;
  String? createdAt;
  String? updatedAt;
  BussinessDetails? bussinessDetails;

  factory TransportPkg.fromJson(Map<String, dynamic> json) => TransportPkg(
    id: json["id"],
    vendorId: json["vendor_id"],
    subServiceId: json["sub_service_id"],
    price: json["price"],
    priceUnit: json["price_unit"],
    priceOn: json["price_on"],
    country: List<String>.from(json["country"].map((x) => x)),
    state: List<String>.from(json["state"].map((x) => x)),
    city: List<String>.from(json["city"].map((x) => x)),
    countryName: List<String>.from(json["country_name"].map((x) => x)),
    stateName: List<String>.from(json["state_name"].map((x) => x)),
    cityName: List<String>.from(json["city_name"].map((x) => x)),
    photos: List<String>.from(json["photos"].map((x) => x)),
    status: json["status"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
    bussinessDetails: BussinessDetails.fromJson(json["bussiness_details"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "vendor_id": vendorId,
    "sub_service_id": subServiceId,
    "price": price,
    "price_unit": priceUnit,
    "price_on": priceOn,
    "country": List<dynamic>.from(country!.map((x) => x)),
    "state": List<dynamic>.from(state!.map((x) => x)),
    "city": List<dynamic>.from(city!.map((x) => x)),
    "country_name": List<dynamic>.from(countryName!.map((x) => x)),
    "state_name": List<dynamic>.from(stateName!.map((x) => x)),
    "city_name": List<dynamic>.from(cityName!.map((x) => x)),
    "photos": List<dynamic>.from(photos!.map((x) => x)),
    "status": status,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "bussiness_details": bussinessDetails!.toJson(),
  };
}

class BussinessDetails {
  BussinessDetails({
    this.id,
    this.userId,
    this.categoryId,
    this.bussinessType,
    this.bussinessName,
    this.addressLine1,
    this.addressLine2,
    this.landmark,
    this.pincode,
    this.city,
    this.state,
    this.country,
    this.shortDescription,
    this.latitude,
    this.longitude,
    this.location,
    this.bussinessLogo,
    this.imagesk,
    this.timezone,
    this.doc,
    this.openTime,
    this.closeTime,
    this.createdAt,
    this.updatedAt,
  });

  int? id;
  String? userId;
  List<String>? categoryId;
  dynamic bussinessType;
  String? bussinessName;
  String? addressLine1;
  String? addressLine2;
  dynamic landmark;
  String? pincode;
  String? city;
  String? state;
  String? country;
  String? shortDescription;
  dynamic latitude;
  dynamic longitude;
  dynamic location;
  String? bussinessLogo;
  List<String>? imagesk;
  String? timezone;
  List<String>? doc;
  List<String>? openTime;
  List<String>? closeTime;
  String? createdAt;
  String? updatedAt;

  factory BussinessDetails.fromJson(Map<String, dynamic> json) => BussinessDetails(
    id: json["id"],
    userId: json["user_id"],
    categoryId: List<String>.from(json["category_id"].map((x) => x)),
    bussinessType: json["bussiness_type"],
    bussinessName: json["bussiness_name"],
    addressLine1: json["address_line1"],
    addressLine2: json["address_line2"],
    landmark: json["landmark"],
    pincode: json["pincode"],
    city: json["city"],
    state: json["state"],
    country: json["country"],
    shortDescription: json["short_description"],
    latitude: json["latitude"],
    longitude: json["longitude"],
    location: json["location"],
    bussinessLogo: json["bussiness_logo"],
    imagesk: List<String>.from(json["imagesk"].map((x) => x)),
    timezone: json["timezone"],
    doc: List<String>.from(json["doc"].map((x) => x)),
    openTime: List<String>.from(json["open_time"].map((x) => x)),
    closeTime: List<String>.from(json["close_time"].map((x) => x)),
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "user_id": userId,
    "category_id": List<dynamic>.from(categoryId!.map((x) => x)),
    "bussiness_type": bussinessType,
    "bussiness_name": bussinessName,
    "address_line1": addressLine1,
    "address_line2": addressLine2,
    "landmark": landmark,
    "pincode": pincode,
    "city": city,
    "state": state,
    "country": country,
    "short_description": shortDescription,
    "latitude": latitude,
    "longitude": longitude,
    "location": location,
    "bussiness_logo": bussinessLogo,
    "imagesk": List<dynamic>.from(imagesk!.map((x) => x)),
    "timezone": timezone,
    "doc": List<dynamic>.from(doc!.map((x) => x)),
    "open_time": List<dynamic>.from(openTime!.map((x) => x == null ? "" : x)),
    "close_time": List<dynamic>.from(closeTime!.map((x) => x == null ? "" : x)),
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}

class Review {
  Review({
    this.id,
    this.bookingId,
    this.bookingNumber,
    this.userId,
    this.vendorId,
    this.categoryId,
    this.ratings,
    this.stayReview,
    this.title,
    this.description,
    this.createdAt,
    this.updatedAt,
    this.users,
  });

  int? id;
  String? bookingId;
  String? bookingNumber;
  String? userId;
  String? vendorId;
  String? categoryId;
  String? ratings;
  dynamic stayReview;
  String? title;
  String? description;
  String? createdAt;
  String? updatedAt;
  Users? users;

  factory Review.fromJson(Map<String, dynamic> json) => Review(
    id: json["id"],
    bookingId: json["booking_id"],
    bookingNumber: json["booking_number"],
    userId: json["user_id"],
    vendorId: json["vendor_id"],
    categoryId: json["category_id"],
    ratings: json["ratings"],
    stayReview: json["stay_review"],
    title: json["title"],
    description: json["description"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
    users: Users.fromJson(json["users"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "booking_id": bookingId,
    "booking_number": bookingNumber,
    "user_id": userId,
    "vendor_id": vendorId,
    "category_id": categoryId,
    "ratings": ratings,
    "stay_review": stayReview,
    "title": title,
    "description": description,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "users": users!.toJson(),
  };
}

class Users {
  Users({
    this.id,
    this.name,
    this.email,
    this.mobileNumber,
    this.referralCode,
    this.userReferralCode,
    this.userType,
    this.gender,
    this.isActive,
    this.logo,
    this.emailVerifiedAt,
    this.deviceOs,
    this.appVersion,
    this.deviceName,
    this.fcmToken,
    this.googleId,
    this.otp,
    this.facebookId,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
    this.servie,
  });

  int? id;
  String? name;
  String? email;
  String? mobileNumber;
  dynamic referralCode;
  dynamic userReferralCode;
  String? userType;
  dynamic gender;
  int? isActive;
  String? logo;
  dynamic emailVerifiedAt;
  dynamic deviceOs;
  dynamic appVersion;
  dynamic deviceName;
  dynamic fcmToken;
  dynamic googleId;
  dynamic otp;
  dynamic facebookId;
  String? createdAt;
  String? updatedAt;
  dynamic deletedAt;
  int? servie;

  factory Users.fromJson(Map<String, dynamic> json) => Users(
    id: json["id"],
    name: json["name"],
    email: json["email"],
    mobileNumber: json["mobile_number"],
    referralCode: json["referral_code"],
    userReferralCode: json["user_referral_code"],
    userType: json["user_type"],
    gender: json["gender"],
    isActive: json["is_active"],
    logo: json["logo"],
    emailVerifiedAt: json["email_verified_at"],
    deviceOs: json["DeviceOS"],
    appVersion: json["AppVersion"],
    deviceName: json["DeviceName"],
    fcmToken: json["FCMToken"],
    googleId: json["google_id"],
    otp: json["otp"],
    facebookId: json["facebook_id"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
    deletedAt: json["deleted_at"],
    servie: json["servie"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "email": email,
    "mobile_number": mobileNumber,
    "referral_code": referralCode,
    "user_referral_code": userReferralCode,
    "user_type": userType,
    "gender": gender,
    "is_active": isActive,
    "logo": logo,
    "email_verified_at": emailVerifiedAt,
    "DeviceOS": deviceOs,
    "AppVersion": appVersion,
    "DeviceName": deviceName,
    "FCMToken": fcmToken,
    "google_id": googleId,
    "otp": otp,
    "facebook_id": facebookId,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "deleted_at": deletedAt,
    "servie": servie,
  };
}
