// ignore_for_file: avoid_print

import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:lasvegas_gts_app/Networking/TransportAPI/transport_pkg_model.dart';
import 'package:lasvegas_gts_app/Networking/TransportAPI/transport_vendor_model.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';

class TransportAPIs {
  Future getTransportVendorData(String pickupLocation, String dropLocation,
      String pickupCity, String dropCity, String selectDate) async {
    Map<String, dynamic> requestBodyData = {
      "pickup": pickupLocation,
      "drop": dropLocation,
      "pickupcity": pickupCity,
      "dropcity": dropCity,
      "date": selectDate,
    };

    print("RequestData==============>  " + requestBodyData.toString());
    print("Token==============>  " + homeToken);

    /*var res = await http.post(
      Uri.parse(
          'http://koolmindapps.com/lasvegas/public/api/v1/transportationvendor'),
      headers: {
        'Authorization': "Bearer " + homeToken,
        'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        'Content-Type': 'multipart/form-data',
      },
      body: jsonEncode(requestBodyData),
    );
    print('object12345============> ${res.request}');
    print('Response==============> ' + res.statusCode.toString());

    if (res.statusCode == 200) {
      print('Response-1==============> ' + res.body);

      final responseJson = json.decode(res.body);
      print('Datares==================> ' + responseJson);
      TransportVendorModel dataRes =
          TransportVendorModel.fromJson(responseJson);
      print('BusinessData==============> ' +
          dataRes.data!.bussinessDetails.toString());
      return dataRes;
    } else {
      print(res.body);
    }*/

    var request = http.MultipartRequest(
        "POST",
        Uri.parse(
            "http://koolmindapps.com/lasvegas/public/api/v1/transportationvendor"));
    request.headers.addAll({
      'Authorization': "Bearer " + homeToken,
      'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
    });

    request.fields['pickup'] = pickupLocation;
    request.fields['drop'] = dropLocation;
    request.fields['pickupcity'] = pickupCity;
    request.fields['dropcity'] = dropCity;
    request.fields['date'] = selectDate;

    var response = await request.send();

    var responsed = await http.Response.fromStream(response);
    final responseData = json.decode(responsed.body);

    if (response.statusCode == 200) {
      print(responseData);
      var data = TransportVendorModel.fromJson(responseData);

      print('DATA_TRANS ${data.toJson()}');

      return data.data;
    } else {
      print("Error======>" + responsed.body);
    }
  }

  Future getTransportPkgData(String transportVendorId) async {
    var res = await http.post(
        Uri.parse(
            'http://koolmindapps.com/lasvegas/public/api/v1/transportationservice'),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "vendor_id": transportVendorId,
        });
    print(res.body);

    if (res.statusCode == 200) {
      print(res.body);

      final responseJson = json.decode(res.body);
      print(responseJson);
      var data = TransportPKgModel.fromJson(responseJson);
      print(data.message);
      return data;
    } else {
      print(res.statusCode);
    }
  }

}
