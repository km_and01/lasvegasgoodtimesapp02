// To parse this JSON data, do
//
//     final cateringChefListModel = cateringChefListModelFromJson(jsonString);

import 'dart:convert';

CateringChefListModel cateringChefListModelFromJson(String str) =>
    CateringChefListModel.fromJson(json.decode(str));

String cateringChefListModelToJson(CateringChefListModel data) =>
    json.encode(data.toJson());

class CateringChefListModel {
  CateringChefListModel({
    required this.message,
    required this.statusCode,
    required this.services,
  });

  String message;
  int statusCode;
  List<ServiceChef> services;

  factory CateringChefListModel.fromJson(Map<String, dynamic> json) =>
      CateringChefListModel(
        message: json["message"],
        statusCode: json["status_code"],
        services: List<ServiceChef>.from(
            json["services"].map((x) => ServiceChef.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "status_code": statusCode,
        "services": List<dynamic>.from(services.map((x) => x.toJson())),
      };
}

class ServiceChef {
  ServiceChef({
    this.id,
    this.vendorId,
    this.eatChefType,
    this.minPeople,
    this.maxPeople,
    this.regions,
    this.typesOfFood,
    this.staff,
    this.disPrice,
    this.country,
    this.state,
    this.countryName,
    this.stateName,
    this.cityName,
    this.foodPrice,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.minPrice,
    this.bussinessDetails,
  });

  int? id;
  String? vendorId;
  String? eatChefType;
  String? minPeople;
  String? maxPeople;
  List<String>? regions;
  List<String>? typesOfFood;
  String? staff;
  int? disPrice;
  List<String>? country;
  List<String>? state;
  List<String>? countryName;
  List<String>? stateName;
  List<String>? cityName;
  List<String>? foodPrice;
  int? status;
  DateTime? createdAt;
  DateTime? updatedAt;
  String? minPrice;
  BussinessDetails? bussinessDetails;

  factory ServiceChef.fromJson(Map<String, dynamic> json) => ServiceChef(
        id: json["id"],
        vendorId: json["vendor_id"],
        eatChefType: json["eat_chef_type"],
        minPeople: json["min_people"],
        maxPeople: json["max_people"],
        regions: List<String>.from(json["regions"].map((x) => x)),
        typesOfFood: List<String>.from(json["types_of_food"].map((x) => x)),
        staff: json["staff"],
        disPrice: json["dis_price"] == null ? 0 : json["dis_price"],
        country: List<String>.from(json["country"].map((x) => x)),
        state: List<String>.from(json["state"].map((x) => x)),
        countryName: List<String>.from(json["country_name"].map((x) => x)),
        stateName: List<String>.from(json["state_name"].map((x) => x)),
        cityName: List<String>.from(json["city_name"].map((x) => x)),
        foodPrice: List<String>.from(json["food_price"].map((x) => x)),
        status: json["status"],
        createdAt: json["created_at"] == null
            ? DateTime.now()
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? DateTime.now()
            : DateTime.parse(json["updated_at"]),
        minPrice: json["min_price"],
        bussinessDetails: BussinessDetails.fromJson(json["bussiness_details"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "vendor_id": vendorId,
        "eat_chef_type": eatChefType,
        "min_people": minPeople,
        "max_people": maxPeople,
        "regions": List<dynamic>.from(regions!.map((x) => x)),
        "types_of_food": List<dynamic>.from(typesOfFood!.map((x) => x)),
        "staff": staff,
        "dis_price": disPrice,
        "country": List<dynamic>.from(country!.map((x) => x)),
        "state": List<dynamic>.from(state!.map((x) => x)),
        "country_name": List<dynamic>.from(countryName!.map((x) => x)),
        "state_name": List<dynamic>.from(stateName!.map((x) => x)),
        "city_name": List<dynamic>.from(cityName!.map((x) => x)),
        "food_price": List<dynamic>.from(foodPrice!.map((x) => x)),
        "status": status,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
        "min_price": minPrice,
        "bussiness_details": bussinessDetails!.toJson(),
      };
}

class BussinessDetails {
  BussinessDetails({
    this.id,
    this.userId,
    this.categoryId,
    this.bussinessType,
    this.bussinessName,
    this.addressLine1,
    this.addressLine2,
    this.landmark,
    this.pincode,
    this.city,
    this.state,
    this.country,
    this.shortDescription,
    this.latitude,
    this.longitude,
    this.location,
    this.bussinessLogo,
    this.imagesk,
    this.timezone,
    this.doc,
    this.openTime,
    this.closeTime,
    this.createdAt,
    this.updatedAt,
  });

  int? id;
  String? userId;
  List<String>? categoryId;
  dynamic bussinessType;
  String? bussinessName;
  String? addressLine1;
  String? addressLine2;
  dynamic landmark;
  String? pincode;
  String? city;
  String? state;
  String? country;
  String? shortDescription;
  dynamic latitude;
  dynamic longitude;
  dynamic location;
  String? bussinessLogo;
  List<String>? imagesk;
  String? timezone;
  List<String>? doc;
  List<String>? openTime;
  List<String>? closeTime;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory BussinessDetails.fromJson(Map<String, dynamic> json) =>
      BussinessDetails(
        id: json["id"],
        userId: json["user_id"],
        categoryId: List<String>.from(json["category_id"].map((x) => x)),
        bussinessType: json["bussiness_type"],
        bussinessName: json["bussiness_name"],
        addressLine1: json["address_line1"],
        addressLine2: json["address_line2"],
        landmark: json["landmark"],
        pincode: json["pincode"],
        city: json["city"],
        state: json["state"],
        country: json["country"],
        shortDescription: json["short_description"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        location: json["location"],
        bussinessLogo: json["bussiness_logo"],
        imagesk: List<String>.from(json["imagesk"].map((x) => x)),
        timezone: json["timezone"],
        doc: List<String>.from(json["doc"].map((x) => x)),
        openTime: List<String>.from(
            json["open_time"].map((x) => x == null ? 'null' : x)),
        closeTime: List<String>.from(
            json["close_time"].map((x) => x == null ? 'null' : x)),
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "category_id": List<dynamic>.from(categoryId!.map((x) => x)),
        "bussiness_type": bussinessType,
        "bussiness_name": bussinessName,
        "address_line1": addressLine1,
        "address_line2": addressLine2,
        "landmark": landmark,
        "pincode": pincode,
        "city": city,
        "state": state,
        "country": country,
        "short_description": shortDescription,
        "latitude": latitude,
        "longitude": longitude,
        "location": location,
        "bussiness_logo": bussinessLogo,
        "imagesk": List<dynamic>.from(imagesk!.map((x) => x)),
        "timezone": timezone,
        "doc": List<dynamic>.from(doc!.map((x) => x)),
        "open_time":
            List<dynamic>.from(openTime!.map((x) => x == null ? 'null' : x)),
        "close_time":
            List<dynamic>.from(closeTime!.map((x) => x == null ? 'null' : x)),
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
      };
}
