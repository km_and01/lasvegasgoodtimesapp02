// To parse this JSON data, do
//
//     final restaurentListModel = restaurentListModelFromJson(jsonString);

import 'dart:convert';

RestaurentListModel restaurentListModelFromJson(String str) =>
    RestaurentListModel.fromJson(json.decode(str));

String restaurentListModelToJson(RestaurentListModel data) =>
    json.encode(data.toJson());

class RestaurentListModel {
  RestaurentListModel({
    this.message,
    this.statusCode,
    required this.servicesRes,
  });

  String? message;
  int? statusCode;
  List<ServiceRes> servicesRes;

  factory RestaurentListModel.fromJson(Map<String, dynamic> json) =>
      RestaurentListModel(
        message: json["message"],
        statusCode: json["status_code"],
        servicesRes: List<ServiceRes>.from(
            json["services"].map((x) => ServiceRes.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "status_code": statusCode,
        "services": List<dynamic>.from(servicesRes.map((x) => x.toJson())),
      };
}

class ServiceRes {
  ServiceRes({
    this.id,
    this.vendorId,
    this.capacity,
    this.serviceName,
    this.noOfTable,
    this.noOfPeopleAllowed,
    this.tableImages,
    this.maxPepople,
    this.status,
    this.quantity,
    this.charges,
    this.disPrice,
    this.typesOfFood,
    this.amenities,
    this.createdAt,
    this.updatedAt,
    this.minPrice,
    this.bussinessDetails,
  });

  int? id;
  String? vendorId;
  List<String>? capacity;
  String? serviceName;
  String? noOfTable;
  int? noOfPeopleAllowed;
  List<String>? tableImages;
  dynamic maxPepople;
  int? status;
  List<String>? quantity;
  List<String>? charges;
  dynamic disPrice;
  List<String>? typesOfFood;
  List<String>? amenities;
  DateTime? createdAt;
  DateTime? updatedAt;
  String? minPrice;
  BussinessDetails? bussinessDetails;

  factory ServiceRes.fromJson(Map<String, dynamic> json) => ServiceRes(
        id: json["id"],
        vendorId: json["vendor_id"],
        capacity: List<String>.from(json["capacity"].map((x) => x)),
        serviceName: json["service_name"],
        noOfTable: json["no_of_table"],
        noOfPeopleAllowed: json["no_of_people_allowed"],
        tableImages: List<String>.from(json["table_images"].map((x) => x)),
        maxPepople: json["max_pepople"],
        status: json["status"],
        quantity: List<String>.from(json["quantity"].map((x) => x)),
        charges: List<String>.from(json["charges"].map((x) => x)),
        disPrice: json["dis_price"],
        typesOfFood: List<String>.from(json["types_of_food"].map((x) => x)),
        amenities: List<String>.from(json["amenities"].map((x) => x)),
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        minPrice: json["min_price"],
        bussinessDetails: BussinessDetails.fromJson(json["bussiness_details"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "vendor_id": vendorId,
        "capacity": List<dynamic>.from(capacity!.map((x) => x)),
        "service_name": serviceName,
        "no_of_table": noOfTable,
        "no_of_people_allowed": noOfPeopleAllowed,
        "table_images": List<dynamic>.from(tableImages!.map((x) => x)),
        "max_pepople": maxPepople,
        "status": status,
        "quantity": List<dynamic>.from(quantity!.map((x) => x)),
        "charges": List<dynamic>.from(charges!.map((x) => x)),
        "dis_price": disPrice,
        "types_of_food": List<dynamic>.from(typesOfFood!.map((x) => x)),
        "amenities": List<dynamic>.from(amenities!.map((x) => x)),
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
        "min_price": minPrice,
        "bussiness_details": bussinessDetails!.toJson(),
      };
}

class BussinessDetails {
  BussinessDetails({
    this.id,
    this.userId,
    this.categoryId,
    this.bussinessType,
    this.bussinessName,
    this.addressLine1,
    this.addressLine2,
    this.landmark,
    this.pincode,
    this.city,
    this.state,
    this.country,
    this.shortDescription,
    this.latitude,
    this.longitude,
    this.location,
    this.bussinessLogo,
    this.imagesk,
    this.timezone,
    this.doc,
    this.openTime,
    this.closeTime,
    this.createdAt,
    this.updatedAt,
  });

  int? id;
  String? userId;
  List<String>? categoryId;
  dynamic bussinessType;
  String? bussinessName;
  String? addressLine1;
  String? addressLine2;
  dynamic landmark;
  String? pincode;
  String? city;
  String? state;
  String? country;
  String? shortDescription;
  dynamic latitude;
  dynamic longitude;
  dynamic location;
  String? bussinessLogo;
  List<String>? imagesk;
  String? timezone;
  List<String>? doc;
  List<dynamic>? openTime;
  List<String>? closeTime;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory BussinessDetails.fromJson(Map<String, dynamic> json) =>
      BussinessDetails(
        id: json["id"],
        userId: json["user_id"],
        categoryId: List<String>.from(json["category_id"].map((x) => x)),
        bussinessType: json["bussiness_type"],
        bussinessName: json["bussiness_name"],
        addressLine1: json["address_line1"],
        addressLine2: json["address_line2"],
        landmark: json["landmark"],
        pincode: json["pincode"],
        city: json["city"],
        state: json["state"],
        country: json["country"],
        shortDescription: json["short_description"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        location: json["location"],
        bussinessLogo: json["bussiness_logo"],
        imagesk: List<String>.from(json["imagesk"].map((x) => x)),
        timezone: json["timezone"],
        doc: List<String>.from(json["doc"].map((x) => x)),
        openTime: List<dynamic>.from(
            json["open_time"].map((x) => x == null ? 'null' : x)),
        closeTime: List<String>.from(
            json["close_time"].map((x) => x == null ? 'null' : x)),
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "category_id": List<dynamic>.from(categoryId!.map((x) => x)),
        "bussiness_type": bussinessType,
        "bussiness_name": bussinessName,
        "address_line1": addressLine1,
        "address_line2": addressLine2,
        "landmark": landmark,
        "pincode": pincode,
        "city": city,
        "state": state,
        "country": country,
        "short_description": shortDescription,
        "latitude": latitude,
        "longitude": longitude,
        "location": location,
        "bussiness_logo": bussinessLogo,
        "imagesk": List<dynamic>.from(imagesk!.map((x) => x)),
        "timezone": timezone,
        "doc": List<dynamic>.from(doc!.map((x) => x)),
        "open_time":
            List<dynamic>.from(openTime!.map((x) => x == null ? 'null' : x)),
        "close_time":
            List<dynamic>.from(closeTime!.map((x) => x == null ? 'null' : x)),
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
      };
}
