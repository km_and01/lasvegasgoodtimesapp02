import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:lasvegas_gts_app/Networking/EatAPI/catering_chef_list_model.dart';
import 'package:lasvegas_gts_app/Networking/EatAPI/restaurent_list_model.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';

class EatServiceAPIs {
  Future<Map<String, dynamic>> bookRestaurent(
    String date,
    String time,
    String no_of_tabel,
    String vendor_id,
    String booking_user_name,
    String amount,
  ) async {

    Map<String, dynamic>? map;

    var details = await http.post(
        Uri.parse("http://koolmindapps.com/lasvegas/public/api/v1/bookeat"),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "vendor_id": vendor_id,
          "booking_user_name": booking_user_name,
          "amount": amount,
          "date": date,
          "time": time,
          "no_of_tabel": no_of_tabel,
          "image_path": no_of_tabel,
        });

    print('RESPONSE:- ${details.body}');
    if (details.statusCode == 200) {
      map = json.decode(details.body);


      return map!;
      // roomdata.services[index].roomName;
    } else {
      return map!;
    }
  }

  Future<List<ServiceRes>?> getRestaurentData(
    String date,
    String time,
    String foodType,
  ) async {
    var res = await http.post(
        Uri.parse('http://koolmindapps.com/lasvegas/public/api/v1/eat_vendor'),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "date": date,
          "time": time,
          "food_name": foodType,
        });

    // print(res.body);

    if (res.statusCode == 200) {
      print(res.body);

      final responseJson = json.decode(res.body);
      print(responseJson);
      var data = RestaurentListModel.fromJson(responseJson);
      print(data.message);
      print("Restaurent DATA in APIs =====" +
          data.servicesRes[0].bussinessDetails!.shortDescription.toString());
      // List<Airport>? airportList;

      // print(airportList?[0].name);

      return data.servicesRes;
    } else {
      print(res.statusCode);
    }
  }


  // ADD BY FZ
  Future<List<ServiceRes>?> getChefData01(
      String date,
      String time,
      String venue,
      String noGuest,
      String service,
      ) async {
    var res = await http.post(
        Uri.parse(
            'http://koolmindapps.com/lasvegas/public/api/v1/eatchefvendor'),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "services": service,
          "date": date,
          "time": time,
          "venue": venue,
          "noofper": noGuest,
        });

    print('Catering=== $service');
    print('Catering=== $date');
    print('Catering=== $time');
    print('Catering=== $venue');
    print('Catering=== $noGuest');
    // print(res.body);

    if (res.statusCode == 200) {
      print(res.body);

      final responseJson = json.decode(res.body);
      print(responseJson);
      var data = RestaurentListModel.fromJson(responseJson);
      print(data.message);

      print('CATERING== ${data.toJson()}');
      print("Restaurent DATA in APIs =====" +
          data.servicesRes[0].bussinessDetails!.shortDescription.toString());
      // List<Airport>? airportList;

      // print(airportList?[0].name);

      return data.servicesRes;
    } else {
      print(res.statusCode);
    }
  }

  Future<List<ServiceChef>?> getChefData(
    String date,
    String time,
    String venue,
    String noGuest,
    String service,
  ) async {
    var res = await http.post(
        Uri.parse(
            'http://koolmindapps.com/lasvegas/public/api/v1/eatchefvendor'),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "services": service,
          "date": date,
          "time": time,
          "venue": venue,
          "noofper": noGuest,
        });

    // print(res.body);

    if (res.statusCode == 200) {
      print(res.body);

      final responseJson = json.decode(res.body);
      // print("Catering  DATA in APIs =====" + responseJson);
      var data = CateringChefListModel.fromJson(responseJson);
      print(data.message);
      print("Restaurent DATA in APIs =====" +
          data.services[0].bussinessDetails!.shortDescription.toString());
      // List<Airport>? airportList;

      // print(airportList?[0].name);

      return data.services;
    } else {
      print(res.statusCode);
    }
  }

  Future bookCateringChef(
    String vendor_id,
    String address,
    String sub_service_id,
    String person,
    String foodName,
    String amount,
    String date,
    String bookingFor,
  ) async {
    var res = await http.post(
        Uri.parse("http://koolmindapps.com/lasvegas/public/api/v1/bookeatchef"),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "vendor_id": vendor_id,
          "address": address,
          "sub_service_id": sub_service_id,
          "person": person,
          "food_name": foodName,
          "amount": amount,
          "date": date,
          "time": bookingFor,
        });
        
    print(res.body);
  }
}
