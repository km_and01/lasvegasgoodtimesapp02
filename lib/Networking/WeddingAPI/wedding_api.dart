import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:lasvegas_gts_app/Networking/WeddingAPI/wedding_pkg_model.dart';
import 'package:lasvegas_gts_app/Networking/WeddingAPI/wedding_vendor_model.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';

class WeddingAPIs {
  // Future getFunActivities() async {
  //   var res = await http.get(
  //     Uri.parse('http://koolmindapps.com/lasvegas/public/api/v1/funextra'),
  //     headers: {
  //       'Authorization': "Bearer " + homeToken,
  //       'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
  //     },
  //   );
  //   List<FunAct> activities = [];

  //   if (res.statusCode == 200) {
  //     final responseJson = json.decode(res.body);
  //     var data = FunActivityListModel.fromJson(responseJson);
  //     print(data.eatdish![0].serviceName!);
  //     return activities = data.eatdish!;
  //   }
  // }

  Future getWeddingVendorData(String city_id, date) async {
    var res = await http.post(
        Uri.parse(
            'http://koolmindapps.com/lasvegas/public/api/v1/weddingvendor'),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "city_id": city_id,
          "date": date,
        });
    print(res.body);

    if (res.statusCode == 200) {
      print(res.body);

      final responseJson = json.decode(res.body);
      print(responseJson);
      var data = WeddingVendorModel.fromJson(responseJson);
      print(data.message);
      print("Restaurent DATA in APIs =====" +
          data.data![0].bussinessDetails!.shortDescription.toString());
      // List<Airport>? airportList;

      // print(airportList?[0].name);

      return data.data;
    } else {
      print(res.statusCode);
    }
  }

  Future<Map<String, dynamic>> bookWedd(
      String vendor_id,
      image_path,
      deposit_amm,
      amount,
      person1,
      person2,
      mobile_number,
      no_guest,
      function_name,
      package_name,
      check_in) async {

    Map<String, dynamic>? map;

    var details = await http.post(
        Uri.parse('http://koolmindapps.com/lasvegas/public/api/v1/bookwedding'),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "vendor_id": vendor_id,
          "image_path": image_path,
          "deposit_amm": deposit_amm,
          "amount": amount,
          "package_id": package_name,
          "check_in": check_in,
          "person1": person1,
          "person2": person2,
          "mobile_number": mobile_number,
          "No_guest": no_guest,
          "function_name": function_name,
        });
    if (details.statusCode == 200) {
      map = json.decode(details.body);


      return map!;
      // roomdata.services[index].roomName;
    } else {
      return map!;
    }
  }

  Future getWeddingPkgData(String vendor_id) async {
    var res = await http.post(
        Uri.parse(
            'http://koolmindapps.com/lasvegas/public/api/v1/weddingservice'),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "vendor_id": vendor_id,
        });
    print(res.body);

    if (res.statusCode == 200) {
      print(res.body);

      final responseJson = json.decode(res.body);
      print(responseJson);
      var data = WeddingPkgModel.fromJson(responseJson);
      print(data.message);
      print("Restaurent DATA in APIs =====" +
          data.data![0].bussinessDetails!.shortDescription.toString());
      // List<Airport>? airportList;

      // print(airportList?[0].name);

      return data.data;
    } else {
      print(res.statusCode);
    }
  }
}
