// ignore_for_file: deprecated_member_use

import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:lasvegas_gts_app/customs/service_grid.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';
import 'package:lasvegas_gts_app/screens/login/login_src.dart';
import 'package:lasvegas_gts_app/screens/services/stay/sta_search_main.dart';
import 'package:lasvegas_gts_app/screens/services/stay/stay_search_filter_scr.dart';
import 'package:shared_preferences/shared_preferences.dart';
bool  isLoading = false;
Uri url = Uri.parse("http://lasvegas.koolmindapps.com/api/v1/login");

Future<void> loginUser(
    String uEmail, String uPassword, BuildContext context) async {
  var res = await http.post(url,
      headers: {'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs'},
      body: {"email": uEmail, "password": uPassword, "user_type": "user"});

  var data = jsonDecode(res.body);
  print(data);

  var userData = UselLoginModel.fromJson(data);
  print(userData.data.user.name);

  if (res.statusCode == 200) {
    final SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('userToken', userData.data.token);
    preferences.setBool('IsLogin', true);
    preferences.setString('u_name', userData.data.user.name);
    preferences.setString('u_email', userData.data.user.email);
    preferences.setString('u_mob', userData.data.user.mobileNumber);
    preferences.setString('u_id', userData.data.user.id.toString());
    // var uToken = preferences.getString('userToken');

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) {
          return HomeScreen(
            /*userName: userData.data.user.name,
            userToken: userData.data.token,
            userEmail: userData.data.user.email,
            userPhone: userData.data.user.mobileNumber,
            userID: userData.data.user.id.toString(),*/
          );
        },
      ),
    );
    // userEmailController.clear();
    // userPasswordController.clear();
  
    print(userData.data.token + " Token is Harvested");
    // GetDataSpecialSuits.userToken = userData.data.token;
    // GetDataPentHouse.userToken = userData.data.token;
    // GetDataPvtBnb.userToken = userData.data.token;
    
  } else {
    showDialog(
        context: context,
        builder: (_) {
          return AlertDialog(
            title: Text('Error'), // To display the title it is optional
            content: Text(
                'Please Enter Valid Email/Password'), // Message which will be pop up on the screen
            // Action widget which will provide the user to acknowledge the choice
            actions: [
              FlatButton(
                // FlatButton widget is used to make a text to work like a button
                textColor: Colors.black,
                onPressed: () {
                  userEmailController.clear();
                  userPasswordController.clear();
                  isLoading = false;
                  Navigator.pop(context);
                }, // function used to perform after pressing the button
                child: Text('CANCEL'),
              ),
              FlatButton(
                textColor: Colors.black,
                onPressed: () {
                  userEmailController.clear();
                  userPasswordController.clear();
                  isLoading = false;
                  Navigator.pop(context);
                },
                child: Text('OK'),
              ),
            ],
          );
        });
  }
  // print(res);
  // print(res.statusCode);
  // print(res.body);
}

// To parse this JSON data, do
//
//     final uselLoginModel = uselLoginModelFromJson(jsonString);

UselLoginModel uselLoginModelFromJson(String str) =>
    UselLoginModel.fromJson(json.decode(str));

String uselLoginModelToJson(UselLoginModel data) => json.encode(data.toJson());

class UselLoginModel {
  UselLoginModel({
    this.status,
    this.code,
    required this.msg,
    this.extraMsg,
    this.version,
    required this.data,
  });

  bool? status;
  int? code;
  String msg;
  String? extraMsg;
  String? version;
  Data data;

  factory UselLoginModel.fromJson(Map<String, dynamic> json) => UselLoginModel(
        status: json["status"],
        code: json["code"],
        msg: json["msg"],
        extraMsg: json["extra_msg"],
        version: json["version"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "code": code,
        "msg": msg,
        "extra_msg": extraMsg,
        "version": version,
        "data": data.toJson(),
      };
}

class Data {
  Data({
    required this.user,
    required this.token,
  });

  User user;
  String token;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        user: User.fromJson(json["user"]),
        token: json["token"],
      );

  Map<String, dynamic> toJson() => {
        "user": user.toJson(),
        "token": token,
      };
}

class User {
  User({
    required this.id,
    required this.name,
    required this.email,
    required this.mobileNumber,
    this.referralCode,
    this.userReferralCode,
    required this.userType,
    this.gender,
    required this.isActive,
    this.logo,
    this.emailVerifiedAt,
    this.deviceOs,
    this.appVersion,
    this.deviceName,
    this.fcmToken,
    this.googleId,
    this.otp,
    this.facebookId,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
    this.servie,
  });

  int id;
  String name;
  String email;
  String mobileNumber;
  dynamic referralCode;
  dynamic userReferralCode;
  String userType;
  dynamic gender;
  int isActive;
  dynamic logo;
  dynamic emailVerifiedAt;
  dynamic deviceOs;
  dynamic appVersion;
  dynamic deviceName;
  dynamic fcmToken;
  dynamic googleId;
  String? otp;
  dynamic facebookId;
  DateTime? createdAt;
  DateTime? updatedAt;
  dynamic deletedAt;
  int? servie;

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        name: json["name"],
        email: json["email"],
        mobileNumber: json["mobile_number"],
        referralCode: json["referral_code"],
        userReferralCode: json["user_referral_code"],
        userType: json["user_type"],
        gender: json["gender"],
        isActive: json["is_active"],
        logo: json["logo"],
        emailVerifiedAt: json["email_verified_at"],
        deviceOs: json["DeviceOS"],
        appVersion: json["AppVersion"],
        deviceName: json["DeviceName"],
        fcmToken: json["FCMToken"],
        googleId: json["google_id"],
        otp: json["otp"],
        facebookId: json["facebook_id"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        deletedAt: json["deleted_at"],
        servie: json["servie"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "email": email,
        "mobile_number": mobileNumber,
        "referral_code": referralCode,
        "user_referral_code": userReferralCode,
        "user_type": userType,
        "gender": gender,
        "is_active": isActive,
        "logo": logo,
        "email_verified_at": emailVerifiedAt,
        "DeviceOS": deviceOs,
        "AppVersion": appVersion,
        "DeviceName": deviceName,
        "FCMToken": fcmToken,
        "google_id": googleId,
        "otp": otp,
        "facebook_id": facebookId,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
        "deleted_at": deletedAt,
        "servie": servie,
      };
}
