// To parse this JSON data, do
//
//     final carNameListModel = carNameListModelFromJson(jsonString);

import 'dart:convert';

CarNameListModel carNameListModelFromJson(String str) => CarNameListModel.fromJson(json.decode(str));

String carNameListModelToJson(CarNameListModel data) => json.encode(data.toJson());

class CarNameListModel {
    CarNameListModel({
        this.message,
        this.statusCode,
        this.eatdish,
    });

    String? message;
    int? statusCode;
    List<CarNameList>? eatdish;

    factory CarNameListModel.fromJson(Map<String, dynamic> json) => CarNameListModel(
        message: json["message"],
        statusCode: json["status_code"],
        eatdish: List<CarNameList>.from(json["eatdish"].map((x) => CarNameList.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "status_code": statusCode,
        "eatdish": List<dynamic>.from(eatdish!.map((x) => x.toJson())),
    };
}

class CarNameList {
    CarNameList({
        this.id,
        this.vendorId,
        this.categoryId,
        this.categoryName,
        this.serviceName,
        this.logo,
        this.status,
        this.createdAt,
        this.updatedAt,
    });

    int? id;
    dynamic vendorId;
    String? categoryId;
    String? categoryName;
    String? serviceName;
    String? logo;
    String? status;
    dynamic createdAt;
    dynamic updatedAt;

    factory CarNameList.fromJson(Map<String, dynamic> json) => CarNameList(
        id: json["id"],
        vendorId: json["vendor_id"],
        categoryId: json["category_id"],
        categoryName: json["category_name"],
        serviceName: json["service_name"],
        logo: json["logo"],
        status: json["status"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "vendor_id": vendorId,
        "category_id": categoryId,
        "category_name": categoryName,
        "service_name": serviceName,
        "logo": logo,
        "status": status,
        "created_at": createdAt,
        "updated_at": updatedAt,
    };
}
