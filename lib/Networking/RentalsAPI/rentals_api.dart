import 'package:lasvegas_gts_app/Networking/RentalsAPI/can_name_models.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:lasvegas_gts_app/Networking/RentalsAPI/car_pkg_model.dart';
import 'package:lasvegas_gts_app/Networking/RentalsAPI/car_vendor_model.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';


Uri urlcarlist =
    Uri.parse("http://koolmindapps.com/lasvegas/public/api/v1/car_name");
class RentalsAPIs {
  Future<List<CarNameList>?> getCarList(String token,) async {
    var res = await http.get(urlcarlist, headers: {
      'Authorization': "Bearer " + token,
      'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
    });

    print(res.body);

    final responseJson = json.decode(res.body);
    print(res.body);
    var datac = CarNameListModel.fromJson(responseJson);
    print(datac);
    List<CarNameList>? carList = datac.eatdish;

    print(carList![0].serviceName);

    return carList;
  }






   Future getRentalVendorData(String city_id, date, activitie_name) async {
    print(homeToken);
    var res = await http.post(
        Uri.parse('http://koolmindapps.com/lasvegas/public/api/v1/rentalvendor'),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "city2": city_id,
          "date": date,
          "car_name": activitie_name == null || activitie_name == 'null' || activitie_name == 'All' ? '' : activitie_name,
        });
    print(res.body);

    if (res.statusCode == 200) {
      print(res.body);

      final responseJson = json.decode(res.body);
      print(responseJson);
      var data =  RentalsVendorModel.fromJson(responseJson);
      print(data.message);
      print("Restaurent DATA in APIs =====" +
          data.data[0].bussinessDetails!.shortDescription.toString());
      // List<Airport>? airportList;

      // print(airportList?[0].name);

      return data.data;
    } else {
      print(res.statusCode);
    }
  }


Future<Map<String, dynamic>> bookRental(String vendor_id, image_path, person, fun_user_name, amount,package_name, check_in,carName) async {

  Map<String, dynamic>? map;
    var details = await http.post(
        Uri.parse('http://koolmindapps.com/lasvegas/public/api/v1/bookrental'),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "vendor_id": vendor_id,
          "image_path": image_path,
          "no_per": person,
          "fun_user_name": fun_user_name,
          "amount": amount,
          "package_name": package_name,
          "check_in": check_in,
          "activitie_name" : carName ,
        });
    if (details.statusCode == 200) {
      map = json.decode(details.body);


      return map!;
      // roomdata.services[index].roomName;
    } else {
      return map!;
    }
  }

  Future getRentalPkgData(String vendor_id) async {
    var res = await http.post(
        Uri.parse('http://koolmindapps.com/lasvegas/public/api/v1/rentalservice'),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "vendor_id": vendor_id,
        });
    print(res.body);

    if (res.statusCode == 200) {
      print(res.body);

      final responseJson = json.decode(res.body);
      print(responseJson);
      var data = RentalsPkgVendorModel.fromJson(responseJson);
      print(data.message);
      print("Restaurent DATA in APIs =====" +
          data.data[0].bussinessDetails!.shortDescription.toString());
      // List<Airport>? airportList;

      // print(airportList?[0].name);

      return data.data;
    } else {
      print(res.statusCode);
    }
  }



}