// To parse this JSON data, do
//
//     final rentalsVendorModel = rentalsVendorModelFromJson(jsonString);

import 'dart:convert';

RentalsVendorModel rentalsVendorModelFromJson(String str) => RentalsVendorModel.fromJson(json.decode(str));

String rentalsVendorModelToJson(RentalsVendorModel data) => json.encode(data.toJson());

class RentalsVendorModel {
    RentalsVendorModel({
        this.message,
        this.statusCode,
        required this.data,
    });

    String? message;
    int? statusCode;
    List<RentalV> data;

    factory RentalsVendorModel.fromJson(Map<String, dynamic> json) => RentalsVendorModel(
        message: json["message"],
        statusCode: json["status_code"],
        data: List<RentalV>.from(json["data"].map((x) => RentalV.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "status_code": statusCode,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class RentalV {
    RentalV({
        this.id,
        this.vendorId,
        this.packageName,
        this.shortDetails,
        this.fullDetails,
        this.bannerImage,
        this.moreImages,
        this.price,
        this.disPrice,
        this.priceUnit,
        this.carBrand,
        this.status,
        this.createdAt,
        this.updatedAt,
        this.minPrice,
        this.fillHerat,
        this.bussinessDetails,
    });

    int? id;
    String? vendorId;
    String? packageName;
    String? shortDetails;
    String? fullDetails;
    String? bannerImage;
    List<String>? moreImages;
    String? price;
    String? disPrice;
    String? priceUnit;
    String? carBrand;
    int? status;
    DateTime? createdAt;
    DateTime? updatedAt;
    String? minPrice;
    int? fillHerat;
    BussinessDetails? bussinessDetails;

    factory RentalV.fromJson(Map<String, dynamic> json) => RentalV(
        id: json["id"],
        vendorId: json["vendor_id"],
        packageName: json["package_name"],
        shortDetails: json["short_details"],
        fullDetails: json["full_details"],
        bannerImage: json["banner_image"],
        moreImages: List<String>.from(json["more_images"].map((x) => x)),
        price: json["price"],
        disPrice: json["dis_price"],
        priceUnit: json["price_unit"],
        carBrand: json["car_brand"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        minPrice: json["min_price"],
        fillHerat: json["fill_herat"],
        bussinessDetails: BussinessDetails.fromJson(json["bussiness_details"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "vendor_id": vendorId,
        "package_name": packageName,
        "short_details": shortDetails,
        "full_details": fullDetails,
        "banner_image": bannerImage,
        "more_images": List<dynamic>.from(moreImages!.map((x) => x)),
        "price": price,
        "dis_price": disPrice,
        "price_unit": priceUnit,
        "car_brand": carBrand,
        "status": status,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
        "min_price": minPrice,
        "fill_herat": fillHerat,
        "bussiness_details": bussinessDetails!.toJson(),
    };
}

class BussinessDetails {
    BussinessDetails({
        this.id,
        this.userId,
        this.categoryId,
        this.bussinessType,
        this.bussinessName,
        this.addressLine1,
        this.addressLine2,
        this.landmark,
        this.pincode,
        this.city,
        this.state,
        this.country,
        this.shortDescription,
        this.latitude,
        this.longitude,
        this.location,
        this.bussinessLogo,
        this.imagesk,
        this.timezone,
        this.doc,
        this.openTime,
        this.closeTime,
        this.createdAt,
        this.updatedAt,
    });

    int? id;
    String? userId;
    List<String>? categoryId;
    dynamic bussinessType;
    String? bussinessName;
    String? addressLine1;
    String? addressLine2;
    dynamic landmark;
    String? pincode;
    String? city;
    String? state;
    String? country;
    String? shortDescription;
    dynamic latitude;
    dynamic longitude;
    dynamic location;
    String? bussinessLogo;
    List<String>? imagesk;
    String? timezone;
    List<String>? doc;
    List<String>? openTime;
    List<String>? closeTime;
    DateTime? createdAt;
    DateTime? updatedAt;

    factory BussinessDetails.fromJson(Map<String, dynamic> json) => BussinessDetails(
        id: json["id"],
        userId: json["user_id"],
        categoryId: List<String>.from(json["category_id"].map((x) => x)),
        bussinessType: json["bussiness_type"],
        bussinessName: json["bussiness_name"],
        addressLine1: json["address_line1"],
        addressLine2: json["address_line2"],
        landmark: json["landmark"],
        pincode: json["pincode"],
        city: json["city"],
        state: json["state"],
        country: json["country"],
        shortDescription: json["short_description"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        location: json["location"],
        bussinessLogo: json["bussiness_logo"],
        imagesk: List<String>.from(json["imagesk"].map((x) => x)),
        timezone: json["timezone"],
        doc: List<String>.from(json["doc"].map((x) => x)),
        openTime: List<String>.from(json["open_time"].map((x) => x == null ? 'null' : x)),
        closeTime: List<String>.from(json["close_time"].map((x) => x == null ? 'null' : x)),
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "category_id": List<dynamic>.from(categoryId!.map((x) => x)),
        "bussiness_type": bussinessType,
        "bussiness_name": bussinessName,
        "address_line1": addressLine1,
        "address_line2": addressLine2,
        "landmark": landmark,
        "pincode": pincode,
        "city": city,
        "state": state,
        "country": country,
        "short_description": shortDescription,
        "latitude": latitude,
        "longitude": longitude,
        "location": location,
        "bussiness_logo": bussinessLogo,
        "imagesk": List<dynamic>.from(imagesk!.map((x) => x)),
        "timezone": timezone,
        "doc": List<dynamic>.from(doc!.map((x) => x)),
        "open_time": List<dynamic>.from(openTime!.map((x) => x == null ? 'null' : x)),
        "close_time": List<dynamic>.from(closeTime!.map((x) => x == null ? 'null' : x)),
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
    };
}
