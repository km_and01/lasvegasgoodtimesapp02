import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:lasvegas_gts_app/Networking/BodyguardAPI/bodyguard_pkg_model.dart';
import 'package:lasvegas_gts_app/Networking/BodyguardAPI/bodyguard_vendor_model.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';

class BodyguardAPIs {
  // Future getFunActivities() async {
  //   var res = await http.get(
  //     Uri.parse('http://koolmindapps.com/lasvegas/public/api/v1/funextra'),
  //     headers: {
  //       'Authorization': "Bearer " + homeToken,
  //       'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
  //     },
  //   );
  //   List<FunAct> activities = [];

  //   if (res.statusCode == 200) {
  //     final responseJson = json.decode(res.body);
  //     var data = FunActivityListModel.fromJson(responseJson);
  //     print(data.eatdish![0].serviceName!);
  //     return activities = data.eatdish!;
  //   }
  // }

  Future<List<BGVendor>?> getBGVendorData(String cityId, securityType) async {
    var res = await http.post(
        Uri.parse(
            'http://koolmindapps.com/lasvegas/public/api/v1/executiveprotectionvendor'),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "city_id": cityId,
          "security_type": securityType,
        });
    print(res.body);

    if (res.statusCode == 200) {
      print(res.body);

      final responseJson = json.decode(res.body);
      print(responseJson);
      var data = BodyguardVendorModel.fromJson(responseJson);
      print(data.message);
      print("BodyguardVendorModel DATA in APIs =====" +
          data.data![0].bussinessDetails!.shortDescription.toString());
      // List<Airport>? airportList;

      // print(airportList?[0].name);

      return data.data;
    } else {
      print(res.statusCode);
    }
  }

  Future<Map<String, dynamic>> bookBG(
      String vendor_id,
      image_path,
      sub_service_id,
      amount,
      name,
      age,
      from_date,
      from_time,
      to_date,
      to_time,
      address) async {

    Map<String, dynamic>? map;

    var details = await http.post(
        Uri.parse('http://koolmindapps.com/lasvegas/public/api/v1/bookexecutiveprotection'),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "vendor_id": vendor_id,
          "image_path": image_path,
          "sub_service_id": sub_service_id,
          "amount": amount,
          "to_time": to_time,
          "address": address,
          "name": name,
          "age": age,
          "from_date": from_date,
          "from_time": from_time,
          "to_date": to_date,
        });
    if (details.statusCode == 200) {
      map = json.decode(details.body);


      return map!;
      // roomdata.services[index].roomName;
    } else {
      return map!;
    }
  }

  Future getBGPkgData(String vendor_id,security_type) async {
    var res = await http.post(
        Uri.parse(
            'http://koolmindapps.com/lasvegas/public/api/v1/executiveprotectionservice'),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "vendor_id": vendor_id,
          "security_type" : security_type,
        });
    print(res.body);

    if (res.statusCode == 200) {
      print(res.body);

      final responseJson = json.decode(res.body);
      print(responseJson);
      var data = BodyguardPkgModel.fromJson(responseJson);
      print(data.message);
      print("BodyguardPkgModel DATA in APIs =====" +
          data.data![0].bussinessDetails!.shortDescription.toString());
      // List<Airport>? airportList;

      // print(airportList?[0].name);

      return data.data;
    } else {
      print(res.statusCode);
    }
  }
}
