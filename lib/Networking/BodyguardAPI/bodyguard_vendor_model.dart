// To parse this JSON data, do
//
//     final bodyguardVendorModel = bodyguardVendorModelFromJson(jsonString);

import 'dart:convert';

BodyguardVendorModel bodyguardVendorModelFromJson(String str) => BodyguardVendorModel.fromJson(json.decode(str));

String bodyguardVendorModelToJson(BodyguardVendorModel data) => json.encode(data.toJson());

class BodyguardVendorModel {
    BodyguardVendorModel({
        this.message,
        this.statusCode,
        this.data,
    });

    String? message;
    int? statusCode;
    List<BGVendor>? data;

    factory BodyguardVendorModel.fromJson(Map<String, dynamic> json) => BodyguardVendorModel(
        message: json["message"],
        statusCode: json["status_code"],
        data: List<BGVendor>.from(json["data"].map((x) => BGVendor.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "status_code": statusCode,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class BGVendor {
    BGVendor({
        this.id,
        this.vendorId,
        this.securityType,
        this.noOfPeople,
        this.price,
        this.priceUnit,
        this.country,
        this.state,
        this.regions,
        this.photos,
        this.status,
        this.countryName,
        this.stateName,
        this.cityName,
        this.createdAt,
        this.updatedAt,
        this.minPrice,
        this.bussinessDetails,
    });

    int? id;
    String? vendorId;
    String? securityType;
    String? noOfPeople;
    String? price;
    String? priceUnit;
    List<String>? country;
    List<String>? state;
    List<String>? regions;
    List<String>? photos;
    int? status;
    List<String>? countryName;
    List<String>? stateName;
    List<String>? cityName;
    DateTime? createdAt;
    DateTime? updatedAt;
    String? minPrice;
    BussinessDetails? bussinessDetails;

    factory BGVendor.fromJson(Map<String, dynamic> json) => BGVendor(
        id: json["id"],
        vendorId: json["vendor_id"],
        securityType: json["security_type"],
        noOfPeople: json["no_of_people"],
        price: json["price"],
        priceUnit: json["price_unit"],
        country: List<String>.from(json["country"].map((x) => x)),
        state: List<String>.from(json["state"].map((x) => x)),
        regions: List<String>.from(json["regions"].map((x) => x)),
        photos: List<String>.from(json["photos"].map((x) => x)),
        status: json["status"],
        countryName: List<String>.from(json["country_name"].map((x) => x)),
        stateName: List<String>.from(json["state_name"].map((x) => x)),
        cityName: List<String>.from(json["city_name"].map((x) => x)),
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        minPrice: json["min_price"],
        bussinessDetails: BussinessDetails.fromJson(json["bussiness_details"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "vendor_id": vendorId,
        "security_type": securityType,
        "no_of_people": noOfPeople,
        "price": price,
        "price_unit": priceUnit,
        "country": List<dynamic>.from(country!.map((x) => x)),
        "state": List<dynamic>.from(state!.map((x) => x)),
        "regions": List<dynamic>.from(regions!.map((x) => x)),
        "photos": List<dynamic>.from(photos!.map((x) => x)),
        "status": status,
        "country_name": List<dynamic>.from(countryName!.map((x) => x)),
        "state_name": List<dynamic>.from(stateName!.map((x) => x)),
        "city_name": List<dynamic>.from(cityName!.map((x) => x)),
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
        "min_price": minPrice,
        "bussiness_details": bussinessDetails!.toJson(),
    };
}

class BussinessDetails {
    BussinessDetails({
        this.id,
        this.userId,
        this.categoryId,
        this.bussinessType,
        this.bussinessName,
        this.addressLine1,
        this.addressLine2,
        this.landmark,
        this.pincode,
        this.city,
        this.state,
        this.country,
        this.shortDescription,
        this.latitude,
        this.longitude,
        this.location,
        this.bussinessLogo,
        this.imagesk,
        this.timezone,
        this.doc,
        this.openTime,
        this.closeTime,
        this.createdAt,
        this.updatedAt,
    });

    int? id;
    String? userId;
    List<String>? categoryId;
    dynamic bussinessType;
    String? bussinessName;
    String? addressLine1;
    String? addressLine2;
    dynamic landmark;
    String? pincode;
    String? city;
    String? state;
    String? country;
    String? shortDescription;
    dynamic latitude;
    dynamic longitude;
    dynamic location;
    String? bussinessLogo;
    List<String>? imagesk;
    String? timezone;
    List<String>? doc;
    List<String>? openTime;
    List<String>? closeTime;
    DateTime? createdAt;
    DateTime? updatedAt;

    factory BussinessDetails.fromJson(Map<String, dynamic> json) => BussinessDetails(
        id: json["id"],
        userId: json["user_id"],
        categoryId: List<String>.from(json["category_id"].map((x) => x)),
        bussinessType: json["bussiness_type"],
        bussinessName: json["bussiness_name"],
        addressLine1: json["address_line1"],
        addressLine2: json["address_line2"],
        landmark: json["landmark"],
        pincode: json["pincode"],
        city: json["city"],
        state: json["state"],
        country: json["country"],
        shortDescription: json["short_description"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        location: json["location"],
        bussinessLogo: json["bussiness_logo"],
        imagesk: List<String>.from(json["imagesk"].map((x) => x)),
        timezone: json["timezone"],
        doc: List<String>.from(json["doc"].map((x) => x)),
        openTime: List<String>.from(json["open_time"].map((x) => x == null ? 'null' : x)),
        closeTime: List<String>.from(json["close_time"].map((x) => x == null ? 'null' : x)),
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "category_id": List<dynamic>.from(categoryId!.map((x) => x)),
        "bussiness_type": bussinessType,
        "bussiness_name": bussinessName,
        "address_line1": addressLine1,
        "address_line2": addressLine2,
        "landmark": landmark,
        "pincode": pincode,
        "city": city,
        "state": state,
        "country": country,
        "short_description": shortDescription,
        "latitude": latitude,
        "longitude": longitude,
        "location": location,
        "bussiness_logo": bussinessLogo,
        "imagesk": List<dynamic>.from(imagesk!.map((x) => x)),
        "timezone": timezone,
        "doc": List<dynamic>.from(doc!.map((x) => x)),
        "open_time": List<dynamic>.from(openTime!.map((x) => x == null ? 'null' : x)),
        "close_time": List<dynamic>.from(closeTime!.map((x) => x == null ? 'null' : x)),
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
    };
}
