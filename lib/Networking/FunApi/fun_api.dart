import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:lasvegas_gts_app/Networking/FunApi/fun_activity_list_model.dart';
import 'package:lasvegas_gts_app/Networking/FunApi/fun_package_model.dart';
import 'package:lasvegas_gts_app/Networking/FunApi/fun_vendor_model.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';

class FunAPIs {
  Future getFunActivities() async {
    var res = await http.get(
      Uri.parse('http://koolmindapps.com/lasvegas/public/api/v1/funextra'),
      headers: {
        'Authorization': "Bearer " + homeToken,
        'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
      },
    );
    List<FunAct> activities = [];

    if (res.statusCode == 200) {
      final responseJson = json.decode(res.body);
      var data = FunActivityListModel.fromJson(responseJson);
      print(data.eatdish![0].serviceName!);
      return activities = data.eatdish!;
    }
  }

  Future getFunVendorData(String city_id, date, activitie_name) async {
    var res = await http.post(
        Uri.parse('http://koolmindapps.com/lasvegas/public/api/v1/funvendor'),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "city_id": city_id,
          "date": date,
          "activitie_name": activitie_name,
        });
    print(res.body);

    if (res.statusCode == 200) {
      print(res.body);

      final responseJson = json.decode(res.body);
      print(responseJson);
      var data = FunVendorListModel.fromJson(responseJson);
      print(data.message);
      print("Restaurent DATA in APIs =====" +
          data.data![0].bussinessDetails!.shortDescription.toString());
      // List<Airport>? airportList;

      // print(airportList?[0].name);

      return data.data;
    } else {
      print(res.statusCode);
    }
  }

  Future<Map<String, dynamic>> bookFun(String vendor_id, image_path, person, fun_user_name, amount,
      package_name, check_in) async {

    Map<String, dynamic>? map;
    var details = await http.post(
        Uri.parse('http://koolmindapps.com/lasvegas/public/api/v1/bookfun'),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "vendor_id": vendor_id,
          "image_path": image_path,
          "person": person,
          "fun_user_name": fun_user_name,
          "amount": amount,
          "package_name": package_name,
          "check_in": check_in,
        });

    if (details.statusCode == 200) {
      map = json.decode(details.body);


      return map!;
      // roomdata.services[index].roomName;
    } else {
      return map!;
    }

  }

  Future getFunPkgData(String vendor_id) async {
    var res = await http.post(
        Uri.parse('http://koolmindapps.com/lasvegas/public/api/v1/funservice'),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "vendor_id": vendor_id,
        });
    print(res.body);

    if (res.statusCode == 200) {
      print(res.body);

      final responseJson = json.decode(res.body);
      print(responseJson);
      var data = FunPackageListModel.fromJson(responseJson);
      print(data.message);
      print("Restaurent DATA in APIs =====" +
          data.data![0].bussinessDetails!.shortDescription.toString());
      // List<Airport>? airportList;

      // print(airportList?[0].name);

      return data.data;
    } else {
      print(res.statusCode);
    }
  }
}
