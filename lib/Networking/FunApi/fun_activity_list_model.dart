// To parse this JSON data, do
//
//     final funActivityListModel = funActivityListModelFromJson(jsonString);

import 'dart:convert';

FunActivityListModel funActivityListModelFromJson(String str) => FunActivityListModel.fromJson(json.decode(str));

String funActivityListModelToJson(FunActivityListModel data) => json.encode(data.toJson());

class FunActivityListModel {
    FunActivityListModel({
        this.message,
        this.statusCode,
        this.eatdish,
    });

    String? message;
    int? statusCode;
    List<FunAct>? eatdish;

    factory FunActivityListModel.fromJson(Map<String, dynamic> json) => FunActivityListModel(
        message: json["message"],
        statusCode: json["status_code"],
        eatdish: List<FunAct>.from(json["eatdish"].map((x) => FunAct.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "status_code": statusCode,
        "eatdish": List<dynamic>.from(eatdish!.map((x) => x.toJson())),
    };
}

class FunAct {
    FunAct({
        this.id,
        this.vendorId,
        this.categoryId,
        this.categoryName,
        this.serviceName,
        this.logo,
        this.status,
        this.createdAt,
        this.updatedAt,
    });

    int? id;
    String? vendorId;
    String? categoryId;
    String? categoryName;
    String? serviceName;
    String? logo;
    String? status;
    dynamic createdAt;
    dynamic updatedAt;

    factory FunAct.fromJson(Map<String, dynamic> json) => FunAct(
        id: json["id"],
        vendorId: json["vendor_id"] == null ? 'null' : json["vendor_id"],
        categoryId: json["category_id"],
        categoryName: json["category_name"],
        serviceName: json["service_name"],
        logo: json["logo"],
        status: json["status"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "vendor_id": vendorId == null ? 'null' : vendorId,
        "category_id": categoryId,
        "category_name": categoryName,
        "service_name": serviceName,
        "logo": logo,
        "status": status,
        "created_at": createdAt,
        "updated_at": updatedAt,
    };
}
