// To parse this JSON data, do
//
//     final funPackageListModel = funPackageListModelFromJson(jsonString);

import 'dart:convert';

FunPackageListModel funPackageListModelFromJson(String str) => FunPackageListModel.fromJson(json.decode(str));

String funPackageListModelToJson(FunPackageListModel data) => json.encode(data.toJson());

class FunPackageListModel {
    FunPackageListModel({
        this.message,
        this.statusCode,
        this.data,
    });

    String? message;
    int? statusCode;
    List<FPkg>? data;

    factory FunPackageListModel.fromJson(Map<String, dynamic> json) => FunPackageListModel(
        message: json["message"],
        statusCode: json["status_code"],
        data: List<FPkg>.from(json["data"].map((x) => FPkg.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "status_code": statusCode,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class FPkg {
    FPkg({
        this.id,
        this.vendorId,
        this.serviceName,
        this.activitieName,
        this.packageName,
        this.shortDetails,
        this.fullDetails,
        this.bannerImage,
        this.moreImages,
        this.price,
        this.disPrice,
        this.priceUnit,
        this.status,
        this.createdAt,
        this.updatedAt,
        this.bussinessDetails,
    });

    int? id;
    String? vendorId;
    dynamic serviceName;
    dynamic activitieName;
    String? packageName;
    String? shortDetails;
    String? fullDetails;
    String? bannerImage;
    List<String>? moreImages;
    String? price;
    String? disPrice;
    String? priceUnit;
    int? status;
    DateTime? createdAt;
    DateTime? updatedAt;
    BussinessDetails? bussinessDetails;

    factory FPkg.fromJson(Map<String, dynamic> json) => FPkg(
        id: json["id"],
        vendorId: json["vendor_id"],
        serviceName: json["service_name"],
        activitieName: json["activitie_name"],
        packageName: json["package_name"],
        shortDetails: json["short_details"],
        fullDetails: json["full_details"],
        bannerImage: json["banner_image"],
        moreImages: List<String>.from(json["more_images"].map((x) => x)),
        price: json["price"],
        disPrice: json["dis_price"],
        priceUnit: json["price_unit"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        bussinessDetails: BussinessDetails.fromJson(json["bussiness_details"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "vendor_id": vendorId,
        "service_name": serviceName,
        "activitie_name": activitieName,
        "package_name": packageName,
        "short_details": shortDetails,
        "full_details": fullDetails,
        "banner_image": bannerImage,
        "more_images": List<dynamic>.from(moreImages!.map((x) => x)),
        "price": price,
        "dis_price": disPrice,
        "price_unit": priceUnit,
        "status": status,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
        "bussiness_details": bussinessDetails!.toJson(),
    };
}

class BussinessDetails {
    BussinessDetails({
        this.id,
        this.userId,
        this.categoryId,
        this.bussinessType,
        this.bussinessName,
        this.addressLine1,
        this.addressLine2,
        this.landmark,
        this.pincode,
        this.city,
        this.state,
        this.country,
        this.shortDescription,
        this.latitude,
        this.longitude,
        this.location,
        this.bussinessLogo,
        this.imagesk,
        this.timezone,
        this.doc,
        this.openTime,
        this.closeTime,
        this.createdAt,
        this.updatedAt,
    });

    int? id;
    String? userId;
    List<String>? categoryId;
    dynamic bussinessType;
    String? bussinessName;
    String? addressLine1;
    String? addressLine2;
    dynamic landmark;
    String? pincode;
    String? city;
    String? state;
    String? country;
    String? shortDescription;
    dynamic latitude;
    dynamic longitude;
    dynamic location;
    String? bussinessLogo;
    List<String>? imagesk;
    String? timezone;
    List<String>? doc;
    List<String>? openTime;
    List<String>? closeTime;
    DateTime? createdAt;
    DateTime? updatedAt;

    factory BussinessDetails.fromJson(Map<String, dynamic> json) => BussinessDetails(
        id: json["id"],
        userId: json["user_id"],
        categoryId: List<String>.from(json["category_id"].map((x) => x)),
        bussinessType: json["bussiness_type"],
        bussinessName: json["bussiness_name"],
        addressLine1: json["address_line1"],
        addressLine2: json["address_line2"],
        landmark: json["landmark"],
        pincode: json["pincode"],
        city: json["city"],
        state: json["state"],
        country: json["country"],
        shortDescription: json["short_description"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        location: json["location"],
        bussinessLogo: json["bussiness_logo"],
        imagesk: List<String>.from(json["imagesk"].map((x) => x)),
        timezone: json["timezone"],
        doc: List<String>.from(json["doc"].map((x) => x)),
        openTime: List<String>.from(json["open_time"].map((x) => x == null ? 'null' : x)),
        closeTime: List<String>.from(json["close_time"].map((x) => x == null ? 'null' : x)),
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "category_id": List<dynamic>.from(categoryId!.map((x) => x)),
        "bussiness_type": bussinessType,
        "bussiness_name": bussinessName,
        "address_line1": addressLine1,
        "address_line2": addressLine2,
        "landmark": landmark,
        "pincode": pincode,
        "city": city,
        "state": state,
        "country": country,
        "short_description": shortDescription,
        "latitude": latitude,
        "longitude": longitude,
        "location": location,
        "bussiness_logo": bussinessLogo,
        "imagesk": List<dynamic>.from(imagesk!.map((x) => x)),
        "timezone": timezone,
        "doc": List<dynamic>.from(doc!.map((x) => x)),
        "open_time": List<dynamic>.from(openTime!.map((x) => x == null ? 'null' :x)),
        "close_time": List<dynamic>.from(closeTime!.map((x) => x == null ? 'null' : x)),
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
    };
}

// enum CloseTime { THE_2000 }

// final closeTimeValues = EnumValues({
//     "20:00": CloseTime.THE_2000
// });

// enum OpenTime { THE_0800 }

// final openTimeValues = EnumValues({
//     "08:00": OpenTime.THE_0800
// });

// class EnumValues<T> {
//     Map<String, T> map;
//     Map<T, String> reverseMap;

//     EnumValues(this.map);

//     Map<T, String> get reverse {
//         if (reverseMap == null) {
//             reverseMap = map.map((k, v) => new MapEntry(v, k));
//         }
//         return reverseMap;
//     }
// }
