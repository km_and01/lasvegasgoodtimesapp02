// To parse this JSON data, do
//
//     final funVendorListModel = funVendorListModelFromJson(jsonString);

import 'dart:convert';

FunVendorListModel funVendorListModelFromJson(String str) => FunVendorListModel.fromJson(json.decode(str));

String funVendorListModelToJson(FunVendorListModel data) => json.encode(data.toJson());

class FunVendorListModel {
    FunVendorListModel({
        this.message,
        this.statusCode,
        this.data,
    });

    String? message;
    int? statusCode;
    List<FVendor>? data;

    factory FunVendorListModel.fromJson(Map<String, dynamic> json) => FunVendorListModel(
        message: json["message"],
        statusCode: json["status_code"],
        data: List<FVendor>.from(json["data"].map((x) => FVendor.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "status_code": statusCode,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class FVendor {
    FVendor({
        this.id,
        this.vendorId,
        this.serviceName,
        this.activitieName,
        this.packageName,
        this.shortDetails,
        this.fullDetails,
        this.bannerImage,
        this.moreImages,
        this.price,
        this.disPrice,
        this.priceUnit,
        this.status,
        this.createdAt,
        this.updatedAt,
        this.minPrice,
        this.bussinessDetails,
    });

    int? id;
    String? vendorId;
    dynamic serviceName;
    dynamic activitieName;
    String? packageName;
    String? shortDetails;
    String? fullDetails;
    String? bannerImage;
    List<String>? moreImages;
    String? price;
    String? disPrice;
    String? priceUnit;
    int? status;
    DateTime? createdAt;
    DateTime? updatedAt;
    String? minPrice;
    BussinessDetails? bussinessDetails;

    factory FVendor.fromJson(Map<String, dynamic> json) => FVendor(
        id: json["id"],
        vendorId: json["vendor_id"],
        serviceName: json["service_name"],
        activitieName: json["activitie_name"],
        packageName: json["package_name"],
        shortDetails: json["short_details"],
        fullDetails: json["full_details"],
        bannerImage: json["banner_image"],
        moreImages: List<String>.from(json["more_images"].map((x) => x)),
        price: json["price"],
        disPrice: json["dis_price"],
        priceUnit: json["price_unit"],
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        minPrice: json["min_price"],
        bussinessDetails: BussinessDetails.fromJson(json["bussiness_details"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "vendor_id": vendorId,
        "service_name": serviceName,
        "activitie_name": activitieName,
        "package_name": packageName,
        "short_details": shortDetails,
        "full_details": fullDetails,
        "banner_image": bannerImage,
        "more_images": List<dynamic>.from(moreImages!.map((x) => x)),
        "price": price,
        "dis_price": disPrice,
        "price_unit": priceUnit,
        "status": status,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
        "min_price": minPrice,
        "bussiness_details": bussinessDetails!.toJson(),
    };
}

class BussinessDetails {
    BussinessDetails({
        this.id,
        this.userId,
        this.categoryId,
        this.bussinessType,
        this.bussinessName,
        this.addressLine1,
        this.addressLine2,
        this.landmark,
        this.pincode,
        this.city,
        this.state,
        this.country,
        this.shortDescription,
        this.latitude,
        this.longitude,
        this.location,
        this.bussinessLogo,
        this.imagesk,
        this.timezone,
        this.doc,
        this.openTime,
        this.closeTime,
        this.createdAt,
        this.updatedAt,
    });

    int? id;
    String? userId;
    List<String>? categoryId;
    dynamic bussinessType;
    String? bussinessName;
    String? addressLine1;
    String? addressLine2;
    dynamic landmark;
    String? pincode;
    String? city;
    String? state;
    String? country;
    String? shortDescription;
    dynamic latitude;
    dynamic longitude;
    dynamic location;
    String? bussinessLogo;
    List<String>? imagesk;
    String? timezone;
    List<String>? doc;
    List<String>? openTime;
    List<String>? closeTime;
    DateTime? createdAt;
    DateTime? updatedAt;

    factory BussinessDetails.fromJson(Map<String, dynamic> json) => BussinessDetails(
        id: json["id"],
        userId: json["user_id"],
        categoryId: List<String>.from(json["category_id"].map((x) => x)),
        bussinessType: json["bussiness_type"],
        bussinessName: json["bussiness_name"],
        addressLine1: json["address_line1"],
        addressLine2: json["address_line2"],
        landmark: json["landmark"],
        pincode: json["pincode"],
        city: json["city"],
        state: json["state"],
        country: json["country"],
        shortDescription: json["short_description"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        location: json["location"],
        bussinessLogo: json["bussiness_logo"],
        imagesk: List<String>.from(json["imagesk"].map((x) => x)),
        timezone: json["timezone"],
        doc: List<String>.from(json["doc"].map((x) => x)),
        openTime: List<String>.from(json["open_time"].map((x) => x == null ? 'null' : x)),
        closeTime: List<String>.from(json["close_time"].map((x) => x == null ? 'null' : x)),
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "category_id": List<dynamic>.from(categoryId!.map((x) => x)),
        "bussiness_type": bussinessType,
        "bussiness_name": bussinessName,
        "address_line1": addressLine1,
        "address_line2": addressLine2,
        "landmark": landmark,
        "pincode": pincode,
        "city": city,
        "state": state,
        "country": country,
        "short_description": shortDescription,
        "latitude": latitude,
        "longitude": longitude,
        "location": location,
        "bussiness_logo": bussinessLogo,
        "imagesk": List<dynamic>.from(imagesk!.map((x) => x)),
        "timezone": timezone,
        "doc": List<dynamic>.from(doc!.map((x) => x)),
        "open_time": List<dynamic>.from(openTime!.map((x) => x == null ? 'null' : x)),
        "close_time": List<dynamic>.from(closeTime!.map((x) => x == null ? 'null' : x)),
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
    };
}
