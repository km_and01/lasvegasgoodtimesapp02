// To parse this JSON data, do
//
//     final customizeModel = customizeModelFromJson(jsonString);

import 'dart:convert';

CustomizeModel customizeModelFromJson(String str) => CustomizeModel.fromJson(json.decode(str));

String customizeModelToJson(CustomizeModel data) => json.encode(data.toJson());

class CustomizeModel {
    CustomizeModel({
        this.message,
        this.statusCode,
        this.services,
    });

    String? message;
    int? statusCode;
    Services? services;

    factory CustomizeModel.fromJson(Map<String, dynamic> json) => CustomizeModel(
        message: json["message"],
        statusCode: json["status_code"],
        services: Services.fromJson(json["services"]),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "status_code": statusCode,
        "services": services!.toJson(),
    };
}

class Services {
    Services({
        this.vendorId,
        this.name,
        this.age,
        this.height,
        this.phone,
        this.email,
        this.gender,
        this.description,
        this.range,
        this.updatedAt,
        this.createdAt,
        this.id,
    });

    String? vendorId;
    String? name;
    String? age;
    String? height;
    String? phone;
    String? email;
    String? gender;
    String? description;
    String? range;
    DateTime? updatedAt;
    DateTime? createdAt;
    int? id;

    factory Services.fromJson(Map<String, dynamic> json) => Services(
        vendorId: json["vendor_id"],
        name: json["name"],
        age: json["age"],
        height: json["height"],
        phone: json["phone"],
        email: json["email"],
        gender: json["gender"],
        description: json["description"],
        range: json["range"],
        updatedAt: DateTime.parse(json["updated_at"]),
        createdAt: DateTime.parse(json["created_at"]),
        id: json["id"],
    );

    Map<String, dynamic> toJson() => {
        "vendor_id": vendorId,
        "name": name,
        "age": age,
        "height": height,
        "phone": phone,
        "email": email,
        "gender": gender,
        "description": description,
        "range": range,
        "updated_at": updatedAt!.toIso8601String(),
        "created_at": createdAt!.toIso8601String(),
        "id": id,
    };
}
