// To parse this JSON data, do
//
//     final fashionBookingModel = fashionBookingModelFromJson(jsonString);

import 'dart:convert';

FashionBookingModel fashionBookingModelFromJson(String str) => FashionBookingModel.fromJson(json.decode(str));

String fashionBookingModelToJson(FashionBookingModel data) => json.encode(data.toJson());

class FashionBookingModel {
    FashionBookingModel({
        this.message,
        this.statusCode,
        this.services,
    });

    String? message;
    int? statusCode;
    Services? services;

    factory FashionBookingModel.fromJson(Map<String, dynamic> json) => FashionBookingModel(
        message: json["message"],
        statusCode: json["status_code"],
        services: Services.fromJson(json["services"]),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "status_code": statusCode,
        "services": services!.toJson(),
    };
}

class Services {
    Services({
        this.bookingNumber,
        this.userId,
        this.vendorId,
        this.categoryId,
        this.categoryName,
        this.name,
        this.subServiceId,
        this.amount,
        this.updatedAt,
        this.createdAt,
        this.id,
    });

    String? bookingNumber;
    int? userId;
    String? vendorId;
    String? categoryId;
    String? categoryName;
    dynamic name;
    String? subServiceId;
    String? amount;
    DateTime? updatedAt;
    DateTime? createdAt;
    int? id;

    factory Services.fromJson(Map<String, dynamic> json) => Services(
        bookingNumber: json["booking_number"],
        userId: json["user_id"],
        vendorId: json["vendor_id"],
        categoryId: json["category_id"],
        categoryName: json["category_name"],
        name: json["name"],
        subServiceId: json["sub_service_id"],
        amount: json["amount"],
        updatedAt: DateTime.parse(json["updated_at"]),
        createdAt: DateTime.parse(json["created_at"]),
        id: json["id"],
    );

    Map<String, dynamic> toJson() => {
        "booking_number": bookingNumber,
        "user_id": userId,
        "vendor_id": vendorId,
        "category_id": categoryId,
        "category_name": categoryName,
        "name": name,
        "sub_service_id": subServiceId,
        "amount": amount,
        "updated_at": updatedAt!.toIso8601String(),
        "created_at": createdAt!.toIso8601String(),
        "id": id,
    };
}
