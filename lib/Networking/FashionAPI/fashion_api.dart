import 'package:lasvegas_gts_app/Networking/FashionAPI/customize_model.dart';
import 'package:lasvegas_gts_app/Networking/FashionAPI/fashion_booking_model.dart';
import 'package:lasvegas_gts_app/Networking/FashionAPI/fashion_product_model.dart';
import 'package:lasvegas_gts_app/Networking/FashionAPI/fashon_vendor_model.dart';
import 'package:lasvegas_gts_app/Networking/RentalsAPI/can_name_models.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';
import 'package:lasvegas_gts_app/screens/services/fashion/city_model.dart';
import 'package:lasvegas_gts_app/screens/services/fashion/country_mode.dart';
import 'package:lasvegas_gts_app/screens/services/fashion/state_model.dart';

Uri urlcarlist =
    Uri.parse("http://koolmindapps.com/lasvegas/public/api/v1/car_name");

class FashionAPIs {
  Future getFashionVendorData() async {
    var res = await http.post(
      Uri.parse('http://koolmindapps.com/lasvegas/public/api/v1/fashionvendor'),
      headers: {
        'Authorization': "Bearer " + homeToken,
        'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
      },
    );
    print(res.body);

    if (res.statusCode == 200) {
      print(res.body);

      final responseJson = json.decode(res.body);
      print(responseJson);
      var data = FashionVendorModel.fromJson(responseJson);
      print(data.message);
      print("Restaurent DATA in APIs =====" +
          data.data[0].bussinessDetails!.shortDescription.toString());
      // List<Airport>? airportList;

      // print(airportList?[0].name);

      return data.data;
    } else {
      print(res.statusCode);
    }
  }


  Future getFashionProductData(String vendor_id) async {
    var res = await http.post(
        Uri.parse(
            'http://koolmindapps.com/lasvegas/public/api/v1/fashionservice'),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "vendor_id": vendor_id,
        });
    print(res.body);

    if (res.statusCode == 200) {
      print(res.body);

      final responseJson = json.decode(res.body);
      print(responseJson);
      var data = FashionProductModel.fromJson(responseJson);
      print(data.message);
      print("Restaurent DATA in APIs =====" +
          data.data[0].bussinessDetails!.shortDescription.toString());
      // List<Airport>? airportList;

      // print(airportList?[0].name);

      return data.data;
    } else {
      print(res.statusCode);
    }
  }

  Future AddProductToCart(String pID, String pAmout) async {
    var res = await http.post(
        Uri.parse(
            'http://koolmindapps.com/lasvegas/public/api/v1/addtocartfashion'),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "product_id": pID,
          "product_price": pAmout,
        });
    print(res.body);
    print("Product added to cart");
  }


Future bookFashionProduct(String vendor_id,String productType,String productPrice, String add1, String add2, String country, String state, String city) async{
 var res = await http.post(
        Uri.parse('http://koolmindapps.com/lasvegas/public/api/v1/bookfashion'),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "vendor_id": vendor_id,
          "product_type": productType,
          "product_price": productPrice,
          "address_line1": add1,
          "address_line2": add2,
          "country": country,
          "state": state,
          "city" : city,
        });
    print(res.body);
    if (res.statusCode == 200) {
      print(res.body);

      final responseJson = json.decode(res.body);
      print(responseJson);
      var data = FashionBookingModel.fromJson(responseJson);
      print(data.message);

      

      return data;
    } else {
      print(res.statusCode);
      return ;
      
    }
    
}


  Future<CustomizeModel> customizedNowData(
      String vendorId,
      String cusName,
      String cusAge,
      String cusHeight,
      String cusPhone,
      String cusEmail,
      String cusGender,
      String cusDescription,
      String rangeFrom,
      String rangeTo) async {
    var res = await http.post(
        Uri.parse('http://koolmindapps.com/lasvegas/public/api/v1/customize'),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "vendor_id": vendorId,
          "name": cusName,
          "age": cusAge,
          "height": cusHeight,
          "phone": cusPhone,
          "email": cusEmail,
          "gender": cusGender,
          "description": cusDescription,
          "range[]": rangeFrom,
          "range[]": rangeTo,
        });
    print(res.body);

    // if (res.statusCode == 200) {
      print(res.body);

      final responseJson = json.decode(res.body);
      print(responseJson);
      var data = CustomizeModel.fromJson(responseJson);
      print(data.message);

      // List<Airport>? airportList;

      // print(airportList?[0].name);

      return data;
    // } else {
    //   print(res.statusCode);
    //   return ;
      
    // }
  }




Future<List<CountryData>?> getCountryList(
    String token,
  ) async {
    var res = await http.get(
       Uri.parse('http://koolmindapps.com/lasvegas/public/api/v1/country'),
        headers: {
      'Authorization': "Bearer " + token,
      'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
    });

    print(res.body);

    final responseJson = json.decode(res.body);
    print(res.body);
    var dataco = CountryModel.fromJson(responseJson);
    print(dataco);
    List<CountryData>? countryList = dataco.data!;

    print(countryList[0].name);

    return countryList;
  }



  Future<List<StateData>?> getStateList(
    String token,String countryID
  ) async {
    var res = await http.post(
       Uri.parse('http://koolmindapps.com/lasvegas/public/api/v1/state'),
        headers: {
      'Authorization': "Bearer " + token,
      'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
    },
    body: {
      "id":countryID,
    }
    );

    print(res.body);

    final responseJson = json.decode(res.body);
    print(res.body);
    var dataco = StateModel.fromJson(responseJson);
    print(dataco);
    List<StateData>? stateList = dataco.data!;

    print(stateList[0].name);

    return stateList;
  }



  Future<List<CityData>> getCityList(
    String token,String stateID
  ) async {
    var res = await http.post(
       Uri.parse('http://koolmindapps.com/lasvegas/public/api/v1/city'),
        headers: {
      'Authorization': "Bearer " + token,
      'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
    },
    body: {
      "id":stateID,
    }
    );

    print(res.body);

    final responseJson = json.decode(res.body);
    print(res.body);
    var dataco = CityModel.fromJson(responseJson);
    print(dataco);
    List<CityData>? cityList = dataco.data!;

    print(cityList[0].name);

    return cityList;
  }

}
