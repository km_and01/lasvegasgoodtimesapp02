// To parse this JSON data, do
//
//     final bookingHistoryModel = bookingHistoryModelFromJson(jsonString);

import 'dart:convert';

BookingHistoryModel bookingHistoryModelFromJson(String str) => BookingHistoryModel.fromJson(json.decode(str));

String bookingHistoryModelToJson(BookingHistoryModel data) => json.encode(data.toJson());

class BookingHistoryModel {
  BookingHistoryModel({
    this.message,
    this.statusCode,
    this.bookings,
  });

  String? message;
  int? statusCode;
  List<Booking>? bookings;

  factory BookingHistoryModel.fromJson(Map<String, dynamic> json) => BookingHistoryModel(
    message: json["message"],
    statusCode: json["status_code"],
    bookings: List<Booking>.from(json["data"].map((x) => Booking.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "message": message,
    "status_code": statusCode,
    "data": List<dynamic>.from(bookings!.map((x) => x.toJson())),
  };
}

class Booking {
  Booking({
    this.id,
    this.bookingNumber,
    this.userId,
    this.vendorId,
    this.categoryId,
    this.categoryName,
    this.subServiceId,
    this.person,
    this.packageId,
    this.amount,
    this.depositAmm,
    this.checkIn,
    this.checkOut,
    this.time,
    this.formair,
    this.toair,
    this.noOfRoom,
    this.venue,
    this.funUserName,
    this.address,
    this.name,
    this.imagePath,
    this.paymentType,
    this.transactionId,
    this.declineMsg,
    this.status,
    this.planeType,
    this.foodName,
    this.createdAt,
    this.updatedAt,
  });

  int? id;
  String? bookingNumber;
  String? userId;
  String? vendorId;
  String? categoryId;
  String? categoryName;
  String? subServiceId;
  String? person;
  String? packageId;
  String? amount;
  String? depositAmm;
  String? checkIn;
  String? checkOut;
  String? time;
  String? formair;
  String? toair;
  int? noOfRoom;
  String? venue;
  dynamic funUserName;
  String? address;
  String? name;
  String? imagePath;
  dynamic paymentType;
  dynamic transactionId;
  dynamic declineMsg;
  int? status;
  dynamic planeType;
  String? foodName;
  String? createdAt;
  String? updatedAt;

  factory Booking.fromJson(Map<String, dynamic> json) => Booking(
    id: json["id"],
    bookingNumber: json["booking_number"],
    userId: json["user_id"],
    vendorId: json["vendor_id"],
    categoryId: json["category_id"],
    categoryName: json["category_name"],
    subServiceId: json["sub_service_id"] == null ? null : json["sub_service_id"],
    person: json["person"] == null ? null : json["person"],
    packageId: json["package_id"] == null ? null : json["package_id"],
    amount: json["amount"],
    depositAmm: json["deposit_amm"] == null ? null : json["deposit_amm"],
    checkIn: json["check_in"] == null ? null : json["check_in"],
    checkOut: json["check_out"] == null ? null : json["check_out"],
    time: json["time"] == null ? null : json["time"],
    formair: json["formair"] == null ? null : json["formair"],
    toair: json["toair"] == null ? null : json["toair"],
    noOfRoom: json["no_of_room"] == null ? null : json["no_of_room"],
    venue: json["venue"] == null ? null : json["venue"],
    funUserName: json["fun_user_name"],
    address: json["address"] == null ? null : json["address"],
    name: json["name"] == null ? null : json["name"],
    imagePath: json["image_path"],
    paymentType: json["payment_type"],
    transactionId: json["transaction_id"],
    declineMsg: json["decline_msg"],
    status: json["status"],
    planeType: json["plane_type"],
    foodName: json["food_name"] == null ? null : json["food_name"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "booking_number": bookingNumber,
    "user_id": userId,
    "vendor_id": vendorId,
    "category_id": categoryId,
    "category_name": categoryName,
    "sub_service_id": subServiceId == null ? null : subServiceId,
    "person": person == null ? null : person,
    "package_id": packageId == null ? null : packageId,
    "amount": amount,
    "deposit_amm": depositAmm == null ? null : depositAmm,
    "check_in": checkIn == null ? null : checkIn,
    "check_out": checkOut == null ? null : checkOut,
    "time": time == null ? null : time,
    "formair": formair == null ? null : formair,
    "toair": toair == null ? null : toair,
    "no_of_room": noOfRoom == null ? null : noOfRoom,
    "venue": venue == null ? null : venue,
    "fun_user_name": funUserName,
    "address": address == null ? null : address,
    "name": name == null ? null : name,
    "image_path": imagePath,
    "payment_type": paymentType,
    "transaction_id": transactionId,
    "decline_msg": declineMsg,
    "status": status,
    "plane_type": planeType,
    "food_name": foodName == null ? null : foodName,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}
