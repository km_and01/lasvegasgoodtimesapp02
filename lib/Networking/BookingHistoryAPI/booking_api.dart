
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:lasvegas_gts_app/Networking/base_url_api.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';
import 'booking_history_model.dart';



class BookingHistoryAPI {
  Future<List<Booking>> getBookingHistory() async {

    List<Booking> bookingList = [];

    var res = await http.get(
      Uri.parse('${UtilsString().BASE_URL}bookinghistory'),
      headers: {
        'Authorization': "Bearer " + homeToken,
        'API_KEY': UtilsString().API_KEY,
      },
    );
    print(res.body);

    if (res.statusCode == 200) {
      print(res.body);

      final responseJson = json.decode(res.body);
      print(responseJson);
      var data = BookingHistoryModel.fromJson(responseJson);
      bookingList = data.bookings!;
      print('BookingsDATA ${bookingList[0].toJson().toString()}');
      // List<Airport>? airportList;
      // print(airportList?[0].name);
      return bookingList;
    } else {
      return bookingList;
      print(res.statusCode);
    }
  }


}
