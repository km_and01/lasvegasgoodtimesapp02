// To parse this JSON data, do
//
//     final eventBookingModel = eventBookingModelFromJson(jsonString);

import 'dart:convert';

EventBookingModel eventBookingModelFromJson(String str) => EventBookingModel.fromJson(json.decode(str));

String eventBookingModelToJson(EventBookingModel data) => json.encode(data.toJson());

class EventBookingModel {
    EventBookingModel({
        this.message,
        this.statusCode,
    });

    String? message;
    int? statusCode;

    factory EventBookingModel.fromJson(Map<String, dynamic> json) => EventBookingModel(
        message: json["message"],
        statusCode: json["status_code"],
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "status_code": statusCode,
    };
}
