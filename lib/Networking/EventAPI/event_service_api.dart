// ignore_for_file: avoid_print

import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:lasvegas_gts_app/Networking/EventAPI/event_booking_model.dart';
import 'package:lasvegas_gts_app/Networking/EventAPI/event_pkg_model.dart';
import 'package:lasvegas_gts_app/Networking/EventAPI/event_vendor_model.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';

class EventAPIs {
  /* Future getFunActivities() async {
    var res = await http.get(
      Uri.parse('http://koolmindapps.com/lasvegas/public/api/v1/funextra'),
      headers: {
        'Authorization': "Bearer " + homeToken,
        'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
      },
    );
    List<FunAct> activities = [];

    if (res.statusCode == 200) {
      final responseJson = json.decode(res.body);
      var data = FunActivityListModel.fromJson(responseJson);
      print(data.eatdish![0].serviceName!);
      return activities = data.eatdish!;
    }
  } */

  Future getEventVendorData(String city_id) async {
    var res = await http.post(
        Uri.parse('http://koolmindapps.com/lasvegas/public/api/v1/eventvendor'),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "city": city_id,
        });
    print(res.body);

    if (res.statusCode == 200) {
      print(res.body);

      final responseJson = json.decode(res.body);
      print(responseJson);
      var data = EventVendorModel.fromJson(responseJson);
      print(data.message);
      return data.services;
    } else {
      print(res.statusCode);
    }
  }

  Future bookEvent(String vendorId, imagePath, serviceID, eventName, amount,
      uNameEvent, uMobEvent, uEmailEvent) async {
    try {
      var res = await http.post(
          Uri.parse(
              'http://koolmindapps.com/lasvegas/public/api/v1/bookeventservice'),
          headers: {
            'Authorization': "Bearer " + homeToken,
            'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
          },
          body: {
            "vendor_id": vendorId,
            "service_id": serviceID,
            "img1": imagePath,
            "price": amount,
            "event_name": eventName,
            "name_name": uNameEvent,
            "email": uEmailEvent,
            "mobile_number": uMobEvent,
          });
      if (res.statusCode == 200) {
        print(res.body);

        final responseJson = json.decode(res.body);
        print(responseJson);
        var data = EventBookingModel.fromJson(responseJson);
        print(data.message);

        return data.message;
      } else {
        print(res.statusCode);
      }
    } catch (e) {
      print(e);
      return e;
    }
  }

  Future getEventPkgData(String vendor_id) async {
    var res = await http.post(
        Uri.parse(
            'http://koolmindapps.com/lasvegas/public/api/v1/eventvendorservice'),
        headers: {
          'Authorization': "Bearer " + homeToken,
          'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        },
        body: {
          "vendor_id": vendor_id,
        });
    print(res.body);

    if (res.statusCode == 200) {
      print(res.body);

      final responseJson = json.decode(res.body);
      print(responseJson);
      var data = EventPkgModel.fromJson(responseJson);
      print(data.message);
      print("Restaurent DATA in APIs =====" +
          data.services![0].bussinessDetails!.shortDescription.toString());
      // List<Airport>? airportList;

      // print(airportList?[0].name);

      return data.services;
    } else {
      print(res.statusCode);
    }
  }
}
