// To parse this JSON data, do
//
//     final eventVendorModel = eventVendorModelFromJson(jsonString);

import 'dart:convert';

EventVendorModel eventVendorModelFromJson(String str) => EventVendorModel.fromJson(json.decode(str));

String eventVendorModelToJson(EventVendorModel data) => json.encode(data.toJson());

class EventVendorModel {
    EventVendorModel({
        this.message,
        this.statusCode,
        this.services,
    });

    String? message;
    int? statusCode;
    List<EventV>? services;

    factory EventVendorModel.fromJson(Map<String, dynamic> json) => EventVendorModel(
        message: json["message"],
        statusCode: json["status_code"],
        services: List<EventV>.from(json["services"].map((x) => EventV.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "status_code": statusCode,
        "services": List<dynamic>.from(services!.map((x) => x.toJson())),
    };
}

class EventV {
    EventV({
        this.id,
        this.vendorId,
        this.categoryName,
        this.eventServiceName,
        this.eventName,
        this.eventType,
        this.location,
        this.mapImage,
        this.peopleArrive,
        this.totalStall,
        this.stallLocation,
        this.landmark,
        this.area,
        this.stallFor,
        this.price,
        this.priceUnit,
        this.terms,
        this.amenities,
        this.status,
        this.createdAt,
        this.updatedAt,
        this.minPrice,
        this.fillHerat,
        this.bussinessDetails,
    });

    int? id;
    String? vendorId;
    String? categoryName;
    String? eventServiceName;
    String? eventName;
    String? eventType;
    String? location;
    List<String>? mapImage;
    String? peopleArrive;
    String? totalStall;
    List<String>? stallLocation;
    List<String>? landmark;
    List<String>? area;
    List<String>? stallFor;
    List<String>? price;
    List<String>? priceUnit;
    List<String>? terms;
    List<String>? amenities;
    int? status;
    DateTime? createdAt;
    DateTime? updatedAt;
    String? minPrice;
    int? fillHerat;
    BussinessDetails? bussinessDetails;

    factory EventV.fromJson(Map<String, dynamic> json) => EventV(
        id: json["id"],
        vendorId: json["vendor_id"],
        categoryName: json["category_name"],
        eventServiceName: json["event_service_name"],
        eventName: json["event_name"],
        eventType: json["event_type"],
        location: json["location"],
        mapImage: List<String>.from(json["map_image"].map((x) => x)),
        peopleArrive: json["people_arrive"],
        totalStall: json["total_stall"],
        stallLocation: List<String>.from(json["stall_location"].map((x) => x)),
        landmark: List<String>.from(json["landmark"].map((x) => x)),
        area: List<String>.from(json["area"].map((x) => x)),
        stallFor: List<String>.from(json["stall_for"].map((x) => x)),
        price: List<String>.from(json["price"].map((x) => x)),
        priceUnit: List<String>.from(json["price_unit"].map((x) => x)),
        terms: List<String>.from(json["terms"].map((x) => x)),
        amenities: List<String>.from(json["amenities"].map((x) => x)),
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        minPrice: json["min_price"],
        fillHerat: json["fill_herat"],
        bussinessDetails: BussinessDetails.fromJson(json["bussiness_details"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "vendor_id": vendorId,
        "category_name": categoryName,
        "event_service_name": eventServiceName,
        "event_name": eventName,
        "event_type": eventType,
        "location": location,
        "map_image": List<dynamic>.from(mapImage!.map((x) => x)),
        "people_arrive": peopleArrive,
        "total_stall": totalStall,
        "stall_location": List<dynamic>.from(stallLocation!.map((x) => x)),
        "landmark": List<dynamic>.from(landmark!.map((x) => x)),
        "area": List<dynamic>.from(area!.map((x) => x)),
        "stall_for": List<dynamic>.from(stallFor!.map((x) => x)),
        "price": List<dynamic>.from(price!.map((x) => x)),
        "price_unit": List<dynamic>.from(priceUnit!.map((x) => x)),
        "terms": List<dynamic>.from(terms!.map((x) => x)),
        "amenities": List<dynamic>.from(amenities!.map((x) => x)),
        "status": status,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
        "min_price": minPrice,
        "fill_herat": fillHerat,
        "bussiness_details": bussinessDetails!.toJson(),
    };
}

class BussinessDetails {
    BussinessDetails({
        this.id,
        this.userId,
        this.categoryId,
        this.bussinessType,
        this.bussinessName,
        this.addressLine1,
        this.addressLine2,
        this.landmark,
        this.pincode,
        this.city,
        this.state,
        this.country,
        this.shortDescription,
        this.latitude,
        this.longitude,
        this.location,
        this.bussinessLogo,
        this.imagesk,
        this.timezone,
        this.doc,
        this.openTime,
        this.closeTime,
        this.createdAt,
        this.updatedAt,
    });

    int? id;
    String? userId;
    List<String>? categoryId;
    dynamic bussinessType;
    String? bussinessName;
    String? addressLine1;
    String? addressLine2;
    dynamic landmark;
    String? pincode;
    String? city;
    String? state;
    String? country;
    String? shortDescription;
    dynamic latitude;
    dynamic longitude;
    dynamic location;
    String? bussinessLogo;
    List<String>? imagesk;
    String? timezone;
    List<String>? doc;
    List<String>? openTime;
    List<String>? closeTime;
    DateTime? createdAt;
    DateTime? updatedAt;

    factory BussinessDetails.fromJson(Map<String, dynamic> json) => BussinessDetails(
        id: json["id"],
        userId: json["user_id"],
        categoryId: List<String>.from(json["category_id"].map((x) => x)),
        bussinessType: json["bussiness_type"],
        bussinessName: json["bussiness_name"],
        addressLine1: json["address_line1"],
        addressLine2: json["address_line2"],
        landmark: json["landmark"],
        pincode: json["pincode"],
        city: json["city"],
        state: json["state"],
        country: json["country"],
        shortDescription: json["short_description"],
        latitude: json["latitude"],
        longitude: json["longitude"],
        location: json["location"],
        bussinessLogo: json["bussiness_logo"],
        imagesk: List<String>.from(json["imagesk"].map((x) => x)),
        timezone: json["timezone"],
        doc: List<String>.from(json["doc"].map((x) => x)),
        openTime: List<String>.from(json["open_time"].map((x) => x == null ? 'null' : x)),
        closeTime: List<String>.from(json["close_time"].map((x) => x == null ? 'null' : x)),
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "category_id": List<dynamic>.from(categoryId!.map((x) => x)),
        "bussiness_type": bussinessType,
        "bussiness_name": bussinessName,
        "address_line1": addressLine1,
        "address_line2": addressLine2,
        "landmark": landmark,
        "pincode": pincode,
        "city": city,
        "state": state,
        "country": country,
        "short_description": shortDescription,
        "latitude": latitude,
        "longitude": longitude,
        "location": location,
        "bussiness_logo": bussinessLogo,
        "imagesk": List<dynamic>.from(imagesk!.map((x) => x)),
        "timezone": timezone,
        "doc": List<dynamic>.from(doc!.map((x) => x)),
        "open_time": List<dynamic>.from(openTime!.map((x) => x == null ? 'null' : x)),
        "close_time": List<dynamic>.from(closeTime!.map((x) => x == null ? 'null' : x)),
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
    };
}
