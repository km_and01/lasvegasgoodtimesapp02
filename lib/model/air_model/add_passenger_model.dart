
class AddPassengerModel {
  String? str_name;
  String? str_email;
  String? str_phone;
  String? str_age;

  AddPassengerModel({this.str_name, this.str_email, this.str_phone,
    this.str_age});
}