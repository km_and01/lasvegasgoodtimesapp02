import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/screens/login/login_src.dart';

class RegisterSucess extends StatelessWidget {
  const RegisterSucess({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgcolor,
      body: Container(
        alignment: Alignment.center,
        margin: EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 60.0),
            Image.asset(
              'assets/images/lvgtlogo.png',
              width: 189.0,
              height: 128.0,
            ),
            SizedBox(height: 86.0),
            Image.asset('assets/images/reg_success.png'),
            SizedBox(height: 25.0),
            Text(
              "Successfully Registered!",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 24.0,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 25.0),
            Text(
              "Your profile will be under \nprocess",
              style: TextStyle(
                color: greytxtcolor,
                fontSize: 17.0,
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 120.0),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (_) => LoginScreen()),
                );
              },
              child: Text(
                'CONTINUE',
                style: TextStyle(color: Colors.black),
              ),
              style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(13.0),
                ),
                fixedSize: Size(389.0, 57.0),
                primary: Color(0xFFFFC71E),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
