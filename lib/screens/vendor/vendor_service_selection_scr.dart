import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_card.dart';
import 'package:lasvegas_gts_app/customs/service_grid.dart';
import 'package:lasvegas_gts_app/screens/vendor/reg_successful.dart';
import 'package:lasvegas_gts_app/screens/vendor/vendor_service_selection_card.dart';
import 'package:drag_select_grid_view/drag_select_grid_view.dart';

class ChoseServiceForVendor extends StatefulWidget {
  const ChoseServiceForVendor({Key? key}) : super(key: key);

  @override
  _ChoseServiceForVendorState createState() => _ChoseServiceForVendorState();
}

class _ChoseServiceForVendorState extends State<ChoseServiceForVendor> {
  @override
  Widget build(BuildContext context) {
    // List<Map<String, String>> serviceList = [
    //   {
    //     'icon': "ic_stay",
    //     'title': "Stay",
    //   },
    //   {
    //     'icon': "ic_air_service",
    //     'title': "Air Service",
    //   },
    //   {
    //     'icon': "ic_eat",
    //     'title': "Eat",
    //   },
    //   {
    //     'icon': "ic_event",
    //     'title': "Event",
    //   },
    //   {
    //     'icon': "ic_event_space",
    //     'title': "Event Space",
    //   },
    //   {
    //     'icon': "ic_transportation",
    //     'title': "Transport",
    //   },
    //   {
    //     'icon': "ic_bodyguard",
    //     'title': "Bodyguards",
    //   },
    //   {
    //     'icon': "ic_celeb_appearence",
    //     'title': "Celebrity Appearance",
    //   },
    //   {
    //     'icon': "ic_rental",
    //     'title': "Rental",
    //   },
    //   {
    //     'icon': "ic_party",
    //     'title': "Party",
    //   },
    //   {
    //     'icon': "ic_fasion_retail",
    //     'title': "Fashion / Retails",
    //   },
    //   {
    //     'icon': "ic_wedding_service",
    //     'title': "Wedding Service",
    //   },
    //   {
    //     'icon': "ic_business_services",
    //     'title': "Business Service",
    //   },
    //   {
    //     'icon': "ic_fun",
    //     'title': "Fun",
    //   },
    // ];
    // print(serviceList.length);

    // int optionSelected = 0;
    // checkOption(int index) {
    //   setState(() {
    //     optionSelected = index;
    //   });
    // }

    var serviceList = [
      CoustomCardVendor(
        icon: "ic_stay",
        title: "Stay", isSelected: false,
        index: 1,
        // onTapped: () {

        // },
      ),
      CoustomCardVendor(
        icon: "ic_air_service",
        title: "Air Service", isSelected: false,
        index: 2,
        // onTapped: () {},
      ),
      CoustomCardVendor(
        icon: "ic_eat",
        title: "Eat", isSelected: false,
        index: 3,
        // onTapped: () {},
      ),
      CoustomCardVendor(
        icon: "ic_event",
        title: "Event", isSelected: false,
        index: 4,
        // onTapped: () {},
      ),
      CoustomCardVendor(
        icon: "ic_event_space",
        title: "Event Space", isSelected: false,
        index: 5,
        // onTapped: () {},
      ),
      CoustomCardVendor(
        icon: "ic_transportation",
        title: "Transport", isSelected: false,
        index: 6,
        // onTapped: () {},
      ),
      CoustomCardVendor(
        icon: "ic_bodyguard",
        title: "Bodyguards", isSelected: false,
        index: 7,
        // onTapped: () {},
      ),
      CoustomCardVendor(
        icon: "ic_celeb_appearence",
        title: "Celebrity Appearance", isSelected: false,
        index: 8,
        // onTapped: () {},
      ),
      CoustomCardVendor(
        icon: "ic_rental",
        title: "Rental", isSelected: false,
        index: 9,
        // onTapped: () {},
      ),
      CoustomCardVendor(
        icon: "ic_party",
        title: "Party", isSelected: false,
        index: 10,
        // onTapped: () {},
      ),
      CoustomCardVendor(
        icon: "ic_fasion_retail",
        title: "Fashion / Retails", isSelected: false,
        index: 11,
        // onTapped: () {},
      ),
      CoustomCardVendor(
        icon: "ic_wedding_service",
        title: "Wedding Service", isSelected: false,
        index: 12,
        // onTapped: () {},
      ),
      CoustomCardVendor(
        icon: "ic_business_services",
        title: "Business Service", isSelected: false,
        index: 13,
        // onTapped: () {},
      ),
      CoustomCardVendor(
        icon: "ic_fun",
        title: "Fun", isSelected: false,
        index: 14,
        // onTapped: () {},
      ),
    ];

    return SafeArea(
      child: Scaffold(
        backgroundColor: bgcolor,
        body: Container(
          margin: EdgeInsets.fromLTRB(25.0, 10, 25.0, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Choose your \nBusiness category",
                style: kHeadText,
              ),
              SizedBox(
                height: 50.0,
              ),

              // GridView.count(
              //   crossAxisCount: 3,
              //   crossAxisSpacing: 5.0,
              //   mainAxisSpacing: 5.0,
              //   children: [
              //     for (var i = 0; i < serviceList.length; i++)
              //       CoustomCardVendor(
              //         icon: serviceList[i]['icon'] as String,
              //         title: serviceList[i]['title'] as String,
              //         onTapped:()=>checkOption(i + 1),
              //         isSelected: i + 1 == optionSelected,
              //       )
              //   ],
              // ),

              // DragSelectGridView(
              //     gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              //         crossAxisCount: 3,
              //         crossAxisSpacing: 5.0,
              //         mainAxisSpacing: 5.0),
              //     itemBuilder: (_, index, isSelected) {
              //       return CoustomCardVendor(
              //         isSelected: isSelected,
              //         icon: serviceList[index]['icon'] as String,
              //         title: serviceList[index]['title'] as String,
              //       );
              //     }),

              GridView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      crossAxisSpacing: 5.0,
                      mainAxisSpacing: 5.0),
                  itemCount: serviceList.length,
                  itemBuilder: (_, index) {
                    print(index);
                    bool selected;
                    return serviceList[index];
                    // GestureDetector(
                    //   onTap: () {
                    //     setState(() {
                    //       print("object");
                    //       serviceList[index].isSelected;
                    //       print(serviceList[index].title);
                    //     });
                    //   },
                    //   child: Container(
                    //       alignment: Alignment.center,
                    //       height: 100.0,
                    //       width: 100.0,
                    //       child: Column(
                    //         crossAxisAlignment: CrossAxisAlignment.center,
                    //         mainAxisAlignment: MainAxisAlignment.center,
                    //         children: [
                    //           Image(
                    //             image: AssetImage(
                    //                 "assets/icons/${serviceList[index].icon}.png"),
                    //             width: 30.0,
                    //             height: 30.0,
                    //           ),
                    //           SizedBox(
                    //             height: 8.0,
                    //           ),
                    //           Text(
                    //             serviceList[index].title,
                    //             textAlign: TextAlign.center,
                    //             style: kTxtStyle,
                    //           )
                    //         ],
                    //       ),
                    //       decoration: BoxDecoration(
                    //         color: serviceList[index].isSelected
                    //             ? Colors.white
                    //             : servicecardfillcolor,
                    //         border: Border.all(color: txtborderbluecolor),
                    //         borderRadius:
                    //             BorderRadius.all(Radius.circular(15.0)),
                    //       )),
                    // );
                  }),
              SizedBox(height: 40.0),
              ElevatedButton(
                onPressed: () {
                  // print();
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (_) => RegisterSucess()),
                  );
                },
                child: Text(
                  'CONTINUE',
                  style: TextStyle(color: Colors.black),
                ),
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(13.0),
                  ),
                  fixedSize: Size(389.0, 57.0),
                  primary: Color(0xFFFFC71E),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
