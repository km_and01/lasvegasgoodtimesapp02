import 'dart:io';

import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_card.dart';
import 'package:lasvegas_gts_app/screens/vendor/vendor_service_selection_scr.dart';
import 'package:lasvegas_gts_app/screens/vendor/add_docs.dart';
import 'package:file_picker/file_picker.dart';

class VendorBusinessDetails extends StatefulWidget {
  const VendorBusinessDetails({Key? key}) : super(key: key);

  @override
  _VendorBusinessDetailsState createState() => _VendorBusinessDetailsState();
}

TextEditingController vendorBusinessAddressController = TextEditingController();
TextEditingController vendorBusinessNameController = TextEditingController();
bool? isAccepted = false;

class _VendorBusinessDetailsState extends State<VendorBusinessDetails> {
  List<File>? files;
  PlatformFile? file;
  FilePickerResult? result;
  bool isPicked = false;
  String fileName = "";
  List nameofFiles = [];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFF121A31),
        // appBar: AppBar(
        //   elevation: 0.0,
        //   leading: Icon(Icons.arrow_back),
        //   backgroundColor: Color(0xFF121A31),
        // ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.fromLTRB(16.0, 0, 16.0, 0),
            // alignment: Alignment.centerLeft,
            // padding: EdgeInsets.all(0),
            color: Color(0xFF121A31),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  // crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    ////back btn for exit the app
                    BackButton(
                      color: Colors.white,
                      onPressed: () {},
                    ),
                    SizedBox(
                      width: 100.0,
                    ),
                    Container(
                      height: 90.0,
                      width: 125.0,
                      child: Image.asset('assets/images/lvgtlogo.png'),
                    ),
                  ],
                ),
                SizedBox(height: 80.0),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: const [
                    Text(
                      'Enter business details',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 22.0,
                          letterSpacing: 0.32),
                    ),
                  ],
                ),
                SizedBox(height: 25.0),
                Text(
                  'Please enter your details to create vendor account',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 14.0,
                    letterSpacing: 0.23,
                  ),
                ),
                SizedBox(height: 25.0),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                  child: Text(
                    'BUSINESS NAME',
                    style: TextStyle(
                      fontSize: 12.0,
                      letterSpacing: 0.8,
                      color: Color(0xFF95A0AE),
                    ),
                  ),
                ),
                TextFormField(
                  style: kTxtStyle,
                  controller: vendorBusinessNameController,
                  decoration: kTxtFieldDecoration,
                ),
                SizedBox(height: 25.0),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                  child: Text(
                    'BUISENNESS ADDRESS',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 12.0,
                      letterSpacing: 0.8,
                      color: Color(0xFF95A0AE),
                    ),
                  ),
                ),
                TextFormField(
                  style: kTxtStyle,
                  maxLines: 2,
                  controller: vendorBusinessAddressController,
                  keyboardType: TextInputType.multiline,
                  decoration: kTxtFieldDecoration,
                ),
                SizedBox(height: 25.0),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                  child: Text(
                    'Upload your Business Document \n (Upto 5 document)',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 12.0,
                      letterSpacing: 0.8,
                      color: Color(0xFF95A0AE),
                    ),
                  ),
                ),
                // CoustomCard(
                //   icon: 'ic_document',
                //   title: 'Add Document', DestinationWidget: AddDocs(),
                // ),

                Row(
                  children: [
                    GestureDetector(
                      onTap: () {
                        print("add Docs");

                        setState(() {
                          pickFiles();
                        });
                      },
                      child: Container(
                          alignment: Alignment.center,
                          height: 100.0,
                          width: 100.0,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image(
                                image:
                                    AssetImage("assets/icons/ic_document.png"),
                                width: 30.0,
                                height: 30.0,
                                color: Colors.white,
                              ),
                              SizedBox(
                                height: 8.0,
                              ),
                              Text(
                                "Add Document(s)",
                                textAlign: TextAlign.center,
                                style: kTxtStyle,
                              )
                            ],
                          ),
                          decoration: BoxDecoration(
                            color: servicecardfillcolor,
                            border: Border.all(color: txtborderbluecolor),
                            borderRadius:
                                BorderRadius.all(Radius.circular(15.0)),
                          )),
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    Column(
                      children: [
                        Text(
                          isPicked == false
                              ? "Select Document(s)"
                              : nameofFiles.reversed.toString(),
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontSize: 12.0,
                            letterSpacing: 0.8,
                            color: Color(0xFF95A0AE),
                          ),
                        ),
                        // Text(
                        //   "1. doc",
                        //   textAlign: TextAlign.left,
                        //   style: TextStyle(
                        //     fontSize: 12.0,
                        //     letterSpacing: 0.8,
                        //     color: Color(0xFF95A0AE),
                        //   ),
                        // ),
                        // Text(
                        //   "1. doc",
                        //   textAlign: TextAlign.left,
                        //   style: TextStyle(
                        //     fontSize: 12.0,
                        //     letterSpacing: 0.8,
                        //     color: Color(0xFF95A0AE),
                        //   ),
                        // ),
                        // Text(
                        //   "1. doc",
                        //   textAlign: TextAlign.left,
                        //   style: TextStyle(
                        //     fontSize: 12.0,
                        //     letterSpacing: 0.8,
                        //     color: Color(0xFF95A0AE),
                        //   ),
                        // ),
                        // Text(
                        //   "1. doc",
                        //   textAlign: TextAlign.left,
                        //   style: TextStyle(
                        //     fontSize: 12.0,
                        //     letterSpacing: 0.8,
                        //     color: Color(0xFF95A0AE),
                        //   ),
                        // ),
                      ],
                    ),
                  ],
                ),

                SizedBox(height: 15.0),
                CheckboxListTile(
                  activeColor: btngoldcolor,
                  controlAffinity: ListTileControlAffinity.leading,
                  // activeColor: Colors.white,
                  // selectedTileColor: Colors.amber,
                  value: isAccepted,
                  onChanged: (val) {
                    setState(() {
                      isAccepted = val;
                    });
                  },
                  title: Text(
                    "Accept terms and condition",
                    style: kTxtStyle,
                  ),
                ),
                SizedBox(height: 25.0),
                isAccepted == true
                    ? ElevatedButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (_) => ChoseServiceForVendor()),
                          );
                        },
                        child: Text(
                          'CONTINUE',
                          style: TextStyle(color: Colors.black),
                        ),
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(13.0),
                          ),
                          fixedSize: Size(389.0, 57.0),
                          primary: Color(0xFFFFC71E),
                        ),
                      )
                    : ElevatedButton(
                        onPressed: () {},
                        child: Text(
                          'CONTINUE',
                          style: TextStyle(color: Colors.black),
                        ),
                        style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(13.0),
                          ),
                          fixedSize: Size(389.0, 57.0),
                          primary: Color(0x55FFC71E),
                        ),
                      ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> pickFiles() async {
    result = await FilePicker.platform.pickFiles(
      allowMultiple: true,
      type: FileType.custom,
      allowedExtensions: ['jpg', 'pdf', 'png', 'doc', 'xls'],
    );
    setState(() {});
    print(result!.count);

    if (result != null) {
      files = result!.paths.map((path) => File(path!)).toList();
      for (var i = result!.count; i > 0; i--) {
        file = result!.files[i - 1]; // .first;
        fileName = file!.name;
        nameofFiles.add(fileName);
        // print(fileName);
      }
      // print(files.single);
      print(nameofFiles);
      isPicked = true;
      // print(file.bytes);
      // print(file.size);
      // print(file.extension);
      // print(file.path);

      // ////// TO SAVE FILES
      // String? outputFile = await FilePicker.platform.saveFile(
      // dialogTitle: 'Please select an output file:',
      // fileName: 'output-file.pdf',
      //   );

// // Upload file
//    Uint8List fileBytes = result.files.first.bytes;
//   String fileName = result.files.first.name;

//   await FirebaseStorage.instance.ref('uploads/$fileName').putData(fileBytes);
    } else {
      // User canceled the picker
    }
  }
}
