import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/screens/userRegistration/otp_verification_screen.dart';
import 'package:lasvegas_gts_app/screens/vendor/vendor_business_details_form.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class VendorPersonalDetail extends StatefulWidget {
  const VendorPersonalDetail({Key? key}) : super(key: key);

  @override
  _VendorPersonalDetailState createState() => _VendorPersonalDetailState();
}

TextEditingController vendorEmailController = TextEditingController();
TextEditingController vendorAddressController = TextEditingController();
TextEditingController vendorNameController = TextEditingController();
TextEditingController vendorPhoneNoController = TextEditingController();

class _VendorPersonalDetailState extends State<VendorPersonalDetail> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFF121A31),
        // appBar: AppBar(
        //   elevation: 0.0,
        //   leading: Icon(Icons.arrow_back),
        //   backgroundColor: Color(0xFF121A31),
        // ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              padding: EdgeInsets.fromLTRB(16.0, 0, 16.0, 0),
              // alignment: Alignment.centerLeft,
              // padding: EdgeInsets.all(0),
              color: Color(0xFF121A31),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    // crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      ////back btn for exit the app
                      /* BackButton(
                        color: Colors.white,
                        onPressed: () {},
                      ), */
                      /* SizedBox(
                        width: 100.0,
                      ), */
                      SizedBox(
                        height: 200.0,
                        width: 200.0,
                        child: Image.asset('assets/images/Exprezzzo_logo.png'),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: const [
                      Text(
                        'Enter personal details',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 22.0,
                            letterSpacing: 0.32),
                      ),
                    ],
                  ),
                  SizedBox(height: 25.0),
                  Text(
                    'Please enter your details to create vendor account',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14.0,
                      letterSpacing: 0.23,
                    ),
                  ),
                  SizedBox(height: 25.0),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                    child: Text(
                      'FULL NAME',
                      style: TextStyle(
                        fontSize: 12.0,
                        letterSpacing: 0.8,
                        color: Color(0xFF95A0AE),
                      ),
                    ),
                  ),
                  TextFormField(
                    style: kTxtStyle,
                    controller: vendorNameController,
                    decoration: kTxtFieldDecoration,
                  ),
                  SizedBox(height: 25.0),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                    child: Text(
                      'PHONE NUMBER',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 12.0,
                        letterSpacing: 0.8,
                        color: Color(0xFF95A0AE),
                      ),
                    ),
                  ),
                  TextFormField(
                    style: kTxtStyle,
                    keyboardType: TextInputType.phone,
                    controller: vendorPhoneNoController,
                    decoration: kTxtFieldDecoration,
                  ),
                  SizedBox(height: 25.0),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                    child: Text(
                      'EMAIL ADDRESS',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 12.0,
                        letterSpacing: 0.8,
                        color: Color(0xFF95A0AE),
                      ),
                    ),
                  ),
                  TextFormField(
                    style: kTxtStyle,
                    keyboardType: TextInputType.emailAddress,
                    controller: vendorEmailController,
                    decoration: kTxtFieldDecoration,
                  ),
                  SizedBox(height: 25.0),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                    child: Text(
                      'ADDRESS',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 12.0,
                        letterSpacing: 0.8,
                        color: Color(0xFF95A0AE),
                      ),
                    ),
                  ),
                  TextFormField(
                    style: kTxtStyle,
                    maxLines: 2,
                    controller: vendorAddressController,
                    keyboardType: TextInputType.multiline,
                    decoration: kTxtFieldDecoration,
                  ),
                  SizedBox(height: 25.0),
                  ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => VendorBusinessDetails(),
                        ),
                      );
                    },
                    child: Text(
                      'CONTINUE',
                      style: TextStyle(color: Colors.black),
                    ),
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(13.0),
                      ),
                      fixedSize: Size(410.0, 57.0),
                      primary: Color(0xFFFFC71E),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
