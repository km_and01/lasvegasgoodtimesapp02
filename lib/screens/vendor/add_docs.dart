import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';

class AddDocs extends StatefulWidget {
  const AddDocs({ Key? key }) : super(key: key);

  @override
  _AddDocsState createState() => _AddDocsState();
}

class _AddDocsState extends State<AddDocs> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          leading: Image.asset("assets/icons/ic_document.png", color: Colors.white,),
          title: Text(
            "Add Document(s)",
            style: kHeadText,
          ),
        ),
        backgroundColor: bgcolor,
        body: Container(
          child: Column(
            children: [
              const SizedBox(
                height: 30.0,
              ),
              const Divider(
                thickness: 2,
                height: 2,
                color: Colors.amber,
              ),
              const SizedBox(
                height: 3.0,
              ),
              const DottedLine(
                lineThickness: 4.5,
                dashRadius: 7,
                dashLength: 4.0,
                // lineLength:  MediaQuery.of(context).size.width,
                dashColor: Colors.amber,
              ),
              const SizedBox(
                height: 30.0,
              ),
            ],
          ),
        ),
      ),
    );
  
  }
}