import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:badges/badges.dart';

class CoustomCardVendor extends StatefulWidget {
  final String icon;
  final String title;
  // final Function? onTapped;
  bool isSelected;
  int? index;

  CoustomCardVendor({
    this.index,
    required this.icon,
    required this.title,
    required this.isSelected,
  });

  @override
  State<CoustomCardVendor> createState() => _CoustomCardVendorState();
}

class _CoustomCardVendorState extends State<CoustomCardVendor> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GestureDetector(
        onTap: () {
          setState(() {
            if (widget.isSelected == true) {
              widget.isSelected = false;
            } else {
              widget.isSelected = true;
            }
          });
          print(widget.index);
        },
        child: Badge(
          elevation: 0.0,
          badgeColor: Colors.transparent,
          badgeContent: widget.isSelected
              ? Image.asset(
                  "assets/icons/ic_tick.png",
                  height: 20,
                  width: 20,
                )
              : SizedBox(),
          child: Container(
              alignment: Alignment.center,
              height: 100.0,
              width: 100.0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image(
                    image: AssetImage("assets/icons/${widget.icon}.png"),
                    width: 30.0,
                    height: 30.0,
                    color: Colors.white,
                  ),
                  SizedBox(
                    height: 8.0,
                  ),
                  Text(
                    widget.title,
                    textAlign: TextAlign.center,
                    style: kTxtStyle,
                  )
                ],
              ),
              decoration: BoxDecoration(
                color: widget.isSelected
                    ? servicecardfillcolor
                    : Colors.transparent,
                border: Border.all(color: txtborderbluecolor),
                borderRadius: BorderRadius.all(Radius.circular(15.0)),
              )),
        ),
      ),
    );
  }
}
