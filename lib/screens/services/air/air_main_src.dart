import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/AirAPI/air_service_apis.dart';
import 'package:lasvegas_gts_app/Networking/AirAPI/airport_list_model.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';
import 'package:lasvegas_gts_app/screens/login/login_src.dart';
import 'package:lasvegas_gts_app/screens/services/air/air_flight_list_scr.dart';
import 'package:lasvegas_gts_app/screens/services/stay/sta_search_main.dart';
import 'package:autocomplete_textfield_ns/autocomplete_textfield_ns.dart';
import 'package:numberpicker/numberpicker.dart';
import 'dart:ui' as ui;

bool isLoading = false;
String userToken = '';

class AirHome extends StatefulWidget {
  final String uToken;
  const AirHome({Key? key, required this.uToken}) : super(key: key);

  @override
  _AirHomeState createState() => _AirHomeState();
}

late BuildContext dialogcontexxt;
bool isJet = true;
bool isHelicopter = false;
bool isRound = false;
bool isoneway = true;
String totalPassengers = '1';

class _AirHomeState extends State<AirHome> {
  DateTime dateTime = DateTime.now();
  String departureDate = '';
  String returnDate = '';
  List<String> fromAirportList = [];
  List<String> toAirportList = [];
  List<String> fromAirportCodeList = [];
  List<String> toAirportCodeList = [];
  String fromairportCode = '';
  String toairportCode = '';
  TextEditingController fromAirportController = TextEditingController();
  String fromAirportSelected = '';
  TextEditingController toAirportController = TextEditingController();
  String toAirportSelected = '';
  GlobalKey<AutoCompleteTextFieldState<String>> fromAirportKey =
      new GlobalKey();

  @override
  void initState() {
    userToken = widget.uToken;
    fetchAirportList();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: GestureDetector(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (_) => FlightList(
                        aircraft: isHelicopter ? 'Helicopter' : 'Private jets',
                        departureDate: departureDate,
                        destination: toairportCode,
                        origin: fromairportCode,
                        passengers: totalPassengers,
                        returnDate: returnDate,
                        fromAP: fromairportCode,
                        fromAPC: fromAirportSelected,
                        toAP: toairportCode,
                        toAPC: toAirportSelected,
                      )));
        },
        child: Container(
          alignment: Alignment.center,
          child: Text(
            "SEARCH NOW",
            style: TextStyle(
                fontSize: 18.0,
                letterSpacing: 0.27,
                fontFamily: 'Helvetica',
                color: Color(0xff121A31)),
          ),
          height: 57.0,
          color: btngoldcolor,
        ),
      ),
      appBar: CustomAppBar(
        lead: Image.asset(
          "assets/icons/ic_air_service.png",
          color: Colors.white,
          height: 24.0,
          width: 24.0,
        ),
        title: Text(
          "Air Jet",
          style: TextStyle(
            color: Colors.white,
            fontFamily: 'Helvetica',
            fontSize: 18.0,
            letterSpacing: 0.29
          ),
        ),
      ),
      backgroundColor: bgcolor,
      body: isLoading
          ? CustomLoadingScr()
          : SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.all(25.0),
                child: Column(
                  children: [
                    // SizedBox(
                    //   height: 30.0,
                    // ),

                    // TextFormField(
                    //   decoration: kSearchTxtFieldDecoration.copyWith(
                    //     fillColor: servicecardfillcolor,
                    //     filled: true,
                    //   ),
                    // ),
                    SizedBox(
                      height: 20.0,
                    ),

                    //////Category selection Tab
                    Row(
                      children: [
                        /////Jet
                        Column(
                          children: [
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  isJet = true;
                                  isHelicopter = false;
                                });
                              },
                              child: Container(
                                alignment: Alignment.center,
                                child: Image.asset(
                                  "assets/icons/ic_jet.png",
                                  height: 30.0,
                                  width: 30.0,
                                ),
                                decoration: BoxDecoration(
                                  color: isJet
                                      ? Colors.white
                                      : servicecardfillcolor,
                                  borderRadius: BorderRadius.circular(40),
                                  boxShadow: const [
                                    BoxShadow(
                                        color: txtborderbluecolor,
                                        spreadRadius: 1.5),
                                  ],
                                ),
                                height: 69.0,
                                width: 69.0,

                                //  kTxtFieldDecoration .copyWith(),
                              ),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              "Private Jet",
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'Raleway-Medium',
                                fontSize: 12.0
                              ),
                            ),
                          ],
                        ),

                        SizedBox(
                          width: 13.0,
                        ),

                        /////Helicopter
                        Column(
                          children: [
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  isHelicopter == false
                                      ? isHelicopter = true
                                      : isHelicopter = false;
                                  isJet = false;
                                });
                              },
                              child: Container(
                                alignment: Alignment.center,
                                child: Image.asset(
                                  "assets/icons/ic_heli.png",
                                  height: 30.0,
                                  width: 30.0,
                                ),
                                decoration: BoxDecoration(
                                  color: isHelicopter
                                      ? Colors.white
                                      : servicecardfillcolor,
                                  borderRadius: BorderRadius.circular(40),
                                  boxShadow: const [
                                    BoxShadow(
                                        color: txtborderbluecolor,
                                        spreadRadius: 1.5),
                                  ],
                                ),
                                height: 69.0,
                                width: 69.0,

                                //  kTxtFieldDecoration .copyWith(),
                              ),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              "Helicopters",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'Raleway-Medium',
                                  fontSize: 12.0
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 19.0,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20.0,
                    ),

                    Row(
                      children: [
                        /////One Way
                        Column(
                          children: [
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  isoneway = true;
                                  isRound = false;
                                });
                              },
                              child: Container(
                                alignment: Alignment.center,
                                child: Text(
                                  "One way",
                                  style: TextStyle(
                                    color:
                                        isoneway ? Colors.black : Colors.white,
                                      fontFamily: 'Raleway-Medium',
                                      fontSize: 12.0
                                  ),
                                ),
                                decoration: BoxDecoration(
                                  color: isoneway
                                      ? Colors.white
                                      : servicecardfillcolor,
                                  borderRadius: BorderRadius.circular(20),
                                  boxShadow: const [
                                    BoxShadow(
                                        color: txtborderbluecolor,
                                        spreadRadius: 1.5),
                                  ],
                                ),
                                height: 40.0,
                                width: 105.0,

                                //  kTxtFieldDecoration .copyWith(),
                              ),
                            ),
                          ],
                        ),

                        SizedBox(
                          width: 13.0,
                        ),

                        /////Round
                        Column(
                          children: [
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  isRound == false
                                      ? isRound = true
                                      : isRound = false;
                                  isoneway = false;
                                });
                              },
                              child: Container(
                                alignment: Alignment.center,
                                child: Text(
                                  "Round Trip",
                                  style: TextStyle(
                                    color:
                                        isRound ? Colors.black : Colors.white,
                                      fontFamily: 'Raleway-Medium',
                                      fontSize: 12.0
                                  ),
                                ),
                                decoration: BoxDecoration(
                                  color: isRound
                                      ? Colors.white
                                      : servicecardfillcolor,
                                  borderRadius: BorderRadius.circular(20),
                                  boxShadow: const [
                                    BoxShadow(
                                        color: txtborderbluecolor,
                                        spreadRadius: 1.5),
                                  ],
                                ),
                                height: 40.0,
                                width: 105.0,

                                //  kTxtFieldDecoration .copyWith(),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 19.0,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    // isoneway
                    //     ? OneWayContainer()
                    //     : isRound
                    //         ? RoundContainer()
                    //         : OneWayContainer(),
                    // isJet
                    //     ? JetContainer()
                    //     : isHelicopter
                    //         ? HelicopterContainer()
                    //         : JetContainer(),
                    Column(
                      children: [
                        ////FROM AIRPORT
                        GestureDetector(
                          onTap: () {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  dialogcontexxt = context;
                                  return Dialog(
                                    child: Directionality(
                                      textDirection: ui.TextDirection.ltr,
                                      child: SimpleAutoCompleteTextField(
                                        textSubmitted: (text) {
                                          setState(() {
                                            if (fromAirportList
                                                .contains(text)) {
                                              fromAirportSelected = text;
                                              var indexfA = fromAirportList
                                                  .indexOf(fromAirportSelected);
                                              fromairportCode =
                                                  fromAirportCodeList[indexfA];
                                            }
                                            // if (fromAirportList
                                            //     .contains(text)) {
                                            //   fromairportCode = text;
                                            // }
                                            print(fromairportCode);
                                            Navigator.pop(dialogcontexxt);
                                          });
                                        },
                                        textChanged: (text) {
                                          setState(() {
                                            fromAirportSelected =
                                                fromAirportController.text;
                                          });
                                        },
                                        clearOnSubmit: false,
                                        controller: fromAirportController,
                                        key: fromAirportKey,
                                        suggestions: fromAirportList,
                                        style: TextStyle(
                                          overflow: TextOverflow.ellipsis,
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          // color: Color(0xff295E65),
                                          // fontFamily: 'Helvetica',
                                          letterSpacing: 0.02,
                                        ),
                                        decoration: InputDecoration(
                                          hintText: "Select Airport",
                                          fillColor: Colors.white,
                                          border: InputBorder.none,
                                          focusedBorder: InputBorder.none,
                                          enabledBorder: InputBorder.none,
                                          errorBorder: InputBorder.none,
                                          disabledBorder: InputBorder.none,
                                          hintStyle: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold,
                                            letterSpacing: 0.02,
                                          ),
                                        ),
                                      ),
                                    ),
                                  );
                                });
                          },
                          child: Container(
                            margin: EdgeInsets.all(10.0),
                            padding: EdgeInsets.all(10.0),
                            alignment: Alignment.center,
                            child: Row(
                              children: [
                                Image.asset(
                                  'assets/icons/ic_air_service.png',
                                  color: Colors.white,
                                  height: 35.0,
                                  width: 35.0,
                                ),
                                SizedBox(
                                  width: 10.0,
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "From",
                                      style: kTxtStyle.copyWith(
                                        color: greytxtcolor,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    SizedBox(
                                      width: 255,
                                      child: Text(
                                        fromAirportSelected,
                                        style: kTxtStyle.copyWith(
                                            overflow: TextOverflow.ellipsis),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            decoration: BoxDecoration(
                              color: bgcolor,
                              // borderRadius: BorderRadius.circular(20),
                              boxShadow: const [
                                BoxShadow(
                                    color: txtborderbluecolor,
                                    spreadRadius: 1.5),
                              ],
                            ),
                            height: 74.0,
                            width: 324.0,
                          ),
                        ),

                        ////////TO AIRPORT
                        GestureDetector(
                          onTap: () {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  dialogcontexxt = context;
                                  return Dialog(
                                    child: Directionality(
                                      textDirection: ui.TextDirection.ltr,
                                      child: SimpleAutoCompleteTextField(
                                        textSubmitted: (text) {
                                          setState(() {
                                            if (toAirportList.contains(text)) {
                                              toAirportSelected = text;
                                              var indextA = toAirportList
                                                  .indexOf(toAirportSelected);
                                              toairportCode =
                                                  toAirportCodeList[indextA];
                                            }
                                            // if (toAirportCodeList
                                            //     .contains(text)) {
                                            //   toairportCode = text;
                                            // }
                                            print(toairportCode);
                                            Navigator.pop(dialogcontexxt);
                                          });
                                        },
                                        textChanged: (text) {
                                          setState(() {
                                            toAirportSelected =
                                                toAirportController.text;
                                          });
                                        },
                                        clearOnSubmit: false,
                                        controller: toAirportController,
                                        key: fromAirportKey,
                                        suggestions: toAirportList,
                                        style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          // color: Color(0xff295E65),
                                          // fontFamily: 'Helvetica',
                                          letterSpacing: 0.02,
                                        ),
                                        decoration: InputDecoration(
                                          hintText: "Select Airport",
                                          fillColor: Colors.white,
                                          border: InputBorder.none,
                                          focusedBorder: InputBorder.none,
                                          enabledBorder: InputBorder.none,
                                          errorBorder: InputBorder.none,
                                          disabledBorder: InputBorder.none,
                                          hintStyle: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold,
                                            letterSpacing: 0.02,
                                          ),
                                        ),
                                      ),
                                    ),
                                  );
                                });
                          },
                          child: Container(
                            margin: EdgeInsets.all(10.0),
                            padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                            alignment: Alignment.center,
                            child: Row(
                              children: [
                                Image.asset(
                                  'assets/icons/ic_loc.png',
                                  color: Colors.white,
                                ),
                                SizedBox(
                                  width: 10.0,
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "To",
                                      style: kTxtStyle.copyWith(
                                        color: greytxtcolor,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10.0,
                                    ),
                                    SizedBox(
                                      width: 260,
                                      child: Text(
                                        toAirportSelected,
                                        style: kTxtStyle.copyWith(
                                            overflow: TextOverflow.ellipsis),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            decoration: BoxDecoration(
                              color: bgcolor,
                              // borderRadius: BorderRadius.circular(20),
                              boxShadow: const [
                                BoxShadow(
                                    color: txtborderbluecolor,
                                    spreadRadius: 1.5),
                              ],
                            ),
                            height: 74.0,
                            width: 324.0,
                          ),
                        ),

                        /////Departure Date
                        InkWell(
                          onTap: () async {
                            DateTime? newDate = await showDatePicker(
                                context: context,
                                initialDate: dateTime,
                                firstDate: DateTime(2021),
                                lastDate: DateTime(2200));
                            if (newDate != null) {
                              setState(() {
                                dateTime = newDate;
                                departureDate =
                                    '${dateTime.day.toString().padLeft(2, '0')}-${dateTime.month.toString().padLeft(2, '0')}-${dateTime.year}';
                              });
                            }
                          },
                          child: Container(
                            margin: EdgeInsets.all(10.0),
                            padding: EdgeInsets.all(10.0),
                            alignment: Alignment.center,
                            child: Row(
                              children: [
                                Image.asset(
                                  'assets/icons/ic_calendar.png',
                                  height: 35.0,
                                  width: 35.0,
                                  color: Colors.white,
                                ),
                                SizedBox(
                                  width: 10.0,
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Departure",
                                      style: kTxtStyle.copyWith(
                                        color: greytxtcolor,
                                      ),
                                    ),
                                    Text(
                                      departureDate != ''
                                          ? departureDate
                                          : "Select Date",
                                      style: kTxtStyle,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            decoration: BoxDecoration(
                              color: bgcolor,
                              // borderRadius: BorderRadius.circular(20),
                              boxShadow: const [
                                BoxShadow(
                                    color: txtborderbluecolor,
                                    spreadRadius: 1.5),
                              ],
                            ),
                            height: 74.0,
                            width: 324.0,
                          ),
                        ),

                        ////////Retun Date (Round)
                        Visibility(
                          visible: isRound,
                          child: InkWell(
                            onTap: () async {
                              DateTime? newDate = await showDatePicker(
                                  context: context,
                                  initialDate: dateTime,
                                  firstDate: DateTime(2021),
                                  lastDate: DateTime(2200));
                              if (newDate != null) {
                                setState(() {
                                  dateTime = newDate;
                                  returnDate =
                                      '${dateTime.day.toString().padLeft(2, '0')}-${dateTime.month.toString().padLeft(2, '0')}-${dateTime.year}';
                                });
                              }
                            },
                            child: Container(
                              margin: EdgeInsets.all(10.0),
                              padding: EdgeInsets.all(10.0),
                              alignment: Alignment.center,
                              child: Row(
                                children: [
                                  Image.asset(
                                    'assets/icons/ic_calendar.png',
                                    color: Colors.white,
                                    height: 35.0,
                                    width: 35.0,
                                  ),
                                  SizedBox(
                                    width: 10.0,
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Return",
                                        style: kTxtStyle.copyWith(
                                          color: greytxtcolor,
                                        ),
                                      ),
                                      Text(
                                        returnDate == ''
                                            ? "Select Date"
                                            : returnDate,
                                        style: kTxtStyle,
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              decoration: BoxDecoration(
                                color: bgcolor,
                                // borderRadius: BorderRadius.circular(20),
                                boxShadow: const [
                                  BoxShadow(
                                      color: txtborderbluecolor,
                                      spreadRadius: 1.5),
                                ],
                              ),
                              height: 74.0,
                              width: 324.0,
                            ),
                          ),
                        ),

                        /////////Passengers
                        InkWell(
                          onTap: () {
                            showDialog(
                                context: context,
                                builder: (_) {
                                  return StatefulBuilder(
                                      builder: (_, StateSetter refresh) {
                                    return AlertDialog(
                                      backgroundColor: servicecardfillcolor,
                                      title: Text(
                                        'Pick No. of passengers',
                                        style: kHeadText,
                                      ),
                                      content: Container(
                                        padding: EdgeInsets.all(10),
                                        // margin: EdgeInsets.all(100),
                                        // color: servicecardfillcolor,
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.4,
                                        height:
                                            MediaQuery.of(context).size.height /
                                                3.8,
                                        child: Column(
                                          children: [
                                            NumberPicker(
                                                textStyle: kTxtStyle,
                                                selectedTextStyle:
                                                    kHeadText.copyWith(
                                                        fontWeight:
                                                            FontWeight.w900),
                                                // itemHeight: 20,
                                                // itemCount: 3,
                                                // haptics: true,
                                                minValue: 1,
                                                maxValue: 61,
                                                value:
                                                    int.parse(totalPassengers),
                                                onChanged: (value) {
                                                  refresh(() {
                                                    setState(() {
                                                      totalPassengers =
                                                          value.toString();
                                                    });
                                                  });

                                                  setState(() {});
                                                }),
                                            SizedBox(
                                              height: 15.0,
                                            ),
                                            ElevatedButton(
                                              style: ButtonStyle(
                                                backgroundColor:
                                                    MaterialStateProperty.all(
                                                        Colors.transparent),
                                                shadowColor:
                                                    MaterialStateProperty.all(
                                                        Colors.transparent),
                                              ),
                                              onPressed: () {
                                                Navigator.pop(context);
                                              },
                                              child: Text(
                                                'OK',
                                                style: kHeadText.copyWith(
                                                    fontWeight:
                                                        FontWeight.w900),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    );
                                  });
                                });
                          },
                          child: Container(
                            margin: EdgeInsets.all(10.0),
                            padding: EdgeInsets.all(10.0),
                            alignment: Alignment.center,
                            child: Row(
                              children: [
                                Image.asset(
                                  'assets/icons/ic_total_guest.png',
                                  height: 35.0,
                                  width: 35.0,
                                  color: Colors.white,
                                ),
                                SizedBox(
                                  width: 10.0,
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Passengers",
                                      style: kTxtStyle.copyWith(
                                        color: greytxtcolor,
                                      ),
                                    ),
                                    Text(
                                      totalPassengers,
                                      style: kTxtStyle,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            decoration: BoxDecoration(
                              color: bgcolor,
                              // borderRadius: BorderRadius.circular(20),
                              boxShadow: const [
                                BoxShadow(
                                    color: txtborderbluecolor,
                                    spreadRadius: 1.5),
                              ],
                            ),
                            height: 74.0,
                            width: 324.0,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
    );
  }

  Future fetchAirportList() async {
    setState(() {
      isLoading = true;
    });
    print(userToken);

    List<Airport>? parsedres = await AirServiceApis().getAirportList(homeToken);
    if (parsedres != null) {
      for (var i = 0; i < parsedres.length; i++) {
        fromAirportList
            .add(parsedres[i].name! + ' (' + parsedres[i].cityCode! + ')');
        fromAirportCodeList.add(parsedres[i].code!);
        toAirportCodeList.add(parsedres[i].code!);
        toAirportList
            .add(parsedres[i].name! + ' (' + parsedres[i].cityCode! + ')');
      }
    }
    print(parsedres?[1].name);
    setState(() {
      isLoading = false;
    });
  }
}
