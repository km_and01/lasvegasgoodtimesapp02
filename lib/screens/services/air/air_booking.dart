import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/model/air_model/add_passenger_model.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';
import 'package:lasvegas_gts_app/screens/services/air/air_checkout.dart';

class AirReviewBooking extends StatefulWidget {
  final String fromAP, toAP, fromAPC, toAPC, dDate, tPass,helijet,planeType,price,rDate,vendorID;
  const AirReviewBooking(
      {Key? key,
      required this.fromAP,
      required this.toAP,
      required this.fromAPC,
      required this.toAPC,
      required this.dDate,
      required this.tPass, 
      required this.helijet, 
      required this.planeType, 
      required this.price, 
      required this.rDate, 
      required this.vendorID})
      : super(key: key);

  @override
  _AirReviewBookingState createState() => _AirReviewBookingState();
}

class _AirReviewBookingState extends State<AirReviewBooking> {
  TextEditingController userPhoneNoController = TextEditingController();
  TextEditingController userNameController = TextEditingController();
  TextEditingController userAgeController = TextEditingController();
  TextEditingController userEmailController = TextEditingController();

  List<TextEditingController> user_names_cnt =  [];
  List<TextEditingController> user_emails_cnt = [];
  List<TextEditingController> user_phones_cnt = [];
  List<TextEditingController> user_ages_cnt = [];

  // Add By FZ For AddRoom LIst
  List<AddPassengerModel> add_passenger_list = [];

  int is_press = 0;

  @override
  void initState() {
    setState(() {
      print(homeUserName);
      print(homeUserEmail);
      print(homeUserPhone);
      userNameController.text = homeUserName;
      userEmailController.text = homeUserEmail;
      userPhoneNoController.text = homeUserPhone;
    });
    firstTimeAddbyDefaultPassenger();
    // TODO: implement initState
    super.initState();
  }

  Future firstTimeAddbyDefaultPassenger() async {
    setState(() {

      /*user_names_cnt.add(userNameController);
      user_emails_cnt.add(userEmailController);
      user_phones_cnt.add(userPhoneNoController);
      user_ages_cnt.add(userAgeController);*/

      if(is_press == 0){

        AddPassengerModel model = AddPassengerModel(
            str_name: homeUserName,
            str_email: homeUserEmail,
            str_phone: homeUserPhone,
            str_age: ''
        );
        add_passenger_list.add(model);
      }else {
        AddPassengerModel model = AddPassengerModel(
            str_name: '',
            str_email: '',
            str_phone: '',
            str_age: ''
        );
        add_passenger_list.add(model);
      }

    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: BackButton(
            color: Colors.white,
          ),
          elevation: 0.00,
          title: Text(
            'Add Details',
            style: kHeadText,
          ),
        ),
        backgroundColor: bgcolor,
        bottomNavigationBar: GestureDetector(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (_) => AirCheckout(
                          dDate: widget.dDate,
                          fromAP: widget.fromAP,
                          fromAPC: widget.fromAPC,
                          toAP: widget.toAP,
                          toAPC: widget.toAPC,
                          tPass: widget.tPass, 
                          helijet: widget.helijet, 
                          page:userAgeController.text, 
                          pEmail: userEmailController.text, 
                          plane_type: widget.planeType, 
                          pMob: userPhoneNoController.text, 
                          pname: userNameController.text, 
                          price: widget.price, 
                          rDate: widget.rDate, 
                          vendorID: widget.vendorID,
                        )));
          },
          child: Container(
            alignment: Alignment.center,
            child: Text(
              "CHECK OUT",
              style: TextStyle(
                  fontSize: 18.0,
                  letterSpacing: 0.7,
                  fontWeight: FontWeight.w600),
            ),
            height: 57.0,
            color: btngoldcolor,
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
              // alignment: Alignment.centerLeft,

              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    children: [
                      ListView.builder(
                        primary: false,
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: add_passenger_list.length,
                          itemBuilder: (context, index){

                            TextEditingController PhoneNoController = TextEditingController();
                            TextEditingController NameController = TextEditingController();
                            TextEditingController AgeController = TextEditingController();
                            TextEditingController EmailController = TextEditingController();

                            PhoneNoController.text =
                            add_passenger_list[index].str_phone == null ? '' : add_passenger_list[index].str_phone!;
                            NameController.text =
                            add_passenger_list[index].str_name == null ? '' : add_passenger_list[index].str_name!;
                            AgeController.text =
                            add_passenger_list[index].str_age == null ? '' : add_passenger_list[index].str_age!;
                            EmailController.text =
                            add_passenger_list[index].str_email == null ? '' : add_passenger_list[index].str_email!;

                              return Column(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(left: 25.0, right: 25.0, top: 15.0, bottom: 0.0),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [

                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                                          child: Text(
                                            'FULL NAME',
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              letterSpacing: 0.8,
                                              color: Color(0xFF95A0AE),
                                            ),
                                          ),
                                        ),
                                        TextFormField(
                                          initialValue: add_passenger_list[index].str_name,
                                          showCursor: true,
                                          cursorColor: Color(0xFF5C75B3),
                                          cursorRadius: Radius.circular(0),
                                          style: kTxtStyle,
                                          //controller: NameController,
                                          decoration: kTxtFieldDecoration,
                                          onChanged: (String val){
                                            setState(() {
                                              user_names_cnt[index].text = val;
                                              add_passenger_list[index].str_name = val;
                                            });
                                          },
                                        ),
                                        SizedBox(height: 25.0),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                                          child: Text(
                                            'EMAIL ADDRESS',
                                            textAlign: TextAlign.left,
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              letterSpacing: 0.8,
                                              color: Color(0xFF95A0AE),
                                            ),
                                          ),
                                        ),
                                        TextFormField(
                                          initialValue: add_passenger_list[index].str_email,
                                          showCursor: true,
                                          cursorColor: Color(0xFF5C75B3),
                                          cursorRadius: Radius.circular(0),
                                          style: kTxtStyle,
                                          // obscureText: true,
                                          //controller: EmailController,
                                          keyboardType: TextInputType.emailAddress,
                                          decoration: kTxtFieldDecoration,
                                          onChanged: (String val){
                                            setState(() {
                                              user_emails_cnt[index].text = val;
                                              add_passenger_list[index].str_email = val;
                                            });
                                          },
                                        ),
                                        SizedBox(height: 25.0),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                                          child: Text(
                                            'PHONE NUMBER',
                                            textAlign: TextAlign.left,
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              letterSpacing: 0.8,
                                              color: Color(0xFF95A0AE),
                                            ),
                                          ),
                                        ),
                                        TextFormField(
                                          initialValue: add_passenger_list[index].str_phone,
                                          showCursor: true,
                                          cursorColor: Color(0xFF5C75B3),
                                          cursorRadius: Radius.circular(0),
                                          style: kTxtStyle,
                                          maxLength: 10,
                                          // obscureText: true,
                                          //controller: PhoneNoController,
                                          keyboardType: TextInputType.number,
                                          decoration: kTxtFieldDecoration,
                                          onChanged: (String val){
                                            setState(() {
                                              user_phones_cnt[index].text = val;
                                              add_passenger_list[index].str_phone = val;
                                            });
                                          },
                                        ),
                                        //SizedBox(height: 25.0),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                                          child: Text(
                                            'AGE',
                                            textAlign: TextAlign.left,
                                            style: TextStyle(
                                              fontSize: 14.0,
                                              letterSpacing: 0.8,
                                              color: Color(0xFF95A0AE),
                                            ),
                                          ),
                                        ),
                                        TextFormField(
                                          initialValue: add_passenger_list[index].str_age,
                                          showCursor: true,
                                          cursorColor: Color(0xFF5C75B3),
                                          cursorRadius: Radius.circular(0),
                                          style: kTxtStyle,
                                          // obscureText: true,
                                          //controller: AgeController,
                                          maxLength: 3,
                                          keyboardType: TextInputType.number,
                                          decoration: kTxtFieldDecoration,
                                          onChanged: (String val){
                                            setState(() {
                                              user_ages_cnt[index].text = val;
                                              add_passenger_list[index].str_age = val;
                                            });
                                          },
                                        ),

                                      ],
                                    ),
                                  ),

                                  /*Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      IconButton(
                                        onPressed: () {
                                          // onDeleteRoom;
                                          setState(() {


                                            // user_names_cnt.removeAt(index);
                                            // user_emails_cnt.removeAt(index);
                                            // user_phones_cnt.removeAt(index);
                                            // user_ages_cnt.removeAt(index);

                                            print('SEL_INDEX$index');
                                            add_passenger_list.removeAt(index);

                                          });

                                        },
                                        icon: Icon(
                                          Icons.delete_outline_outlined,
                                          color: btngoldcolor,
                                        ),
                                      ),
                                    ],
                                  ),*/
                                  Divider(
                                    thickness: 2.0,
                                    color: Colors.grey,
                                  )
                                ],
                              );
                          })
                    ],
                  ),

                  Container(
                    margin: EdgeInsets.only(top:10.0, left: 25.0, right: 25.0, bottom: 25.0),
                    child: InkWell(
                      onTap: () {
                        setState(() {

                          /*userNameController.text = '';
                          userEmailController.text = '';
                          userPhoneNoController.text = '';
                          userAgeController.text = '';

                          user_names_cnt.add(userNameController);
                          user_emails_cnt.add(userEmailController);
                          user_phones_cnt.add(userPhoneNoController);
                          user_ages_cnt.add(userAgeController);*/

                          /*AddPassengerModel model = AddPassengerModel(
                              str_name: null,
                              str_email: null,
                              str_phone: null,
                              str_age: null
                          );
                          add_passenger_list.add(model);*/

                          is_press = 1;
                          firstTimeAddbyDefaultPassenger();
                        });
                      },
                      child: Container(
                        padding: EdgeInsets.fromLTRB(10, 18, 10, 18),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.add,
                              color: Colors.white,
                            ),
                            SizedBox(
                              width: 10.0,
                            ),
                            Text(
                              "ADD MORE PASSENGER",
                              style: kTxtStyle,
                            ),
                          ],
                        ),
                        decoration: kBorderDecoration,
                      ),
                    ),
                  )
                ],
              )),
        ),
      ),
    );
  }
}
