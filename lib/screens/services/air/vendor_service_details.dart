import 'dart:convert';
import 'dart:io';

import 'package:carousel_images/carousel_images.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lasvegas_gts_app/Networking/AirAPI/air_service_apis.dart';
import 'package:lasvegas_gts_app/Networking/AirAPI/flight_list_model.dart';
import 'package:lasvegas_gts_app/customs/anamitis_card.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart' as home;
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/Networking/StayAPI/amenities_model.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_card_static.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'package:lasvegas_gts_app/screens/services/air/air_booking.dart';
import 'package:lasvegas_gts_app/screens/services/air/air_main_src.dart';
import 'package:lasvegas_gts_app/screens/services/stay/stay_checkout_scr.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

AmenitiesListModel amidata = AmenitiesListModel(statusCode: 200, ami: []);
String APIkey = 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs';
final List<String> listImagesAirLine = [];

String imageURL =
    "http://koolmindapps.com/lasvegas/public/images/business_detail/";

Uri amiURL = Uri.parse(
    "http://koolmindapps.com/lasvegas/public/api/v1/stayservicedetail");

const String roomImgURL =
    'http://koolmindapps.com/lasvegas/public/images/stay/';

bool isHotelDisLoading = false;

class AirLineVendorDetailsScreen extends StatefulWidget {
  final String businessName;
  final String flightName;
  final String flightDetails;
  final String address1;
  final String address2;
  final List<String> imagesk;
  final String discPrice;
  final List<String> amaities;

  // final String totalRooms;
  final String totalGuest;
  final String DepartureDate;
  final String returnDate;
  final String fromAP,toAP,toAPC,fromAPC,vendorID,helijet;

  const AirLineVendorDetailsScreen(
      {Key? key,
        required this.businessName,
        // required this.roomName,
        // required this.roomDetails,
        required this.address1,
        required this.address2,
        required this.imagesk,
        required this.discPrice,
        // required this.totalRooms,
        required this.totalGuest,
        // required this.checkInDate,
        // required this.checkOutDate,
        required this.amaities,
        required this.flightName,
        required this.flightDetails,
        required this.DepartureDate,
        required this.returnDate, required this.fromAP, required this.toAP, required this.toAPC, required this.fromAPC, required this.vendorID, required this.helijet})
      : super(key: key);

  @override
  _AirLineVendorDetailsScreenState createState() => _AirLineVendorDetailsScreenState();
}

class _AirLineVendorDetailsScreenState extends State<AirLineVendorDetailsScreen> {

  int is_sel = 0; // if 0 = Vendor Services List AND 1 = Vendor Details

  List<ServiceAir> servicesData = [];

  @override
  void initState() {
    // TODO: implement initState
    // getAllAmis();

    getAirVendorDetailsList();
    super.initState();
    setState(() {});

    print("URL ===" + imageURL);
    int length = widget.imagesk.length;
    listImagesAirLine.clear();
    for (var i = 0; i < widget.imagesk.length; i++) {
      listImagesAirLine.add(imageURL + widget.imagesk[i]);
    }
    print('LIST_IMGSS ${listImagesAirLine.toString()}');
    setState(() {});//.add(imageURL + widget.images.forEach((widget.images) { });
  }

  Future getAirVendorDetailsList() async{

    final SharedPreferences preferences = await SharedPreferences.getInstance();
    String homeToken = preferences.getString('userToken')!;

    String sub_type = widget.helijet;

    setState(() {
      isLoading = true;
    });

    servicesData = (await AirServiceApis().getSubFlightListing(
        widget.helijet,
        widget.vendorID
    )) as List<ServiceAir>;


    setState(() {
      isLoading = false;
    });

  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: bgcolor,
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          elevation: 0.0,
          leading: BackButton(),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                widget.helijet,
                style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'Helvetica',
                    fontSize: 18.0,
                    letterSpacing: 0.29
                ),
              ),

            ],
          ),
          bottom: PreferredSize(
            child: Column(
              children: const [
                Divider(
                  thickness: 2,
                  height: 2,
                  color: Colors.amber,
                ),
                SizedBox(
                  height: 3.0,
                ),
                DottedLine(
                  lineThickness: 4.5,
                  dashRadius: 7,
                  dashLength: 4.0,
                  // lineLength:  MediaQuery.of(context).size.width,
                  dashColor: Colors.amber,
                ),
              ],
            ),
            preferredSize: Size.fromHeight(20),
          ),
        ),
        body: isHotelDisLoading
            ? CustomLoadingScr()
            : Column(

              children: [
                Expanded(
                    flex: 2,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [

                        Container(
                          padding: EdgeInsets.only(left: 20.0, top: 8.0, bottom: 8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                widget.businessName,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'Raleway-Medium',
                                    fontSize: 18.0,
                                    letterSpacing: 0.29
                                ),
                              ),
                              RatingBar.builder(
                                glow: false,
                                itemSize: 15.0,
                                initialRating: 3,
                                minRating: 1,
                                direction: Axis.horizontal,
                                allowHalfRating: true,
                                itemCount: 5,
                                /*itemPadding: EdgeInsets.symmetric(horizontal: 4.0),*/
                                itemBuilder: (context, _) => Icon(
                                  Icons.star,
                                  color: Colors.amber,
                                  //   Image.asset(
                                  // "assets/icons/rating_star.png",
                                  // color: Colors.amber,
                                ),
                                // ),
                                unratedColor: Colors.grey,
                                onRatingUpdate: (rating) {
                                  print(rating);
                                },
                              ),
                              SizedBox(height: 5.0),
                              Text(
                                widget.address1,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'Manrope-Regular',
                                    fontSize: 12.0,
                                    letterSpacing: 0.29
                                ),
                              ),
                              Text(
                                widget.address2,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'Manrope-Regular',
                                    fontSize: 12.0,
                                    letterSpacing: 0.29
                                ),
                              ),
                            ],
                          ),
                        ),

                        Divider(
                          height: 1.0,
                          thickness: 1.0,
                          color: greytxtcolor,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [

                            Expanded(
                              flex: 1,
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    is_sel = 0;
                                  });
                                },
                                child: Container(
                                  height: 45.0,
                                  color: is_sel == 0 ? Color(0xffEDBA1F) : Colors.transparent,
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: Text(
                                      'Service',
                                      style: TextStyle(
                                          fontSize: 16.0,
                                          color: is_sel == 0 ? Color(0xff121A31) : Colors.white,
                                          fontFamily: 'Helvetica'
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    is_sel = 1;
                                  });
                                },
                                child: Container(
                                  height: 45.0,
                                  color: is_sel == 1 ? Color(0xffEDBA1F) : Colors.transparent,
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: Text(
                                      'Details',
                                      style: TextStyle(
                                          fontSize: 16.0,
                                          color: is_sel == 1 ? Color(0xff121A31) : Colors.white,
                                          fontFamily: 'Helvetica'
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),

                          ],
                        ),
                        Divider(
                          height: 1.0,
                          thickness: 1.0,
                          color: greytxtcolor,
                        ),
                      ],
                    ),),

                Expanded(
                    flex: 7,
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Visibility(
                              visible: is_sel == 0 ? true : false,
                              child: ListView.builder(
                                  physics: NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: servicesData.length,
                                  itemBuilder: (_, index) {

                                    return CustomRoomDisplayCard(

                                      vendorID: servicesData[index].vendorId!,
                                      discPrice: servicesData[index].priceForPj == null ? '0' : servicesData[index].priceForPj!,
                                      addLine1: servicesData[index].bussinessDetails!.addressLine1!,
                                      addLine2: servicesData[index].bussinessDetails!.addressLine2!,
                                      roomDetails: servicesData[index].descr!,
                                      businessName: servicesData[index].pjSname == null ? '' : servicesData[index].pjSname,
                                      imagesk: servicesData[index].bussinessDetails!.imagesk!,
                                      totalGuest: widget.totalGuest,
                                      bLogo: servicesData[index].bussinessDetails!.bussinessLogo!,
                                      DepartureDate: widget.DepartureDate,
                                      fromAP: widget.fromAP,
                                      fromAPC: widget.fromAPC,
                                      helijet: widget.helijet,
                                      returnDate: widget.returnDate,
                                      toAP: widget.toAP,
                                      toAPC: widget.toAPC,
                                      seat_avai: servicesData[index].personPj.toString(),
                                      service_type: servicesData[index].airServiceType!,

                                    );

                                  })),


                          Visibility(
                              visible: is_sel == 1 ? true : false,
                              child: ListView(
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                children: [
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(20.0, 0, 0, 0),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [

                                        CarouselImages(
                                          viewportFraction: 0.9,
                                          scaleFactor: 0.1,
                                          listImages: listImagesAirLine,
                                          height: 250.0,
                                          borderRadius: 8.0,
                                          cachedNetworkImage: true,
                                          verticalAlignment: Alignment.center,
                                          onTap: (index) {
                                            print('Tapped on page $index');
                                          },
                                        ),

                                        Text(
                                          '${widget.businessName} | ${widget.flightName}',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontFamily: 'Helvetica',
                                              fontSize: 18.0,
                                              letterSpacing: 0.29
                                          ),
                                        ),

                                        SizedBox(height: 10.0,),
                                        Text(
                                          'Price Starts from ${widget.discPrice}',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontFamily: 'Raleway-Bold',
                                              fontSize: 18.0,
                                              letterSpacing: 0.29
                                          ),
                                        ),

                                        SizedBox(height: 10.0,),

                                        Text(
                                          "Jet Details",
                                          style: TextStyle(
                                              fontFamily: 'Raleway-Bold',
                                              fontSize: 14.0,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                        SizedBox(
                                          height: 10.0,
                                        ),
                                        Text(
                                          widget.flightDetails,
                                          // """The Hotel Pennsylvania is conveniently located within one block of the Empire State Building,Madison Square Garden, Penn Station and Macy's flagship store. Located within walking distance are Times Square, the Broadway Theatre District, New York Public Library, Jacob K. Javits Convention Center, and two of New York's most iconic…""",
                                          style: TextStyle(
                                              color: greytxtcolor,
                                              fontFamily: 'Raleway',
                                              fontSize: 12.0
                                          ),
                                        ),
                                        SizedBox(
                                          height: 30.0,
                                        ),
                                        Text(
                                          "Contact",
                                          style: TextStyle(
                                              fontSize: 18.0,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                        SizedBox(
                                          height: 10.0,
                                        ),
                                        Text(
                                          widget.address1 + ' , ' + widget.address2 + ' . ',
                                          // """T3131 Las Vegas Boulevard South, 89109, Las Vegas""",
                                          style: kTxtStyle.copyWith(color: greytxtcolor),
                                        ),
                                        SizedBox(
                                          height: 10.0,
                                        ),
                                        Container(
                                          child:
                                          Image.asset("assets/images/banner_img_2.png"),
                                          height: 215.0,
                                          width: 380.0,
                                          color: bgcolor,
                                        ),
                                        SizedBox(
                                          height: 40.0,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ))

                        ],
                      ),))
              ],
            ),
      ),
    );
  }

  Future getAllAmis() async {
    setState(() {
      isHotelDisLoading = true;
    });
    print(home.homeToken);
    var amires = await http.post(
      amiURL,
      headers: {
        'API_KEY': APIkey,
        'Authorization': "Bearer " + home.homeToken,
      },
    );

    if (amires.statusCode == 200) {
      print(amires.body);
      amidata = AmenitiesListModel.fromJson(jsonDecode(amires.body));
      print(amidata.ami[0].amenitiesName);
      // return amidata;
    }
    setState(() {
      isHotelDisLoading = false;
    });
  }
}

class ReviewBarIndicator extends StatelessWidget {
  final String title;
  final double rating;

  ReviewBarIndicator({Key? key, required this.title, required this.rating})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: kTxtStyle,
        ),
        SizedBox(
          height: 10.0,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            LinearPercentIndicator(
              width: 140.0,
              lineHeight: 8.0,
              percent: rating * 0.1,
              backgroundColor: Color(0xFFFFE8EB),
              progressColor: btngoldcolor,
            ),
            SizedBox(
              width: 3.0,
            ),
            Text(
              rating.toString(),
              style: kTxtStyle.copyWith(fontFamily: 'Regular'),
            ),
          ],
        ),
        SizedBox(
          height: 15.0,
        ),
      ],
    );
  }
}


// Services List
class CustomRoomDisplayCard extends StatefulWidget {
  //final String roomName;
  final String discPrice;
  final String addLine2;
  final String addLine1;
  final String roomDetails;
  final String businessName;
  final List<String> imagesk;
  final String bLogo;
  final String totalGuest;
  final String vendorID;
  final String DepartureDate;
  final String returnDate;
  final String fromAP,toAP,toAPC,fromAPC,helijet;
  final String seat_avai;
  final String service_type;

  const CustomRoomDisplayCard(
      {Key? key,
        //required this.roomName,
        required this.discPrice,
        required this.addLine2,
        required this.addLine1,
        required this.roomDetails,
        required this.businessName,
        required this.imagesk,
        required this.totalGuest,
        required this.bLogo,
        required this.vendorID,
        required this.DepartureDate,
        required this.returnDate,
        required this.fromAP,
        required this.toAP,
        required this.toAPC,
        required this.fromAPC,
        required this.helijet,
        required this.seat_avai,
        required this.service_type,

      })
      : super(key: key);

  @override
  State<CustomRoomDisplayCard> createState() => _CustomRoomDisplayCardState();
}

class _CustomRoomDisplayCardState extends State<CustomRoomDisplayCard> {

  String heli_url = 'http://koolmindapps.com/lasvegas/public/frontend/images/helicop.jpg';
  String jet_url = 'http://koolmindapps.com/lasvegas/public/frontend/images/jet.jpg';

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20.0),
      alignment: Alignment.center,
      padding: EdgeInsets.fromLTRB(20.0, 20.0, 15.00, 20),
      child: Column(
        //
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        '${widget.businessName} / ',
                        style: TextStyle(
                            fontFamily: 'Manrope-SemiBold',
                            fontSize: 18.0,
                            color: Colors.white),
                      ),

                      Text(
                        widget.service_type,
                        style: TextStyle(
                            fontFamily: 'Manrope-Medium',
                            color: greytxtcolor),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5.0,
                  ),

                  Text(
                    '${widget.seat_avai} Seats',
                    style: TextStyle(
                        fontFamily: 'Manrope-Medium',
                        color: greytxtcolor),
                  ),
                  SizedBox(
                    height: 5.0,
                  ),

                  /*Text(
                    '${widget.quantity} Rooms Available',
                    style: TextStyle(
                        fontFamily: 'Manrope-Medium',
                        color: greytxtcolor),
                  ),*/
                  SizedBox(
                    height: 5.0,
                  ),

                  SizedBox(
                    width: 180.0,
                    child: Text(
                      '${widget.roomDetails}',
                      maxLines: 3,
                      style: TextStyle(
                          fontFamily: 'Manrope-Medium',
                          color: greytxtcolor),
                    ),
                  ),


                ],
              ),

              /////

              Container(
                  margin: EdgeInsets.all(5.0),
                  child: Image.network(
                   /* 'assets/images/banner_img_3.png',*/
                    widget.service_type == 'Helicopter' ? heli_url : jet_url,
                    height: 108,
                    width: 108,
                    fit: BoxFit.fill,
                  )),
            ],
          ),
          SizedBox(
            height: 25.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "\$ " + widget.discPrice,
                    style: TextStyle(
                      fontFamily: 'Manrope-SemiBold',
                      fontSize: 18.0,
                      color: Colors.white,
                    ),
                  ),
                  Text(
                    "Per Day",
                    style: TextStyle(color: greytxtcolor),
                  ),
                ],
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) => AirReviewBooking(
                            dDate: widget.DepartureDate,
                            fromAP: widget.fromAP,
                            fromAPC: widget.fromAPC,
                            toAP: widget.toAP,
                            toAPC: widget.toAPC,
                            tPass: widget.totalGuest,
                            helijet: widget.helijet,
                            planeType: widget.businessName,
                            price: widget.discPrice,
                            rDate: widget.returnDate,
                            vendorID:widget.vendorID,)
                      ));
                },
                child: Text(
                  "Book Now",
                  style: TextStyle(
                      fontFamily: 'Raleway-Medium',
                      color: Color(0xff121A31),
                      fontSize: 14.0),
                ),
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  fixedSize: Size(108.0, 40.0),
                  primary: Color(0xFFFFC71E),
                ),
              ),
            ],
          ),
        ],
      ),
      decoration: BoxDecoration(
        color: Colors.transparent,
        border: Border.all(color: txtborderbluecolor),
        borderRadius: BorderRadius.all(Radius.circular(15.0)),
      ),
    );
  }
}
