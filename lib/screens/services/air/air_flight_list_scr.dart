import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/AirAPI/air_service_apis.dart';
import 'package:lasvegas_gts_app/Networking/AirAPI/flight_list_model.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/custom_flight_list_card.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';

String takeoffdate = '';
String landDate = '';
String passengerNo = '';
String fromAP = '', toAP = '', toAPC = '', fromAPC = '';

class FlightList extends StatefulWidget {
  final String departureDate,
      returnDate,
      origin,
      destination,
      passengers,
      aircraft;
  final String fromAP, toAP, toAPC, fromAPC;
  const FlightList(
      {Key? key,
      required this.departureDate,
      required this.returnDate,
      required this.origin,
      required this.destination,
      required this.passengers,
      required this.aircraft,
      required this.fromAP,
      required this.toAP,
      required this.toAPC,
      required this.fromAPC})
      : super(key: key);

  @override
  _FlightListState createState() => _FlightListState();
}

bool isJet = true;
bool isHelicopter = false;
bool isFlightLoading = false;
bool isErrorOcc = false;
List<ServiceAir> FlightData = [];

class _FlightListState extends State<FlightList> {
  @override
  void initState() {
    fromAP = widget.fromAP;
    fromAPC = widget.fromAPC;
    toAP = widget.toAP;
    toAPC = widget.toAPC;
    // TODO: implement initState
    takeoffdate = widget.departureDate;
    landDate = widget.returnDate;
    passengerNo = widget.passengers;
    fetchFlightListing('');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
          lead: Image.asset(
            "assets/icons/ic_air_service.png",
            color: Colors.white,
            height: 24.0,
            width: 24.0,
          ),
          title: Text(
            "Air Jet",
            style: TextStyle(
                color: Colors.white,
                fontFamily: 'Helvetica',
                fontSize: 18.0,
                letterSpacing: 0.29
            ),
          ),
        ),
        backgroundColor: bgcolor,
        body: isFlightLoading
            ? CustomLoadingScr()
            : SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(25.0, 20.0, 25.0, 10.0),
                  child: Column(
                    children: [
                      /*SizedBox(
                        height: 10.0,
                      ),*/
                      TextFormField(
                        decoration: kSearchTxtFieldDecoration.copyWith(
                          fillColor: servicecardfillcolor,
                          filled: true,
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),

                      //////Category selection Tab
                      Row(
                        children: [
                          /////Jet
                          Column(
                            children: [
                              GestureDetector(
                                onTap: () {
                                  setState(() async {
                                    isJet = true;
                                    isHelicopter = false;
                                    await fetchFlightListing('Private Jets');
                                  });
                                },
                                child: Container(
                                  alignment: Alignment.center,
                                  child: Image.asset(
                                    "assets/icons/ic_jet.png",
                                    height: 30.0,
                                    width: 30.0,
                                  ),
                                  decoration: BoxDecoration(
                                    color: isJet
                                        ? Colors.white
                                        : servicecardfillcolor,
                                    borderRadius: BorderRadius.circular(40),
                                    boxShadow: const [
                                      BoxShadow(
                                          color: txtborderbluecolor,
                                          spreadRadius: 1.5),
                                    ],
                                  ),
                                  height: 69.0,
                                  width: 69.0,

                                  //  kTxtFieldDecoration .copyWith(),
                                ),
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              Text(
                                "Private Jet",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'Raleway-Medium',
                                    fontSize: 12.0
                                ),
                              ),
                            ],
                          ),

                          SizedBox(
                            width: 13.0,
                          ),

                          /////Helicopter
                          Column(
                            children: [
                              GestureDetector(
                                onTap: () {
                                  setState(() async {
                                    isHelicopter == false
                                        ? isHelicopter = true
                                        : isHelicopter = false;
                                    isJet = false;
                                    await fetchFlightListing('Helicopter');
                                  });
                                },
                                child: Container(
                                  alignment: Alignment.center,
                                  child: Image.asset(
                                    "assets/icons/ic_heli.png",
                                    height: 30.0,
                                    width: 30.0,
                                  ),
                                  decoration: BoxDecoration(
                                    color: isHelicopter
                                        ? Colors.white
                                        : servicecardfillcolor,
                                    borderRadius: BorderRadius.circular(40),
                                    boxShadow: const [
                                      BoxShadow(
                                          color: txtborderbluecolor,
                                          spreadRadius: 1.5),
                                    ],
                                  ),
                                  height: 69.0,
                                  width: 69.0,

                                  //  kTxtFieldDecoration .copyWith(),
                                ),
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              Text(
                                "Helicopters",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'Raleway-Medium',
                                    fontSize: 12.0
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            width: 19.0,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      isErrorOcc
                          ? Text('No Data Found', style: kHeadText)
                          : isHelicopter
                              ? HelicopterWidget()
                              : JetWidget(),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Future fetchFlightListing(String aircraft) async {
    setState(() {
      isFlightLoading = true;
    });
    try {
      FlightData = await AirServiceApis().getFlightListing(
          widget.departureDate,
          widget.returnDate,
          widget.origin,
          widget.destination,
          widget.passengers,
          aircraft == null ? '' : aircraft);
      setState(() {
        isErrorOcc = false;
      });

      print(FlightData[0].bussinessDetails!.bussinessName);
    } catch (e) {
      print(e);
      setState(() {
        isErrorOcc = true;
      });
    }

    setState(() {
      isFlightLoading = false;
    });
  }
}

/////////Jet Widget
class JetWidget extends StatefulWidget {
  const JetWidget({
    Key? key,
  }) : super(key: key);

  @override
  State<JetWidget> createState() => _JetWidgetState();
}

class _JetWidgetState extends State<JetWidget> {
  @override
  Widget build(BuildContext context) {
    return isFlightLoading
        ? CustomLoadingScr()
        : Container(
            padding: EdgeInsets.all(0),
            child: Column(
              children: [
                ListView.builder(
                    shrinkWrap: true,
                    itemCount: FlightData.length,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (_, index) {
                      return CustomFlightListingCard(
                        helijet: FlightData[index].airServiceType!,
                        vendorId: FlightData[index].vendorId!,
                        address1:
                            FlightData[index].bussinessDetails!.addressLine1!,
                        address2:
                            FlightData[index].bussinessDetails!.addressLine2!,
                        amaities: FlightData[index].bussinessDetails!.imagesk!,
                        businessName:
                            FlightData[index].bussinessDetails!.bussinessName!,
                        DepartureDate: takeoffdate,
                        discPrice: FlightData[index].minPrice!,
                        flightDetails: FlightData[index]
                            .bussinessDetails!
                            .shortDescription!,
                        // flightName: FlightData[index].pjSname,
                        flightName: FlightData[index].pjSname == null
                            ? " "
                            : FlightData[index].pjSname,
                        imagesk: FlightData[index].bussinessDetails!.imagesk!,
                        returnDate: landDate,
                        totalGuest: passengerNo,
                        logo:
                            FlightData[index].bussinessDetails!.bussinessLogo!,
                        fromAP: fromAP,
                        fromAPC: fromAPC,
                        toAP: toAP,
                        toAPC: toAPC,
                      );
                    }),
              ],
            ),
          );
  }
}

///////Heli Widget
class HelicopterWidget extends StatefulWidget {
  const HelicopterWidget({
    Key? key,
  }) : super(key: key);

  @override
  State<HelicopterWidget> createState() => _HelicopterWidgetState();
}

class _HelicopterWidgetState extends State<HelicopterWidget> {
  @override
  void initState() {
    // fetchFlightListingHeli();

    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return isFlightLoading
        ? CustomLoadingScr()
        : Container(
            padding: EdgeInsets.all(0),
            child: Column(
              children: [
                ListView.builder(
                    shrinkWrap: true,
                    itemCount: FlightData.length,
                    // physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (_, index) {
                      return CustomFlightListingCard(
                        logo:
                            FlightData[index].bussinessDetails!.bussinessLogo!,
                        address1:
                            FlightData[index].bussinessDetails!.addressLine1!,
                        address2:
                            FlightData[index].bussinessDetails!.addressLine2!,
                        amaities: FlightData[index].bussinessDetails!.imagesk!,
                        businessName:
                            FlightData[index].bussinessDetails!.bussinessName!,
                        DepartureDate: takeoffdate,
                        discPrice: FlightData[index].minPrice!,
                        flightDetails: FlightData[index]
                            .bussinessDetails!
                            .shortDescription!,
                        flightName: FlightData[index].pjSname == null
                            ? ""
                            : FlightData[index].pjSname,
                        imagesk: FlightData[index].bussinessDetails!.imagesk!,
                        returnDate: landDate,
                        totalGuest: passengerNo,
                        fromAP: fromAP,
                        fromAPC: fromAPC,
                        toAP: toAP,
                        toAPC: toAPC,
                        helijet: FlightData[index].airServiceType!,
                        vendorId: FlightData[index].vendorId!,
                      );
                    }),
                SizedBox(
                  height: 10.0,
                ),
              ],
            ),
          );
  }

//   Future fetchFlightListingHeli() async{
//      setState(() {
//       isFlightLoading = true;
//     });
//     await AirServiceApis().getFlightListing()
//     // FlightData = await AirServiceApis().getFlightListing(
//     //     widget.departureDate,
//     //     widget.returnDate,
//     //     widget.origin,
//     //     widget.destination,
//     //     widget.passengers,
//     //     widget.aircraft);
//     setState(() {
//       isFlightLoading = false;
//     });
//   }
// }
}
