import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/AirAPI/air_service_apis.dart';
import 'package:lasvegas_gts_app/Networking/AirAPI/flight_list_model.dart';
import 'package:lasvegas_gts_app/Networking/StayAPI/stay_search_specil_suits_api.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'package:lasvegas_gts_app/screens/services/stay/hotel_room_details.dart';
import 'package:lasvegas_gts_app/Networking/StayAPI/stay_vendol_model.dart'
as v;
import 'package:shared_preferences/shared_preferences.dart';

const String roomImgURL =
    'http://koolmindapps.com/lasvegas/public/images/air/';

class AirSelectRoom extends StatefulWidget {
  final String businessName;
  final String flightName;
  final String flightDetails;
  final String address1;
  final String address2;
  final List<String> imagesk;
  final String discPrice;
  final List<String> amaities;

  // final String totalRooms;
  final String totalGuest;
  final String DepartureDate;
  final String returnDate;
  final String fromAP,toAP,toAPC,fromAPC,vendorID,helijet;

  const AirSelectRoom(
      {Key? key,
        required this.businessName,
        // required this.roomName,
        // required this.roomDetails,
        required this.address1,
        required this.address2,
        required this.imagesk,
        required this.discPrice,
        // required this.totalRooms,
        required this.totalGuest,
        // required this.checkInDate,
        // required this.checkOutDate,
        required this.amaities,
        required this.flightName,
        required this.flightDetails,
        required this.DepartureDate,
        required this.returnDate, required this.fromAP, required this.toAP, required this.toAPC, required this.fromAPC, required this.vendorID, required this.helijet})
      : super(key: key);

  @override
  _AirSelectRoomState createState() => _AirSelectRoomState();
}

class _AirSelectRoomState extends State<AirSelectRoom> {

  List<ServiceAir> servicesData = [];
  bool isLoading = false;

  @override
  void initState() {
    // TODO: implement initState
    getRoomList();
    super.initState();
  }

  Future getRoomList() async{

    final SharedPreferences preferences = await SharedPreferences.getInstance();
    String homeToken = preferences.getString('userToken')!;

    String sub_type = widget.helijet;

    setState(() {
      isLoading = true;
    });

    servicesData = (await AirServiceApis().getSubFlightListing(
        widget.vendorID,
        widget.helijet
    )) as List<ServiceAir>;


    setState(() {
      isLoading = false;
    });

  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
          lead: BackButton(),
          title: Text(
            "Select Rooms",
            style: kHeadText,
          ),
        ),
        backgroundColor: bgcolor,
        body: isLoading
            ? CustomLoadingScr()
            :SingleChildScrollView(
          child: Column(
            children: [
              ListView.builder(
                  physics: NeverScrollableScrollPhysics(
                      parent: AlwaysScrollableScrollPhysics()),
                  shrinkWrap: true,
                  itemCount: servicesData.length,
                  itemBuilder: (_, index) {
                    /*return CustomRoomDisplayCard(
                      vendorID: widget.vendorID,
                      roomName: widget.roomName,
                      discPrice: widget.discPrice,
                      addLine1: widget.addLine1,
                      addLine2: widget.addLine2,
                      roomDetails: widget.roomDetails,
                      businessName: widget.businessName,
                      imagesk: widget.imagesk,
                      checkInDate: widget.checkInDate,
                      checkOutDate: widget.checkOutDate,
                      totalGuest: widget.totalGuest,
                      totalRooms: widget.totalRooms,
                      bLogo: widget.bLogo,
                      stayRoomPhoto: widget.stayRoomPhoto,
                      amani: widget.amani,
                    );*/

                    return CustomRoomDisplayCard(

                      vendorID: servicesData[index].vendorId!,
                      discPrice: servicesData[index].price!,
                      addLine1: servicesData[index].bussinessDetails!.addressLine1!,
                      addLine2: servicesData[index].bussinessDetails!.addressLine2!,
                      roomDetails: servicesData[index].descr!,
                      businessName: servicesData[index].pjSname == null ? '' : servicesData[index].pjSname,
                      imagesk: servicesData[index].bussinessDetails!.imagesk!,
                      totalGuest: widget.totalGuest,
                      bLogo: servicesData[index].bussinessDetails!.bussinessLogo!,
                      seat_avai: servicesData[index].personPj.toString(),
                      service_type: servicesData[index].airServiceType!,

                    );

                  }),

              // ///////Room Display Card

              // CustomRoomDisplayCard(),
              // CustomRoomDisplayCard(),
              // CustomRoomDisplayCard(),
              // CustomRoomDisplayCard(),
              // CustomRoomDisplayCard(),
            ],
          ),
        ),
      ),
    );
  }
}

class CustomRoomDisplayCard extends StatefulWidget {
  //final String roomName;
  final String discPrice;
  final String addLine2;
  final String addLine1;
  final String roomDetails;
  final String businessName;
  final List<String> imagesk;
  final String bLogo;
  final String totalGuest;
  final String vendorID;
  final String seat_avai;
  final String service_type;
  //final String quantity;

  const CustomRoomDisplayCard(
      {Key? key,
        //required this.roomName,
        required this.discPrice,
        required this.addLine2,
        required this.addLine1,
        required this.roomDetails,
        required this.businessName,
        required this.imagesk,
        required this.totalGuest,
        required this.bLogo,
        required this.vendorID,
        required this.seat_avai,
        required this.service_type,

      })
      : super(key: key);

  @override
  State<CustomRoomDisplayCard> createState() => _CustomRoomDisplayCardState();
}

class _CustomRoomDisplayCardState extends State<CustomRoomDisplayCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20.0),
      alignment: Alignment.center,
      padding: EdgeInsets.fromLTRB(20.0, 20.0, 15.00, 20),
      child: Column(
        //
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        '${widget.businessName} / ',
                        style: TextStyle(
                            fontFamily: 'Manrope-SemiBold',
                            fontSize: 18.0,
                            color: Colors.white),
                      ),

                      Text(
                        widget.service_type,
                        style: TextStyle(
                            fontFamily: 'Manrope-Medium',
                            color: greytxtcolor),
                      ),
                    ],
                  ),
                  /*SizedBox(
                    height: 5.0,
                  ),
                  Text(
                    "Room Size : 27 m^2",
                    style: TextStyle(
                        fontFamily: 'Manrope-Medium',
                        color: greytxtcolor),
                  ),*/
                  SizedBox(
                    height: 5.0,
                  ),

                  Text(
                    '${widget.seat_avai} Seats',
                    style: TextStyle(
                        fontFamily: 'Manrope-Medium',
                        color: greytxtcolor),
                  ),
                  SizedBox(
                    height: 5.0,
                  ),

                  Text(
                    '${widget.roomDetails}',
                    style: TextStyle(
                        fontFamily: 'Manrope-Medium',
                        color: greytxtcolor),
                  ),

                  Visibility(
                    visible: false,
                    child: Row(
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.white,
                          child: Image.asset(
                            "assets/icons/ic_wifi.png",
                            height: 20.0,
                            width: 20.0,
                            color: Colors.purple,
                          ),
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        CircleAvatar(
                          backgroundColor: Colors.white,
                          child: Image.asset("assets/icons/ic_breakfasst.png",
                              height: 20.0, width: 20.0, color: Colors.purple),
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        CircleAvatar(
                          backgroundColor: Colors.white,
                          child: Image.asset("assets/icons/ic_swiming.png",
                              height: 20.0, width: 20.0, color: Colors.purple),
                        ),
                      ],
                    ),
                  ),
                ],
              ),

              /////

              Container(
                  margin: EdgeInsets.all(5.0),
                  child: Image.network(
                    roomImgURL + widget.imagesk[0],
                    height: 108,
                    width: 108,
                    fit: BoxFit.fill,
                  )),
            ],
          ),
          SizedBox(
            height: 25.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "\$ " + widget.discPrice,
                    style: TextStyle(
                      fontFamily: 'Manrope-SemiBold',
                      fontSize: 18.0,
                      color: Colors.white,
                    ),
                  ),
                  Text(
                    "Per Day",
                    style: TextStyle(color: greytxtcolor),
                  ),
                ],
              ),
              ElevatedButton(
                onPressed: () {
                  /*Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) => HottelDetailsScreen(
                            vendorID: widget.vendorID,
                            address1: widget.addLine1,
                            address2: widget.addLine2,
                            roomDetails: widget.roomDetails,
                            businessName: widget.businessName,
                            discPrice: widget.discPrice,
                            imagesk: widget.imagesk,
                            roomName: widget.roomName,
                            checkInDate: widget.checkInDate,
                            checkOutDate: widget.checkOutDate,
                            totalGuest: widget.totalGuest,
                            totalRooms: widget.totalRooms,
                            amaities: widget.amani,
                            roomImg: widget.stayRoomPhoto[0],
                          )));*/
                },
                child: Text(
                  "Book Now",
                  style: TextStyle(
                      fontFamily: 'Raleway-Medium',
                      color: Color(0xff121A31),
                      fontSize: 14.0),
                ),
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  fixedSize: Size(108.0, 40.0),
                  primary: Color(0xFFFFC71E),
                ),
              ),
            ],
          ),
        ],
      ),
      decoration: BoxDecoration(
        color: Colors.transparent,
        border: Border.all(color: txtborderbluecolor),
        borderRadius: BorderRadius.all(Radius.circular(15.0)),
      ),
    );
  }
}
