import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/AirAPI/air_service_apis.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';
import 'package:lasvegas_gts_app/screens/services/air/air_flight_list_scr.dart';
import 'package:lasvegas_gts_app/utils/alert_utils.dart';

String heliIMG =
    'http://koolmindapps.com/lasvegas/public/frontend/images/helicop.jpg';
bool isAirBooking = false;

class AirCheckout extends StatefulWidget {
  final String fromAP,
      toAP,
      fromAPC,
      toAPC,
      dDate,
      tPass,
      rDate,
      helijet,
      vendorID,
      pname,
      page,
      pEmail,
      price,
      pMob,
      plane_type;
  const AirCheckout(
      {Key? key,
      required this.fromAP,
      required this.toAP,
      required this.fromAPC,
      required this.toAPC,
      required this.dDate,
      required this.tPass,
      required this.rDate,
      required this.helijet,
      required this.vendorID,
      required this.pname,
      required this.page,
      required this.pEmail,
      required this.price,
      required this.pMob,
      required this.plane_type})
      : super(key: key);

  @override
  _AirCheckoutState createState() => _AirCheckoutState();
}

class _AirCheckoutState extends State<AirCheckout> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomSheet: GestureDetector(
        onTap: () async {
          setState(() {
            isAirBooking = true;
          });
          await AirServiceApis().airBooking(
            widget.dDate,
            widget.rDate,
            widget.fromAPC,
            widget.toAPC,
            widget.tPass,
            widget.helijet,
            homeUserId,
            widget.vendorID,
            widget.price,
            widget.pname,
            widget.pMob,
            widget.pEmail,
            widget.page,
            widget.plane_type,
          );
          setState(() {
            isAirBooking = false;
          });
/*for (var i = 0; i < 5; i++) {
   Navigator.pop(context);
}*/

          int count = 0;
          Navigator.popUntil(context, (route) {
            return count++ == 5;
          });
          AlertUtils.showAutoCloseDialogue(context, "Air Booking successfully", 3, 'Successful');
         

          // push(
          // context,
          // MaterialPageRoute(
          //     builder: (_) => FlightList(
          //               aircraft: isHelicopter ? 'Helicopter' : 'Private jets',
          //               departureDate:  widget.dDate,
          //               destination: widget.toAPC,
          //               origin: widget.fromAPC,
          //               passengers: widget.tPass,
          //               returnDate: widget.rDate,
          //               fromAP: widget.fromAP,
          //               fromAPC: widget.fromAPC,
          //               toAP: widget.toAP,
          //               toAPC: widget.toAPC,
          //             )));
        },
        child: Container(
          alignment: Alignment.center,
          child: Text(
            "PAY NOW",
            style: TextStyle(fontSize: 18.0, letterSpacing: 0.7),
          ),
          height: 57.0,
          color: btngoldcolor,
        ),
      ),
      backgroundColor: bgcolor,
      appBar: CustomAppBar(
          lead: BackButton(
            color: Colors.white,
          ),
          title: Text('Checkout')),
      body: isAirBooking
          ? CustomLoadingScr()
          : Container(
              alignment: Alignment.center,
              margin: EdgeInsets.all(10),
              child: Container(
                child: Column(
                  children: [
                    ///////white container
                    Container(
                      padding: EdgeInsets.fromLTRB(10, 20, 10, 10),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(color: txtborderbluecolor),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(15.0),
                            topRight: Radius.circular(15.0),
                          )),
                      // color: Colors.white,
                      height: 100,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Column(
                            children: [
                              SizedBox(
                                height: 10.0,
                              ),
                              Text(
                                widget.fromAP,
                                style: kHeadText.copyWith(color: Colors.black),
                              ),
                              Text(
                                widget.fromAPC,
                                style: kTxtStyle.copyWith(
                                  color: Colors.black,
                                  fontSize: 8,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            width: 8.0,
                          ),
                          Image.asset(
                            "assets/icons/plane.png",
                            height: 70,
                            width: 70,
                          ),
                          SizedBox(
                            width: 8.0,
                          ),
                          Column(
                            children: [
                              SizedBox(
                                height: 10.0,
                              ),
                              Text(
                                widget.toAP,
                                style: kHeadText.copyWith(color: Colors.black),
                              ),
                              Text(
                                widget.toAPC,
                                style: kTxtStyle.copyWith(
                                  color: Colors.black,
                                  fontSize: 8,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),

                    ////// blue container
                    Container(
                      padding: EdgeInsets.all(20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Date",
                                      style: kTxtStyle.copyWith(
                                          color: Colors.white38, fontSize: 14),
                                    ),
                                    SizedBox(
                                      height: 10.0,
                                    ),
                                    Text(
                                      widget.dDate,
                                      style: kTxtStyle.copyWith(
                                          color: Colors.white),
                                    ),
                                  ]),
                              SizedBox(width: 100.0),
                              Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Jet No.",
                                      style: kTxtStyle.copyWith(
                                          color: Colors.white38, fontSize: 14),
                                    ),
                                    SizedBox(
                                      height: 10.0,
                                    ),
                                    Text(
                                      'data',
                                      style: kTxtStyle.copyWith(
                                          color: Colors.white),
                                    ),
                                  ])
                            ],
                          ),
                          SizedBox(height: 20.0),
                          Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Boarding Time",
                                  style: kTxtStyle.copyWith(
                                      color: Colors.white38, fontSize: 14),
                                ),
                                SizedBox(
                                  height: 10.0,
                                ),
                                Text(
                                  'data',
                                  style:
                                      kTxtStyle.copyWith(color: Colors.white),
                                ),
                              ]),
                          SizedBox(height: 20.0),
                          Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Total Passengers",
                                  style: kTxtStyle.copyWith(
                                      color: Colors.white38, fontSize: 14),
                                ),
                                SizedBox(
                                  height: 10.0,
                                ),
                                Text(
                                  widget.tPass,
                                  style:
                                      kTxtStyle.copyWith(color: Colors.white),
                                ),
                              ]),
                        ],
                      ),
                    )

                    // Container(
                    //   decoration:
                    //       kBorderDecoration.copyWith(color: servicecardfillcolor),
                    //   // color: Colors.white,
                    //   height: 00,
                    // ),
                  ],
                ),
                decoration:
                    kBorderDecoration.copyWith(color: servicecardfillcolor),
                height: 500,
                width: 340,
              ),
            ),
    );
  }
}
