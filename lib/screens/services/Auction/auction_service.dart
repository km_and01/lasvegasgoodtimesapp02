// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/AuctionServiceAPI/auction_service_api.dart';
import 'package:lasvegas_gts_app/Networking/AuctionServiceAPI/auction_service_model.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'package:lasvegas_gts_app/screens/services/Auction/custom_auction_service_cart.dart';

bool isAuctionSLoading = false;
List<AuctionS> auctionServiceData = [];

// ignore: must_be_immutable
class AuctionService extends StatefulWidget {
  AuctionService({Key? key, required this.serviceTitle}) : super(key: key);

  String serviceTitle;

  @override
  State<AuctionService> createState() => _AuctionServiceState();
}

class _AuctionServiceState extends State<AuctionService> {
  @override
  void initState() {
    fetchAuctionServiceData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
            lead: Image.asset(
              "assets/icons/ic_event.png",
              color: Colors.white,
            ),
            title: Text(
              widget.serviceTitle,
              style: TextStyle(
                  fontSize: 20.0, fontFamily: 'Helvetica', color: Colors.white),
            )),
        backgroundColor: bgcolor,
        body: isAuctionSLoading
            ? CustomLoadingScr()
            : SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      ListView.builder(
                          physics: NeverScrollableScrollPhysics(
                              parent: AlwaysScrollableScrollPhysics()),
                          shrinkWrap: true,
                          itemCount: auctionServiceData.length,
                          itemBuilder: (_, index) {
                            return CustomAuctionServiceCard(
                              auctionID: auctionServiceData[index]
                                  .id!
                                  .toString(),
                              auctionServiceIMG:
                                  auctionServiceData[index].image!.first,
                              bname: auctionServiceData[index].name!,
                              catName: auctionServiceData[index].categoryName!,
                              price: auctionServiceData[index].openingPrice!,
                              openingTime:
                                  auctionServiceData[index].openingTime!,
                              closingTime:
                                  auctionServiceData[index].closingTime!,
                              timeZone: auctionServiceData[index].timezone!,
                              auctionIMGs: auctionServiceData[index].image!,
                              add1: auctionServiceData[index].description!,
                              add2: auctionServiceData[index]
                                  .bussinessDetails!
                                  .addressLine2!,
                            );
                          }),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Future fetchAuctionServiceData() async {
    setState(() {
      isAuctionSLoading = true;
    });

    try {
      auctionServiceData = await AuctionServiceAPIs().getAuctionServiceData(
        widget.serviceTitle,
      );
      print('objectData=============> ' + auctionServiceData.length.toString());
    } catch (e) {
      print("Error DATA =====" + e.toString());
    }
    setState(() {
      isAuctionSLoading = false;
    });
  }
}
