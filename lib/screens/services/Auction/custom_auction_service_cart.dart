// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/screens/services/Auction/auction_service_details_page.dart';

String auctionID = '';
String serviceEventID = '';
String imageUrl = 'http://koolmindapps.com/lasvegas/public/images/auction/';

class CustomAuctionServiceCard extends StatefulWidget {
  // final String shortDisc;
  final String auctionID, add1, add2;
  final String auctionServiceIMG,
      bname,
      catName,
      price,
      openingTime,
      closingTime,
      timeZone;
  final List auctionIMGs;

  const CustomAuctionServiceCard({
    Key? key,
    required this.auctionID,
    required this.auctionServiceIMG,
    required this.bname,
    required this.catName,
    required this.price,
    required this.openingTime,
    required this.closingTime,
    required this.timeZone,
    required this.auctionIMGs,
    required this.add1,
    required this.add2,
  }) : super(key: key);

  @override
  State<CustomAuctionServiceCard> createState() => _CustomAuctionServiceCard();
}

class _CustomAuctionServiceCard extends State<CustomAuctionServiceCard> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        print('objectAuctionID5554545============>  ${widget.auctionID}');
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => AuctionServiceDetailPage(
              auctionID: widget.auctionID,
              auctionBIMGS: widget.auctionIMGs,
              add1: widget.add1,
              add2: widget.add2,
              bname: widget.bname,
              catName: widget.catName,
            ),
          ),
        );
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 7.5),
        decoration: BoxDecoration(
          // color: servicecardfillcolor,
          border: Border.all(color: txtborderbluecolor),
          borderRadius: BorderRadius.all(Radius.circular(7.0)),
        ),
        child: Row(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(7.0),
                bottomLeft: Radius.circular(7.0),
              ),
              child: Image.network(
                imageUrl + widget.auctionServiceIMG,
                fit: BoxFit.fill,
                height: 130,
                width: 130,
              ),
            ),
            SizedBox(
              width: 10.0,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.bname,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18.0,
                        fontFamily: 'Raleway-Medium'),
                  ),
                  SizedBox(
                    height: 8.0,
                  ),
                  Text(
                    widget.add1,
                    softWrap: false,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: TextStyle(
                      fontFamily: 'Manrope',
                      fontSize: 14.0,
                      color: Colors.grey,
                    ),
                  ),
                  SizedBox(
                    height: 3.0,
                  ),
                  Text(
                    "Opens at " + widget.openingTime,
                    style: TextStyle(
                      fontFamily: 'Manrope',
                      fontSize: 14.0,
                      color: Colors.grey,
                    ),
                  ),
                  SizedBox(
                    height: 3.0,
                  ),
                  Text(
                    "Closes at " + widget.closingTime,
                    style: TextStyle(
                        fontFamily: 'Manrope',
                        fontSize: 14.0,
                        color: Colors.grey),
                  ),
                  SizedBox(
                    height: 3.0,
                  ),
                  Text(
                    widget.timeZone,
                    style: TextStyle(
                        fontFamily: 'Manrope',
                        fontSize: 14.0,
                        color: Colors.grey),
                  ),
                  SizedBox(
                    height: 3.0,
                  ),
                  Text(
                    "Opening Price: \$" + widget.price,
                    style: TextStyle(
                        fontSize: 16.0,
                        fontFamily: 'Manrope-SemiBold',
                        color: Colors.white),
                  ),
                  SizedBox(
                    height: 3.0,
                  ),
                  Text(
                    "Highest Bid: \$" + widget.price,
                    style: TextStyle(
                        fontSize: 16.0,
                        fontFamily: 'Manrope-SemiBold',
                        color: Colors.white),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
