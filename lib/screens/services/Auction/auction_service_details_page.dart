// ignore_for_file: avoid_print

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/AuctionServiceAPI/auction_service_api.dart';
import 'package:lasvegas_gts_app/Networking/AuctionServiceAPI/auction_service_details_model.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';

class AuctionServiceDetailPage extends StatefulWidget {
  const AuctionServiceDetailPage({
    Key? key,
    required this.auctionID,
    required this.auctionBIMGS,
    required this.add1,
    required this.add2,
    required this.bname,
    required this.catName,
  }) : super(key: key);

  final String auctionID;
  final String add1, add2, bname, catName;
  final List auctionBIMGS;

  @override
  State<AuctionServiceDetailPage> createState() =>
      _AuctionServiceDetailPageState();
}

class _AuctionServiceDetailPageState extends State<AuctionServiceDetailPage> {
  bool isEventPkgLoading = false;
  AuctionServiceDetailsModel? auctionData;
  String highestBid = '50000.26';
  List<int> amount = [];
  List<String> auctionIMGList = [];
  String auctionBusinessLOGO =
      "http://koolmindapps.com/lasvegas/public/images/auction/";
  int? selectedRadio;
  bool isVisible = false;

  @override
  void initState() {
    int len = widget.auctionBIMGS.length;
    auctionIMGList.clear();
    for (var i = 0; i < len; i++) {
      auctionIMGList.add(auctionBusinessLOGO + widget.auctionBIMGS[i]);
    }
    fetchauctionServiceDetailsData();
    selectedRadio = 0;
    super.initState();
  }

  setSelectedRadio(int val) {
    setState(() {
      selectedRadio = val;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
            lead: Image.asset(
              "assets/icons/ic_event.png",
              color: Colors.white,
            ),
            title: Text(
              widget.catName,
              style: TextStyle(
                  fontSize: 20.0, fontFamily: 'Helvetica', color: Colors.white),
            )),
        backgroundColor: bgcolor,
        body: isEventPkgLoading
            ? CustomLoadingScr()
            : SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 20),
                      CarouselSlider(
                        items: auctionIMGList
                            .map(
                              (x) => Container(
                                width: double.infinity,
                                decoration: BoxDecoration(
                                  //  color: Colors.yellow,
                                  borderRadius: BorderRadius.circular(8.0),
                                  image: DecorationImage(
                                    fit: BoxFit.fill,
                                    image: NetworkImage(x, scale: 1),
                                  ),
                                ),
                              ),
                            )
                            .toList(),
                        // autoPlay: true,
                        // height: 200.0,
                        options: CarouselOptions(
                          height: 200,
                          aspectRatio: 16 / 9,
                          viewportFraction: 0.9,
                          initialPage: 0,
                          enableInfiniteScroll: true,
                          reverse: false,
                          autoPlay: true,
                          autoPlayInterval: Duration(seconds: 3),
                          autoPlayAnimationDuration:
                              Duration(milliseconds: 800),
                          autoPlayCurve: Curves.fastOutSlowIn,
                          enlargeCenterPage: true,
                          // onPageChanged: callbackFunction,
                          scrollDirection: Axis.horizontal,
                        ),
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                      Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  widget.bname,
                                  style: TextStyle(
                                      fontSize: 24.0,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                      fontFamily: 'Manrope-SemiBold'),
                                ),
                                ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                      primary: buttonBackColor),
                                  onPressed: () {
                                    placeYourBidDialog();
                                  },
                                  child: Text(
                                    'I am interested',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 18.0,
                                        fontFamily: 'Manrope_Medium'),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 18.0,
                            ),
                            Text(
                              widget.add1 + ", " + widget.add2,
                              softWrap: false,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 2,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16.0,
                                  fontFamily: 'Manrope_Medium'),
                            ),
                          ]),
                      SizedBox(
                        height: 20.0,
                      ),
                      Row(
                        children: [
                          Text(
                            "Highest Bid: ",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20.0,
                                fontFamily: 'Manrope-SemiBold',
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            '\$' + highestBid,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20.0,
                                fontFamily: 'Manrope-SemiBold',
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 14.0,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Auction Includes: ",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 22.0,
                                fontFamily: 'Manrope-SemiBold',
                                fontWeight: FontWeight.bold),
                          ),
                          ListView.builder(
                              physics: NeverScrollableScrollPhysics(
                                parent: AlwaysScrollableScrollPhysics(),
                              ),
                              shrinkWrap: true,
                              itemCount:
                                  auctionData!.data!.auctionIncludes!.length,
                              itemBuilder: (_, index) {
                                return Text(
                                  '${index + 1}'.toString() +
                                      '. ' +
                                      auctionData!
                                          .data!.auctionIncludes![index],
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16.0,
                                      fontFamily: 'Manrope-SemiBold',
                                      fontWeight: FontWeight.bold),
                                );
                              }),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            "Auction Excludes: ",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 22.0,
                                fontFamily: 'Manrope-SemiBold',
                                fontWeight: FontWeight.bold),
                          ),
                          ListView.builder(
                              physics: NeverScrollableScrollPhysics(
                                parent: AlwaysScrollableScrollPhysics(),
                              ),
                              shrinkWrap: true,
                              itemCount:
                                  auctionData!.data!.auctionExcludes!.length,
                              itemBuilder: (_, index) {
                                return Text(
                                  '${index + 1}'.toString() +
                                      '. ' +
                                      auctionData!
                                          .data!.auctionExcludes![index],
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16.0,
                                      fontFamily: 'Manrope-SemiBold',
                                      fontWeight: FontWeight.bold),
                                );
                              }),
                        ],
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Row(
                        children: [
                          Text(
                            "Time Zone: ",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20.0,
                                fontFamily: 'Manrope-SemiBold',
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            auctionData!.data!.timezone.toString(),
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18.0,
                                fontFamily: 'Manrope-SemiBold',
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Row(
                        children: [
                          Text(
                            "Closes At: ",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20.0,
                                fontFamily: 'Manrope-SemiBold',
                                fontWeight: FontWeight.bold),
                          ),
                          Text(
                            auctionData!.data!.closingTime.toString(),
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18.0,
                                fontFamily: 'Manrope-SemiBold',
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 50,
                        padding: EdgeInsets.all(10.0),
                        decoration: BoxDecoration(
                          color: Colors.transparent,
                          border: Border.all(
                            color: Colors.blue,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                        child: Center(
                          child: Text(
                            'Bid History',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 22.0,
                                fontFamily: 'Manrope-SemiBold',
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      ListView.builder(
                          physics: NeverScrollableScrollPhysics(
                            parent: AlwaysScrollableScrollPhysics(),
                          ),
                          shrinkWrap: true,
                          itemCount: auctionData!.demo!.length,
                          itemBuilder: (_, index) {
                            return Container(
                              width: MediaQuery.of(context).size.width,
                              height: 50,
                              padding: EdgeInsets.all(10.0),
                              decoration: BoxDecoration(
                                color: Colors.transparent,
                                border: Border.all(
                                  color: Colors.blue,
                                  width: 1,
                                ),
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                              child: Center(
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Text(
                                      auctionData!.demo![index].date! +
                                          ' ' +
                                          auctionData!.demo![index].time!,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18.0,
                                          fontFamily: 'Manrope-SemiBold',
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      '\$' + auctionData!.demo![index].ammount!,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18.0,
                                          fontFamily: 'Manrope-SemiBold',
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      '@' + auctionData!.demo![index].name!,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18.0,
                                          fontFamily: 'Manrope-SemiBold',
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          }),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  placeYourBidDialog() {
    print('Button Clicked!');
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            backgroundColor: bgcolor,
            elevation: 5,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0)),
            child: SizedBox(
              height: 300,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      'PLACE YOUR BID',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                          fontFamily: 'Manrope_Medium'),
                    ),
                    SizedBox(
                      height: 12.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'AUCTION PRICE: ',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20.0,
                                fontFamily: 'Manrope_Medium'),
                          ),
                          SizedBox(
                            width: 22.0,
                          ),
                          Text(
                            '\$' + highestBid,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20.0,
                                fontFamily: 'Manrope_Medium'),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 12.0,
                    ),
                    Row(
                      children: [
                        Radio(
                          value: 1,
                          groupValue: selectedRadio,
                          activeColor: Colors.white,
                          onChanged: (int? val) {
                            setSelectedRadio(val!);
                          },
                        ),
                        Text(
                          '\$25',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 14.0,
                              fontFamily: 'Manrope-SemiBold',
                              fontWeight: FontWeight.bold),
                        ),
                        Radio(
                          value: 2,
                          groupValue: selectedRadio,
                          activeColor: Colors.white,
                          onChanged: (int? val) {
                            setSelectedRadio(val!);
                          },
                        ),
                        Text(
                          '\$50',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 14.0,
                              fontFamily: 'Manrope-SemiBold',
                              fontWeight: FontWeight.bold),
                        ),
                        Radio(
                          value: 3,
                          groupValue: selectedRadio,
                          activeColor: Colors.white,
                          onChanged: (int? val) {
                            setSelectedRadio(val!);
                          },
                        ),
                        Text(
                          '\$75',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 14.0,
                              fontFamily: 'Manrope-SemiBold',
                              fontWeight: FontWeight.bold),
                        ),
                        Radio(
                          value: 4,
                          groupValue: selectedRadio,
                          activeColor: Colors.white,
                          onChanged: (int? val) {
                            setState(() {
                              isVisible = true;
                            });
                            setSelectedRadio(val!);
                          },
                        ),
                        Text(
                          'CUSTOM',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 14.0,
                              fontFamily: 'Manrope-SemiBold',
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 12.0,
                    ),
                    Visibility(
                      visible: isVisible,
                      child: Row(
                        children: [
                          Text(
                            'ENTER A BID AMOUNT',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20.0,
                                fontFamily: 'Manrope_Medium'),
                          ),
                          SizedBox(
                            width: 12.0,
                          ),
                          Text(widget.bname),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 12.0,
                    ),
                    Visibility(
                      visible: selectedRadio != 0,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'NEXT BID PRICE: ',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20.0,
                                fontFamily: 'Manrope_Medium'),
                          ),
                          SizedBox(
                            width: 22.0,
                          ),
                          Text(
                            '\$' + highestBid,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20.0,
                                fontFamily: 'Manrope_Medium'),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  Future fetchauctionServiceDetailsData() async {
    setState(() {
      isEventPkgLoading = true;
    });
    try {
      auctionData = await AuctionServiceAPIs()
          .getAuctionServiceDetailData(widget.auctionID);
      print("PKG EVENT DATA =====  ${auctionData!.demo!.toList()}");
      print("PKG EVENT LENGTH =====" + auctionData!.demo!.length.toString());
      print(
          "Amount List : ${auctionData!.demo!.map((e) => e.ammount!.toString())}");
      for (int i = 0; i < auctionData!.demo!.length; i++) {
        int auctionAm = double.parse(auctionData!.demo![i].ammount!).toInt();
        amount.add(auctionAm.toInt());
      }
      print("Amount List : ${amount.toList()}");
      amount.sort();
      print("Smallest value in the list : ${amount.first}");
      highestBid = amount.first.toString();
    } catch (e) {
      print("Error DATA =====" + e.toString());
    }

    setState(() {
      isEventPkgLoading = false;
    });
  }
}
