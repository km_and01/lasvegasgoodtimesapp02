// ignore_for_file: avoid_print

import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/EventAPI/event_pkg_model.dart';
import 'package:lasvegas_gts_app/Networking/EventAPI/event_service_api.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/custom_event_pkg_card.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';

bool isEventPkgLoading = false;
List<EventPKGData> EventPkgData = [];
List<String> pkgIMGList = [];
String eventBusinessLOGO =
    "http://koolmindapps.com/lasvegas/public/images/business_detail/";

class EventPkgListingScr extends StatefulWidget {
  final String eventVendorID;
  final String add1, add2, bname;
  final List eventBIMGS;
  const EventPkgListingScr({
    Key? key,
    required this.eventVendorID,
    required this.eventBIMGS,
    required this.add1,
    required this.add2,
    required this.bname,
  }) : super(key: key);

  @override
  State<EventPkgListingScr> createState() => _EventPkgListingScrState();
}

class _EventPkgListingScrState extends State<EventPkgListingScr> {
  @override
  void initState() {
    int len = widget.eventBIMGS.length;
    pkgIMGList.clear();
    for (var i = 0; i < len; i++) {
      pkgIMGList.add(eventBusinessLOGO + widget.eventBIMGS[i]);
    }
    fetchEventPkgData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
            lead: Image.asset(
              "assets/icons/ic_event.png",
              color: Colors.white,
            ),
            title: Text(
              "Event",
              style: TextStyle(
                  fontSize: 20.0, fontFamily: 'Helvetica', color: Colors.white),
            )),
        backgroundColor: bgcolor,
        body: isEventPkgLoading
            ? CustomLoadingScr()
            : SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 20),
                      CarouselSlider(
                        items: pkgIMGList
                            .map(
                              (x) => Container(
                                width: double.infinity,
                                decoration: BoxDecoration(
                                  //  color: Colors.yellow,
                                  borderRadius: BorderRadius.circular(8.0),
                                  image: DecorationImage(
                                    fit: BoxFit.fill,
                                    image: NetworkImage(x, scale: 1),
                                  ),
                                ),
                              ),
                            )
                            .toList(),
                        // autoPlay: true,
                        // height: 200.0,
                        options: CarouselOptions(
                          height: 200,
                          aspectRatio: 16 / 9,
                          viewportFraction: 0.9,
                          initialPage: 0,
                          enableInfiniteScroll: true,
                          reverse: false,
                          autoPlay: true,
                          autoPlayInterval: Duration(seconds: 3),
                          autoPlayAnimationDuration:
                              Duration(milliseconds: 800),
                          autoPlayCurve: Curves.fastOutSlowIn,
                          enlargeCenterPage: true,
                          // onPageChanged: callbackFunction,
                          scrollDirection: Axis.horizontal,
                        ),
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                      Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.bname,
                              style: TextStyle(
                                  fontSize: 23.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                  fontFamily: 'Manrope-SemiBold'),
                            ),
                            Text(
                              widget.add1, // + ", " + widget.add2,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18.0,
                                  fontFamily: 'Manrope_Medium'),
                            ),
                          ]),
                      SizedBox(
                        height: 20.0,
                      ),
                      Text(
                        "Ongoing Events : ",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 18.0,
                            fontFamily: 'Manrope-SemiBold',
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      ListView.builder(
                          physics: NeverScrollableScrollPhysics(
                              parent: AlwaysScrollableScrollPhysics()),
                          shrinkWrap: true,
                          itemCount: EventPkgData.length,
                          itemBuilder: (_, index) {
                            return CustomEventPackageCard(
                              packageIMG: EventPkgData[index].mapImage![0],
                              packageName: EventPkgData[index].eventName!,
                              priceUnit: EventPkgData[index].priceUnit![0],
                              packagePrice: EventPkgData[index].price![0],
                              packageDiscPrice: EventPkgData[index].price![0],
                              add1: EventPkgData[index]
                                  .bussinessDetails!
                                  .addressLine1!,
                              add2: EventPkgData[index]
                                  .bussinessDetails!
                                  .addressLine2!,
                              fullDetails: EventPkgData[index]
                                  .bussinessDetails!
                                  .shortDescription!,
                              pkgIMGList: EventPkgData[index]
                                  .bussinessDetails!
                                  .imagesk!,
                              pkgVendorID: EventPkgData[index].vendorId!,
                              // shortDetails: EventPkgData[index]
                              //     .bussinessDetails!
                              //     .shortDescription!,
                              eventName: EventPkgData[index].eventServiceName!,
                              location: EventPkgData[index].location!,
                              totalStall: EventPkgData[index].totalStall!,
                              gatherPeople: EventPkgData[index].peopleArrive!,
                              stallArea: EventPkgData[index].area!,
                              stallfees: EventPkgData[index].price!,
                              stallFor: EventPkgData[index].stallFor!,
                              stallLoc: EventPkgData[index].stallLocation!,
                              stallFeeUnit: EventPkgData[index].priceUnit!,
                              serviceID: EventPkgData[index].id!.toString(),
                            );
                          }),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Future fetchEventPkgData() async {
    setState(() {
      isEventPkgLoading = true;
    });
    try {
      EventPkgData = await EventAPIs().getEventPkgData(widget.eventVendorID);
      print("PKG EVENT DATA =====" + EventPkgData[0].eventName!);
      print("PKG EVENT LENGTH =====" + EventPkgData.length.toString());
    } catch (e) {
      print("Error DATA =====" + e.toString());
    }

    setState(() {
      isEventPkgLoading = false;
    });
  }
}
