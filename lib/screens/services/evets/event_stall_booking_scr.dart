import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/EventAPI/event_service_api.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';
import 'package:lasvegas_gts_app/screens/services/eventSpace/event_vendor_listing_scr.dart';
import 'dart:ui' as ui;

import 'package:lasvegas_gts_app/utils/alert_utils.dart';

bool isEventStallBooking = false;
TextEditingController uNameController = TextEditingController();
TextEditingController uEmailController = TextEditingController();
TextEditingController uMobController = TextEditingController();

String bookMsg = '';

class EventStallBookings extends StatefulWidget {
  final String vendorID, pkgIMG, stallPrice, eventName, serviceID;

  const EventStallBookings({
    Key? key,
    required this.vendorID,
    required this.pkgIMG,
    required this.stallPrice,
    required this.eventName,
    required this.serviceID,
  }) : super(key: key);

  @override
  _EventStallBookingsState createState() => _EventStallBookingsState();
}

class _EventStallBookingsState extends State<EventStallBookings> {
  @override
  void initState() {
    uMobController.text = homeUserPhone;
    uEmailController.text = homeUserEmail;
    uNameController.text = homeUserName;

    // selectedTime = selectedTimeES;
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar: ElevatedButton(
          onPressed: () async {
            await bookEvent();


            if(bookMsg.contains('Vendor Service Successfully Done')){
              int count = 0;
              Navigator.popUntil(context, (route) {
                return count++ == 5;
              });
              AlertUtils.showAutoCloseDialogue(context, "Event Booking successfully", 3, 'Successful');
            }else {
              AlertUtils.showAutoCloseDialogue(context, 'Booking Failed', 3, 'Oops!');
            }


          },
          child: Text(
            'BOOK NOW',
            style: TextStyle(
                fontFamily: 'Helvetica',
                color: Color(0xff121A31),
                fontSize: 18.0,
                letterSpacing: 0.7
            ),
          ),
          style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(13.0),
            ),
            fixedSize: Size(389.0, 57.0),
            primary: Color(0xFFFFC71E),
          ),
        ),
        backgroundColor: bgcolor,
        appBar: CustomAppBar(
          lead: BackButton(),
          title: Text(
            "Booking Details",
            style: TextStyle(
                fontSize: 20.0,
                fontFamily: 'Helvetica',
                color: Colors.white
            ),
          ),
        ),
        body: isEventStallBooking
            ? CustomLoadingScr()
            : SingleChildScrollView(
                reverse: true,
                child: Container(
                  margin: EdgeInsets.all(20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      //////Name user
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                        child: Text(
                          'Name',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontSize: 14.0,
                            letterSpacing: 0.8,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      TextFormField(
                        cursorColor: Colors.white,
                        controller: uNameController,
                        style: kTxtStyle,
                        keyboardType: TextInputType.text,
                        // controller: vendorPhoneNoController,
                        decoration: kTxtFieldDecoration,
                      ),
                      SizedBox(height: 25.0),

                      //////Email
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                        child: Text(
                          'Email-ID',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontSize: 14.0,
                            letterSpacing: 0.8,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      TextFormField(
                        cursorColor: Colors.white,
                        controller: uEmailController,
                        style: kTxtStyle,
                        keyboardType: TextInputType.emailAddress,
                        // controller: vendorPhoneNoController,
                        decoration: kTxtFieldDecoration,
                      ),
                      SizedBox(height: 25.0),

                      //////Phone
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                        child: Text(
                          'Mobile Number',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontSize: 14.0,
                            letterSpacing: 0.8,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      TextFormField(
                        cursorColor: Colors.white,
                        controller: uMobController,
                        style: kTxtStyle,
                        keyboardType: TextInputType.number,
                        // controller: vendorPhoneNoController,
                        decoration: kTxtFieldDecoration,
                      ),
                      SizedBox(height: 25.0),

                      //////Amount
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Amount",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 14.0,
                              letterSpacing: 0.8,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 10.0),
                            height: 46.0,
                            width: 166.0,
                            child: Text(
                              widget.stallPrice,
                              // widget.amount,
                              textAlign: TextAlign.center,
                              style: kTxtStyle.copyWith(
                                fontSize: 16.0,
                                fontFamily: 'Regular',
                              ),
                            ),
                            decoration: BoxDecoration(
                              // color: servicecardfillcolor,
                              border: Border.all(color: txtborderbluecolor),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 25.0),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Future bookEvent() async {
    setState(() {
      isEventStallBooking = true;
    });

    bookMsg = await EventAPIs().bookEvent(
      widget.vendorID,
      widget.pkgIMG,
      widget.serviceID,
      widget.eventName,
      widget.stallPrice,
      uNameController.text,
      uMobController.text,
      uEmailController.text,
    );

   

    setState(() {
      isEventStallBooking = false;
    });
  }
}
