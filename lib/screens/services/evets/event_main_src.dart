// ignore_for_file: avoid_print

import 'package:autocomplete_textfield_ns/autocomplete_textfield_ns.dart';
import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/StayAPI/stay_search_specil_suits_api.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';
import 'package:lasvegas_gts_app/screens/services/evets/event_vendor_listinf_scr.dart';
import 'package:lasvegas_gts_app/screens/services/stay/city_list_model.dart';
import 'dart:ui' as ui;



bool isLoadingcity = false;

String eventCityCode = '';
String eventCitytSelected = '';
class EventHome extends StatefulWidget {
  const EventHome({ Key? key }) : super(key: key);

  @override
  _EventHomeState createState() => _EventHomeState();
}

class _EventHomeState extends State<EventHome> {
    List<String> cityList = [];
  List<String> cityCodeList = [];

  TextEditingController cityController = TextEditingController();
  GlobalKey<AutoCompleteTextFieldState<String>> cityKey = GlobalKey();

  @override
  void initState() {
    // TODO: implement initState
    
    fetchCityList();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
     appBar: CustomAppBar(
          lead: Image.asset(
            "assets/icons/ic_event.png",
            color: Colors.white,
          ),
          title: Text(
            "Event",
            style: TextStyle(
              fontSize: 20.0,
              fontFamily: 'Helvetica',
              color: Colors.white
            ),
          )),
          bottomNavigationBar: GestureDetector(
        onTap: () async {
          setState(() {
            print(eventCitytSelected);
            print(eventCityCode);

            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) {
                  return EventVendorListingScr(
                    eventCityCode: eventCityCode,
                    eventCityName: eventCitytSelected,
                  );
                },
              ),
            );
          });
        },
        child: Container(
          alignment: Alignment.center,
          child: Text(
            "SEARCH EVENTS NOW",
            style: TextStyle(
              fontFamily: 'Helvetica',
                color: Color(0xff121A31),
                fontSize: 18.0, letterSpacing: 0.7),
          ),
          height: 57.0,
          color: btngoldcolor,
        ),
      ),
      backgroundColor: bgcolor,
       body: isLoadingcity
          ? CustomLoadingScr()
          : SingleChildScrollView(
              child:  Container(
          padding: EdgeInsets.all(30.0),
        child: Column(
          children: [
             SizedBox(
                      height: 10.0,
                    ),

                    ///////Select City
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Image.asset(
                              "assets/icons/ic_stay.png",
                              height: 20.0,
                              width: 20.0,
                              color: Colors.white,
                            ),
                            SizedBox(
                              width: 15.0,
                            ),
                            Text(
                              "Select City",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontSize: 14.0,
                                letterSpacing: 0.8,
                                color: Colors.white,
                                fontFamily: 'Raleway-Bold',
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Container(
                          decoration: BoxDecoration(
                            color: servicecardfillcolor,
                            border: Border.all(color: txtborderbluecolor),
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                          ),
                          padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                          child: Directionality(
                            textDirection: ui.TextDirection.ltr,
                            child: SimpleAutoCompleteTextField(
                              textSubmitted: (text) {
                                if (cityList.contains(text)) {
                                  // cityController.text = text;
                                  eventCitytSelected = text;
                                  var cityCodeindex =
                                      cityList.indexOf(eventCitytSelected);
                                  eventCityCode = cityCodeList[cityCodeindex];
                                  print(cityCodeindex);
                                  print(eventCityCode);
                                }
                              },
                              textChanged: (text) {
                                // setState(() {
                                //   citytSelected = text;
                                // });
                                // for (var i = 0; i < cityList.length; i++) {
                                if (cityList.contains(eventCitytSelected)) {
                                  // var cityCodeindex =
                                  //     cityCodeList.indexOf(citytSelected);
                                  // print(cityCodeindex);
                                  // print(cityCode);
                                }
                                // }
                              },
                              clearOnSubmit: false,
                              controller: cityController,
                              key: cityKey,
                              suggestions: cityList,
                              style: kTxtStyle.copyWith(fontSize: 16.0),
                              decoration: InputDecoration.collapsed(
                                hintText: "search city",
                                fillColor: servicecardfillcolor,

                                border: InputBorder.none,
                                // focusedBorder:
                                //     InputBorder.none,
                                // enabledBorder:
                                //     InputBorder.none,
                                // errorBorder:
                                //     InputBorder.none,
                                // disabledBorder:
                                //     InputBorder.none,
                                hintStyle: kTxtStyle,
                                //  TextStyle(
                                //     color: Color(
                                //         ColorConst
                                //             .grayColor),
                                //     fontSize: 11.5.sp),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),

                    SizedBox(height: 25.0),

          ],
        ),
        
      ),
    ),
    );
  }

  Future fetchCityList() async {
    setState(() {
      isLoadingcity = true;
    });

    print(homeToken);
    String tkn = homeToken;

    List<Datum>? parsedRes = await GetCityList().getCityList(homeToken);
    if (parsedRes != null) {
      for (var i = 0; i < parsedRes.length; i++) {
        if (parsedRes[i].city != null) {
          cityList.add(parsedRes[i].name! + ' - ' + parsedRes[i].city!.name!);
          cityCodeList.add(parsedRes[i].id.toString());
        }
      }
    }
    print(cityList[3]);
    print(cityCodeList[3]);

    setState(() {
      isLoadingcity = false;
    });
  }
}