// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/EventAPI/event_service_api.dart';
import 'package:lasvegas_gts_app/Networking/EventAPI/event_vendor_model.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'package:lasvegas_gts_app/screens/services/evets/custom_event_vendor_cart.dart';

bool isEventVLoading = false;
List<EventV> EventVendorData = [];

class EventVendorListingScr extends StatefulWidget {
  final String eventCityName;
  final String eventCityCode;
  const EventVendorListingScr(
      {Key? key, required this.eventCityName, required this.eventCityCode})
      : super(key: key);

  @override
  State<EventVendorListingScr> createState() => _EventVendorListingScrState();
}

class _EventVendorListingScrState extends State<EventVendorListingScr> {
  @override
  void initState() {
    // TODO: implement initState
    fetchEventVendorData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
            lead: Image.asset(
              "assets/icons/ic_event.png",
              color: Colors.white,
            ),
            title: Text(
              "Event",
              style: TextStyle(
                  fontSize: 20.0,
                  fontFamily: 'Helvetica',
                  color: Colors.white
              ),
            )),
        backgroundColor: bgcolor,
        body: isEventVLoading
            ? CustomLoadingScr()
            : SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 30.0,
                      ),
                      TextFormField(
                        decoration: kSearchTxtFieldDecoration.copyWith(
                          fillColor: servicecardfillcolor,
                          filled: true,
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      ListView.builder(
                          physics: NeverScrollableScrollPhysics(
                              parent: AlwaysScrollableScrollPhysics()),
                          shrinkWrap: true,
                          itemCount: EventVendorData.length,
                          itemBuilder: (_, index) {
                            return CustomEventVendorCard(
                                vendorID: EventVendorData[index].vendorId!,
                                EventVendorIMG: EventVendorData[index]
                                    .bussinessDetails!
                                    .bussinessLogo!,
                                bname: EventVendorData[index]
                                    .bussinessDetails!
                                    .bussinessName!,
                                price: EventVendorData[index].price![0],
                                priceUnit:
                                    EventVendorData[index].priceUnit![0],
                                    VendorIMGs: EventVendorData[index].bussinessDetails!.imagesk!,
                                     add1: EventVendorData[index].bussinessDetails!.addressLine1!,
                                     add2: EventVendorData[index].bussinessDetails!.addressLine2!,
                                     );
                          }),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Future fetchEventVendorData() async {
    setState(() {
      isEventVLoading = true;
    });

    try {
      /*EventVendorData =
          await EventAPIs().getEventVendorData(widget.eventCityCode);*/
      EventVendorData =
      await EventAPIs().getEventVendorData('783');
    } catch (e) {
      print("Error DATA =====" + e.toString());
    }
    setState(() {
      isEventVLoading = false;
    });
  }
}
