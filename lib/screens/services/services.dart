import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/service_grid.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Services extends StatefulWidget {
  final String? userToken;
  const Services({Key? key, this.userToken}) : super(key: key);

  @override
  _ServicesState createState() => _ServicesState();
}

class _ServicesState extends State<Services> {
  String _userToken = '';
  Future getToken() async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();
    var token = preferences.getString('userToken');

    setState(() {
      _userToken = token!;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(25.0, 30.0, 25.0, 0),
      alignment: Alignment.center,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
            child: Text(
              "All Services",
              style: kHeadText,
            ),
          ),
          SizedBox(
            height: 35.0,
          ),
          Expanded(
            child: SingleChildScrollView(
              child: ServiceGrid(
                userToken: _userToken,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
