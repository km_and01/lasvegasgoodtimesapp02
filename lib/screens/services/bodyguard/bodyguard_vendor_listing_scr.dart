import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/BodyguardAPI/bodyguard_api.dart';
import 'package:lasvegas_gts_app/Networking/BodyguardAPI/bodyguard_vendor_model.dart';
import 'package:lasvegas_gts_app/Networking/WeddingAPI/wedding_api.dart';
import 'package:lasvegas_gts_app/Networking/WeddingAPI/wedding_vendor_model.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/custom_bg_vendor_card.dart';
import 'package:lasvegas_gts_app/customs/custom_fun_vendor_card.dart';
import 'package:lasvegas_gts_app/customs/custom_wedd_vendor_card.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';

bool isBGVLoading = false;
List<BGVendor> BGVendorData = [];

class BGVendorListingScr extends StatefulWidget {
  final String BGCityName, BGCityCode, secType;
  const BGVendorListingScr({
    Key? key,
    required this.BGCityName,
    required this.BGCityCode,
    required this.secType,
  }) : super(key: key);

  @override
  _BGVendorListingScrState createState() => _BGVendorListingScrState();
}

class _BGVendorListingScrState extends State<BGVendorListingScr> {
  @override
  void initState() {
    fetchBGVendorData();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
            lead: Image.asset(
              "assets/icons/ic_bodyguard.png",
              color: Colors.white,
            ),
            title: Text(
              "Bodyguard",
              style: TextStyle(
                  fontSize: 20.0,
                  fontFamily: 'Helvetica',
                  color: Colors.white
              ),
            )),
        backgroundColor: bgcolor,
        body: isBGVLoading
            ? CustomLoadingScr()
            : SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 30.0,
                      ),
                      TextFormField(
                        decoration: kSearchTxtFieldDecoration.copyWith(
                          fillColor: servicecardfillcolor,
                          filled: true,
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      ListView.builder(
                          physics: NeverScrollableScrollPhysics(
                              parent: AlwaysScrollableScrollPhysics()),
                          shrinkWrap: true,
                          itemCount: BGVendorData.length,
                          itemBuilder: (_, index) {
                            return CustomBGVendorCard(
                                short_des: BGVendorData[index]
                                    .bussinessDetails!
                                    .shortDescription!,

                                vendorID: BGVendorData[index].vendorId!,
                                weddVendorIMG: BGVendorData[index]
                                    .bussinessDetails!
                                    .bussinessLogo!,
                                bname: BGVendorData[index]
                                    .bussinessDetails!
                                    .bussinessName!,
                                minPrice: BGVendorData[index].minPrice!,
                                priceUnit: BGVendorData[index].priceUnit!, 
                                secType: BGVendorData[index].securityType!,);
                          }),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Future fetchBGVendorData() async {
    setState(() {
      isBGVLoading = true;
    });

    try {
      BGVendorData = (await BodyguardAPIs()
          .getBGVendorData(widget.BGCityCode, widget.secType))!;
    } catch (e) {
      print("Error DATA =====" + e.toString());
    }

    setState(() {
      isBGVLoading = false;
    });
  }
}
