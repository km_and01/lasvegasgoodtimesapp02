import 'package:autocomplete_textfield_ns/autocomplete_textfield_ns.dart';
import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/StayAPI/stay_search_specil_suits_api.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';
import 'package:lasvegas_gts_app/screens/services/bodyguard/bodyguard_vendor_listing_scr.dart';
import 'dart:ui' as ui;
import 'package:lasvegas_gts_app/screens/services/stay/city_list_model.dart';

String BGCitySelected = '';
bool isLoadingcity = false;

class BodyguardHome extends StatefulWidget {
  const BodyguardHome({Key? key}) : super(key: key);

  @override
  _BodyguardHomeState createState() => _BodyguardHomeState();
}

List<String> securityList = [
  "Executive Security",
  "Personal Security",
];
DropdownMenuItem<String> buildMenuItem(String item) =>
    DropdownMenuItem(value: item, child: Text(item));
String? selectedSecurity = 'Executive Security';

class _BodyguardHomeState extends State<BodyguardHome> {
  List<String> cityList = [];
  List<String> cityCodeList = [];
  String cityCode = '';
  String citytSelected = '';
  TextEditingController cityController = TextEditingController();
  GlobalKey<AutoCompleteTextFieldState<String>> cityKey = GlobalKey();

  @override
  void initState() {
    fetchCityList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
          lead: Image.asset(
            "assets/icons/ic_bodyguard.png",
            color: Colors.white,
          ),
          title: Text(
            "BodyGuard",
            style: TextStyle(
                fontSize: 20.0,
                fontFamily: 'Helvetica',
                color: Colors.white
            ),
          )),
      backgroundColor: bgcolor,
      bottomNavigationBar: GestureDetector(
        onTap: () async {
          setState(() {
            // print(wddingSelectedDate);
            print(cityCode);
            // print(eventServiceSeleted);
            BGCitySelected = citytSelected;
            print('BG CITY SELECTED ========' + BGCitySelected);
            print('BG CITY SELECTED ========' + selectedSecurity!);
            //   print(cityCode + citytSelected);
            //   isCatering ?
            Navigator.push(
              context,
              MaterialPageRoute(builder: (_) {
                return BGVendorListingScr(
                    BGCityName: BGCitySelected,
                    BGCityCode: cityCode,
                    secType: selectedSecurity!);
              }),
            );
            //       :
            //   Navigator.push(
            //       context, MaterialPageRoute(builder: (_) =>
            //       ChefList(
            //         noPerson:nooftableControler.text,
            //         sCity: cityCode,
            //         sDate: selectedDate,
            //         sType: selectedType,
            //         )
            //       ));
          });
        },
        child: Container(
          alignment: Alignment.center,
          child: Text(
            "SEARCH NOW",
            style: TextStyle(
                fontFamily: 'Helvetica',
                color: Color(0xff121A31),
                fontSize: 18.0,
                letterSpacing: 0.7),
          ),
          height: 57.0,
          color: btngoldcolor,
        ),
      ),
      body: isLoadingcity
          ? CustomLoadingScr()
          : SingleChildScrollView(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top:25.0, left: 20.0),
                      child: Text(
                        "Check Availability",
                        style: TextStyle(
                          fontSize: 20.0,
                          fontFamily: 'Raleway-Bold',
                          color: Colors.white,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 15.0,
                    ),

                    ///////Select City
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Image.asset(
                                "assets/icons/ic_stay.png",
                                height: 20.0,
                                width: 20.0,
                                color: Colors.white,
                              ),
                              SizedBox(
                                width: 10.0,
                              ),
                              Text(
                                "Select City",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontSize: 14.0,
                                  letterSpacing: 0.8,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Container(
                            decoration: BoxDecoration(
                              color: servicecardfillcolor,
                              border: Border.all(color: txtborderbluecolor),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                            ),
                            padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                            child: Directionality(
                              textDirection: ui.TextDirection.ltr,
                              child: SimpleAutoCompleteTextField(
                                textSubmitted: (text) {
                                  if (cityList.contains(text)) {
                                    // cityController.text = text;
                                    citytSelected = text;
                                    var cityCodeindex =
                                        cityList.indexOf(citytSelected);
                                    cityCode = cityCodeList[cityCodeindex];
                                    print(cityCodeindex);
                                    print(cityCode);
                                  }
                                },
                                textChanged: (text) {
                                  // setState(() {
                                  //   citytSelected = text;
                                  // });
                                  // for (var i = 0; i < cityList.length; i++) {
                                  if (cityList.contains(citytSelected)) {
                                    // var cityCodeindex =
                                    //     cityCodeList.indexOf(citytSelected);
                                    // print(cityCodeindex);
                                    // print(cityCode);
                                  }
                                  // }
                                },
                                clearOnSubmit: false,
                                controller: cityController,
                                key: cityKey,
                                suggestions: cityList,
                                style: kTxtStyle.copyWith(fontSize: 16.0),
                                decoration: InputDecoration.collapsed(
                                  hintText: "search city",
                                  fillColor: servicecardfillcolor,

                                  border: InputBorder.none,
                                  // focusedBorder:
                                  //     InputBorder.none,
                                  // enabledBorder:
                                  //     InputBorder.none,
                                  // errorBorder:
                                  //     InputBorder.none,
                                  // disabledBorder:
                                  //     InputBorder.none,
                                  hintStyle: kTxtStyle,
                                  //  TextStyle(
                                  //     color: Color(
                                  //         ColorConst
                                  //             .grayColor),
                                  //     fontSize: 11.5.sp),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 10.0),
                        ],
                      ),
                    ),

                    //////Select Security Type
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Image.asset(
                                "assets/icons/ic_bodyguard.png",
                                height: 20.0,
                                width: 20.0,
                                color: Colors.white,
                              ),
                              SizedBox(
                                width: 10.0,
                              ),
                              Text(
                                "Select Security Type",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontSize: 14.0,
                                  letterSpacing: 0.8,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 10.0),
                            height: 46.0,
                            width: MediaQuery.of(context).size.width,
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton<String>(
                                  dropdownColor: servicecardfillcolor,
                                  style: kTxtStyle.copyWith(fontSize: 16.0),
                                  isExpanded: true,
                                  alignment: AlignmentDirectional.center,
                                  icon: Icon(
                                    Icons.arrow_drop_down,
                                    color: Colors.white,
                                  ),
                                  value: selectedSecurity,
                                  items: securityList.map(buildMenuItem).toList(),
                                  onChanged: (value) {
                                    setState(() {
                                      selectedSecurity = value!;
                                    });
                                  }),
                            ),
                            decoration: BoxDecoration(
                              color: servicecardfillcolor,
                              border: Border.all(color: txtborderbluecolor),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
    );
  }

  Future fetchCityList() async {
    setState(() {
      isLoadingcity = true;
    });

    print(homeToken);
    String tkn = homeToken;

    List<Datum>? parsedRes = await GetCityList().getCityList(homeToken);
    if (parsedRes != null) {
      for (var i = 0; i < parsedRes.length; i++) {
        if (parsedRes[i].city != null) {
          cityList.add(parsedRes[i].name! + ' - ' + parsedRes[i].city!.name!);
          cityCodeList.add(parsedRes[i].id.toString());
        }
      }
    }
    print(cityList[3]);
    print(cityCodeList[3]);

    setState(() {
      isLoadingcity = false;
    });
  }
}
