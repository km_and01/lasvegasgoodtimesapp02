import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/BodyguardAPI/bodyguard_api.dart';
import 'package:lasvegas_gts_app/Networking/BodyguardAPI/bodyguard_pkg_model.dart';
import 'package:lasvegas_gts_app/Networking/FunApi/fun_api.dart';
import 'package:lasvegas_gts_app/Networking/FunApi/fun_package_model.dart';
import 'package:lasvegas_gts_app/Networking/WeddingAPI/wedding_api.dart';
import 'package:lasvegas_gts_app/Networking/WeddingAPI/wedding_pkg_model.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/custom_bg_pkg_card.dart';
import 'package:lasvegas_gts_app/customs/custom_fun_pkg_card.dart';
import 'package:lasvegas_gts_app/customs/custom_wedd_pkg_card.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';

bool isBGPkgLoading = false;
List<BGPKG> BGPkgData = [];

class BGPkgListingScr extends StatefulWidget {
  final String bgvendorID, secType, bName;
  const BGPkgListingScr(
      {Key? key,
      required this.bgvendorID,
      required this.secType,
      required this.bName})
      : super(key: key);

  @override
  _BGPkgListingScrState createState() => _BGPkgListingScrState();
}

class _BGPkgListingScrState extends State<BGPkgListingScr> {
  @override
  void initState() {
    fetchFunPkgData();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
            lead: Image.asset(
              "assets/icons/ic_bodyguard.png",
              color: Colors.white,
            ),
            title: Text(
              widget.bName == '' || widget.bName == null ? "Bodyguard" : widget.bName,
              style: TextStyle(
                  fontSize: 20.0,
                  fontFamily: 'Helvetica',
                  color: Colors.white
              ),
            )),
        backgroundColor: bgcolor,
        body: isBGPkgLoading
            ? CustomLoadingScr()
            : SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 20.0,
                      ),
                      Text(
                        widget.bName,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                            fontFamily: 'Raleway-Bold'),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      ListView.builder(
                          physics: NeverScrollableScrollPhysics(
                              parent: AlwaysScrollableScrollPhysics()),
                          shrinkWrap: true,
                          itemCount: BGPkgData.length,
                          itemBuilder: (_, index) {
                            return CustomBGPackageCard(
                              packageIMG: BGPkgData[index].photos![0],
                              packageName: BGPkgData[index].securityType!,
                              priceUnit: BGPkgData[index].priceUnit!,
                              packagePrice: BGPkgData[index].price!,
                              packageDiscPrice: BGPkgData[index].price!,
                              add1: BGPkgData[index]
                                  .bussinessDetails!
                                  .addressLine1!,
                              add2: BGPkgData[index]
                                  .bussinessDetails!
                                  .addressLine2!,
                              fullDetails: BGPkgData[index]
                                  .bussinessDetails!
                                  .shortDescription!,
                              pkgIMGList: BGPkgData[index].photos!,
                              pkgVendorID: BGPkgData[index].vendorId!,
                              // shortDetails: BGPkgData[index].shortDetails!,
                              // pkgInclude: BGPkgData[index].details!,
                            );
                          }),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Future fetchFunPkgData() async {
    setState(() {
      isBGPkgLoading = true;
    });
    try {
      BGPkgData =
          await BodyguardAPIs().getBGPkgData(widget.bgvendorID, widget.secType);
      print("PKG WeddPkgData DATA =====" + BGPkgData[0].securityType!);
      print("PKG WeddPkgData LENGTH =====" + BGPkgData.length.toString());
    } catch (e) {
      print("Error DATA =====" + e.toString());
    }

    setState(() {
      isBGPkgLoading = false;
    });
  }
}
