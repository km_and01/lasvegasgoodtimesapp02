import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/FunApi/fun_api.dart';
import 'package:lasvegas_gts_app/Networking/FunApi/fun_package_model.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/custom_fun_pkg_card.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';

bool isFunPkgLoading = false;
List<FPkg> FunPkgData = [];

class FunPkgListingScr extends StatefulWidget {
  final String funvendorID;
  final String bname;
  const FunPkgListingScr({Key? key,
    required this.funvendorID,
    required this.bname})
      : super(key: key);

  @override
  _FunPkgListingScrState createState() => _FunPkgListingScrState();
}

class _FunPkgListingScrState extends State<FunPkgListingScr> {
  @override
  void initState() {
    fetchFunPkgData();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
            lead: Image.asset(
              "assets/icons/ic_fun.png",
              color: Colors.white,
            ),
            title: Text(
              widget.bname == null || widget.bname == '' ? "Fun" : widget.bname,
              style: TextStyle(
                  fontSize: 20.0,
                  fontFamily: 'Helvetica',
                  color: Colors.white
              ),
            )),
        backgroundColor: bgcolor,
        body: isFunPkgLoading
            ? CustomLoadingScr()
            : SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 20.0,
                      ),
                      Text(
                        "Our Packages",
                        style: TextStyle(
                            fontSize: 20.0,
                            fontFamily: 'Helvetica',
                            color: Colors.white
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      ListView.builder(
                          physics: NeverScrollableScrollPhysics(
                              parent: AlwaysScrollableScrollPhysics()),
                          shrinkWrap: true,
                          itemCount: FunPkgData.length,
                          itemBuilder: (_, index) {
                            return CustomFunPackageCard(
                                packageIMG: FunPkgData[index].bannerImage!,
                                packageName: FunPkgData[index].packageName!,
                                priceUnit: FunPkgData[index].priceUnit!,
                                packagePrice: FunPkgData[index].price!,
                                packageDiscPrice: FunPkgData[index].disPrice!,
                                add1: FunPkgData[index]
                                    .bussinessDetails!
                                    .addressLine1!,
                                add2: FunPkgData[index]
                                    .bussinessDetails!
                                    .addressLine2!,
                                fullDetails: FunPkgData[index].fullDetails!,
                                pkgIMGList: FunPkgData[index].moreImages!,
                                pkgVendorID: FunPkgData[index].vendorId!,
                                shortDetails: FunPkgData[index].shortDetails!);
                          }),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Future fetchFunPkgData() async {
    setState(() {
      isFunPkgLoading = true;
    });
    try {
      FunPkgData = await FunAPIs().getFunPkgData(widget.funvendorID);
      print("PKG Fun DATA =====" + FunPkgData[0].packageName!);
      print("PKG Fun LENGTH =====" + FunPkgData.length.toString());
    } catch (e) {
      print("Error DATA =====" + e.toString());
    }

    setState(() {
      isFunPkgLoading = false;
    });
  }
}
