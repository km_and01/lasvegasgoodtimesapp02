import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/FunApi/fun_api.dart';
import 'package:lasvegas_gts_app/Networking/FunApi/fun_vendor_model.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/custom_fun_vendor_card.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';

bool isFunVLoading = false;
List<FVendor> FunVendorData = [];

class FunVendorListingScr extends StatefulWidget {
  final String funDate, funCityName, funActi, funCityCode;
  const FunVendorListingScr(
      {Key? key,
      required this.funDate,
      required this.funCityName,
      required this.funActi,
      required this.funCityCode})
      : super(key: key);

  @override
  _FunVendorListingScrState createState() => _FunVendorListingScrState();
}

class _FunVendorListingScrState extends State<FunVendorListingScr> {
  @override
  void initState() {
    fetchFunVendorData();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
          lead: Image.asset(
            "assets/icons/ic_fun.png",
            color: Colors.white,
          ),
          title: Text(
            "Fun",
            style: TextStyle(
                fontSize: 20.0,
                fontFamily: 'Helvetica',
                color: Colors.white
            ),
          )),
        backgroundColor: bgcolor,
        body: isFunVLoading
            ? CustomLoadingScr()
            : SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 30.0,
                      ),
                      TextFormField(
                        decoration: kSearchTxtFieldDecoration.copyWith(
                          fillColor: servicecardfillcolor,
                          filled: true,
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      ListView.builder(
                          physics: NeverScrollableScrollPhysics(
                              parent: AlwaysScrollableScrollPhysics()),
                          shrinkWrap: true,
                          itemCount: FunVendorData.length,
                          itemBuilder: (_, index) {
                            return CustomFunVendorCard(
                              shortDisc: FunVendorData[index].bussinessDetails!.shortDescription!, 
                              vendorID: FunVendorData[index].vendorId!, 
                              FunVendorIMG: FunVendorData[index].bussinessDetails!.bussinessLogo!, 
                              bname: FunVendorData[index].bussinessDetails!.bussinessName!, 
                              price: FunVendorData[index].disPrice!, 
                              priceUnit: FunVendorData[index].priceUnit!); 
                          }),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Future fetchFunVendorData() async {
    setState(() {
      isFunVLoading = true;
    });

    try {
      FunVendorData = await FunAPIs()
          .getFunVendorData(widget.funCityCode, widget.funDate, widget.funActi);
    } catch (e) {
      print("Error DATA =====" + e.toString());
    }

    setState(() {
      isFunVLoading = false;
    });
  }
}
