import 'package:autocomplete_textfield_ns/autocomplete_textfield_ns.dart';
import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/FunApi/fun_activity_list_model.dart';
import 'package:lasvegas_gts_app/Networking/FunApi/fun_api.dart';
import 'package:lasvegas_gts_app/Networking/StayAPI/stay_search_specil_suits_api.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';
import 'package:lasvegas_gts_app/screens/services/fun/fun_vendor_listing_scr.dart';
import 'package:lasvegas_gts_app/screens/services/stay/city_list_model.dart';
import 'dart:ui' as ui;

import 'package:lasvegas_gts_app/utils/alert_utils.dart';

String funselectedDate = '';
bool isLoadingfuncity = false;
String FunCityCode = '';
String FunCitytSelected = '';
String funServiceSeleted = 'Fun&Family';
List<String> funActivityList = [];
DropdownMenuItem<String> buildMenuItem(String item) =>
    DropdownMenuItem(value: item, child: Text(item));

class FunHome extends StatefulWidget {
  const FunHome({Key? key}) : super(key: key);

  @override
  _FunHomeState createState() => _FunHomeState();
}

class _FunHomeState extends State<FunHome> {
  List<String> cityList = [];
  List<String> cityCodeList = [];

  TextEditingController cityController = TextEditingController();
  GlobalKey<AutoCompleteTextFieldState<String>> cityKey = GlobalKey();

  DateTime dateTime = DateTime.now();

  @override
  void initState() {
    fetchCityList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
          lead: Image.asset(
            "assets/icons/ic_fun.png",
            color: Colors.white,
          ),
          title: Text(
            "Fun",
            style: TextStyle(
                fontSize: 20.0,
                fontFamily: 'Helvetica',
                color: Colors.white
            ),
          )),
      backgroundColor: bgcolor,
      bottomNavigationBar: GestureDetector(
        onTap: () async {
          setState(() {
            print(FunCitytSelected);
            print(FunCityCode);
            print(funServiceSeleted);
            print(funselectedDate);

            if(FunCityCode.isEmpty){
              AlertUtils.showAutoCloseDialogue(context, "Please select city", 2, 'Required field');
            }else  {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) {
                    return FunVendorListingScr(
                      funActi: funServiceSeleted,
                      funDate: funselectedDate,
                      funCityCode: FunCityCode,
                      funCityName: FunCitytSelected,
                    );
                  },
                ),
              );
            }


          });
        },
        child: Container(
          alignment: Alignment.center,
          child: Text(
            "SEARCH NOW",
            style: TextStyle(
                fontFamily: 'Helvetica',
                color: Color(0xff121A31),
                fontSize: 18.0,
                letterSpacing: 0.7),
          ),
          height: 57.0,
          color: btngoldcolor,
        ),
      ),
      body: isLoadingfuncity
          ? CustomLoadingScr()
          : SingleChildScrollView(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [

                    Padding(
                      padding: const EdgeInsets.only(top: 20.0, left: 20.0),
                      child: Text(
                        "Search For Fun",
                        style: TextStyle(
                          fontSize: 20.0,
                          fontFamily: 'Raleway-Bold',
                          color: Colors.white,
                        ),
                      ),
                    ),



                    ///////Select City
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Image.asset(
                                "assets/icons/ic_stay.png",
                                height: 20.0,
                                width: 20.0,
                                color: Colors.white,
                              ),
                              SizedBox(
                                width: 10.0,
                              ),
                              Text(
                                "Select City",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    fontSize: 14.0,
                                    letterSpacing: 0.8,
                                    color: Colors.white,
                                    fontFamily: 'Raleway-Bold'
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Container(
                            decoration: BoxDecoration(
                              color: servicecardfillcolor,
                              border: Border.all(color: txtborderbluecolor),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                            ),
                            padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                            child: Directionality(
                              textDirection: ui.TextDirection.ltr,
                              child: SimpleAutoCompleteTextField(
                                textSubmitted: (text) {
                                  if (cityList.contains(text)) {
                                    // cityController.text = text;
                                    FunCitytSelected = text;
                                    var cityCodeindex =
                                        cityList.indexOf(FunCitytSelected);
                                    FunCityCode = cityCodeList[cityCodeindex];
                                    print(cityCodeindex);
                                    print(FunCityCode);
                                  }
                                },
                                textChanged: (text) {
                                  // setState(() {
                                  //   citytSelected = text;
                                  // });
                                  // for (var i = 0; i < cityList.length; i++) {
                                  if (cityList.contains(FunCitytSelected)) {
                                    // var cityCodeindex =
                                    //     cityCodeList.indexOf(citytSelected);
                                    // print(cityCodeindex);
                                    // print(cityCode);
                                  }
                                  // }
                                },
                                clearOnSubmit: false,
                                controller: cityController,
                                key: cityKey,
                                suggestions: cityList,
                                style: kTxtStyle.copyWith(fontSize: 16.0),
                                decoration: InputDecoration.collapsed(
                                  hintText: "search city",
                                  fillColor: servicecardfillcolor,

                                  border: InputBorder.none,
                                  // focusedBorder:
                                  //     InputBorder.none,
                                  // enabledBorder:
                                  //     InputBorder.none,
                                  // errorBorder:
                                  //     InputBorder.none,
                                  // disabledBorder:
                                  //     InputBorder.none,
                                  hintStyle: kTxtStyle,
                                  //  TextStyle(
                                  //     color: Color(
                                  //         ColorConst
                                  //             .grayColor),
                                  //     fontSize: 11.5.sp),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    ///////Select Date
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Image.asset(
                                "assets/icons/ic_calendar.png",
                                height: 20.0,
                                width: 20.0,
                                color: Colors.white,
                              ),
                              SizedBox(
                                width: 10.0,
                              ),
                              Text(
                                'Select Event Date',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    fontSize: 14.0,
                                    letterSpacing: 0.8,
                                    color: Colors.white,
                                    fontFamily: 'Raleway-Bold'
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          InkWell(
                            onTap: () async {
                              DateTime? newDate = await showDatePicker(
                                  context: context,
                                  initialDate: dateTime,
                                  firstDate: DateTime(2021),
                                  lastDate: DateTime(2200));
                              if (newDate != null) {
                                setState(() {
                                  dateTime = newDate;
                                  funselectedDate =
                                      '${dateTime.day.toString().padLeft(2, '0')}-${dateTime.month.toString().padLeft(2, '0')}-${dateTime.year}';
                                });
                              }
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 10.0),
                              height: 50.0,
                              width: MediaQuery.of(context).size.width,
                              child: Text(
                                funselectedDate,
                                textAlign: TextAlign.left,
                                style: kTxtStyle.copyWith(
                                  fontSize: 16.0,
                                  fontFamily: 'Regular',
                                ),
                              ),
                              decoration: BoxDecoration(
                                color: servicecardfillcolor,
                                border: Border.all(color: txtborderbluecolor),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                              ),
                            ),
                          ),
                          SizedBox(height: 25.0),
                        ],
                      ),
                    ),

//////Fun Service
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: Row(
                        children: const [
                          Icon(
                            Icons.fastfood_outlined,
                            color: Colors.white,
                          ),
                          SizedBox(
                            width: 10.0,
                          ),
                          Text(
                            "Select Fun Activity",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                fontSize: 14.0,
                                letterSpacing: 0.8,
                                color: Colors.white,
                                fontFamily: 'Raleway-Bold'
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        height: 46.0,
                        width: MediaQuery.of(context).size.width,
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                              dropdownColor: servicecardfillcolor,
                              style: kTxtStyle.copyWith(fontSize: 16.0),
                              isExpanded: true,
                              alignment: AlignmentDirectional.center,
                              icon: Icon(
                                Icons.arrow_drop_down,
                                color: Colors.white,
                              ),
                              value: funServiceSeleted,
                              items: funActivityList.map(buildMenuItem).toList(),
                              onChanged: (value) {
                                setState(() {
                                  funServiceSeleted = value!;
                                });
                              }),
                        ),
                        decoration: BoxDecoration(
                          color: servicecardfillcolor,
                          border: Border.all(color: txtborderbluecolor),
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
    );
  }

  Future fetchCityList() async {
    setState(() {
      isLoadingfuncity = true;
    });

    print(homeToken);
    String tkn = homeToken;

    List<Datum>? parsedRes = await GetCityList().getCityList(homeToken);
    if (parsedRes != null) {
      for (var i = 0; i < parsedRes.length; i++) {
        if (parsedRes[i].city != null) {
          cityList.add(parsedRes[i].name! + ' - ' + parsedRes[i].city!.name!);
          cityCodeList.add(parsedRes[i].id.toString());
        }
      }
    }
    fetchFunActivityList();
    print(cityList[3]);
    print(cityCodeList[3]);

    setState(() {
      isLoadingfuncity = false;
    });
  }

  Future fetchFunActivityList() async {
    List<FunAct> activityList = await FunAPIs().getFunActivities();
    if (activityList != null) {
      funActivityList.clear();
      for (var i = 0; i < activityList.length; i++) {
        if (activityList[i].serviceName != null) {
          funActivityList.add(activityList[i].serviceName!);
          // cityCodeList.add(parsedRes[i].id.toString());
        }
      }
    }
    print(
        "funActivityList LENGTH ======= " + funActivityList.length.toString());
  }
}
