import 'package:carousel_images/carousel_images.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'package:lasvegas_gts_app/screens/services/fun/fun_booking_scr.dart';

bool isPkgDisLoading = false;
String pkgIMGListURL = 'http://lasvegas.koolmindapps.com/images/fun/';
List<String> listImagesPkg = [];

class PackageDetailsFun extends StatefulWidget {
  final String pkgName,
      pkgVendorID,
      pkgIMG,
      priceUnit,
      discPrice,
      pkgDetails,
      address1,
      address2,
      pkgShortDetails;
  final List<String> pkgIMGList;
  const PackageDetailsFun(
      {Key? key,
      required this.pkgName,
      required this.priceUnit,
      required this.discPrice,
      required this.pkgDetails,
      required this.address1,
      required this.address2,
      required this.pkgIMGList,
      required this.pkgShortDetails,
      required this.pkgVendorID,
      required this.pkgIMG})
      : super(key: key);

  @override
  _PackageDetailsFunState createState() => _PackageDetailsFunState();
}

class _PackageDetailsFunState extends State<PackageDetailsFun> {
  @override
  void initState() {
    print("URL ===" + pkgIMGListURL);
    int length = widget.pkgIMGList.length;
    listImagesPkg.clear();
    for (var i = 0; i < widget.pkgIMGList.length; i++) {
      listImagesPkg.add(pkgIMGListURL + widget.pkgIMGList[i]);
    }
    print(listImagesPkg.toString()); // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgcolor,
      /*appBar: AppBar(
        elevation: 0.0,
        leading: BackButton(),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              widget.pkgName,
              style: kHeadText.copyWith(fontSize: 16),
            ),
            RatingBar.builder(
              glow: false,
              itemSize: 20.0,
              initialRating: 3,
              minRating: 1,
              direction: Axis.horizontal,
              allowHalfRating: true,
              itemCount: 5,
              itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
              itemBuilder: (context, _) => Icon(
                Icons.star,
                color: Colors.amber,
                //   Image.asset(
                // "assets/icons/rating_star.png",
                // color: Colors.amber,
              ),
              // ),
              unratedColor: Colors.grey,
              onRatingUpdate: (rating) {
                print(rating);
              },
            ),
          ],
        ),
        bottom: PreferredSize(
          child: Column(
            children: const [
              Divider(
                thickness: 2,
                height: 2,
                color: Colors.amber,
              ),
              SizedBox(
                height: 3.0,
              ),
              DottedLine(
                lineThickness: 4.5,
                dashRadius: 7,
                dashLength: 4.0,
                // lineLength:  MediaQuery.of(context).size.width,
                dashColor: Colors.amber,
              ),
            ],
          ),
          preferredSize: Size.fromHeight(20),
        ),
      ),*/
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(20.0),
        child: ElevatedButton(
          onPressed: () async {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (_) => FunBookings(
                      vendorID: widget.pkgVendorID,
                      amount: widget.discPrice,
                      pkgIMG: widget.pkgIMG,
                      pkgName: widget.pkgName,
                    )));
          },
          child: Text(
            'Book Now',
            style: TextStyle(
                fontFamily: 'Helvetica',
                color: Color(0xff121A31),
                fontSize: 16.0,
                letterSpacing: 0.7),
          ),
          style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(13.0),
            ),
            fixedSize: Size(389.0, 57.0),
            primary: Color(0xFFFFC71E),
          ),
        ),
      ),
      body: isPkgDisLoading
          ? CustomLoadingScr()
          : SingleChildScrollView(
              child: Container(
                /*margin: EdgeInsets.all(10),
                padding: EdgeInsets.all(0.01),*/
                child: Column(
                  children: [

                    CarouselSlider(
                      items: listImagesPkg
                          .map(
                            (x) => Container(
                          width: double.infinity,
                          decoration: BoxDecoration(
                            //  color: Colors.yellow,
                            borderRadius: BorderRadius.circular(8.0),
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: NetworkImage(x, scale: 1),
                            ),
                          ),
                        ),
                      )
                          .toList(),
                      // autoPlay: true,
                      // height: 200.0,
                      options: CarouselOptions(
                        height: 200,
                        aspectRatio: 0,
                        viewportFraction: 1.0,
                        initialPage: 0,
                        enableInfiniteScroll: true,
                        reverse: false,
                        autoPlay: true,
                        autoPlayInterval: Duration(seconds: 3),
                        autoPlayAnimationDuration:
                        Duration(milliseconds: 800),
                        autoPlayCurve: Curves.fastOutSlowIn,
                        enlargeCenterPage: true,
                        // onPageChanged: callbackFunction,
                        scrollDirection: Axis.horizontal,
                      ),
                    ),

                    Container(
                      margin: EdgeInsets.all(15.0),
                      padding: EdgeInsets.all(3.0),
                      alignment: Alignment.topLeft,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            widget.pkgName,
                            style: TextStyle(
                                fontFamily: 'Raleway-Medium',
                                fontSize: 18.0,
                                color: Colors.white
                            ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          RatingBar.builder(
                            glow: false,
                            itemSize: 15.0,
                            initialRating: 3,
                            minRating: 1,
                            direction: Axis.horizontal,
                            allowHalfRating: true,
                            itemCount: 5,
                            //itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                            itemBuilder: (context, _) => Icon(
                              Icons.star,
                              color: Colors.amber,
                              //   Image.asset(
                              // "assets/icons/rating_star.png",
                              // color: Colors.amber,
                            ),
                            // ),
                            unratedColor: Colors.grey,
                            onRatingUpdate: (rating) {
                              print(rating);
                            },
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            widget.address1 + ', ' + widget.address2,
                            style: TextStyle(
                                fontFamily: 'Manrope',
                                color: Colors.white
                            ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),

                          Text(
                            widget.priceUnit +
                                " " +
                                widget.discPrice,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16.0,
                              fontFamily: 'Manrope-SemiBold',
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      height: 1,
                      color: greytxtcolor,
                      thickness: 1.0,
                    ),

                    Padding(
                      padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          //////Service Name
                          Text(
                            widget.pkgShortDetails,
                            style: TextStyle(
                                fontFamily: 'Raleway',
                                color: greytxtcolor,
                                fontSize: 14.0
                            ),
                          ),

                          SizedBox(
                            height: 30.0,
                          ),
                          Text(
                            "Package Description",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18.0,
                                fontFamily: 'Raleway-Bold'),
                          ),
                          SizedBox(
                            height: 15.0,
                          ),
                          Text(
                            widget.pkgDetails,
                            // """The Hotel Pennsylvania is conveniently located within one block of the Empire State Building,Madison Square Garden, Penn Station and Macy's flagship store. Located within walking distance are Times Square, the Broadway Theatre District, New York Public Library, Jacob K. Javits Convention Center, and two of New York's most iconic…""",
                            style: TextStyle(
                                fontFamily: 'Raleway',
                                color: greytxtcolor,
                                fontSize: 14.0
                            ),
                          ),
                          SizedBox(
                            height: 30.0,
                          ),

                          // /////Possible Events
                          // Text(
                          //   "Possible Events :",
                          //   style: kTxtStyle.copyWith(
                          //     color: greytxtcolor,
                          //   ),
                          // ),
                          // SizedBox(
                          //   height: 5.0,
                          // ),
                          // Text(
                          //   widget.possibleEvents,
                          //   style: kTxtStyle.copyWith(fontSize: 18),
                          // ),
                          // SizedBox(
                          //   height: 30.0,
                          // ),

                          // /////Capacity
                          // Text(
                          //   "No. of People Allowed :",
                          //   style: kTxtStyle.copyWith(color: greytxtcolor),
                          // ),
                          // SizedBox(
                          //   height: 5.0,
                          // ),
                          // Text(
                          //   widget.pkgCapacity,
                          //   style: kTxtStyle.copyWith(fontSize: 18),
                          // ),
                          // SizedBox(
                          //   height: 30.0,
                          // ),

                          Text(
                            "Address",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18.0,
                                fontFamily: 'Raleway-Bold'),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            widget.address1 + ' , ' + widget.address2 + ' . ',
                            // """T3131 Las Vegas Boulevard South, 89109, Las Vegas""",
                            style: TextStyle(
                                fontFamily: 'Raleway',
                                color: greytxtcolor,
                                fontSize: 14.0
                            ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          //       Container(
                          //         child:
                          //             Image.asset("assets/images/banner_img_2.png"),
                          //         height: 215.0,
                          //         width: 380.0,
                          //         color: bgcolor,
                          //       ),
                          //       SizedBox(
                          //         height: 40.0,
                          //       ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}
