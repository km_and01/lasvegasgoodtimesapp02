import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/FunApi/fun_api.dart';
import 'package:lasvegas_gts_app/Networking/FunApi/fun_package_model.dart';
import 'package:lasvegas_gts_app/Networking/RentalsAPI/car_pkg_model.dart';
import 'package:lasvegas_gts_app/Networking/RentalsAPI/rentals_api.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/custom_fun_pkg_card.dart';
import 'package:lasvegas_gts_app/customs/custom_rentals_pkg_card.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';

bool isRentalsPkgLoading = false;
List<RentalPKG> RentalsPkgData = [];

class RentalPkgListingScr extends StatefulWidget {
  final String rentalsvendorID;
  final String bname;
  const RentalPkgListingScr({Key? key,
    required this.rentalsvendorID,
    required this.bname})
      : super(key: key);

  @override
  _RentalPkgListingScrState createState() => _RentalPkgListingScrState();
}

class _RentalPkgListingScrState extends State<RentalPkgListingScr> {
  @override
  void initState() {
    fetchRentalsPkgData();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
            lead: Image.asset(
              "assets/icons/ic_rental.png",
              color: Colors.white,
            ),
            title: Text(
              /*"Rental Service",*/
                widget.bname == '' || widget.bname == null ? "Rental Service" : widget.bname,
              style: TextStyle(
                  fontSize: 20.0,
                  fontFamily: 'Helvetica',
                  color: Colors.white
              ),
            )),
        backgroundColor: bgcolor,
        body: isRentalsPkgLoading
            ? CustomLoadingScr()
            : SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 20.0,
                      ),
                      Text(
                        "Our Packages",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                            fontFamily: 'Raleway-Bold'),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      ListView.builder(
                          physics: NeverScrollableScrollPhysics(
                              parent: AlwaysScrollableScrollPhysics()),
                          shrinkWrap: true,
                          itemCount: RentalsPkgData.length,
                          itemBuilder: (_, index) {
                            return CustomRentalsPKGCard(
                                packageIMG: RentalsPkgData[index].bannerImage!,
                                packageName: RentalsPkgData[index].packageName!,
                                priceUnit: RentalsPkgData[index].priceUnit!,
                                packagePrice: RentalsPkgData[index].price!,
                                packageDiscPrice: RentalsPkgData[index].disPrice!,
                                add1: RentalsPkgData[index]
                                    .bussinessDetails!
                                    .addressLine1!,
                                add2: RentalsPkgData[index]
                                    .bussinessDetails!
                                    .addressLine2!,
                                fullDetails: RentalsPkgData[index].fullDetails!,
                                pkgIMGList: RentalsPkgData[index].moreImages!,
                                pkgVendorID: RentalsPkgData[index].vendorId!,
                                shortDetails: RentalsPkgData[index].shortDetails!,
                                 bannerIMG: RentalsPkgData[index].bannerImage!,);
                          }),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Future fetchRentalsPkgData() async {
    setState(() {
      isRentalsPkgLoading = true;
    });
    try {
      RentalsPkgData = await RentalsAPIs().getRentalPkgData(widget.rentalsvendorID);
      print("PKG Fun DATA =====" + RentalsPkgData[0].packageName!);
      print("PKG Fun LENGTH =====" + RentalsPkgData.length.toString());
    } catch (e) {
      print("Error DATA =====" + e.toString());
    }

    setState(() {
      isRentalsPkgLoading = false;
    });
  }
}
