import 'package:carousel_images/carousel_images.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/constants/image_pager.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'package:lasvegas_gts_app/screens/services/fun/fun_booking_scr.dart';
import 'package:lasvegas_gts_app/screens/services/rental/rental_booking_scr.dart';

bool isPkgDisLoading = false;
String pkgIMGListURL = 'http://lasvegas.koolmindapps.com/images/rental/';
List<String> listImagesPkg = [];

class PackageDetailsRentals extends StatefulWidget {
  final String pkgName,
      pkgVendorID,
      pkgIMG,
      priceUnit,
      discPrice,
      pkgDetails,
      address1,
      address2,
      pkgShortDetails;
  final List<String> pkgIMGList;
  const PackageDetailsRentals(
      {Key? key,
      required this.pkgName,
      required this.priceUnit,
      required this.discPrice,
      required this.pkgDetails,
      required this.address1,
      required this.address2,
      required this.pkgIMGList,
      required this.pkgShortDetails,
      required this.pkgVendorID,
      required this.pkgIMG})
      : super(key: key);

  @override
  _PackageDetailsRentalsState createState() => _PackageDetailsRentalsState();
}

class _PackageDetailsRentalsState extends State<PackageDetailsRentals> {
  @override
  void initState() {
    print("URL ===" + pkgIMGListURL);
    int length = widget.pkgIMGList.length;
    listImagesPkg.clear();
    for (var i = 0; i < widget.pkgIMGList.length; i++) {
      listImagesPkg.add(pkgIMGListURL + widget.pkgIMGList[i]);
    }
    print(listImagesPkg.toString()); // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgcolor,
      /*appBar: AppBar(
        elevation: 0.0,
        leading: BackButton(),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              widget.pkgName,
              style: kHeadText.copyWith(fontSize: 16),
            ),
            RatingBar.builder(
              glow: false,
              itemSize: 20.0,
              initialRating: 3,
              minRating: 1,
              direction: Axis.horizontal,
              allowHalfRating: true,
              itemCount: 5,
              itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
              itemBuilder: (context, _) => Icon(
                Icons.star,
                color: Colors.amber,
                //   Image.asset(
                // "assets/icons/rating_star.png",
                // color: Colors.amber,
              ),
              // ),
              unratedColor: Colors.grey,
              onRatingUpdate: (rating) {
                print(rating);
              },
            ),
          ],
        ),
        bottom: PreferredSize(
          child: Column(
            children: const [
              Divider(
                thickness: 2,
                height: 2,
                color: Colors.amber,
              ),
              SizedBox(
                height: 3.0,
              ),
              DottedLine(
                lineThickness: 4.5,
                dashRadius: 7,
                dashLength: 4.0,
                // lineLength:  MediaQuery.of(context).size.width,
                dashColor: Colors.amber,
              ),
            ],
          ),
          preferredSize: Size.fromHeight(20),
        ),
      ),*/
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(20.0),
        child: ElevatedButton(
          onPressed: () async {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (_) => RentalsBookings(
                      vendorID: widget.pkgVendorID,
                      amount: widget.discPrice,
                      pkgIMG: widget.pkgIMG,
                      pkgName: widget.pkgName,
                    )));
          },
          child: Text(
            'Book Now',
            style: TextStyle(
                fontFamily: 'Helvetica',
                color: Color(0xff121A31),
                fontSize: 16.0,
                letterSpacing: 0.7),
          ),
          style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(13.0),
            ),
            fixedSize: Size(389.0, 57.0),
            primary: Color(0xFFFFC71E),
          ),
        ),
      ),
      body: isPkgDisLoading
          ? CustomLoadingScr()
          : SingleChildScrollView(
              child: Container(
                /*margin: EdgeInsets.all(10),
                padding: EdgeInsets.all(0.01),*/
                child: Column(
                  children: [

                    Column(
                      children: [
                        // Image.network(
                        //  imageURL + widget.images,
                        // ),
                        ImagePager(
                          viewportFraction: 1.0,
                          scaleFactor: 1.0,
                          listImages: listImagesPkg,
                          height: 200.0,
                          /*borderRadius: 8.0,*/
                          cachedNetworkImage: true,
                          //verticalAlignment: Alignment.center,
                          onTap: (index) {
                            print('Tapped on page $index');
                          },
                        ),

                      ],
                    ),

                    Container(
                      margin: EdgeInsets.all(15.0),
                      padding: EdgeInsets.all(3.0),
                      alignment: Alignment.topLeft,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            widget.pkgName,
                            style: TextStyle(
                                fontFamily: 'Raleway-Medium',
                                fontSize: 18.0,
                                color: Colors.white
                            ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          RatingBar.builder(
                            glow: false,
                            itemSize: 15.0,
                            initialRating: 3,
                            minRating: 1,
                            direction: Axis.horizontal,
                            allowHalfRating: true,
                            itemCount: 5,
                            //itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                            itemBuilder: (context, _) => Icon(
                              Icons.star,
                              color: Colors.amber,
                              //   Image.asset(
                              // "assets/icons/rating_star.png",
                              // color: Colors.amber,
                            ),
                            // ),
                            unratedColor: Colors.grey,
                            onRatingUpdate: (rating) {
                              print(rating);
                            },
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            widget.address1 + ', ' + widget.address2,
                            style: TextStyle(
                                fontFamily: 'Manrope',
                                color: Colors.white
                            ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),

                          Text(
                            widget.priceUnit +
                                " " +
                                widget.discPrice +
                                "/Day ",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16.0,
                              fontFamily: 'Manrope-SemiBold',
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      height: 1,
                      color: greytxtcolor,
                      thickness: 1.0,
                    ),

                    Container(
                      margin: EdgeInsets.all(15.0),
                      padding: EdgeInsets.all(3.0),
                      alignment: Alignment.topLeft,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [

                          Text(
                            "Package Description",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18.0,
                                fontFamily: 'Raleway-Bold'),
                          ),
                          SizedBox(
                            height: 15.0,
                          ),
                          Text(
                            widget.pkgDetails,
                            // """The Hotel Pennsylvania is conveniently located within one block of the Empire State Building,Madison Square Garden, Penn Station and Macy's flagship store. Located within walking distance are Times Square, the Broadway Theatre District, New York Public Library, Jacob K. Javits Convention Center, and two of New York's most iconic…""",
                            style: TextStyle(
                                fontFamily: 'Raleway',
                                color: greytxtcolor,
                                fontSize: 14.0
                            ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),

                          Text(
                            "Address",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18.0,
                                fontFamily: 'Raleway-Bold'),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            widget.address1 + ' , ' + widget.address2 + ' . ',
                            // """T3131 Las Vegas Boulevard South, 89109, Las Vegas""",
                            style: TextStyle(
                                fontFamily: 'Raleway',
                                color: greytxtcolor,
                                fontSize: 14.0
                            ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                        ],
                      ),
                    )

                  ],
                ),
              ),
            ),
    );
  }
}
