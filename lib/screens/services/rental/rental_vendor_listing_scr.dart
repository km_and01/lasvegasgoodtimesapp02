import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/FunApi/fun_api.dart';
import 'package:lasvegas_gts_app/Networking/FunApi/fun_vendor_model.dart';
import 'package:lasvegas_gts_app/Networking/RentalsAPI/car_vendor_model.dart';
import 'package:lasvegas_gts_app/Networking/RentalsAPI/rentals_api.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/custom_fun_vendor_card.dart';
import 'package:lasvegas_gts_app/customs/custom_rental_vendor_card.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';

bool isRentalVLoading = false;
List<RentalV> RentalVendorData = [];

class RentalVendorListingScr extends StatefulWidget {
  final String rentalDate, rentalCityName, rentalCityCode,rentalCarName;
  const RentalVendorListingScr(
      {Key? key, required this.rentalDate, 
      required this.rentalCityName, 
      required this.rentalCityCode, 
      required this.rentalCarName,})
      : super(key: key);

  @override
  _RentalVendorListingScrState createState() => _RentalVendorListingScrState();
}

class _RentalVendorListingScrState extends State<RentalVendorListingScr> {
  @override
  void initState() {
    fetchRentalsVendorData();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
          lead: Image.asset(
            "assets/icons/ic_rental.png",
            color: Colors.white,
          ),
          title: Text(
            "Rental Service",
            style: TextStyle(
                fontSize: 20.0,
                fontFamily: 'Helvetica',
                color: Colors.white
            ),
          )),
        backgroundColor: bgcolor,
        body: isRentalVLoading
            ? CustomLoadingScr()
            : SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 30.0,
                      ),
                      TextFormField(
                        decoration: kSearchTxtFieldDecoration.copyWith(
                          fillColor: servicecardfillcolor,
                          filled: true,
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      ListView.builder(
                          physics: NeverScrollableScrollPhysics(
                              parent: AlwaysScrollableScrollPhysics()),
                          shrinkWrap: true,
                          itemCount: RentalVendorData.length,
                          itemBuilder: (_, index) {
                            return CustomRentalsVendorCard(
                              shortDisc: RentalVendorData[index].bussinessDetails!.shortDescription!, 
                              vendorID: RentalVendorData[index].vendorId!, 
                              RentalsVendorIMG: RentalVendorData[index].bussinessDetails!.bussinessLogo!, 
                              bname: RentalVendorData[index].bussinessDetails!.bussinessName!, 
                              price: RentalVendorData[index].disPrice!, 
                              priceUnit: RentalVendorData[index].priceUnit!); 
                          }),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Future fetchRentalsVendorData() async {
    setState(() {
      isRentalVLoading = true;
    });

    try {
      RentalVendorData = await RentalsAPIs()
      .getRentalVendorData(widget.rentalCityCode, widget.rentalDate, 
          widget.rentalCarName);
    } catch (e) {
      print("Error DATA =====" + e.toString());
    }

    setState(() {
      isRentalVLoading = false;
    });
  }


}
