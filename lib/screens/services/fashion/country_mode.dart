// To parse this JSON data, do
//
//     final countryModel = countryModelFromJson(jsonString);

import 'dart:convert';

CountryModel countryModelFromJson(String str) => CountryModel.fromJson(json.decode(str));

String countryModelToJson(CountryModel data) => json.encode(data.toJson());

class CountryModel {
    CountryModel({
        this.message,
        this.statusCode,
        this.data,
    });

    String? message;
    int? statusCode;
    List<CountryData>? data;

    factory CountryModel.fromJson(Map<String, dynamic> json) => CountryModel(
        message: json["message"],
        statusCode: json["status_code"],
        data: List<CountryData>.from(json["data"].map((x) => CountryData.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "status_code": statusCode,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class CountryData {
    CountryData({
        this.id,
        this.sortname,
        this.name,
        this.phonecode,
    });

    int? id;
    String? sortname;
    String? name;
    int? phonecode;

    factory CountryData.fromJson(Map<String, dynamic> json) => CountryData(
        id: json["id"],
        sortname: json["sortname"],
        name: json["name"],
        phonecode: json["phonecode"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "sortname": sortname,
        "name": name,
        "phonecode": phonecode,
    };
}
