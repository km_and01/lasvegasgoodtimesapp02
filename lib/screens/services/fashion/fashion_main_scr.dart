import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/FashionAPI/fashion_api.dart';
import 'package:lasvegas_gts_app/Networking/FashionAPI/fashon_vendor_model.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar_with_cart.dart';
import 'package:lasvegas_gts_app/customs/custom_fashion_vendor_card.dart';
import 'package:lasvegas_gts_app/customs/custom_rental_vendor_card.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';

bool isFashionVLoading = false;
List<FashonV> FashionVendorData = [];

class FashionHome extends StatefulWidget {
  const FashionHome({Key? key}) : super(key: key);

  @override
  _FashionHomeState createState() => _FashionHomeState();
}

class _FashionHomeState extends State<FashionHome> {
  @override
  void initState() {
    fetchFashionVendorData();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBarCart(
            lead: Image.asset(
              "assets/icons/ic_fasion_retail.png",
              color: Colors.white,
            ),
            title: Text(
              "Fashion/Retail",
              style: TextStyle(
                  fontSize: 20.0,
                  fontFamily: 'Helvetica',
                  color: Colors.white
              ),
            ),),
        backgroundColor: bgcolor,
        body: isFashionVLoading
            ? CustomLoadingScr()
            : SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 30.0,
                      ),
                      TextFormField(
                        decoration: kSearchTxtFieldDecoration.copyWith(
                          fillColor: servicecardfillcolor,
                          filled: true,
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      ListView.builder(
                          physics: NeverScrollableScrollPhysics(
                              parent: AlwaysScrollableScrollPhysics()),
                          shrinkWrap: true,
                          itemCount: FashionVendorData.length,
                          itemBuilder: (_, index) {
                            return CustomFashionVendorCard(
                                shortDisc: FashionVendorData[index]
                                    .bussinessDetails!
                                    .shortDescription!,
                                vendorID: FashionVendorData[index].vendorId!,
                                fashionVendorLogo: FashionVendorData[index]
                                    .bussinessDetails!
                                    .bussinessLogo!,
                                bname: FashionVendorData[index]
                                    .bussinessDetails!
                                    .bussinessName!,
                                price: FashionVendorData[index].productPrice!,
                                priceUnit: FashionVendorData[index].priceUnit!, 
                                FashionVendorIMG: FashionVendorData[index].bussinessDetails!.imagesk!, 
                                add1: FashionVendorData[index].bussinessDetails!.addressLine1!, 
                                add2: FashionVendorData[index].bussinessDetails!.addressLine2!, 
                                vendrDetail: FashionVendorData[index].bussinessDetails!.shortDescription!,);
                          }),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Future fetchFashionVendorData() async {
    setState(() {
      isFashionVLoading = true;
    });

    try {
      FashionVendorData = await FashionAPIs().getFashionVendorData();
      // RentalsAPIs().getRentalVendorData(widget.rentalCityCode, widget.rentalDate, widget.rentalCarName);
    } catch (e) {
      print("Error DATA =====" + e.toString());
    }

    setState(() {
      isFashionVLoading = false;
    });
  }
}
