// To parse this JSON data, do
//
//     final cityModel = cityModelFromJson(jsonString);

import 'dart:convert';

CityModel cityModelFromJson(String str) => CityModel.fromJson(json.decode(str));

String cityModelToJson(CityModel data) => json.encode(data.toJson());

class CityModel {
    CityModel({
        this.message,
        this.statusCode,
        this.data,
    });

    String? message;
    int? statusCode;
    List<CityData>? data;

    factory CityModel.fromJson(Map<String, dynamic> json) => CityModel(
        message: json["message"],
        statusCode: json["status_code"],
        data: List<CityData>.from(json["data"].map((x) => CityData.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "status_code": statusCode,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class CityData {
    CityData({
        this.id,
        this.name,
        this.stateId,
    });

    int? id;
    String? name;
    int? stateId;

    factory CityData.fromJson(Map<String, dynamic> json) => CityData(
        id: json["id"],
        name: json["name"],
        stateId: json["state_id"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "state_id": stateId,
    };
}
