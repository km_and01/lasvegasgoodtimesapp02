import 'package:autocomplete_textfield_ns/autocomplete_textfield_ns.dart';
import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/FashionAPI/fashion_api.dart';
import 'package:lasvegas_gts_app/Networking/FashionAPI/fashion_booking_model.dart';
import 'package:lasvegas_gts_app/Networking/StayAPI/stay_search_specil_suits_api.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';
import 'package:lasvegas_gts_app/screens/services/eventSpace/event_vendor_listing_scr.dart';
import 'package:lasvegas_gts_app/screens/services/fashion/city_model.dart';
import 'package:lasvegas_gts_app/screens/services/fashion/country_mode.dart';
import 'package:lasvegas_gts_app/screens/services/fashion/state_model.dart';
import 'package:lasvegas_gts_app/screens/services/stay/city_list_model.dart';
import 'dart:ui' as ui;

import 'package:lasvegas_gts_app/utils/alert_utils.dart';

FashionBookingModel fashionBookingModel = FashionBookingModel();
String? specialReq;
bool isLoadingcity = false;
List<String> cityList = [];
List<String> cityCodeList = [];
String cityCode = '';
String citytSelected = '';
TextEditingController cityController = TextEditingController();
GlobalKey<AutoCompleteTextFieldState<String>> cityKey = GlobalKey();

List<String> countryList = [];
List<String> countryCodeList = [];
String countryCode = '';
String countrySelected = '';
TextEditingController countryController = TextEditingController();
GlobalKey<AutoCompleteTextFieldState<String>> countryKey = GlobalKey();

List<String> stateList = [];
List<String> stateCodeList = [];
String stateCode = '';
String stateSelected = '';
TextEditingController stateController = TextEditingController();
GlobalKey<AutoCompleteTextFieldState<String>> stateKey = GlobalKey();

TextEditingController guestController = TextEditingController();
TextEditingController hrsController = TextEditingController();
TextEditingController uNameController = TextEditingController();
TextEditingController shippingAddController = TextEditingController();
TextEditingController uEmailController = TextEditingController();
TextEditingController uMobController = TextEditingController();
bool isCountrySel = false;
bool isStateSel = false;

class FashionBookings extends StatefulWidget {
  final String vendorID, pkgIMG, productPrice, productType;
  // amount, no_of_people_allowed, , pkgService;

  const FashionBookings({
    Key? key,
    required this.vendorID,
    // required this.amount,
    // required this.no_of_people_allowed,
    required this.pkgIMG,
    required this.productPrice,
    required this.productType,
    // required this.pkgService
  }) : super(key: key);

  @override
  _FashionBookingsState createState() => _FashionBookingsState();
}

class _FashionBookingsState extends State<FashionBookings> {
  DateTime dateTime = DateTime.now();
  TimeOfDay time = TimeOfDay.now();
  String selectedDate = '';
  String selectedTime = " ";

  bool isLoading = false;
  @override
  void initState() {
    selectedDate = selectedDateES;
    uMobController.text = homeUserPhone;
    uEmailController.text = homeUserEmail;
    uNameController.text = homeUserName;

    fetchCountryList();
    // selectedTime = selectedTimeES;
    // TODO: implement initState
    super.initState();
  }

  Future validationCheck() async{

    Map<String, dynamic>? map;

    if(uEmailController.text.isEmpty){
      AlertUtils.showAutoCloseDialogue(context, "Please enter valid email", 2, 'Required field');
    }else if(uNameController.text.isEmpty){
      AlertUtils.showAutoCloseDialogue(context, "Please enter name", 2, 'Required field');
    }else if(uMobController.text.isEmpty || uMobController.text.length < 12){
      AlertUtils.showAutoCloseDialogue(context, "Please enter valid mobile number", 2, 'Required field');
    }else if(shippingAddController.text.isEmpty){
      AlertUtils.showAutoCloseDialogue(context, "Please enter shipping address", 2, 'Required field');
    }else if(countryCode.isEmpty){
      AlertUtils.showAutoCloseDialogue(context, "Please select country", 2, 'Required field');
    }else if(stateCode.isEmpty){
      AlertUtils.showAutoCloseDialogue(context, "Please select state", 2, 'Required field');
    }else if(cityCode.isEmpty){
      AlertUtils.showAutoCloseDialogue(context, "Please select city", 2, 'Required field');
    }else {

      await bookFashion();
      //ScaffoldMessenger.of(context).showSnackBar(

      if(fashionBookingModel != null){
        if(fashionBookingModel.statusCode == 200){

          int count = 0;
          Navigator.popUntil(context, (route) {
            return count++ == 3;
          });

          AlertUtils.showAutoCloseDialogue(context, fashionBookingModel.message!, 3, 'Successful');

        }else {
          AlertUtils.showAutoCloseDialogue(context, fashionBookingModel.message!, 3, 'Oops!');
        }
      }else {
        AlertUtils.showAutoCloseDialogue(context, "Try Again", 3, 'Oops!');
      }

      setState(() {
    isLoadingcity = false;
      });

    }

  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar: ElevatedButton(
          onPressed: () async {


            validationCheck();

          },
          child: Text(
            'CONTINUE',
            style: TextStyle(
                fontFamily: 'Helvetica',
                color: Color(0xff121A31),
                fontSize: 16.0,
                letterSpacing: 0.7),
          ),
          style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(13.0),
            ),
            fixedSize: Size(389.0, 57.0),
            primary: Color(0xFFFFC71E),
          ),
        ),
        backgroundColor: bgcolor,
        appBar: CustomAppBar(
          lead: BackButton(),
          title: Text(
            "Booking Details",
            style: TextStyle(
                fontSize: 20.0,
                fontFamily: 'Helvetica',
                color: Colors.white
            ),
          ),
        ),
        body: isLoadingcity
            ? CustomLoadingScr()
            : SingleChildScrollView(
                reverse: true,
                child: Container(
                  margin: EdgeInsets.all(20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // SizedBox(height: 25.0),

                      // //////City
                      // Padding(
                      //   padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                      //   child: Text(
                      //     'City',
                      //     textAlign: TextAlign.left,
                      //     style: TextStyle(
                      //       fontSize: 14.0,
                      //       letterSpacing: 0.8,
                      //       color: Colors.white,
                      //       fontWeight: FontWeight.bold,
                      //     ),
                      //   ),
                      // ),
                      // Container(
                      //   padding:
                      //       EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                      //   height: 46.0,
                      //   // width: 166.0,
                      //   child: Text(
                      //     selectedCityES,
                      //     textAlign: TextAlign.center,
                      //     style: kTxtStyle.copyWith(
                      //       fontSize: 16.0,
                      //       fontFamily: 'Regular',
                      //     ),
                      //   ),
                      //   decoration: BoxDecoration(
                      //     // color: servicecardfillcolor,
                      //     border: Border.all(color: txtborderbluecolor),
                      //     borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      //   ),
                      // ),
                      // SizedBox(height: 25.0),

                      //////Name user
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                        child: Text(
                          'Name',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontSize: 14.0,
                            letterSpacing: 0.8,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      TextFormField(
                        controller: uNameController,
                        style: kTxtStyle,
                        keyboardType: TextInputType.text,
                        // controller: vendorPhoneNoController,
                        decoration: kTxtFieldDecoration,
                      ),
                      SizedBox(height: 25.0),

                      //////Email
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                        child: Text(
                          'Email-ID',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontSize: 14.0,
                            letterSpacing: 0.8,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      TextFormField(
                        controller: uEmailController,
                        style: kTxtStyle,
                        keyboardType: TextInputType.emailAddress,
                        // controller: vendorPhoneNoController,
                        decoration: kTxtFieldDecoration,
                      ),
                      SizedBox(height: 25.0),

                      //////Phone
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                        child: Text(
                          'Mobile Number',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontSize: 14.0,
                            letterSpacing: 0.8,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      TextFormField(
                        controller: uMobController,
                        style: kTxtStyle,
                        keyboardType: TextInputType.number,
                        // controller: vendorPhoneNoController,
                        decoration: kTxtFieldDecoration,
                      ),
                      SizedBox(height: 25.0),

                      //////Address
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                        child: Text(
                          'Shipping Address',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontSize: 14.0,
                            letterSpacing: 0.8,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      TextFormField(
                        controller: shippingAddController,
                        style: kTxtStyle,
                        // keyboardType: TextInputType.number,
                        // controller: vendorPhoneNoController,
                        decoration: kTxtFieldDecoration,
                      ),
                      SizedBox(height: 25.0),

                      ///////Select Country
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              // Image.asset(
                              //   "assets/icons/ic_stay.png",
                              //   height: 20.0,
                              //   width: 20.0,
                              //   color: Colors.white,
                              // ),
                              // SizedBox(
                              //   width: 15.0,
                              // ),
                              Text(
                                "Select Country",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontSize: 14.0,
                                  letterSpacing: 0.8,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Container(
                            decoration: BoxDecoration(
                              color: servicecardfillcolor,
                              border: Border.all(color: txtborderbluecolor),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                            ),
                            padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                            child: Directionality(
                              textDirection: ui.TextDirection.ltr,
                              child: SimpleAutoCompleteTextField(
                                textSubmitted: (text) {
                                  if (countryList.contains(text)) {
                                    // cityController.text = text;
                                    countrySelected = text;
                                    var countryCodeindex =
                                        countryList.indexOf(countrySelected);
                                    countryCode =
                                        countryCodeList[countryCodeindex];
                                    print(countryCodeindex);
                                    print(countryCode);
                                    fetchStateList();
                                    setState(() {
                                      isCountrySel = true;
                                    });
                                  }
                                },
                                textChanged: (text) {
                                  // setState(() {
                                  //   citytSelected = text;
                                  // });
                                  // for (var i = 0; i < cityList.length; i++) {
                                  if (countryList.contains(countrySelected)) {
                                    // var cityCodeindex =
                                    //     cityCodeList.indexOf(citytSelected);
                                    // print(cityCodeindex);
                                    // print(cityCode);
                                  }
                                  // }
                                },
                                clearOnSubmit: false,
                                controller: countryController,
                                key: countryKey,
                                suggestions: countryList,
                                style: kTxtStyle.copyWith(fontSize: 16.0),
                                decoration: InputDecoration.collapsed(
                                  hintText: "search country",
                                  fillColor: servicecardfillcolor,

                                  border: InputBorder.none,
                                  // focusedBorder:
                                  //     InputBorder.none,
                                  // enabledBorder:
                                  //     InputBorder.none,
                                  // errorBorder:
                                  //     InputBorder.none,
                                  // disabledBorder:
                                  //     InputBorder.none,
                                  hintStyle: kTxtStyle,
                                  //  TextStyle(
                                  //     color: Color(
                                  //         ColorConst
                                  //             .grayColor),
                                  //     fontSize: 11.5.sp),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 25.0),
                        ],
                      ),

///////Select State
                      Visibility(
                        visible: isCountrySel,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: const [
                                Text(
                                  "Select State",
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    letterSpacing: 0.8,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Container(
                              decoration: BoxDecoration(
                                color: servicecardfillcolor,
                                border: Border.all(color: txtborderbluecolor),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                              ),
                              padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                              child: Directionality(
                                textDirection: ui.TextDirection.ltr,
                                child: SimpleAutoCompleteTextField(
                                  textSubmitted: (text) {
                                    if (stateList.contains(text)) {
                                      // cityController.text = text;
                                      stateSelected = text;
                                      var stateCodeindex =
                                          stateList.indexOf(stateSelected);
                                      stateCode = stateCodeList[stateCodeindex];
                                      print(stateCodeindex);
                                      print(stateCode);
                                      fetchCityList();
                                      setState(() {
                                        isStateSel = true;
                                      });
                                    }
                                  },
                                  textChanged: (text) {
                                    // setState(() {
                                    //   citytSelected = text;
                                    // });
                                    // for (var i = 0; i < cityList.length; i++) {
                                    if (stateList.contains(stateSelected)) {
                                      // var cityCodeindex =
                                      //     cityCodeList.indexOf(citytSelected);
                                      // print(cityCodeindex);
                                      // print(cityCode);
                                    }
                                    // }
                                  },
                                  clearOnSubmit: false,
                                  controller: stateController,
                                  key: stateKey,
                                  suggestions: stateList,
                                  style: kTxtStyle.copyWith(fontSize: 16.0),
                                  decoration: InputDecoration.collapsed(
                                    hintText: "search state",
                                    fillColor: servicecardfillcolor,

                                    border: InputBorder.none,
                                    // focusedBorder:
                                    //     InputBorder.none,
                                    // enabledBorder:
                                    //     InputBorder.none,
                                    // errorBorder:
                                    //     InputBorder.none,
                                    // disabledBorder:
                                    //     InputBorder.none,
                                    hintStyle: kTxtStyle,
                                    //  TextStyle(
                                    //     color: Color(
                                    //         ColorConst
                                    //             .grayColor),
                                    //     fontSize: 11.5.sp),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: 25.0),
                          ],
                        ),
                      ),

                      ////////Select City
                      ///
                      Visibility(
                        visible: isStateSel,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                // Image.asset(
                                //   "assets/icons/ic_stay.png",
                                //   height: 20.0,
                                //   width: 20.0,
                                //   color: Colors.white,
                                // ),
                                // SizedBox(
                                //   width: 15.0,
                                // ),
                                Text(
                                  "Select City",
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    letterSpacing: 0.8,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Container(
                              decoration: BoxDecoration(
                                color: servicecardfillcolor,
                                border: Border.all(color: txtborderbluecolor),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                              ),
                              padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                              child: Directionality(
                                textDirection: ui.TextDirection.ltr,
                                child: SimpleAutoCompleteTextField(
                                  textSubmitted: (text) {
                                    if (cityList.contains(text)) {
                                      // cityController.text = text;
                                      citytSelected = text;
                                      var cityCodeindex =
                                          cityList.indexOf(citytSelected);
                                      cityCode = cityCodeList[cityCodeindex];
                                      print(cityCodeindex);
                                      print(cityCode);
                                    }
                                  },
                                  textChanged: (text) {
                                    // setState(() {
                                    //   citytSelected = text;
                                    // });
                                    // for (var i = 0; i < cityList.length; i++) {
                                    if (cityList.contains(citytSelected)) {
                                      // var cityCodeindex =
                                      //     cityCodeList.indexOf(citytSelected);
                                      // print(cityCodeindex);
                                      // print(cityCode);
                                    }
                                    // }
                                  },
                                  clearOnSubmit: false,
                                  controller: cityController,
                                  key: cityKey,
                                  suggestions: cityList,
                                  style: kTxtStyle.copyWith(fontSize: 16.0),
                                  decoration: InputDecoration.collapsed(
                                    hintText: "search country",
                                    fillColor: servicecardfillcolor,

                                    border: InputBorder.none,
                                    // focusedBorder:
                                    //     InputBorder.none,
                                    // enabledBorder:
                                    //     InputBorder.none,
                                    // errorBorder:
                                    //     InputBorder.none,
                                    // disabledBorder:
                                    //     InputBorder.none,
                                    hintStyle: kTxtStyle,
                                    //  TextStyle(
                                    //     color: Color(
                                    //         ColorConst
                                    //             .grayColor),
                                    //     fontSize: 11.5.sp),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: 25.0),
                          ],
                        ),
                      ),

/////Amount & date
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ///// Date
                            // Column(
                            //   crossAxisAlignment: CrossAxisAlignment.start,
                            //   children: [
                            //     Text(
                            //       'Select Date',
                            //       textAlign: TextAlign.left,
                            //       style: TextStyle(
                            //         fontSize: 14.0,
                            //         letterSpacing: 0.8,
                            //         color: Colors.white,
                            //         fontWeight: FontWeight.bold,
                            //       ),
                            //     ),
                            //     SizedBox(
                            //       height: 10.0,
                            //     ),
                            //     InkWell(
                            //       onTap: () async {
                            //         DateTime? newDate = await showDatePicker(
                            //             context: context,
                            //             initialDate: dateTime,
                            //             firstDate: DateTime(2021),
                            //             lastDate: DateTime(2200));
                            //         if (newDate != null) {
                            //           setState(() {
                            //             dateTime = newDate;
                            //             selectedDate =
                            //                 '${dateTime.day.toString().padLeft(2, '0')}  -  ${dateTime.month.toString().padLeft(2, '0')}  -  ${dateTime.year}';
                            //           });
                            //         }
                            //       },
                            //       child: Container(
                            //         padding: EdgeInsets.symmetric(
                            //             vertical: 10.0, horizontal: 10.0),
                            //         height: 46.0,
                            //         width: 166.0,
                            //         child: Text(
                            //           selectedDate,
                            //           textAlign: TextAlign.center,
                            //           style: kTxtStyle.copyWith(
                            //             fontSize: 16.0,
                            //             fontFamily: 'Regular',
                            //           ),
                            //         ),
                            //         decoration: BoxDecoration(
                            //           // color: servicecardfillcolor,
                            //           border: Border.all(color: txtborderbluecolor),
                            //           borderRadius:
                            //               BorderRadius.all(Radius.circular(10.0)),
                            //         ),
                            //       ),
                            //     ),
                            //   ],
                            // ),

                            //////Amount
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Amount",
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    letterSpacing: 0.8,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                SizedBox(
                                  height: 10.0,
                                ),
                                Container(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  height: 46.0,
                                  width: 166.0,
                                  child: Text(
                                    widget.productPrice,
                                    // widget.amount,
                                    textAlign: TextAlign.center,
                                    style: kTxtStyle.copyWith(
                                      fontSize: 16.0,
                                      fontFamily: 'Regular',
                                    ),
                                  ),
                                  decoration: BoxDecoration(
                                    // color: servicecardfillcolor,
                                    border:
                                        Border.all(color: txtborderbluecolor),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10.0)),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                      SizedBox(height: 25.0),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Future fetchCityList() async {
    setState(() {
      isLoadingcity = true;
    });
    print("State Selected ======" + stateSelected);

    List<CityData>? parsedRes =
        await FashionAPIs().getCityList(homeToken, stateCode);
    if (parsedRes != null) {
      for (var i = 0; i < parsedRes.length; i++) {
        if (parsedRes[i] != null) {
          cityList.add(parsedRes[i].name!);
          cityCodeList.add(parsedRes[i].id.toString());
        }
      }
    }
    print(cityList[3]);
    print(cityCodeList[3]);

    setState(() {
      isLoadingcity = false;
    });
  }

  Future fetchStateList() async {
    setState(() {
      isLoadingcity = true;
    });

    List<StateData>? stateRes =
        await FashionAPIs().getStateList(homeToken, countryCode);
    if (stateRes != null) {
      for (var i = 0; i < stateRes.length; i++) {
        if (stateRes[i] != null) {
          stateList.add(stateRes[i].name!);
          stateCodeList.add(stateRes[i].id.toString());
        }
      }
    }
    print(stateList[3]);
    print(stateCodeList[3]);

    setState(() {
      isLoadingcity = false;
    });
  }

  Future fetchCountryList() async {
    setState(() {
      isLoadingcity = true;
    });

    List<CountryData>? countryRes =
        await FashionAPIs().getCountryList(homeToken);
    if (countryRes != null) {
      for (var i = 0; i < countryRes.length; i++) {
        if (countryRes[i] != null) {
          countryList.add(countryRes[i].name!);
          countryCodeList.add(countryRes[i].id.toString());
        }
      }
    }
    print(countryList[3]);
    print(countryCodeList[3]);

    setState(() {
      isLoadingcity = false;
    });
  }

  Future bookFashion() async {

    setState(() {
      isLoadingcity = true;
    });

     fashionBookingModel =
        await FashionAPIs().bookFashionProduct(
      widget.vendorID,
      widget.productType,
      widget.productPrice,
      shippingAddController.text,
      shippingAddController.text,
      countryCode,
      stateCode,
      cityCode,
    );

    // bookingMsg = fashionBookingModel.message!;

    setState(() {
      isLoadingcity = false;
    });
  }
}
