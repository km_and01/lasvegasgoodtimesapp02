import 'dart:async';

import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:flutter_credit_card/credit_card_brand.dart';
import 'package:lasvegas_gts_app/Networking/StayAPI/stay_search_specil_suits_api.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:flutter_credit_card/flutter_credit_card.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'package:lasvegas_gts_app/utils/alert_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'sta_search_main.dart';

final GlobalKey<FormState> formKey = GlobalKey<FormState>();

class StayPayment extends StatefulWidget {

  final String discPrise;
  final String roomName;
  final String roomImg;
  final String totalRooms;
  final String totalGuest;
  final String checkInDate;
  final String checkOutDate,vendorID;

  const StayPayment(
      {Key? key,
        required this.discPrise,
        required this.roomName,
        required this.totalRooms,
        required this.totalGuest,
        required this.checkInDate,
        required this.checkOutDate, required this.roomImg, required this.vendorID})
      : super(key: key);

  @override
  _StayPaymentState createState() => _StayPaymentState();
}

class _StayPaymentState extends State<StayPayment> {
  String cardNumber = '';
  String expiryDate = '';
  String cardHolderName = '';
  String cvvCode = '';
  bool isCvvFocused = false;
  bool useGlassMorphism = false;
  bool useBackgroundImage = false;
  OutlineInputBorder? border;


  Future AddStayBookingAPI() async{

    Map<String, dynamic>? map;
    final SharedPreferences preferences = await SharedPreferences.getInstance();
    String homeToken = preferences.getString('userToken')!;

    String sub_type = '';



    if(isSpecialSelected){
      sub_type = 'Special Suits';
    }else if(isPentHouseSelected){
      sub_type = 'Pent Houses';
    }else if(isBnBSelecte){
      sub_type = 'Private BNB';
    }else {
      sub_type = '';
    }

    setState(() {
      isLoading = true;
    });

    map = (await GetStayVendorData().AddstaycategoryBooking(
      "4",
        widget.vendorID,
        widget.totalRooms,
        sub_type,
        widget.totalGuest,
        widget.checkInDate,
        widget.checkOutDate,
        widget.roomImg,
        widget.discPrise,
        homeToken
    ));

    if(map != null){
      if(map['status_code'] == 200){


        int count = 0;
        Navigator.popUntil(context, (route) {
          return count++ == 6;
        });

        AlertUtils.showAutoCloseDialogue(context, "Stay Booking successfully", 3, 'Successful');

        /*Timer(Duration(seconds: 3), () {
          int count = 0;
          Navigator.popUntil(context, (route) {
            return count++ == 7;
          });
          setState(() {
            isLoading = false;
          });
        });*/

      }else {
        AlertUtils.showAutoCloseDialogue(context, map['message'], 3, 'Oops!');
      }
    }else {
      AlertUtils.showAutoCloseDialogue(context, "Try Again", 3, 'Oops!');
    }

    setState(() {
      isLoading = false;
    });

  }

  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: bgcolor,
        appBar: CustomAppBar(lead: BackButton(), title: Text("Payment",style: kHeadText,),),
        bottomNavigationBar: GestureDetector(
          onTap: () {
            if (formKey.currentState!.validate()) {
              AddStayBookingAPI();
            } else {
              print('invalid!');
            }
          },
          child: Container(
            alignment: Alignment.center,
            padding: EdgeInsets.all(18.0),
            height: 57.0,
            color: btngoldcolor,
            child: Text(
              "CONFIRM",
              style: TextStyle(
                  fontSize: 16.0,
                  fontFamily: 'Raleway-Regular',

                  color: Color(0xff121A31), letterSpacing: 1.0),
            ),
          ),
        ),
        body: isLoading ?
        CustomLoadingScr()
            : SingleChildScrollView(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 8.0, bottom: 8.0, left: 15.0, right: 10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    // crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      CircleAvatar(
                        backgroundColor: btngoldcolor,
                        child: Text(
                          "1",
                          style: kTxtStyle,
                        ),
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Text(
                        "Book & Review",
                        style: kTxtStyle.copyWith(color: btngoldcolor),
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Expanded(
                        child: Divider(
                          thickness: 1.0,
                          height: 1.0,
                          color: btngoldcolor,
                        ),
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      CircleAvatar(
                        backgroundColor: btngoldcolor,
                        child: Text(
                          "2",
                          style: kTxtStyle.copyWith(color: favcolor),
                        ),
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Text(
                        "Checkout",
                        style: kTxtStyle.copyWith(color: btngoldcolor),
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Expanded(
                        child: Divider(
                          thickness: 1.0,
                          height: 1.0,
                          color: btngoldcolor,
                        ),
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      CircleAvatar(
                        backgroundColor: greytxtcolor,
                        child: Text(
                          "3",
                          style: kTxtStyle.copyWith(color: favcolor),
                        ),
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Text(
                        "Payment",
                        style: kTxtStyle.copyWith(color: greytxtcolor),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 40.0,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 15.0),
                  child: Text(
                    "Add Card",
                    style: TextStyle(
                        fontSize: 18.0,
                        fontFamily: 'Raleway-SemiBold',
                        color: Colors.white),
                  ),
                ),
                SizedBox(
                  height: 25.0,
                ),
                CreditCardWidget(
                  glassmorphismConfig:
                      useGlassMorphism ? Glassmorphism.defaultConfig() : null,
                  cardNumber: cardNumber,
                  expiryDate: expiryDate,
                  cardHolderName: cardHolderName,
                  cvvCode: cvvCode,
                  showBackView: isCvvFocused,
                  obscureCardNumber: true,
                  obscureCardCvv: true,
                  isHolderNameVisible: true,
                  cardBgColor: Colors.red,
                  backgroundImage:
                      useBackgroundImage ? 'assets/card_bg.png' : null,
                  isSwipeGestureEnabled: true,
                  onCreditCardWidgetChange:
                      (CreditCardBrand creditCardBrand) {},
                  customCardTypeIcons: <CustomCardTypeIcon>[
                    CustomCardTypeIcon(
                      cardType: CardType.mastercard,
                      cardImage: Image.asset(
                        'assets/mastercard.png',
                        height: 48,
                        width: 48,
                      ),
                    ),
                  ],
                ),
                CreditCardForm(
                  formKey: formKey,
                  obscureCvv: true,
                  obscureNumber: true,
                  cardNumber: cardNumber,
                  cvvCode: cvvCode,
                  isHolderNameVisible: true,
                  isCardNumberVisible: true,
                  isExpiryDateVisible: true,
                  cardHolderName: cardHolderName,
                  expiryDate: expiryDate,
                  themeColor: Colors.blue,
                  textColor: Colors.white,
                  cardNumberDecoration: InputDecoration(
                    labelText: 'Number',
                    hintText: 'XXXX XXXX XXXX XXXX',
                    hintStyle: const TextStyle(color: Colors.white),
                    labelStyle: const TextStyle(color: Colors.white),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xFF5C75B3)),
                      borderRadius: BorderRadius.all(
                        Radius.circular(10.0),
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xFF5C75B3)),
                      borderRadius: BorderRadius.all(
                        Radius.circular(10.0),
                      ),
                    ),
                  ),
                  expiryDateDecoration: InputDecoration(
                    hintStyle: const TextStyle(color: Colors.white),
                    labelStyle: const TextStyle(color: Colors.white),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xFF5C75B3)),
                      borderRadius: BorderRadius.all(
                        Radius.circular(10.0),
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xFF5C75B3)),
                      borderRadius: BorderRadius.all(
                        Radius.circular(10.0),
                      ),
                    ),
                    labelText: 'Expired Date',
                    hintText: 'XX/XX',
                  ),
                  cvvCodeDecoration: InputDecoration(
                    hintStyle: const TextStyle(color: Colors.white),
                    labelStyle: const TextStyle(color: Colors.white),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xFF5C75B3)),
                      borderRadius: BorderRadius.all(
                        Radius.circular(10.0),
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xFF5C75B3)),
                      borderRadius: BorderRadius.all(
                        Radius.circular(10.0),
                      ),
                    ),
                    labelText: 'CVV',
                    hintText: 'XXX',
                  ),
                  cardHolderDecoration: InputDecoration(
                    hintStyle: const TextStyle(color: Colors.white),
                    labelStyle: const TextStyle(color: Colors.white),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xFF5C75B3)),
                      borderRadius: BorderRadius.all(
                        Radius.circular(10.0),
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xFF5C75B3)),
                      borderRadius: BorderRadius.all(
                        Radius.circular(10.0),
                      ),
                    ),
                    labelText: 'Card Holder',
                  ),
                  onCreditCardModelChange: onCreditCardModelChange,
                ),
                const SizedBox(
                  height: 20,
                ),

                /*ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0),
                            ),
                            primary: const Color(0xff1b447b),
                          ),
                          child: Container(
                            margin: const EdgeInsets.all(12),
                            child: const Text(
                              'Validate',
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'halter',
                                fontSize: 14,
                                package: 'flutter_credit_card',
                              ),
                            ),
                          ),
                          onPressed: () {
                            if (formKey.currentState!.validate()) {
                              print('valid!');
                            } else {
                              print('invalid!');
                            }
                          },
                        ),*/
              ],
            ),
          ),
        ),
      ),
    );
  }

  void onCreditCardModelChange(CreditCardModel? creditCardModel) {
    setState(() {
      cardNumber = creditCardModel!.cardNumber;
      expiryDate = creditCardModel.expiryDate;
      cardHolderName = creditCardModel.cardHolderName;
      cvvCode = creditCardModel.cvvCode;
      isCvvFocused = creditCardModel.isCvvFocused;
    });
  }
}


class PayNowBtn extends StatelessWidget {

  final String discPrise;
  final String roomName;
  final String roomImg;
  final String totalRooms;
  final String totalGuest;
  final String checkInDate;
  final String checkOutDate,vendorID;

  const PayNowBtn(
      {Key? key,
        required this.discPrise,
        required this.roomName,
        required this.totalRooms,
        required this.totalGuest,
        required this.checkInDate,
        required this.checkOutDate, required this.roomImg, required this.vendorID})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (formKey.currentState!.validate()) {
          print('valid!');
        } else {
          print('invalid!');
        }
      },
      child: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(18.0),
        height: 57.0,
        color: btngoldcolor,
        child: Text(
          "CONFIRM",
          style: TextStyle(
              fontSize: 16.0,
              fontFamily: 'Raleway-Regular',

              color: Color(0xff121A31), letterSpacing: 1.0),
        ),
      ),
    );
  }
}
