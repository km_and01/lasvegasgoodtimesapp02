import 'package:badges/badges.dart';
// import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:lasvegas_gts_app/Networking/StayAPI/special_suit_detail_model.dart';
import 'package:lasvegas_gts_app/Networking/StayAPI/stay_search_pent_house_api.dart';
import 'package:lasvegas_gts_app/Networking/StayAPI/stay_search_pvt_bnb_api.dart';
import 'package:lasvegas_gts_app/Networking/StayAPI/stay_search_specil_suits_api.dart';
import 'package:lasvegas_gts_app/Networking/StayAPI/stay_vendol_model.dart'
    as v;
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/custom_hotel_display_card.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'package:lasvegas_gts_app/screens/services/stay/stay_select_room.dart';

// late String hotelname ;
//  late  String roomddDiscription;
//  late  String discountPrice;
// late   String basePrice;
bool isLoading = false;
List<v.Service> servicesData = [];
// String catType = '';

class StaySearchMain extends StatefulWidget {
  final String check_in;
  final String check_out;
  final String no_of_person;
  final String no_of_rooms;
  final String uToken;
  final String city;

  const StaySearchMain({
    Key? key,
    required this.check_in,
    required this.check_out,
    required this.no_of_person,
    required this.no_of_rooms,
    required this.uToken,
    required this.city,
  }) : super(key: key);

  @override
  _StaySearchMainState createState() => _StaySearchMainState();
}

bool defaultView = true;
bool isSpecialSelected = false;
bool isPentHouseSelected = false;
bool isBnBSelecte = false;
bool isFav = false;

// class GetDataSpecialSuits {

//   static String hotelName = GetDataSpecialSuits.hotelName;
//   static String roomddDiscription = GetDataSpecialSuits.roomddDiscription;
//   static String discountPrice = GetDataSpecialSuits.discountPrice;
//   static String basePrice = GetDataSpecialSuits.basePrice;
//   static String vendorID = GetDataSpecialSuits.vendorID;
//   static String userToken = GetDataSpecialSuits.userToken;
//   static String suCategory = GetDataSpecialSuits.suCategory;
//   static int length = GetDataSpecialSuits.length;
//   static String roomName = GetDataSpecialSuits.roomName;
//   static String addLine2 = GetDataSpecialSuits.addLine1;
//   static String addLine1 = GetDataSpecialSuits.addLine2;
//   static List<String> images = GetDataSpecialSuits.images;
//   static String logo = GetDataSpecialSuits.logo;

//   //  String hotelname = GetData().hotelName;
//   //  String roomddDiscription;
//   //  String discountPrice;
//   //  String basePrice;

// }

// class GetDataPvtBnb {
//   static String hotelName = GetDataPvtBnb.hotelName;
//   static String roomddDiscription = GetDataPvtBnb.roomddDiscription;
//   static String discountPrice = GetDataPvtBnb.discountPrice;
//   static String basePrice = GetDataPvtBnb.basePrice;
//   static String vendorID = GetDataPvtBnb.vendorID;
//   static String userToken = GetDataPvtBnb.userToken;
//   static String suCategory = GetDataPvtBnb.suCategory;
//   static String roomName = GetDataPvtBnb.roomName;
//   static int length = GetDataPvtBnb.length;
//   static String logo = GetDataPvtBnb.logo;

//   static String addLine2 = GetDataPvtBnb.addLine1;
//   static String addLine1 = GetDataPvtBnb.addLine2;
//   static List<String> images = GetDataPvtBnb.images;

//   //  String hotelname = GetData().hotelName;
//   //  String roomddDiscription;
//   //  String discountPrice;
//   //  String basePrice;

// }

// class GetDataPentHouse {
//   static String hotelName = GetDataPentHouse.hotelName;
//   static String roomddDiscription = GetDataPentHouse.roomddDiscription;
//   static String discountPrice = GetDataPentHouse.discountPrice;
//   static String basePrice = GetDataPentHouse.basePrice;
//   static String vendorID = GetDataPentHouse.vendorID;
//   static String userToken = GetDataPentHouse.userToken;
//   static String suCategory = GetDataPentHouse.suCategory;
//   static String roomName = GetDataPentHouse.roomName;
//   static int length = GetDataPentHouse.length;
//   static String logo = GetDataPentHouse.logo;

//   static String addLine2 = GetDataPentHouse.addLine1;
//   static String addLine1 = GetDataPentHouse.addLine2;
//   static List<String> images = GetDataPentHouse.images;

//   //  String hotelname = GetData().hotelName;
//   //  String roomddDiscription;
//   //  String discountPrice;
//   //  String basePrice;

// }

class _StaySearchMainState extends State<StaySearchMain> {
//   late List<SpecialSuitDetailsModel> _specialSuits;
  late bool _loading;


  @override
  void initState() {
    // TODO: implement initState
    fetchStayData('');
    super.initState();
    // _loading = true;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        // bottomNavigationBar: GestureDetector(
        //   onTap: () {
        //     Navigator.push(
        //         context, MaterialPageRoute(builder: (_) => StaySearchMain()));
        //   },
        //   child: Container(
        //     alignment: Alignment.center,
        //     child: Text(
        //       "SEARCH NOW",
        //       style: TextStyle(fontSize: 18.0, letterSpacing: 0.7),
        //     ),
        //     height: 57.0,
        //     color: btngoldcolor,
        //   ),
        // ),
        appBar: CustomAppBar(
          lead: Image.asset(
            "assets/icons/ic_stay.png",
            color: Colors.white,
            height: 24.0,
            width: 24.0,
          ),
          title: Text(
            "Stay",
            style: kHeadText,
          ),
        ),
        backgroundColor: bgcolor,
        body: isLoading
            ? CustomLoadingScr()
            : SingleChildScrollView(
                physics: ScrollPhysics(),
                child: Container(
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.all(17.0),
                        child: Column(
                          children: [
                            /////Search Box
                            TextFormField(
                              decoration: kSearchTxtFieldDecoration.copyWith(
                                fillColor: servicecardfillcolor,
                                filled: true,
                              ),
                            ),
                            SizedBox(
                              height: 20.0,
                            ),

                            //////Category selection Tab
                            Row(
                              children: [
                                /////specialSuites
                                Column(
                                  children: [
                                    GestureDetector(
                                      onTap: () async {
                                        setState(() {
                                          isSpecialSelected == false
                                              ? isSpecialSelected = true
                                              : isSpecialSelected = false;
                                          isSpecialSelected
                                              ? defaultView = false
                                              : defaultView = true;
                                          isPentHouseSelected = false;
                                          isBnBSelecte = false;
                                          print(widget.check_in);
                                          print(widget.check_out);
                                          print(widget.no_of_person);
                                          print(widget.no_of_rooms);
                                          fetchStayData("Special Suits");
                                        });
                                        // await StaySearchSpecialSuits(
                                        //   widget.check_in,
                                        //   widget.check_out,
                                        //   widget.no_of_person,
                                        //   widget.no_of_rooms,
                                        // );
                                      },
                                      child: Container(
                                        alignment: Alignment.center,
                                        child: Image.asset(
                                          "assets/icons/2.png",
                                          height: 30.0,
                                          width: 30.0,
                                        ),
                                        decoration: BoxDecoration(
                                          color: isSpecialSelected
                                              ? Colors.white
                                              : servicecardfillcolor,
                                          borderRadius:
                                              BorderRadius.circular(40),
                                          boxShadow: const [
                                            BoxShadow(
                                                color: txtborderbluecolor,
                                                spreadRadius: 1.5),
                                          ],
                                        ),
                                        height: 69.0,
                                        width: 69.0,

                                        //  kTxtFieldDecoration .copyWith(),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10.0,
                                    ),
                                    Text(
                                      "Special Suites",
                                      style: kTxtStyle,
                                    ),
                                  ],
                                ),

                                SizedBox(
                                  width: 13.0,
                                ),

                                /////Pent House
                                Column(
                                  children: [
                                    GestureDetector(
                                      onTap: () async {
                                        // await StaySearchPentHouse(
                                        //     widget.check_in,
                                        //     widget.check_out,
                                        //     widget.no_of_person,
                                        //     widget.no_of_rooms);
                                        setState(() {
                                          isPentHouseSelected == false
                                              ? isPentHouseSelected = true
                                              : isPentHouseSelected = false;
                                          isPentHouseSelected
                                              ? defaultView = false
                                              : defaultView = true;
                                          isSpecialSelected = false;
                                          isBnBSelecte = false;
                                          fetchStayData("Pent Houses");
                                        });
                                      },
                                      child: Container(
                                        alignment: Alignment.center,
                                        child: Image.asset(
                                          "assets/icons/3.png",
                                          height: 30.0,
                                          width: 30.0,
                                        ),
                                        decoration: BoxDecoration(
                                          color: isPentHouseSelected
                                              ? Colors.white
                                              : servicecardfillcolor,
                                          borderRadius:
                                              BorderRadius.circular(40),
                                          boxShadow: const [
                                            BoxShadow(
                                                color: txtborderbluecolor,
                                                spreadRadius: 1.5),
                                          ],
                                        ),
                                        height: 69.0,
                                        width: 69.0,

                                        //  kTxtFieldDecoration .copyWith(),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10.0,
                                    ),
                                    Text(
                                      "Pent House",
                                      style: kTxtStyle,
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  width: 19.0,
                                ),
                                /////Pvt BNB
                                Column(
                                  children: [
                                    GestureDetector(
                                      onTap: () async {
                                        // await StaySearchPrivateBnB(
                                        //     widget.check_in,
                                        //     widget.check_out,
                                        //     widget.no_of_person,
                                        //     widget.no_of_rooms);
                                        setState(() {
                                          isBnBSelecte == false
                                              ? isBnBSelecte = true
                                              : isBnBSelecte = false;
                                          isSpecialSelected = false;
                                          isPentHouseSelected = false;
                                          isBnBSelecte
                                              ? defaultView = false
                                              : defaultView = true;
                                          fetchStayData("Private BNB");
                                        });
                                      },
                                      child: Container(
                                        alignment: Alignment.center,
                                        child: Image.asset(
                                          "assets/icons/1.png",
                                          height: 30.0,
                                          width: 30.0,
                                        ),
                                        decoration: BoxDecoration(
                                          color: isBnBSelecte
                                              ? Colors.white
                                              : servicecardfillcolor,
                                          borderRadius:
                                              BorderRadius.circular(40),
                                          boxShadow: const [
                                            BoxShadow(
                                                color: txtborderbluecolor,
                                                spreadRadius: 1.5),
                                          ],
                                        ),
                                        height: 69.0,
                                        width: 69.0,

                                        //  kTxtFieldDecoration .copyWith(),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10.0,
                                    ),
                                    Text(
                                      "Private bnb",
                                      style: kTxtStyle,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20.0,
                            ),
                            isSpecialSelected
                                ? SpecialSuitView(
                                    checkInDate: widget.check_in,
                                    checkOutDate: widget.check_out,
                                    totalGuest: widget.no_of_person,
                                    totalRooms: widget.no_of_rooms,
                                  )
                                : isBnBSelecte
                                    ? PrivateBnBView(
                                        checkInDate: widget.check_in,
                                        checkOutDate: widget.check_out,
                                        totalGuest: widget.no_of_person,
                                        totalRooms: widget.no_of_rooms,
                                      )
                                    : isPentHouseSelected
                                        ? PentHouseView(
                                            checkInDate: widget.check_in,
                                            checkOutDate: widget.check_out,
                                            totalGuest: widget.no_of_person,
                                            totalRooms: widget.no_of_rooms,
                                          )
                                        : DefaultView(
                                            checkInDate: widget.check_in,
                                            checkOutDate: widget.check_out,
                                            totalGuest: widget.no_of_person,
                                            totalRooms: widget.no_of_rooms,
                                          ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Future fetchStayData(String catType) async {
    setState(() {
      isLoading = true;
    });

    servicesData = (await GetStayVendorData().getStayData(
      widget.check_in,
      widget.check_out,
      widget.no_of_person,
      widget.no_of_rooms,
      widget.uToken,
      catType,
      widget.city,
    )) as List<v.Service>;

    print("NAME +++=====" + servicesData[0].bussinessDetails!.bussinessName);
    print('''
    hotelName: ${servicesData[0].bussinessDetails!.bussinessName},
                    basePrice: ${servicesData[0].basePrice!},
                    discPrice: ${servicesData[0].disPrice!},
                    subCatName: ${servicesData[0].subServiceId!},
                    vendorID: ${servicesData[0].vendorId!},
                    roomName:${servicesData[0].roomName!},
                    addLine1: ${servicesData[0].bussinessDetails!.addressLine1!},
                    images: ${servicesData[0].stayPhotos!},
                    addLine2: ${servicesData[0].bussinessDetails!.addressLine2!},
                    businessDetails: ${servicesData[0].roomDescription!},
                  
                    bLogo:${servicesData[0].bussinessDetails!.bussinessLogo!},
    ''');
    setState(() {
      isLoading = false;
    });
  }
}

//////// Pent House Container
class PentHouseView extends StatefulWidget {
  final String totalRooms;
  final String totalGuest;
  final String checkInDate;
  final String checkOutDate;
  const PentHouseView({
    Key? key,
    required this.totalRooms,
    required this.totalGuest,
    required this.checkInDate,
    required this.checkOutDate,
  }) : super(key: key);

  @override
  State<PentHouseView> createState() => _PentHouseViewState();
}

class _PentHouseViewState extends State<PentHouseView> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(1.00),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Exclusive Pent House",
                style: TextStyle(
                    fontFamily: 'Raleway_Bold',
                    color: Colors.white,
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold),
              ),
              TextButton(
                onPressed: () {},
                child: Text(
                  "See All",
                  style: kTxtStyle,
                ),
              ),
            ],
          ),
          /////// generic Container 0f hotel Display card
          ///
          ///

          Container(
            padding: EdgeInsets.all(1),
            color: Colors.transparent,
            child: ListView.builder(
                scrollDirection: Axis.vertical,
                // physics:NeverScrollableScrollPhysics,
                physics: BouncingScrollPhysics(
                    parent: AlwaysScrollableScrollPhysics()),
                itemCount: servicesData.length, //GetDataSpecialSuits.length,
                shrinkWrap: true,
                itemBuilder: (_, index) {
                  // print(GetDataPentHouse.hotelName + "Hottel Name for Display");
                  // return Text("data");
                  return CustomHotelDisplayCard(
                    hotelName:
                        servicesData[index].bussinessDetails!.bussinessName,
                    basePrice: servicesData[index].basePrice!,
                    discPrice: servicesData[index].disPrice!,
                    subCatName: servicesData[index].subServiceId!,
                    vendorID: servicesData[index].vendorId!,
                    roomName: servicesData[index].roomName!,
                    addLine1:
                        servicesData[index].bussinessDetails!.addressLine1!,
                    imagesk: servicesData[index].bussinessDetails!.imagesk!,
                    addLine2:
                        servicesData[index].bussinessDetails!.addressLine2!,
                    roomDetails: servicesData[index].roomDescription!,
                    checkInDate: widget.checkInDate,
                    checkOutDate: widget.checkOutDate,
                    totalGuest: widget.totalGuest,
                    totalRooms: widget.totalRooms,
                    bLogo: servicesData[index].bussinessDetails!.bussinessLogo!,
                    stayPhotos: servicesData[index].stayPhotos!,
                    roomQuantity: servicesData[index].quantity!,
                    amanities: servicesData[index].amenities!,
                  );
                }),
          ),
        ],
      ),
    );
  }
}

/////////Private BnB Container
class PrivateBnBView extends StatefulWidget {
  final String totalRooms;
  final String totalGuest;
  final String checkInDate;
  final String checkOutDate;
  const PrivateBnBView({
    Key? key,
    required this.totalRooms,
    required this.totalGuest,
    required this.checkInDate,
    required this.checkOutDate,
  }) : super(key: key);

  @override
  State<PrivateBnBView> createState() => _PrivateBnBViewState();
}

class _PrivateBnBViewState extends State<PrivateBnBView> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(1.00),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Private bnb",
                style: TextStyle(
                    fontFamily: 'Raleway_Bold',
                    color: Colors.white,
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold),
              ),
              TextButton(
                onPressed: () {},
                child: Text(
                  "See All",
                  style: kTxtStyle,
                ),
              ),
            ],
          ),
          /////// generic Container 0f hotel Display card
          ///
          ///
          ///
          ListView.builder(
              scrollDirection: Axis.vertical,
              physics: BouncingScrollPhysics(
                  parent: AlwaysScrollableScrollPhysics()),
              itemCount:servicesData.length, // servicesData.length, // GetDataSpecialSuits.length,
              shrinkWrap: true,
              itemBuilder: (_, index) {
                return servicesData[index].subServiceId! == "Private BNB"
                    ? CustomHotelDisplayCard(
                        hotelName:
                            servicesData[index].bussinessDetails!.bussinessName,
                        basePrice: servicesData[index].basePrice!,
                        discPrice: servicesData[index].disPrice!,
                        subCatName: servicesData[index].subServiceId!,
                        vendorID: servicesData[index].vendorId!,
                        roomName: servicesData[index].roomName!,
                        addLine1:
                            servicesData[index].bussinessDetails!.addressLine1!,
                        imagesk: servicesData[index].bussinessDetails!.imagesk!,
                        addLine2:
                            servicesData[index].bussinessDetails!.addressLine2!,
                        roomDetails: servicesData[index].roomDescription!,
                        checkInDate: widget.checkInDate,
                        checkOutDate: widget.checkOutDate,
                        totalGuest: widget.totalGuest,
                        totalRooms: widget.totalRooms,
                        bLogo: servicesData[index]
                            .bussinessDetails!
                            .bussinessLogo!,
                        amanities: servicesData[index].amenities!,
                        stayPhotos: servicesData[index].stayPhotos!,
                        roomQuantity: servicesData[index].quantity!,
                      )
                    : Text(
                        "No Data Available",
                        style: kHeadText,
                      );
              }),
        ],
      ),
    );
  }
}

////// Special Suit Container
class SpecialSuitView extends StatefulWidget {
  final String totalRooms;
  final String totalGuest;
  final String checkInDate;
  final String checkOutDate;
  const SpecialSuitView({
    Key? key,
    required this.totalRooms,
    required this.totalGuest,
    required this.checkInDate,
    required this.checkOutDate,
  }) : super(key: key);

  @override
  State<SpecialSuitView> createState() => _SpecialSuitViewState();
}

class _SpecialSuitViewState extends State<SpecialSuitView> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(1.00),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Special for you",
                style: TextStyle(
                    fontFamily: 'Raleway_Bold',
                    color: Colors.white,
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold),
              ),
              TextButton(
                onPressed: () {},
                child: Text(
                  "See All",
                  style: kTxtStyle,
                ),
              ),
            ],
          ),
          ListView.builder(
              scrollDirection: Axis.vertical,
              physics: NeverScrollableScrollPhysics(),
              itemCount:  servicesData.length,
              // servicesData[0].subServiceId! == "Special Suits"
              //     ? 1
              //     :
              shrinkWrap: true,
              itemBuilder: (_, index) {
                ////stay phtos === room imgs
                ///imagesk === slider
                ///business logo ==== in front
                return servicesData[index].subServiceId! == "Special Suits"
                    ? CustomHotelDisplayCard(
                        hotelName:
                            servicesData[index].bussinessDetails!.bussinessName,
                        basePrice: servicesData[index].basePrice!,
                        discPrice: servicesData[index].disPrice!,
                        subCatName: servicesData[index].subServiceId!,
                        vendorID: servicesData[index].vendorId!,
                        roomName: servicesData[index].roomName!,
                        addLine1:
                            servicesData[index].bussinessDetails!.addressLine1!,
                        imagesk: servicesData[index].bussinessDetails!.imagesk!,
                        addLine2:
                            servicesData[index].bussinessDetails!.addressLine2!,
                        roomDetails: servicesData[index].roomDescription!,
                        checkInDate: widget.checkInDate,
                        checkOutDate: widget.checkOutDate,
                        totalGuest: widget.totalGuest,
                        totalRooms: widget.totalRooms,
                        bLogo: servicesData[index]
                            .bussinessDetails!
                            .bussinessLogo!,
                        stayPhotos: servicesData[index].stayPhotos!,
                        roomQuantity: servicesData[index].quantity!,
                        amanities: servicesData[index].amenities!,
                      )
                    : Text(
                        "No Data Available",
                        style: kHeadText,
                      );
              }),
        ],
      ),
    );
  }
}

///////Default View Containetr
class DefaultView extends StatefulWidget {
  final String totalRooms;
  final String totalGuest;
  final String checkInDate;
  final String checkOutDate;
  const DefaultView({
    Key? key,
    required this.totalRooms,
    required this.totalGuest,
    required this.checkInDate,
    required this.checkOutDate,
  }) : super(key: key);

  @override
  State<DefaultView> createState() => _DefaultViewState();
}

class _DefaultViewState extends State<DefaultView> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(1.00),
      // color: Colors.red,
      child: Column(
        children: [
          ////////Exclusive Paint House in Default View
          Container(
            color: Colors.transparent,
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Exclusive Pent House",
                      style: TextStyle(
                          fontFamily: 'Raleway_Bold',
                          color: Colors.white,
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold),
                    ),
                    TextButton(
                      onPressed: () {},
                      child: Text(
                        "See All",
                        style: kTxtStyle,
                      ),
                    ),
                  ],
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.all(10.0),
                        height: 173.0,
                        width: 133.0,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                  "assets/images/penthouse_thumnail_1.png")),
                          border: Border.all(color: txtborderbluecolor),
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.all(10.0),
                        height: 173.0,
                        width: 133.0,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                  "assets/images/penthouse_thumnail_2.png")),
                          border: Border.all(color: txtborderbluecolor),
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.all(10.0),
                        height: 173.0,
                        width: 133.0,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                  "assets/images/penthouse_thumnail_3.png")),
                          border: Border.all(color: txtborderbluecolor),
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          const SizedBox(
            height: 30.0,
          ),

          /////Pvt BNB in Default View
          Container(
            color: Colors.transparent,
            child: Column(
              children: [
                /*Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Exclusive Private BnB",
                      style: TextStyle(
                          fontFamily: 'Raleway_Bold',
                          color: Colors.white,
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold),
                    ),
                    TextButton(
                      onPressed: () {
                        setState(() {
                          isBnBSelecte = true;
                        });
                      },
                      child: Text(
                        "See All",
                        style: kTxtStyle,
                      ),
                    ),
                  ],
                ),*/


                ListView.builder(
                    scrollDirection: Axis.vertical,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount:  servicesData.length,
                    // servicesData[0].subServiceId! == "Special Suits"
                    //     ? 1
                    //     :
                    shrinkWrap: true,
                    itemBuilder: (_, index) {
                      ////stay phtos === room imgs
                      ///imagesk === slider
                      ///business logo ==== in front
                      return CustomHotelDisplayCard(
                        hotelName: servicesData[index].bussinessDetails!.bussinessName,
                        basePrice: servicesData[index].basePrice!,
                        discPrice: servicesData[index].disPrice!,
                        subCatName: servicesData[index].subServiceId!,
                        vendorID: servicesData[index].vendorId!,
                        roomName: servicesData[index].roomName!,
                        addLine1: servicesData[index].bussinessDetails!.addressLine1!,
                        imagesk: servicesData[index].bussinessDetails!.imagesk!,
                        addLine2: servicesData[index].bussinessDetails!.addressLine2!,
                        roomDetails: servicesData[index].roomDescription!,
                        checkInDate: widget.checkInDate,
                        checkOutDate: widget.checkOutDate,
                        totalGuest: widget.totalGuest,
                        totalRooms: widget.totalRooms,
                        bLogo: servicesData[index].bussinessDetails!.bussinessLogo!,
                        stayPhotos: servicesData[index].stayPhotos!,
                        roomQuantity: servicesData[index].quantity!,
                        amanities: servicesData[index].amenities!,
                      );
                    }),


                /////// generic Container 0f hotel Display card

                SizedBox(
                  height: 5.0,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
