import 'dart:convert';
import 'dart:io';

import 'package:carousel_images/carousel_images.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lasvegas_gts_app/customs/anamitis_card.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart' as home;
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/Networking/StayAPI/amenities_model.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_card_static.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'package:lasvegas_gts_app/screens/services/air/air_main_src.dart';
import 'package:lasvegas_gts_app/screens/services/stay/stay_checkout_scr.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:http/http.dart' as http;

AmenitiesListModel amidata = AmenitiesListModel(statusCode: 200, ami: []);
String APIkey = 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs';
final List<String> listImagesHotel = [];

String imageURL =
    "http://koolmindapps.com/lasvegas/public/images/stay/";

Uri amiURL = Uri.parse(
    "http://koolmindapps.com/lasvegas/public/api/v1/stayservicedetail");

/*const String roomImgURL =
    'http://koolmindapps.com/lasvegas/public/images/air/';*/

bool isHotelDisLoading = false;

class HottelDetailsScreen extends StatefulWidget {
  final String businessName;
  final String roomName;
  final String roomDetails;
  final String address1;
  final String address2;
  final List<String> imagesk;
  final List<String> imgs_rooms;
  final String discPrice;
  final String roomImg;
  final List<String> amaities;

  final String totalRooms;
  final String totalGuest;
  final String checkInDate;
  final String checkOutDate,vendorID;

  const HottelDetailsScreen(
      {Key? key,
      required this.businessName,
      required this.roomName,
      required this.roomDetails,
      required this.address1,
      required this.address2,
      required this.imagesk,
      required this.discPrice,
      required this.totalRooms,
      required this.totalGuest,
      required this.checkInDate,
      required this.checkOutDate,
      required this.imgs_rooms,
      required this.amaities, required this.roomImg, required this.vendorID})
      : super(key: key);

  @override
  _HottelDetailsScreenState createState() => _HottelDetailsScreenState();
}

class _HottelDetailsScreenState extends State<HottelDetailsScreen> {
  @override
  void initState() {
    // TODO: implement initState
    getAllAmis();

    super.initState();
    setState(() {});

    print("URL ===" + imageURL);
    int length = widget.imagesk.length;
    listImagesHotel.clear();
    for (var i = 0; i < widget.imgs_rooms.length; i++) {
      listImagesHotel.add(imageURL + widget.imgs_rooms[i]);
    }
    print(listImagesHotel
        .toString()); //.add(imageURL + widget.images.forEach((widget.images) { });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: bgcolor,
        appBar: AppBar(
          elevation: 0.0,
          leading: BackButton(),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                widget.businessName + " | " + widget.roomName,
                style: TextStyle(
                  fontFamily: 'Raleway-SemiBold',
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
              RatingBar.builder(
                glow: false,
                itemSize: 15.0,
                initialRating: 3,
                minRating: 1,
                direction: Axis.horizontal,
                allowHalfRating: true,
                itemCount: 5,
                //itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                itemBuilder: (context, _) => Icon(
                  Icons.star,
                  color: Colors.amber,
                  //   Image.asset(
                  // "assets/icons/rating_star.png",
                  // color: Colors.amber,
                ),
                // ),
                unratedColor: Colors.grey,
                onRatingUpdate: (rating) {
                  print(rating);
                },
              ),
            ],
          ),
          bottom: PreferredSize(
            child: Column(
              children: const [
                Divider(
                  thickness: 2,
                  height: 2,
                  color: Colors.amber,
                ),
                SizedBox(
                  height: 3.0,
                ),
                DottedLine(
                  lineThickness: 4.5,
                  dashRadius: 7,
                  dashLength: 4.0,
                  // lineLength:  MediaQuery.of(context).size.width,
                  dashColor: Colors.amber,
                ),
              ],
            ),
            preferredSize: Size.fromHeight(20),
          ),
        ),
        bottomNavigationBar: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.all(18.0),
          height: 75.0,
          color: servicecardfillcolor,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Price/room/night starts from",
                    style: TextStyle(color: Colors.white),
                  ),
                  Text(
                    "\$ " + widget.discPrice,
                    style: kTxtStyle,
                  ),
                ],
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => StayCheckout(
                          vendorID: widget.vendorID,
                          discPrise: widget.discPrice,
                          roomName: widget.roomName,
                          checkInDate: widget.checkInDate,
                          checkOutDate: widget.checkOutDate,
                          totalGuest: widget.totalGuest,
                          totalRooms: widget.totalRooms,
                          roomImg: widget.roomImg,
                        ),
                      ));
                },
                child: Text(
                  "Book Now",
                  style: TextStyle(color: Colors.black),
                ),
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  fixedSize: Size(108.0, 40.0),
                  primary: Color(0xFFFFC71E),
                ),
              ),
            ],
          ),
        ),
        body: isHotelDisLoading
            ? CustomLoadingScr()
            : SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.all(0.01),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 30.0,
                      ),
                      Column(
                        children: [
                          // Image.network(
                          //  imageURL + widget.images,
                          // ),
                          CarouselImages(
                            viewportFraction: 0.9,
                            scaleFactor: 0.1,
                            listImages: listImagesHotel,
                            height: 357.0,
                            borderRadius: 8.0,
                            cachedNetworkImage: true,
                            verticalAlignment: Alignment.center,
                            onTap: (index) {
                              print('Tapped on page $index');
                            },
                          ),
                          SizedBox(
                            height: 30.0,
                          ),
                          Container(
                            padding: EdgeInsets.all(20.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Rating & Reviews",
                                  style: TextStyle(
                                    fontFamily: 'Raleway-SemiBold',
                                    fontSize: 18.0,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  ),
                                ),
                                SizedBox(
                                  height: 15.0,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      alignment: Alignment.center,
                                      height: 50,
                                      width: 55,
                                      child: Text(
                                        "8.9",
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            color: Colors.white,
                                            fontFamily: 'Manrope-SemiBold'),
                                      ),
                                      decoration: BoxDecoration(
                                        border: Border.all(color: btngoldcolor),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5.0)),
                                      ),
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Impressive",
                                          style: TextStyle(
                                            fontSize: 15.0,
                                            fontFamily: 'Raleway-SemiBold',
                                            color: Colors.white,
                                          ),
                                        ),
                                        Text(
                                          "Rating based on 6969 reviews across the web.",
                                          style: TextStyle(
                                            fontSize: 12.0,
                                            fontFamily: 'Manrope-SemiBold',
                                            color: greytxtcolor,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                SizedBox(height: 30.0),
                                Row(
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        ReviewBarIndicator(
                                            title: 'Confort', rating: 8.8),
                                        ReviewBarIndicator(
                                            title: 'Cleanliness', rating: 7.7),
                                        ReviewBarIndicator(
                                            title: 'Location', rating: 5.5),
                                      ],
                                    ),
                                    SizedBox(
                                      width: 30.0,
                                    ),
                                    Column(
                                      children: [
                                        ReviewBarIndicator(
                                          rating: 5.5,
                                          title: 'Service',
                                        ),
                                        ReviewBarIndicator(
                                            title: 'Food', rating: 3.5),
                                      ],
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      Divider(
                        height: 1.0,
                        thickness: 1.0,
                        color: greytxtcolor,
                      ),
                      SizedBox(
                        height: 25.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(20.0, 0, 0, 0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            widget.amaities == null || widget.amaities.isEmpty
                                ? SizedBox(
                                    height: 0.0,
                                  )
                                : Text(
                                    "Main Amenities",
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white),
                                  ),
                            widget.amaities == null || widget.amaities.isEmpty
                                ? SizedBox(
                                    height: 0.0,
                                  )
                                : SizedBox(
                                    height: 15.0,
                                  ),
                            widget.amaities == null || widget.amaities.isEmpty
                                ? SizedBox(
                                    height: 0.0,
                                  )
                                : Container(
                                    height: 120,
                                    padding: EdgeInsets.all(0),
                                    child: ListView.builder(
                                        physics: BouncingScrollPhysics(
                                            parent:
                                                AlwaysScrollableScrollPhysics()),
                                        scrollDirection: Axis.horizontal,
                                        shrinkWrap: true,
                                        itemCount: amidata.ami.length,
                                        itemBuilder: (_, index) {
                                          return widget.amaities.contains(
                                                  amidata.ami[index].id
                                                      .toString())
                                              ? AmenitiesCard(
                                                  icon: amidata.ami[index]
                                                      .amenitiesImage,
                                                  title: amidata
                                                      .ami[index].amenitiesName)
                                              : SizedBox(
                                                  height: 0,
                                                );
                                        }),
                                  ),
                            widget.amaities == null || widget.amaities.isEmpty
                                ? SizedBox(
                                    height: 0.0,
                                  )
                                : SizedBox(
                                    height: 30.0,
                                  ),
                            Text(
                              "Hotel Details",
                              style: TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            Text(
                              widget.roomDetails,
                              // """The Hotel Pennsylvania is conveniently located within one block of the Empire State Building,Madison Square Garden, Penn Station and Macy's flagship store. Located within walking distance are Times Square, the Broadway Theatre District, New York Public Library, Jacob K. Javits Convention Center, and two of New York's most iconic…""",
                              style: kTxtStyle.copyWith(color: greytxtcolor),
                            ),
                            SizedBox(
                              height: 30.0,
                            ),
                            Text(
                              "Contact",
                              style: TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Text(
                              widget.address1 + ' , ' + widget.address2 + ' . ',
                              // """T3131 Las Vegas Boulevard South, 89109, Las Vegas""",
                              style: kTxtStyle.copyWith(color: greytxtcolor),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            /*Container(
                              child:
                                  Image.asset("assets/images/banner_img_2.png"),
                              height: 215.0,
                              width: 380.0,
                              color: bgcolor,
                            ),*/
                            SizedBox(
                              height: 40.0,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Future getAllAmis() async {
    setState(() {
      isHotelDisLoading = true;
    });
    print(home.homeToken);
    var amires = await http.post(
      amiURL,
      headers: {
        'API_KEY': APIkey,
        'Authorization': "Bearer " + home.homeToken,
      },
    );

    if (amires.statusCode == 200) {
      print(amires.body);
      amidata = AmenitiesListModel.fromJson(jsonDecode(amires.body));
      print(amidata.ami[0].amenitiesName);
      // return amidata;
    }
    setState(() {
      isHotelDisLoading = false;
    });
  }
}

class ReviewBarIndicator extends StatelessWidget {
  final String title;
  final double rating;

  ReviewBarIndicator({Key? key, required this.title, required this.rating})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          title,
          style: TextStyle(
            color: Colors.white,
            fontSize: 12.0,
            fontFamily: 'Raleway'
          ),
        ),
        SizedBox(
          height: 10.0,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            LinearPercentIndicator(
              width: 140.0,
              lineHeight: 8.0,
              percent: rating * 0.1,
              backgroundColor: Color(0xFFFFE8EB),
              progressColor: btngoldcolor,
            ),
            SizedBox(
              width: 3.0,
            ),
            Text(
              rating.toString(),
              style: kTxtStyle.copyWith(fontFamily: 'Regular'),
            ),
          ],
        ),
        SizedBox(
          height: 15.0,
        ),
      ],
    );
  }
}
