import 'package:autocomplete_textfield_ns/autocomplete_textfield_ns.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/StayAPI/stay_search_specil_suits_api.dart';
// import 'package:lasvegas_gts_app/Networking/StayAPI/stay_search_pent_house_api.dart';
// import 'package:lasvegas_gts_app/Networking/StayAPI/stay_search_pvt_bnb_api.dart';
// import 'package:lasvegas_gts_app/Networking/StayAPI/stay_search_specil_suits_api.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'package:lasvegas_gts_app/model/stay_model/add_room_model.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';
import 'package:lasvegas_gts_app/screens/services/air/air_main_src.dart';
import 'package:lasvegas_gts_app/screens/services/stay/add_rooms.dart';
import 'package:lasvegas_gts_app/screens/services/stay/city_list_model.dart';
import 'package:lasvegas_gts_app/screens/services/stay/rooms.dart';
import 'package:lasvegas_gts_app/screens/services/stay/sta_search_main.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:ui' as ui;

bool isLoadingcity = false;

class StaySearchFilter extends StatefulWidget {
  final String utoken;

  const StaySearchFilter({Key? key, required this.utoken}) : super(key: key);

  @override
  _StaySearchFilterState createState() => _StaySearchFilterState();
}

late BuildContext dialogcontext;

class _StaySearchFilterState extends State<StaySearchFilter> {
  // String userToken = widget.userToken;
  // Future getToken() async {
  //   final SharedPreferences preferences = await SharedPreferences.getInstance();
  //   var token = preferences.getString('userToken');

  //   setState(() {
  //     userToken = token!;
  //   });
  // }

  DateTime dateTime = DateTime.now();
  DateTime indateTime = DateTime.now();
  DateTime outdateTime = DateTime.now();
  String inDate = '';
  String outDate = '';
  final adults = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'];
  final childrn = ['0', '1', '2', '3', '4', '5'];
  String? ano;
  String? cno;
  List<Rooms> roomslist = [];
  int aGust = 0;
  int cGust = 0;

  int tGust = 0;

  List<Widget> adds = [];
  List<int> totalGuest = [];
  num totalCandidate = 0;

  List<String> cityList = [];
  List<String> cityCodeList = [];
  String cityCode = '';
  String citytSelected = '';
  TextEditingController cityController = TextEditingController();
  GlobalKey<AutoCompleteTextFieldState<String>> cityKey = GlobalKey();


  // Add By FZ For AddRoom LIst
  List<AddRoomModel> add_room_list = [];
  @override
  void initState() {
    // Add By FZ Add room
    firstTimeAddbyDefaultRoom();
    fetchCityList();
    super.initState();
  }

  Future firstTimeAddbyDefaultRoom() async {

    setState(() {
      AddRoomModel model = AddRoomModel(
          aGust: 0,
          cGust: 0,
          tGust: 0,
          str_agust: null,
          str_cgust: null);
      add_room_list.add(model);
    });
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar: GestureDetector(
          onTap: () async {
            // print(tGust);
            // comment by FZ
            /*totalGuest.add(tGust);
            for (num e in totalGuest) {
              totalCandidate += e;
            }*/

            // ADD by FZ
            for(int i=0; i<add_room_list.length; i++){
              totalCandidate += add_room_list[i].tGust;
            }

            print("no of person" + totalCandidate.toString());
            print(inDate);
            print(outDate);
            print("rooms " + (adds.length + 1).toString());
            print("Selected City ==== " + citytSelected);
            // var cityCodeindex = cityList.indexOf(citytSelected);
            // print(cityCodeindex);
            print("Selected City Code ==== " + cityCode);
            String checkIn = inDate;
            String checkOut = outDate;
            String noOfPerson = totalCandidate.toString();

            // String noOfRooms = (adds.length + 1).toString();

            // Add by FZ
            String noOfRooms = (add_room_list.length).toString();


            // searchdata(checkIn, checkOut, noOfPerson, noOfRooms);
            //  await StaySearchPentHouse(checkIn, checkOut, noOfRooms, noOfPerson);
            //  await StaySearchPrivateBnB(checkIn, checkOut, noOfRooms, noOfPerson);
            //  await StaySearchSpecialSuits(
            //       checkIn, checkOut, noOfPerson, noOfRooms);

            // ADD BY FZ
            defaultView = true;
            isSpecialSelected = false;
            isPentHouseSelected = false;
            isBnBSelecte = false;
            isFav = false;

            if(inDate == '' || outDate == '' || noOfPerson == '0' || citytSelected == ''){
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text('Please fill all data'),
                  backgroundColor: (Colors.black12),
                  action: SnackBarAction(
                    label: 'dismiss',
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ),
              );
            }else {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (_) => StaySearchMain(
                        check_in: checkIn,
                        check_out: checkOut,
                        no_of_rooms: noOfRooms,
                        no_of_person: noOfPerson,
                        uToken: homeToken, // widget.utoken,
                        city: cityCode,
                      )));

              totalGuest.clear();

              setState(() {
                adds.clear();
                totalCandidate = 0;
                // tGust = 0;
                ano = '0';
                cno = '0';

                add_room_list.clear();
              });
            }
          },
          child: Container(
            alignment: Alignment.center,
            child: Text(
              "SEARCH NOW",
              style: TextStyle(fontSize: 18.0, letterSpacing: 0.7),
            ),
            height: 57.0,
            color: btngoldcolor,
          ),
        ),
        appBar: CustomAppBar(
          lead: Image.asset(
            "assets/icons/ic_stay.png",
            color: Colors.white,
            height: 24.0,
            width: 24.0,
          ),
          title: Text(
            "Stay",
            style: kHeadText,
          ),
        ),
        backgroundColor: bgcolor,
        body: isLoadingcity
            ? CustomLoadingScr()
            : SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.all(20.0),
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 30.0,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              ////checkIN Date
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Image.asset(
                                        "assets/icons/ic_calendar.png",
                                        height: 20.0,
                                        width: 20.0,
                                        color: Colors.white,
                                      ),
                                      SizedBox(
                                        width: 15.0,
                                      ),
                                      Text(
                                        "Check-In date",
                                        style: TextStyle(
                                          color: greytxtcolor,
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  InkWell(
                                    onTap: () async {
                                      DateTime? newDate = await showDatePicker(
                                          context: context,
                                          initialDate: indateTime,
                                          firstDate: indateTime,
                                          lastDate: DateTime(2200));

                                      if (newDate != null) {
                                        setState(() {
                                          dateTime = newDate;
                                          //indateTime = newDate;
                                          inDate =
                                              '${dateTime.day.toString().padLeft(2, '0')}-${dateTime.month.toString().padLeft(2, '0')}-${dateTime.year}';
                                        });
                                      }
                                    },
                                    child: Container(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                      height: 46.0,
                                      width: 166.0,
                                      child: Text(
                                        inDate == null
                                            ? '${dateTime.day.toString().padLeft(2, '0')}-${dateTime.month.toString().padLeft(2, '0')}-${dateTime.year}'
                                            : inDate,
                                        textAlign: TextAlign.center,
                                        style:
                                            kTxtStyle.copyWith(fontSize: 16.0),
                                      ),
                                      decoration: BoxDecoration(
                                        color: servicecardfillcolor,
                                        border: Border.all(
                                            color: txtborderbluecolor),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10.0)),
                                      ),
                                      // TextField(
                                      //   controller: inDate,
                                      //   textAlign: TextAlign.center,
                                      //   style: kTxtStyle,
                                      //   decoration: kTxtFieldDecoration.copyWith(
                                      //       hintText: "dd mm yyyy",
                                      //       contentPadding: EdgeInsets.symmetric(
                                      //           vertical: 5.0, horizontal: 10.0),
                                      //       hintStyle: kTxtStyle,
                                      //       filled: true,
                                      //       fillColor: servicecardfillcolor),
                                      // ),
                                    ),
                                  ),
                                ],
                              ),

                              //////checkOUT Date
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Image.asset(
                                        "assets/icons/ic_calendar.png",
                                        height: 20.0,
                                        width: 20.0,
                                        color: Colors.white,
                                      ),
                                      SizedBox(
                                        width: 15.0,
                                      ),
                                      Text(
                                        "Check-Out date",
                                        style: TextStyle(
                                          color: greytxtcolor,
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),

                                  InkWell(
                                    onTap: () async {
                                      DateTime? newDate = await showDatePicker(
                                          context: context,
                                          initialDate:
                                              dateTime.add(Duration(days: 1)),
                                          firstDate: dateTime.add(Duration(days: 1)),
                                          lastDate: DateTime(2200));
                                      if (newDate != null) {
                                        setState(() {
                                          //dateTime = newDate;
                                          outdateTime = newDate;
                                          outDate =
                                          '${outdateTime.day.toString().padLeft(2, '0')}-${outdateTime.month.toString().padLeft(2, '0')}-${outdateTime.year}';

                                        });
                                      }
                                    },
                                    child: Container(
                                      padding: EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                      height: 46.0,
                                      width: 166.0,
                                      child: Text(
                                        outDate,
                                        textAlign: TextAlign.center,
                                        style:
                                            kTxtStyle.copyWith(fontSize: 16.0),
                                      ),
                                      decoration: BoxDecoration(
                                        color: servicecardfillcolor,
                                        border: Border.all(
                                            color: txtborderbluecolor),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10.0)),
                                      ),
                                    ),
                                  ),

                                  // Text(
                                  //   "Duration",
                                  //   style: TextStyle(
                                  //     color: greytxtcolor,
                                  //   ),
                                  // ),
                                  // SizedBox(
                                  //   height: 20.0,
                                  // ),
                                  // Container(
                                  //   height: 46.0,
                                  //   width: 136.0,
                                  //   child: TextField(
                                  //     style: kTxtStyle,
                                  //     decoration: kTxtFieldDecoration.copyWith(
                                  //         suffixIcon: Icon(
                                  //           Icons.arrow_drop_down,
                                  //           color: Colors.white,
                                  //         ),
                                  //         hintText: "1 Night",
                                  //         contentPadding: EdgeInsets.symmetric(
                                  //             vertical: 5.0, horizontal: 15.0),
                                  //         hintStyle: kTxtStyle,
                                  //         filled: true,
                                  //         fillColor: servicecardfillcolor),
                                  //   ),
                                  // )
                                ],
                              )
                            ],
                          ),
                          SizedBox(
                            height: 30.0,
                          ),

                          ///////Select City
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Select City",
                                style: TextStyle(
                                  color: greytxtcolor,
                                ),
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              Container(
                                decoration: BoxDecoration(
                                  color: servicecardfillcolor,
                                  border: Border.all(color: txtborderbluecolor),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                ),
                                padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                                child: Directionality(
                                  textDirection: ui.TextDirection.ltr,
                                  child: SimpleAutoCompleteTextField(
                                    textSubmitted: (text) {
                                      if (cityList.contains(text)) {
                                        // cityController.text = text;
                                        citytSelected = text;
                                        var cityCodeindex =
                                            cityList.indexOf(citytSelected);
                                        cityCode = cityCodeList[cityCodeindex];
                                        print(cityCodeindex);
                                        print(cityCode);
                                      }
                                    },
                                    textChanged: (text) {
                                      // setState(() {
                                      //   citytSelected = text;
                                      // });
                                      // for (var i = 0; i < cityList.length; i++) {
                                      if (cityList.contains(citytSelected)) {
                                        // var cityCodeindex =
                                        //     cityCodeList.indexOf(citytSelected);
                                        // print(cityCodeindex);
                                        // print(cityCode);
                                      }
                                      // }
                                    },
                                    clearOnSubmit: false,
                                    controller: cityController,
                                    key: cityKey,
                                    suggestions: cityList,
                                    style: kTxtStyle.copyWith(fontSize: 16.0),
                                    decoration: InputDecoration.collapsed(
                                      hintText: "search city",
                                      fillColor: servicecardfillcolor,

                                      border: InputBorder.none,
                                      // focusedBorder:
                                      //     InputBorder.none,
                                      // enabledBorder:
                                      //     InputBorder.none,
                                      // errorBorder:
                                      //     InputBorder.none,
                                      // disabledBorder:
                                      //     InputBorder.none,
                                      hintStyle: kTxtStyle,
                                      //  TextStyle(
                                      //     color: Color(
                                      //         ColorConst
                                      //             .grayColor),
                                      //     fontSize: 11.5.sp),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),

                          SizedBox(
                            height: 30.0,
                          ),

                          ////// ADD Rooms Form

                          // Container(
                          //   child: ListView.builder(
                          //       shrinkWrap: true,
                          //       itemCount: adds.length,
                          //       itemBuilder: (_, i) =>
                          //           AddRooms(i, onDeleteRoom(i))),
                          // ),

                          // Comment BY FZ
                          /*Column(
                            children: adds,
                          ),

                          addRoom(adds.length + 1),*/

                          // Add By FZ For Add Room List
                          ListView.builder(
                              primary: false,
                              shrinkWrap: true,
                              physics: const NeverScrollableScrollPhysics(),
                              itemCount: add_room_list.length,
                              itemBuilder: (context, index){

                                return addRoom01(index);

                              }
                          ),


                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ElevatedButton(
                                onPressed: () {
                                  setState(() {
                                    /*totalGuest.add(tGust);

                                    adds.add(addRoom(adds.length + 1));*/

                                    AddRoomModel model = AddRoomModel(
                                        aGust: 0,
                                        cGust: 0,
                                        tGust: 0,
                                        str_agust: null,
                                        str_cgust: null);
                                    add_room_list.add(model);

                                  });


                                  // count = count! + 1;
                                  // index = count;
                                },
                                style: ElevatedButton.styleFrom(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(13.0),
                                  ),
                                  fixedSize: Size(150.0, 57.0),
                                  primary: Color(0xFFFFC71E),
                                ),
                                child: Text(
                                  "Add Room",
                                  style:
                                      kTxtStyle.copyWith(color: Colors.black),
                                ),
                              ),
                              // SizedBox(
                              //   width: 50.0,
                              // ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }

  Container addRoom(
    int index,
  ) {
    // setState(() {});
    return Container(
      margin: EdgeInsets.fromLTRB(0, 5, 0, 10),
      padding: EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        // color: servicecardfillcolor,

        border: Border.all(color: txtborderbluecolor),
        borderRadius: BorderRadius.all(Radius.circular(15.0)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Room " + index.toString(),
                textAlign: TextAlign.left,
                style: kTxtStyle,
              ),
              // ElevatedButton(
              //   onPressed: () {
              //     setState(() {
              //       adds.removeLast();
              //       ano = '';
              //       cno = '';
              //     });

              //     // count = count! + 1;
              //     // index = count;
              //   },
              //   style: ElevatedButton.styleFrom(
              //     shape: RoundedRectangleBorder(
              //       borderRadius: BorderRadius.circular(13.0),
              //     ),
              //     // fixedSize: Size(117.0, 30.0),
              //     primary: Color(0xFFFFC71E),
              //   ),
              //   child:

              IconButton(
                onPressed: () {
                  // onDeleteRoom;
                  setState(() {
                    adds.removeAt(index);
                    ano = '';
                    cno = '';
                  });

                  // count = count! + 1;
                  // index = count;
                },
                icon: Icon(
                  Icons.delete_outline_outlined,
                  color: btngoldcolor,
                ),
              ),

              //  Text(
              //   "Delete Room",
              //   style:
              //       kTxtStyle.copyWith(color: Colors.black),
              // ),
              // ),
            ],
          ),
          SizedBox(
            height: 10.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text(
                        "Adults \n(above 12 years)",
                        style: TextStyle(
                          color: greytxtcolor,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    height: 46.0,
                    width: 166.0,
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        // alignment: Alignment.center,
                        dropdownColor: servicecardfillcolor,
                        icon: Icon(
                          Icons.arrow_drop_down,
                          color: Colors.white,
                        ),
                        style: kTxtStyle.copyWith(fontSize: 16.0),
                        isExpanded: true,
                        value: ano,
                        items: adults.map(buildMenuItem).toList(),
                        onChanged: (value) => setState(() {
                          ano = value;
                          aGust = int.parse(ano!);
                          // print(ano);
                          // print(tGust);
                        }),
                      ),
                    ),
                    //  Text(
                    //   '2',
                    //   textAlign: TextAlign.center,
                    //   style: kTxtStyle.copyWith(
                    //       fontSize: 16.0),
                    // ),
                    padding:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                    decoration: BoxDecoration(
                      color: servicecardfillcolor,
                      border: Border.all(color: txtborderbluecolor),
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                  ),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      // Image.asset(
                      //   "assets/icons/ic_calendar.png",
                      //   height: 20.0,
                      //   width: 20.0,
                      // ),
                      // SizedBox(
                      //   width: 15.0,
                      // ),
                      Text(
                        "Children\n(1 - 12 years)",
                        style: TextStyle(
                          color: greytxtcolor,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    alignment: Alignment.center,
                    height: 46.0,
                    width: 166.0,
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        // alignment: Alignment.center,
                        dropdownColor: servicecardfillcolor,
                        icon: Icon(
                          Icons.arrow_drop_down,
                          color: Colors.white,
                        ),
                        style: kTxtStyle.copyWith(fontSize: 16.0),
                        isExpanded: true,
                        value: cno,
                        items: childrn.map(buildMenuItem).toList(),
                        onChanged: (value) => setState(() {
                          cno = value;
                          cGust = int.parse(cno!);
                        }),
                      ),
                    ),
                    padding:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                    decoration: BoxDecoration(
                      color: servicecardfillcolor,
                      border: Border.all(color: txtborderbluecolor),
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                  ),
                ],
              )
            ],
          ),
          SizedBox(
            height: 30.0,
          ),
          Row(
            children: [
              SizedBox(
                width: 15.0,
              ),
              Text(
                "Total Guests",
                style: TextStyle(
                  color: greytxtcolor,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10.0,
          ),
          Container(
            width: 430,
            child: Text(
              (tGust = aGust + cGust).toString() + "  Guest",

              // TotalGuest(this.ano, this.cno),
              // tGust.toString(),
              style: kTxtStyle.copyWith(fontSize: 16.0),
            ),
            padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
            decoration: BoxDecoration(
              color: servicecardfillcolor,
              border: Border.all(color: txtborderbluecolor),
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
          )
        ],
      ),
    );
  }


  // Add by FZ
  Container addRoom01(
      int index,
      ) {
    // setState(() {});
    int room_no = index+1;
    return Container(
      margin: EdgeInsets.fromLTRB(0, 5, 0, 10),
      padding: EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        // color: servicecardfillcolor,

        border: Border.all(color: txtborderbluecolor),
        borderRadius: BorderRadius.all(Radius.circular(15.0)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Room " + room_no.toString(),
                textAlign: TextAlign.left,
                style: kTxtStyle,
              ),
              // ElevatedButton(
              //   onPressed: () {
              //     setState(() {
              //       adds.removeLast();
              //       ano = '';
              //       cno = '';
              //     });

              //     // count = count! + 1;
              //     // index = count;
              //   },
              //   style: ElevatedButton.styleFrom(
              //     shape: RoundedRectangleBorder(
              //       borderRadius: BorderRadius.circular(13.0),
              //     ),
              //     // fixedSize: Size(117.0, 30.0),
              //     primary: Color(0xFFFFC71E),
              //   ),
              //   child:

              IconButton(
                onPressed: () {
                  // onDeleteRoom;
                  setState(() {
                    /*adds.removeAt(index);
                    ano = '';
                    cno = '';*/

                    add_room_list.removeAt(index);

                  });

                  // count = count! + 1;
                  // index = count;
                },
                icon: Icon(
                  Icons.delete_outline_outlined,
                  color: btngoldcolor,
                ),
              ),

              //  Text(
              //   "Delete Room",
              //   style:
              //       kTxtStyle.copyWith(color: Colors.black),
              // ),
              // ),
            ],
          ),
          SizedBox(
            height: 10.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text(
                        "Adults \n(above 12 years)",
                        style: TextStyle(
                          color: greytxtcolor,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    height: 46.0,
                    width: 166.0,
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        // alignment: Alignment.center,
                        dropdownColor: servicecardfillcolor,
                        icon: Icon(
                          Icons.arrow_drop_down,
                          color: Colors.white,
                        ),
                        style: kTxtStyle.copyWith(fontSize: 16.0),
                        isExpanded: true,
                        value: add_room_list[index].str_agust,
                        items: adults.map(buildMenuItem).toList(),
                        onChanged: (value) => setState(() {
                          /*ano = value;
                          aGust = int.parse(ano!);*/

                          ano = value;
                          add_room_list[index].str_agust = value!;
                          add_room_list[index].aGust = int.parse(value);

                          add_room_list[index].tGust = add_room_list[index].cGust + add_room_list[index].aGust;
                          // print(ano);
                          // print(tGust);
                        }),
                      ),
                    ),
                    //  Text(
                    //   '2',
                    //   textAlign: TextAlign.center,
                    //   style: kTxtStyle.copyWith(
                    //       fontSize: 16.0),
                    // ),
                    padding:
                    EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                    decoration: BoxDecoration(
                      color: servicecardfillcolor,
                      border: Border.all(color: txtborderbluecolor),
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                  ),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      // Image.asset(
                      //   "assets/icons/ic_calendar.png",
                      //   height: 20.0,
                      //   width: 20.0,
                      // ),
                      // SizedBox(
                      //   width: 15.0,
                      // ),
                      Text(
                        "Children\n(1 - 12 years)",
                        style: TextStyle(
                          color: greytxtcolor,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    alignment: Alignment.center,
                    height: 46.0,
                    width: 166.0,
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        // alignment: Alignment.center,
                        dropdownColor: servicecardfillcolor,
                        icon: Icon(
                          Icons.arrow_drop_down,
                          color: Colors.white,
                        ),
                        style: kTxtStyle.copyWith(fontSize: 16.0),
                        isExpanded: true,
                        value: add_room_list[index].str_cgust,
                        items: childrn.map(buildMenuItem).toList(),
                        onChanged: (value) => setState(() {
                          /*cno = value;
                          cGust = int.parse(cno!);*/

                          cno = value;
                          add_room_list[index].str_cgust = value!;
                          add_room_list[index].cGust = int.parse(value);

                          add_room_list[index].tGust = add_room_list[index].cGust + add_room_list[index].aGust;
                        }),
                      ),
                    ),
                    padding:
                    EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                    decoration: BoxDecoration(
                      color: servicecardfillcolor,
                      border: Border.all(color: txtborderbluecolor),
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                  ),
                ],
              )
            ],
          ),
          SizedBox(
            height: 30.0,
          ),
          Row(
            children: [
              SizedBox(
                width: 15.0,
              ),
              Text(
                "Total Guests",
                style: TextStyle(
                  color: greytxtcolor,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10.0,
          ),
          Container(
            width: 430,
            child: Text(
              /*(tGust = aGust + cGust).toString() + "  Guest",*/
              '${add_room_list[index].tGust.toString()} Guest',

              // TotalGuest(this.ano, this.cno),
              // tGust.toString(),
              style: kTxtStyle.copyWith(fontSize: 16.0),
            ),
            padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
            decoration: BoxDecoration(
              color: servicecardfillcolor,
              border: Border.all(color: txtborderbluecolor),
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
          )
        ],
      ),
    );
  }

  DropdownMenuItem<String> buildMenuItem(String item) =>
      DropdownMenuItem(value: item, child: Text(item));

  Future fetchCityList() async {
    setState(() {
      isLoadingcity = true;
    });

    print(widget.utoken);
    String tkn = widget.utoken;

    List<Datum>? parsedRes = await GetCityList().getCityList(homeToken);
    if (parsedRes != null) {
      for (var i = 0; i < parsedRes.length; i++) {
        if (parsedRes[i].city != null) {
          cityList.add(parsedRes[i].name! + ' - ' + parsedRes[i].city!.name!);
          cityCodeList.add(parsedRes[i].id.toString());
        }
      }
    }
    print(cityList[3]);
    print(cityCodeList[3]);

    setState(() {
      isLoadingcity = false;
    });
  }

  onDeleteRoom(int i) {
    adds.removeAt(i);
  }
}
