import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/screens/services/stay/stay_payment_scr.dart';
import 'package:lasvegas_gts_app/screens/services/stay/stay_select_room.dart';

class StayCheckout extends StatefulWidget {
  final String discPrise;
  final String roomName;
  final String roomImg;
  final String totalRooms;
  final String totalGuest;
  final String checkInDate;
  final String checkOutDate,vendorID;

  const StayCheckout(
      {Key? key,
      required this.discPrise,
      required this.roomName,
      required this.totalRooms,
      required this.totalGuest,
      required this.checkInDate,
      required this.checkOutDate, required this.roomImg, required this.vendorID})
      : super(key: key);

  @override
  _StayCheckoutState createState() => _StayCheckoutState();
}

class _StayCheckoutState extends State<StayCheckout> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: bgcolor,
        appBar: CustomAppBar(
          lead: BackButton(),
          title: Text(
            "Checkout",
            style: kHeadText,
          ),
        ),
        bottomNavigationBar: GestureDetector(
          onTap: () async{

await 
            Navigator.push(
                context, MaterialPageRoute(builder: (_) => StayPayment(
              vendorID: widget.vendorID,
              discPrise: widget.discPrise,
              roomName: widget.roomName,
              checkInDate: widget.checkInDate,
              checkOutDate: widget.checkOutDate,
              totalGuest: widget.totalGuest,
              totalRooms: widget.totalRooms,
              roomImg: widget.roomImg,
            )));
          },
          child: Container(
            alignment: Alignment.center,
            padding: EdgeInsets.all(18.0),
            height: 57.0,
            color: btngoldcolor,
            child: Text(
              "PAY NOW",
              style: TextStyle(
                  fontSize: 16.0,
                  fontFamily: 'Raleway-Regular',
                  color: Colors.black),
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 8.0, bottom: 8.0, left: 15.0, right: 10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    // crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      CircleAvatar(
                        backgroundColor: btngoldcolor,
                        child: Text(
                          "1",
                          style: kTxtStyle,
                        ),
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Text(
                        "Book & Review",
                        style: kTxtStyle.copyWith(color: btngoldcolor),
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Expanded(
                        child: Divider(
                          thickness: 1.0,
                          height: 1.0,
                          color: btngoldcolor,
                        ),
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      CircleAvatar(
                        backgroundColor: greytxtcolor,
                        child: Text(
                          "2",
                          style: kTxtStyle.copyWith(color: favcolor),
                        ),
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Text(
                        "Checkout",
                        style: kTxtStyle.copyWith(color: greytxtcolor),
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Expanded(
                        child: Divider(
                          thickness: 1.0,
                          height: 1.0,
                          color: greytxtcolor,
                        ),
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      CircleAvatar(
                        backgroundColor: greytxtcolor,
                        child: Text(
                          "3",
                          style: kTxtStyle.copyWith(color: favcolor),
                        ),
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Text(
                        "Payment",
                        style: kTxtStyle.copyWith(color: greytxtcolor),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 25.0,
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(25.0, 15.0, 15.0, 15.0),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text(
                                widget.roomName,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20.0,
                                  fontFamily: 'Manrope-SemiBold'
                                ),
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Text(
                                "Room Size : 27 m^2",
                                style: TextStyle(
                                  fontFamily: 'Manrope_Medium',
                                    color: greytxtcolor),
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Text(
                                "Room for 2 - 3 People",
                                style: TextStyle(
                                    fontFamily: 'Manrope_Medium',
                                    color: greytxtcolor),
                              ),
                              SizedBox(
                                height: 5.0,
                              ),
                              Row(
                                children: [
                                  CircleAvatar(
                                    backgroundColor: Colors.white,
                                    child: Image.asset(
                                      "assets/icons/ic_wifi.png",
                                      height: 20.0,
                                      width: 20.0,
                                      color: Colors.purple,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 15.0,
                                  ),
                                  CircleAvatar(
                                    backgroundColor: Colors.white,
                                    child: Image.asset(
                                      "assets/icons/ic_breakfasst.png",
                                      height: 20.0,
                                      width: 20.0,
                                      color: Colors.purple,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 15.0,
                                  ),
                                  CircleAvatar(
                                    backgroundColor: Colors.white,
                                    child: Image.asset(
                                      "assets/icons/ic_swiming.png",
                                      height: 20.0,
                                      width: 20.0,
                                      color: Colors.purple,
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              Text(
                                "\$ " + widget.discPrise,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'Manrope-SemiBold',
                                  fontSize: 20.0
                                ),
                              ),
                              Text(
                                "Per Night",
                                style: TextStyle(color: greytxtcolor),
                              ),
                            ],
                          ),

                          /////

                          Image.network(
                              roomImgURL + widget.roomImg,
                              height: 108.0,
                              width: 108.0,
                              fit: BoxFit.fill,),
                        ],
                      ),
                      SizedBox(
                        height: 25.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [],
                          ),
                          // ElevatedButton(
                          //   onPressed: () {
                          //     Navigator.push(context,
                          //         MaterialPageRoute(builder: (_) => HottelDetailsScreen()));
                          //   },
                          //   child: Text(
                          //     "Book Now",
                          //     style: TextStyle(color: Colors.black),
                          //   ),
                          //   style: ElevatedButton.styleFrom(
                          //     shape: RoundedRectangleBorder(
                          //       borderRadius: BorderRadius.circular(5.0),
                          //     ),
                          //     fixedSize: Size(108.0, 40.0),
                          //     primary: Color(0xFFFFC71E),
                          //   ),
                          // ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                Divider(
                  height: 1,
                  thickness: 1.0,
                  color: greytxtcolor,
                ),
                Container(
                  padding: EdgeInsets.all(20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Details",
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                      Row(
                        // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Check-In Date",
                                style: TextStyle(
                                  fontFamily: 'Raleway-Regular',
                                  color: greytxtcolor,
                                ),
                              ),
                              SizedBox(
                                height: 8.0,
                              ),
                              Text(
                                widget.checkInDate,
                                style: kTxtStyle.copyWith(
                                    fontFamily: 'Manrope_Medium',
                                    fontSize: 16),
                              ),
                            ],
                          ),
                          SizedBox(
                            width: 130.0,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Check-Out Date",
                                style: TextStyle(
                                    fontFamily: 'Raleway-Regular',
                                    color: greytxtcolor),
                              ),
                              SizedBox(
                                height: 8.0,
                              ),
                              Text(
                                widget.checkOutDate,
                                style: kTxtStyle.copyWith(
                                    fontSize: 16,
                                    fontFamily: 'Manrope_Medium'),
                              ),
                            ],
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Total Guest & Rooms",
                            style: TextStyle(
                                fontFamily: 'Raleway-Regular',
                                color: greytxtcolor),
                          ),
                          SizedBox(
                            height: 8.0,
                          ),
                          Text(
                            widget.totalGuest +
                                " Guest & " +
                                widget.totalRooms +
                                " Room(s)",
                            style: kTxtStyle.copyWith(
                                fontSize: 16, fontFamily: 'Manrope_Medium'),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
