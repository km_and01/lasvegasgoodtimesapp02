// To parse this JSON data, do
//
//     final cityListModel = cityListModelFromJson(jsonString);

import 'dart:convert';

CityListModel cityListModelFromJson(String str) => CityListModel.fromJson(json.decode(str));

String cityListModelToJson(CityListModel data) => json.encode(data.toJson());

class CityListModel {
    CityListModel({
        this.message,
        this.statusCode,
        required this.data,
    });

    String? message;
    int? statusCode;
    List<Datum>? data;

    factory CityListModel.fromJson(Map<String, dynamic> json) => CityListModel(
        message: json["message"],
        statusCode: json["status_code"],
        data: List<Datum>.from(json["data"].map((x) => Datum?.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "message": message,
        "status_code": statusCode,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
    };
}

class Datum {
    Datum({
        required this.id,
        required this.name,
        required this.stateId,
        required this.city,
    });

    int? id;
    String ?name;
    int? stateId;
    City? city;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        name: json["name"],
        stateId: json["state_id"],
        city:json["city"] == null ? null : City?.fromJson(json["city"]),// City?.fromJson(json["city"]),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "state_id": stateId,
        "city": city!.toJson(),
    };
}

class City {
    City({
        required this.id,
        required this.name,
        required this.countryId,
    });

    int? id;
    String? name;
    int? countryId;

    factory City.fromJson(Map<String, dynamic> json) => City(
        id: json["id"],
        name: json["name"],
        countryId: json["country_id"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "country_id": countryId,
    };
}
