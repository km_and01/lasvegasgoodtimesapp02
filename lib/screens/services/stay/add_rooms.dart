import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/screens/services/stay/rooms.dart';

typedef OnDeleteRoom();

class AddRooms extends StatefulWidget {
  // final Rooms rooms;
  final int index;

  final OnDeleteRoom onDeleteRoom;

  final addRoomsState = _AddRoomsState();
  AddRooms( this.index, this.onDeleteRoom,
   );
  @override
  State<AddRooms> createState() => addRoomsState;
}

class _AddRoomsState extends State<AddRooms> {
  final adults = ['1', '2'];
  final childrn = ['0', '1', '2'];
  String? ano;
  String? cno;
  List<Rooms> roomslist = [];
  int aGust = 0;
  int cGust = 0;

  int tGust = 0;

  List<Widget> adds = [];
  List<int> totalGuest = [];
  num totalCandidate = 0;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 5, 0, 10),
      padding: EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        // color: servicecardfillcolor,

        border: Border.all(color: txtborderbluecolor),
        borderRadius: BorderRadius.all(Radius.circular(15.0)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Room " + (widget.index + 1).toString(),
                textAlign: TextAlign.left,
                style: kTxtStyle,
              ),
              // ElevatedButton(
              //   onPressed: () {
              //     setState(() {
              //       adds.removeLast();
              //       ano = '';
              //       cno = '';
              //     });

              //     // count = count! + 1;
              //     // index = count;
              //   },
              //   style: ElevatedButton.styleFrom(
              //     shape: RoundedRectangleBorder(
              //       borderRadius: BorderRadius.circular(13.0),
              //     ),
              //     // fixedSize: Size(117.0, 30.0),
              //     primary: Color(0xFFFFC71E),
              //   ),
              //   child:

              IconButton(
                onPressed: 
                // () {
                  widget.onDeleteRoom,
                  // setState(() {
                  //   adds.removeAt(widget.index);
                  //   ano = '';
                  //   cno = '';
                  // });

                  // count = count! + 1;
                  // index = count;
                // },
                icon: Icon(
                  Icons.delete_outline_outlined,
                  color: btngoldcolor,
                ),
              ),

              //  Text(
              //   "Delete Room",
              //   style:
              //       kTxtStyle.copyWith(color: Colors.black),
              // ),
              // ),
            ],
          ),
          SizedBox(
            height: 10.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text(
                        "Adults \n(above 12 years)",
                        style: TextStyle(
                          color: greytxtcolor,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    height: 46.0,
                    width: 166.0,
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        // alignment: Alignment.center,
                        dropdownColor: servicecardfillcolor,
                        icon: Icon(
                          Icons.arrow_drop_down,
                          color: Colors.white,
                        ),
                        style: kTxtStyle.copyWith(fontSize: 16.0),
                        isExpanded: true,
                        value: ano,
                        items: adults.map(buildMenuItem).toList(),
                        onChanged: (value) => setState(() {
                          ano = value;
                          aGust = int.parse(ano!);
                          // print(ano);
                          // print(tGust);
                        }),
                      ),
                    ),
                    //  Text(
                    //   '2',
                    //   textAlign: TextAlign.center,
                    //   style: kTxtStyle.copyWith(
                    //       fontSize: 16.0),
                    // ),
                    padding:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                    decoration: BoxDecoration(
                      color: servicecardfillcolor,
                      border: Border.all(color: txtborderbluecolor),
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                  ),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      // Image.asset(
                      //   "assets/icons/ic_calendar.png",
                      //   height: 20.0,
                      //   width: 20.0,
                      // ),
                      // SizedBox(
                      //   width: 15.0,
                      // ),
                      Text(
                        "Children\n(1 - 12 years)",
                        style: TextStyle(
                          color: greytxtcolor,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    alignment: Alignment.center,
                    height: 46.0,
                    width: 166.0,
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        // alignment: Alignment.center,
                        dropdownColor: servicecardfillcolor,
                        icon: Icon(
                          Icons.arrow_drop_down,
                          color: Colors.white,
                        ),
                        style: kTxtStyle.copyWith(fontSize: 16.0),
                        isExpanded: true,
                        value: cno,
                        items: childrn.map(buildMenuItem).toList(),
                        onChanged: (value) => setState(() {
                          cno = value;
                          cGust = int.parse(cno!);
                        }),
                      ),
                    ),
                    padding:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                    decoration: BoxDecoration(
                      color: servicecardfillcolor,
                      border: Border.all(color: txtborderbluecolor),
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                  ),
                ],
              )
            ],
          ),
          SizedBox(
            height: 30.0,
          ),
          Row(
            children: [
              SizedBox(
                width: 15.0,
              ),
              Text(
                "Total Guests",
                style: TextStyle(
                  color: greytxtcolor,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10.0,
          ),
          Container(
            width: 430,
            child: Text(
              (tGust = aGust + cGust).toString() + "  Guest",

              // TotalGuest(this.ano, this.cno),
              // tGust.toString(),
              style: kTxtStyle.copyWith(fontSize: 16.0),
            ),
            padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
            decoration: BoxDecoration(
              color: servicecardfillcolor,
              border: Border.all(color: txtborderbluecolor),
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
          )
        ],
      ),
    );
  }

    DropdownMenuItem<String> buildMenuItem(String item) =>
      DropdownMenuItem(value: item, child: Text(item));
}
