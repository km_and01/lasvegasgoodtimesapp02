import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/eventSpaceAPI/event_space_api.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/custom_event_space_vendor_card.dart';
import 'package:lasvegas_gts_app/customs/custom_eventspace_package_card.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'package:lasvegas_gts_app/Networking/eventSpaceAPI/event_space_package_list_model.dart';

bool isPkgESLoaging = false;
List<EventSpacePackageData> ESPackageDat = [];

class EventSpacePackageListingScr extends StatefulWidget {
  final String eventSpsvendorID, eventSpcService;
  final String vendor;
  const EventSpacePackageListingScr({
    Key? key,
    required this.eventSpsvendorID,
    required this.eventSpcService,
    required this.vendor
  }) : super(key: key);

  @override
  _EventSpacePackageListingScrState createState() =>
      _EventSpacePackageListingScrState();
}

class _EventSpacePackageListingScrState
    extends State<EventSpacePackageListingScr> {
  @override
  void initState() {
    fetchEventPackageData();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print(widget.eventSpcService + " " + widget.eventSpsvendorID);
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
            lead: Image.asset(
              "assets/icons/ic_event_space.png",
              color: Colors.white,
            ),
            title: Text(
              /*"Event Space",*/
              widget.vendor == null ? 'Event Space' : widget.vendor,
              style: TextStyle(
                  fontSize: 20.0,
                  fontFamily: 'Helvetica',
                  color: Colors.white
              ),
            )),
        backgroundColor: bgcolor,
        body: isPkgESLoaging
            ? CustomLoadingScr()
            : SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 20.0,
                      ),
                      Text(
                        "Our Packages",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                            fontFamily: 'Raleway-Bold'),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      ListView.builder(
                          physics: NeverScrollableScrollPhysics(
                              parent: AlwaysScrollableScrollPhysics()),
                          shrinkWrap: true,
                          itemCount: ESPackageDat.length,
                          itemBuilder: (_, index) {
                            return CustomEventSpacePackageCard(
                              packageIMG: ESPackageDat[index].logo!,
                              packageName: ESPackageDat[index].name!,
                              packagePrice: ESPackageDat[index].price!,
                              priceUnit: ESPackageDat[index].priceUnit!,
                              packageDiscPrice: ESPackageDat[index].disPrice!,
                              capacity: ESPackageDat[index].noOfPeopleAllowed!,
                              add1: ESPackageDat[index]
                                  .bussinessDetails!
                                  .addressLine1!,
                              add2: ESPackageDat[index]
                                  .bussinessDetails!
                                  .addressLine2!,
                              pkgIMGList: ESPackageDat[index].photos!,
                              shortDiscp: ESPackageDat[index].shortDescription!,
                              amaities: ESPackageDat[index].amenities!, 
                            
                              pkgService: ESPackageDat[index].eventSpaceServiceId!, 
                              possibleEvents: ESPackageDat[index].eventToDoPossbly!, 
                              pkgVendorID: ESPackageDat[index].vendorId!,
                            );
                          }),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Future fetchEventPackageData() async {
    setState(() {
      isPkgESLoaging = true;
    });
    try {
      ESPackageDat = await EventSpaceAPIs().getEventSpsPackageData(
          widget.eventSpsvendorID, widget.eventSpcService);
      print("PKG ES DATA =====" + ESPackageDat[0].name!);
    } catch (e) {
      print("Error DATA =====" + e.toString());
    }

    setState(() {
      isPkgESLoaging = false;
    });
  }
}
