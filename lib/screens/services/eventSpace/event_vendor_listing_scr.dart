import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/eventSpaceAPI/event_space_api.dart';
import 'package:lasvegas_gts_app/Networking/eventSpaceAPI/event_space_vendor_list_model.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/custom_event_space_vendor_card.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';

bool isESVLLoading = false;
String selectedDateES = '';

List<ServiceEventSpace> ESVendorData = [];

class EventSpaceVendorListingScr extends StatefulWidget {
  final String eventSpsDate, eventSpcService, eventSpsCity;
  const EventSpaceVendorListingScr(
      {Key? key,
      required this.eventSpsDate,
      required this.eventSpcService,
      required this.eventSpsCity})
      : super(key: key);

  @override
  _EventSpaceVendorListingScrState createState() =>
      _EventSpaceVendorListingScrState();
}

class _EventSpaceVendorListingScrState
    extends State<EventSpaceVendorListingScr> {
  @override
  void initState() {
    selectedDateES = widget.eventSpsDate;
    fetchEventVendorData();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print(widget.eventSpcService +
        " " +
        widget.eventSpsDate +
        " " +
        widget.eventSpsCity);
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
            lead: Image.asset(
              "assets/icons/ic_event_space.png",
              color: Colors.white,
            ),
            title: Text(
              "Event Space",
              style: TextStyle(
                  fontSize: 20.0,
                  fontFamily: 'Helvetica',
                  color: Colors.white
              ),
            )),
        backgroundColor: bgcolor,
        body: isESVLLoading
            ? CustomLoadingScr()
            : SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 30.0,
                      ),
                      TextFormField(
                        decoration: kSearchTxtFieldDecoration.copyWith(
                          fillColor: servicecardfillcolor,
                          filled: true,
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      ListView.builder(
                          physics: NeverScrollableScrollPhysics(
                              parent: AlwaysScrollableScrollPhysics()),
                          shrinkWrap: true,
                          itemCount: ESVendorData.length,
                          itemBuilder: (_, index) {
                            return CustomEventSpaceVendorCard(
                              shortDisc: ESVendorData[index].shortDescription!,
                              vendorID: ESVendorData[index].vendorId!,
                              sDate: sDate,
                              sType: sType,
                              sCity: sCity,
                              cateIMG: ESVendorData[index]
                                  .bussinessDetails!
                                  .bussinessLogo!,
                              bname: ESVendorData[index]
                                  .bussinessDetails!
                                  .bussinessName!,
                              price: ESVendorData[index].disPrice!,
                              eventspsService:widget.eventSpcService,
                              priceUnit: ESVendorData[index].priceUnit!,
                              Disc: ESVendorData[index].bussinessDetails!.shortDescription!,
                            );
                          }),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Future fetchEventVendorData() async {
    setState(() {
      isESVLLoading = true;
    });
    try {
      ESVendorData = await EventSpaceAPIs().getEventSpsVendorData(
          widget.eventSpsDate, widget.eventSpcService, widget.eventSpsCity);
    } catch (e) {
      print("Error DATA =====" + e.toString());
    }
    setState(() {
      isESVLLoading = false;
    });
  }
}
