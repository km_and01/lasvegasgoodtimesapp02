import 'package:autocomplete_textfield_ns/autocomplete_textfield_ns.dart';
import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/StayAPI/stay_search_specil_suits_api.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'dart:ui' as ui;

import 'package:lasvegas_gts_app/screens/home/home_scr.dart';
import 'package:lasvegas_gts_app/screens/services/eventSpace/event_vendor_listing_scr.dart';
import 'package:lasvegas_gts_app/screens/services/stay/city_list_model.dart';

String selectedDate = '';
bool isLoadingcity = false;
String eventServiceSeleted = '';
String selectedCityES = '';

bool isPvtCoopSpc = false, isWeddSpc = false, isBanquet = false, isHalls = false;

class EventSpaceHome extends StatefulWidget {
  const EventSpaceHome({Key? key}) : super(key: key);

  @override
  _EventSpaceHomeState createState() => _EventSpaceHomeState();
}

class _EventSpaceHomeState extends State<EventSpaceHome> {
  List<String> cityList = [];
  List<String> cityCodeList = [];
  String cityCode = '';
  String citytSelected = '';
  TextEditingController cityController = TextEditingController();
  GlobalKey<AutoCompleteTextFieldState<String>> cityKey = GlobalKey();

  DateTime dateTime = DateTime.now();
  TimeOfDay time = TimeOfDay.now();

  @override
  void initState() {

    fetchCityList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
            lead: Image.asset(
              "assets/icons/ic_event_space.png",
              color: Colors.white,
            ),
            title: Text(
              "Event Space",
              style: TextStyle(
                  fontSize: 20.0,
                  fontFamily: 'Helvetica',
                  color: Colors.white
              ),
            )),
        backgroundColor: bgcolor,
        bottomNavigationBar: GestureDetector(
          onTap: () async {
            setState(() {
              print(selectedDate);
              print(cityCode);
              print(eventServiceSeleted);
              selectedCityES = citytSelected;
                print('EVENT SPACE CITY SELECTED ========'+ selectedCityES);
              //   print(cityCode + citytSelected);
              //   isCatering ?
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (_) => EventSpaceVendorListingScr(
                        eventSpsDate: selectedDate,
                        eventSpcService: eventServiceSeleted,
                        eventSpsCity: cityCode)),
              );
              //       :
              //   Navigator.push(
              //       context, MaterialPageRoute(builder: (_) =>
              //       ChefList(
              //         noPerson:nooftableControler.text,
              //         sCity: cityCode,
              //         sDate: selectedDate,
              //         sType: selectedType,
              //         )
              //       ));
            });
          },
          child: Container(
            alignment: Alignment.center,
            child: Text(
              "SEARCH NOW",
              style: TextStyle(
                  fontFamily: 'Helvetica',
                  color: Color(0xff121A31),
                  fontSize: 18.0,
                  letterSpacing: 0.7),
            ),
            height: 57.0,
            color: btngoldcolor,
          ),
        ),
        body: isLoadingcity
            ? CustomLoadingScr()
            : SingleChildScrollView(
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top:25.0, left: 20.0),
                        child: Text(
                          "Check Availability",
                          style: TextStyle(
                            fontSize: 20.0,
                            fontFamily: 'Raleway-Bold',
                            color: Colors.white,

                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                      SingleChildScrollView(
                        padding: EdgeInsets.all(2),
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            /////Private & Coop
                            SizedBox(width: 15.0,),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      isPvtCoopSpc == true
                                          ? isPvtCoopSpc = false
                                          : isPvtCoopSpc = true;
                                      isBanquet = false;
                                      isHalls = false;
                                      isWeddSpc = false;
                                      eventServiceSeleted = 'Private and Corporate Space';
                                    });
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: Image.asset(
                                      "assets/icons/ic_pvt_coop_eventspace.png",
                                      height: 30.0,
                                      width: 30.0,
                                    ),
                                    decoration: BoxDecoration(
                                      color: isPvtCoopSpc
                                          ? Colors.white
                                          : servicecardfillcolor,
                                      borderRadius: BorderRadius.circular(40),
                                      boxShadow: const [
                                        BoxShadow(
                                            color: txtborderbluecolor,
                                            spreadRadius: 1.5),
                                      ],
                                    ),
                                    height: 69.0,
                                    width: 69.0,

                                    //  kTxtFieldDecoration .copyWith(),
                                  ),
                                ),
                                SizedBox(
                                  height: 10.0,
                                ),
                                Text(
                                  "Private &\nCorporate",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontFamily: 'Raleway',
                                    color: Colors.white,
                                    fontSize: 12.0
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              width: 15.0,
                            ),

                            /////isWeddSpc
                            Column(
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      isWeddSpc == true
                                          ? isWeddSpc = false
                                          : isWeddSpc = true;
                                      isBanquet = false;
                                      isHalls = false;
                                      isPvtCoopSpc = false;
                                      eventServiceSeleted = 'Wedding space';
                                    });
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: Image.asset(
                                      "assets/icons/ic_wedd_eventspc.png",
                                      height: 30.0,
                                      width: 30.0,
                                    ),
                                    decoration: BoxDecoration(
                                      color: isWeddSpc
                                          ? Colors.white
                                          : servicecardfillcolor,
                                      borderRadius: BorderRadius.circular(40),
                                      boxShadow: const [
                                        BoxShadow(
                                            color: txtborderbluecolor,
                                            spreadRadius: 1.5),
                                      ],
                                    ),
                                    height: 69.0,
                                    width: 69.0,

                                    //  kTxtFieldDecoration .copyWith(),
                                  ),
                                ),
                                SizedBox(
                                  height: 10.0,
                                ),
                                Text(
                                  "Wedding",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontFamily: 'Raleway',
                                      color: Colors.white,
                                      fontSize: 12.0
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              width: 15.0,
                            ),

                            /////Banquet
                            Column(
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      isBanquet == true
                                          ? isBanquet = false
                                          : isBanquet = true;
                                      isWeddSpc = false;
                                      isHalls = false;
                                      isPvtCoopSpc = false;
                                      eventServiceSeleted = 'Banquet';
                                    });
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: Image.asset(
                                      "assets/icons/ic_banquet_eventspc.png",
                                      height: 30.0,
                                      width: 30.0,
                                    ),
                                    decoration: BoxDecoration(
                                      color: isBanquet
                                          ? Colors.white
                                          : servicecardfillcolor,
                                      borderRadius: BorderRadius.circular(40),
                                      boxShadow: const [
                                        BoxShadow(
                                            color: txtborderbluecolor,
                                            spreadRadius: 1.5),
                                      ],
                                    ),
                                    height: 69.0,
                                    width: 69.0,

                                    //  kTxtFieldDecoration .copyWith(),
                                  ),
                                ),
                                SizedBox(
                                  height: 10.0,
                                ),
                                Text(
                                  "Banquet",
                                  style: TextStyle(
                                      fontFamily: 'Raleway',
                                      color: Colors.white,
                                      fontSize: 12.0
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              width: 15.0,
                            ),

                            /////Halls
                            Column(
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      isHalls == true
                                          ? isHalls = false
                                          : isHalls = true;
                                      isWeddSpc = false;
                                      isBanquet = false;
                                      isPvtCoopSpc = false;
                                      eventServiceSeleted = 'Halls';
                                    });
                                  },
                                  child: Container(
                                    alignment: Alignment.center,
                                    child: Image.asset(
                                      "assets/icons/halls_eventspc.png",
                                      height: 30.0,
                                      width: 30.0,
                                    ),
                                    decoration: BoxDecoration(
                                      color: isHalls
                                          ? Colors.white
                                          : servicecardfillcolor,
                                      borderRadius: BorderRadius.circular(40),
                                      boxShadow: const [
                                        BoxShadow(
                                            color: txtborderbluecolor,
                                            spreadRadius: 1.5),
                                      ],
                                    ),
                                    height: 69.0,
                                    width: 69.0,

                                    //  kTxtFieldDecoration .copyWith(),
                                  ),
                                ),
                                SizedBox(
                                  height: 10.0,
                                ),
                                Text(
                                  "Halls",
                                  style: TextStyle(
                                      fontFamily: 'Raleway',
                                      color: Colors.white,
                                      fontSize: 12.0
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              width: 15.0,
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 25.0,
                      ),

                      ///////Select City
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Image.asset(
                                  "assets/icons/ic_stay.png",
                                  height: 20.0,
                                  width: 20.0,
                                  color: Colors.white,
                                ),
                                SizedBox(
                                  width: 15.0,
                                ),
                                Text(
                                  "Select City",
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    letterSpacing: 0.8,
                                    color: Colors.white,
                                    fontFamily: 'Raleway-Bold'
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Container(
                              decoration: BoxDecoration(
                                color: servicecardfillcolor,
                                border: Border.all(color: txtborderbluecolor),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                              ),
                              padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                              child: Directionality(
                                textDirection: ui.TextDirection.ltr,
                                child: SimpleAutoCompleteTextField(
                                  textSubmitted: (text) {
                                    if (cityList.contains(text)) {
                                      // cityController.text = text;
                                      citytSelected = text;
                                      var cityCodeindex =
                                          cityList.indexOf(citytSelected);
                                      cityCode = cityCodeList[cityCodeindex];
                                      print(cityCodeindex);
                                      print(cityCode);
                                    }
                                  },
                                  textChanged: (text) {
                                    // setState(() {
                                    //   citytSelected = text;
                                    // });
                                    // for (var i = 0; i < cityList.length; i++) {
                                    if (cityList.contains(citytSelected)) {
                                      // var cityCodeindex =
                                      //     cityCodeList.indexOf(citytSelected);
                                      // print(cityCodeindex);
                                      // print(cityCode);
                                    }
                                    // }
                                  },
                                  clearOnSubmit: false,
                                  controller: cityController,
                                  key: cityKey,
                                  suggestions: cityList,
                                  style: kTxtStyle.copyWith(fontSize: 16.0),
                                  decoration: InputDecoration.collapsed(
                                    hintText: "search city",
                                    fillColor: servicecardfillcolor,

                                    border: InputBorder.none,
                                    // focusedBorder:
                                    //     InputBorder.none,
                                    // enabledBorder:
                                    //     InputBorder.none,
                                    // errorBorder:
                                    //     InputBorder.none,
                                    // disabledBorder:
                                    //     InputBorder.none,
                                    hintStyle: kTxtStyle,
                                    //  TextStyle(
                                    //     color: Color(
                                    //         ColorConst
                                    //             .grayColor),
                                    //     fontSize: 11.5.sp),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),


                      Padding(
                        padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Image.asset(
                                  "assets/icons/ic_calendar.png",
                                  height: 20.0,
                                  width: 20.0,
                                  color: Colors.white,
                                ),
                                SizedBox(
                                  width: 10.0,
                                ),
                                Text(
                                  'Select Event Date',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    letterSpacing: 0.8,
                                    color: Colors.white,
                                    fontFamily: 'Raleway-Bold'
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            InkWell(
                              onTap: () async {
                                DateTime? newDate = await showDatePicker(
                                    context: context,
                                    initialDate: dateTime,
                                    firstDate: DateTime(2021),
                                    lastDate: DateTime(2200));
                                if (newDate != null) {
                                  setState(() {
                                    dateTime = newDate;
                                    selectedDate =
                                        '${dateTime.day.toString().padLeft(2, '0')}-${dateTime.month.toString().padLeft(2, '0')}-${dateTime.year}';
                                  });
                                }
                              },
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 10.0),
                                height: 50.0,
                                width: MediaQuery.of(context).size.width,
                                child: Text(
                                  selectedDate,
                                  textAlign: TextAlign.left,
                                  style: kTxtStyle.copyWith(
                                    fontSize: 16.0,
                                    fontFamily: 'Regular',
                                  ),
                                ),
                                decoration: BoxDecoration(
                                  color: servicecardfillcolor,
                                  border: Border.all(color: txtborderbluecolor),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                ),
                              ),
                            ),
                            SizedBox(height: 25.0),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Future fetchCityList() async {
    setState(() {
      isLoadingcity = true;
    });

    print(homeToken);
    String tkn = homeToken;

    List<Datum>? parsedRes = await GetCityList().getCityList(homeToken);
    if (parsedRes != null) {
      for (var i = 0; i < parsedRes.length; i++) {
        if (parsedRes[i].city != null) {
          cityList.add(parsedRes[i].name! + ' - ' + parsedRes[i].city!.name!);
          cityCodeList.add(parsedRes[i].id.toString());
        }
      }
    }
    print(cityList[3]);
    print(cityCodeList[3]);

    setState(() {
      
      isLoadingcity = false;
    });
  }
}
