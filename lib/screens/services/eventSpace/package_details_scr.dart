import 'dart:convert';

import 'package:carousel_images/carousel_images.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lasvegas_gts_app/Networking/StayAPI/amenities_model.dart';
import 'package:lasvegas_gts_app/Networking/StayAPI/stay_search_specil_suits_api.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/constants/image_pager.dart';
import 'package:lasvegas_gts_app/customs/anamitis_card.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';
import 'package:lasvegas_gts_app/screens/services/eventSpace/event_space_booking_scr.dart';
import 'package:lasvegas_gts_app/screens/services/stay/hotel_room_details.dart';
import 'package:http/http.dart' as http;

bool isPkgDisLoading = false;
String pkgIMGListURL = 'http://lasvegas.koolmindapps.com/images/eventspace/';
List<String> listImagesPkg = [];

class PackageDetailsEventSpace extends StatefulWidget {
  final String pkgName,pkgVendorID,pkgIMG,
      priceUnit,
      discPrice,
      pkgDetails,
      address1,
      address2,
      pkgService,
      possibleEvents,
      pkgCapacity;
  final List<String> pkgIMGList, amaities;
  const PackageDetailsEventSpace(
      {Key? key,
      required this.pkgName,
      required this.priceUnit,
      required this.discPrice,
      required this.pkgDetails,
      required this.address1,
      required this.address2,
      required this.pkgIMGList,
      required this.amaities,
      required this.pkgService,
      required this.possibleEvents,
      required this.pkgCapacity, required this.pkgVendorID, required this.pkgIMG})
      : super(key: key);

  @override
  _PackageDetailsEventSpaceState createState() =>
      _PackageDetailsEventSpaceState();
}

class _PackageDetailsEventSpaceState extends State<PackageDetailsEventSpace> {
  @override
  void initState() {
    print("URL ===" + pkgIMGListURL);
    int length = widget.pkgIMGList.length;
    listImagesPkg.clear();
    for (var i = 0; i < widget.pkgIMGList.length; i++) {
      listImagesPkg.add(pkgIMGListURL + widget.pkgIMGList[i]);
    }
    print(listImagesPkg.toString()); // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgcolor,
      /*appBar: AppBar(
        elevation: 0.0,
        leading: BackButton(),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              // 'widget.bName' + " | " +
              widget.pkgName,
              style: TextStyle(
                  fontSize: 20.0,
                  fontFamily: 'Helvetica',
                  color: Colors.white
              ),
            ),
            RatingBar.builder(
              glow: false,
              itemSize: 15.0,
              initialRating: 3,
              minRating: 1,
              direction: Axis.horizontal,
              allowHalfRating: true,
              itemCount: 5,
              //itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
              itemBuilder: (context, _) => Icon(
                Icons.star,
                color: Colors.amber,
                //   Image.asset(
                // "assets/icons/rating_star.png",
                // color: Colors.amber,
              ),
              // ),
              unratedColor: Colors.grey,
              onRatingUpdate: (rating) {
                print(rating);
              },
            ),
          ],
        ),
        bottom: PreferredSize(
          child: Column(
            children: const [
              Divider(
                thickness: 2,
                height: 2,
                color: Colors.amber,
              ),
              SizedBox(
                height: 3.0,
              ),
              DottedLine(
                lineThickness: 4.5,
                dashRadius: 7,
                dashLength: 4.0,
                // lineLength:  MediaQuery.of(context).size.width,
                dashColor: Colors.amber,
              ),
            ],
          ),
          preferredSize: Size.fromHeight(20),
        ),
      ),*/
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(20.0),
        child: ElevatedButton(
          onPressed: () async {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (_) =>
                        EventSpaceBookings(
                          vendorID: widget.pkgVendorID,
                          amount: widget.discPrice, no_of_people_allowed: widget.pkgCapacity,
                          pkgIMG: widget.pkgIMG, pkgService: widget.pkgService,)
                ));
          },
          child: Text(
            'Book Now',
            style: TextStyle(
                fontFamily: 'Helvetica',
                color: Color(0xff121A31),
                fontSize: 16.0,
                letterSpacing: 0.7),
          ),
          style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(13.0),
            ),
            fixedSize: Size(389.0, 57.0),
            primary: Color(0xFFFFC71E),
          ),
        ),
      ),
      body: isPkgDisLoading
          ? CustomLoadingScr()
          : SingleChildScrollView(
              child: Container(
                child: Column(
                  children: [

                    Column(
                      children: [
                        // Image.network(
                        //  imageURL + widget.images,
                        // ),
                        ImagePager(
                          viewportFraction: 1.0,
                          scaleFactor: 1.0,
                          listImages: listImagesPkg,
                          height: 200.0,
                          cachedNetworkImage: true,
                          //verticalAlignment: Alignment.center,
                          onTap: (index) {
                            print('Tapped on page $index');
                          },
                        ),
                      ],
                    ),

                    Container(
                      margin: EdgeInsets.all(15.0),
                      padding: EdgeInsets.all(3.0),
                      alignment: Alignment.topLeft,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            widget.pkgName,
                            style: TextStyle(
                                fontFamily: 'Raleway-Medium',
                                fontSize: 18.0,
                                color: Colors.white
                            ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          RatingBar.builder(
                            glow: false,
                            itemSize: 15.0,
                            initialRating: 3,
                            minRating: 1,
                            direction: Axis.horizontal,
                            allowHalfRating: true,
                            itemCount: 5,
                            //itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                            itemBuilder: (context, _) => Icon(
                              Icons.star,
                              color: Colors.amber,
                              //   Image.asset(
                              // "assets/icons/rating_star.png",
                              // color: Colors.amber,
                            ),
                            // ),
                            unratedColor: Colors.grey,
                            onRatingUpdate: (rating) {
                              print(rating);
                            },
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            widget.address1 + ', ' + widget.address2,
                            style: TextStyle(
                                fontFamily: 'Manrope',
                                color: Colors.white
                            ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),

                          Text(
                            widget.priceUnit +
                                " " +
                                widget.discPrice +
                                "/Day ",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16.0,
                              fontFamily: 'Manrope-SemiBold',
                            ),
                          ),
                        ],
                      ),
                    ),

                    Divider(
                      height: 1.0,
                      thickness: 1.0,
                      color: greytxtcolor,
                    ),
                    SizedBox(
                      height: 25.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20.0, 0, 0, 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // widget.amaities == null || widget.amaities.isEmpty
                          //     ? SizedBox(
                          //         height: 0.0,
                          //       )
                          //     : Text(
                          //         "Main Amenities",
                          //         style: TextStyle(
                          //             fontSize: 18.0,
                          //             fontWeight: FontWeight.bold,
                          //             color: Colors.white),
                          //       ),
                          // widget.amaities == null || widget.amaities.isEmpty
                          //     ? SizedBox(
                          //         height: 0.0,
                          //       )
                          //     : SizedBox(
                          //         height: 15.0,
                          //       ),
                          // widget.amaities == null || widget.amaities.isEmpty
                          //     ? SizedBox(
                          //         height: 0.0,
                          //       )
                          //     : Container(
                          //         height: 120,
                          //         padding: EdgeInsets.all(0),
                          //         child: ListView.builder(
                          //             physics: BouncingScrollPhysics(
                          //                 parent:
                          //                     AlwaysScrollableScrollPhysics()),
                          //             scrollDirection: Axis.horizontal,
                          //             shrinkWrap: true,
                          //             itemCount: amidata.ami.length,
                          //             itemBuilder: (_, index) {
                          //               return widget.amaities.contains(
                          //                       amidata.ami[index].id
                          //                           .toString())
                          //                   ? AmenitiesCard(
                          //                       icon: amidata.ami[index]
                          //                           .amenitiesImage,
                          //                       title: amidata
                          //                           .ami[index].amenitiesName)
                          //                   : SizedBox(
                          //                       height: 0,
                          //                     );
                          //             }),
                          //       ),
                          // widget.amaities == null || widget.amaities.isEmpty
                          //     ? SizedBox(
                          //         height: 0.0,
                          //       )
                          //     : SizedBox(
                          //         height: 30.0,
                          //       ),
                          Text(
                            "Package Description",
                            style: TextStyle(
                                fontSize: 18.0,
                                fontFamily: 'Raleway-SemiBold',
                                color: Colors.white),
                          ),
                          SizedBox(
                            height: 15.0,
                          ),
                          Text(
                            widget.pkgDetails,
                            // """The Hotel Pennsylvania is conveniently located within one block of the Empire State Building,Madison Square Garden, Penn Station and Macy's flagship store. Located within walking distance are Times Square, the Broadway Theatre District, New York Public Library, Jacob K. Javits Convention Center, and two of New York's most iconic…""",
                            style: TextStyle(
                              fontFamily: 'Raleway',
                              fontSize: 14.0,
                              color: greytxtcolor
                            ),
                          ),
                          SizedBox(
                            height: 30.0,
                          ),

                          //////Service Name
                          Text(
                            "Service Name :",
                            style: TextStyle(
                                fontFamily: 'Raleway',
                                fontSize: 14.0,
                                color: greytxtcolor
                            ),
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Text(
                            widget.pkgService,
                            style: TextStyle(
                                fontFamily: 'Raleway',
                                fontSize: 18.0,
                                color: Colors.white
                            ),
                          ),
                          SizedBox(
                            height: 30.0,
                          ),

                          /////Possible Events
                          Text(
                            "Possible Events :",
                            style: TextStyle(
                                fontFamily: 'Raleway',
                                fontSize: 14.0,
                                color: greytxtcolor
                            ),
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Text(
                            widget.possibleEvents,
                            style: TextStyle(
                                fontFamily: 'Raleway',
                                fontSize: 18.0,
                                color: Colors.white
                            ),
                          ),
                          SizedBox(
                            height: 30.0,
                          ),

                          /////Capacity
                          Text(
                            "No. of People Allowed :",
                            style: TextStyle(
                                fontFamily: 'Raleway',
                                fontSize: 14.0,
                                color: greytxtcolor
                            ),
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Text(
                            widget.pkgCapacity,
                            style: TextStyle(
                              fontFamily: 'Raleway',
                              fontSize: 18.0,
                              color: Colors.white
                          ),
                          ),
                          SizedBox(
                            height: 30.0,
                          ),

                          Text(
                            "Address",
                            style: TextStyle(
                                fontSize: 18.0,
                                fontFamily: 'Raleway-SemiBold',
                                color: Colors.white),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            widget.address1 + ' , ' + widget.address2 + ' . ',
                            // """T3131 Las Vegas Boulevard South, 89109, Las Vegas""",
                            style: TextStyle(
                                fontFamily: 'Raleway',
                                fontSize: 14.0,
                                color: greytxtcolor
                            ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          //       Container(
                          //         child:
                          //             Image.asset("assets/images/banner_img_2.png"),
                          //         height: 215.0,
                          //         width: 380.0,
                          //         color: bgcolor,
                          //       ),
                          //       SizedBox(
                          //         height: 40.0,
                          //       ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
    );
  }

  Future getAllAmis() async {
    setState(() {
      isPkgDisLoading = true;
    });
    print(homeToken);
    var amires = await http.post(
      amiURL,
      headers: {
        'API_KEY': 'pASDASfszTddANGLN8989561HKzaXoelFo1Gs',
        'Authorization': "Bearer " + homeToken,
      },
    );

    if (amires.statusCode == 200) {
      print(amires.body);
      amidata = AmenitiesListModel.fromJson(jsonDecode(amires.body));
      print(amidata.ami[0].amenitiesName);
      // return amidata;
    }
    setState(() {
      isPkgDisLoading = false;
    });
  }
}
