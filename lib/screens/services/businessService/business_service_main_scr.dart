import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/CannabiesAPIModel/cannabies_api.dart';
import 'package:lasvegas_gts_app/Networking/CannabiesAPIModel/cannabies_vendor_model.dart';
import 'package:lasvegas_gts_app/Networking/FashionAPI/fashon_vendor_model.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar_with_cart.dart';
import 'package:lasvegas_gts_app/customs/custom_cannabies_vendor_card.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';


List<CannabiesV> CannabiesVendorData = [];

class BusinessServiceHome extends StatefulWidget {
  const BusinessServiceHome({ Key? key }) : super(key: key);

  @override
  _BusinessServiceHomeState createState() => _BusinessServiceHomeState();
}

class _BusinessServiceHomeState extends State<BusinessServiceHome> {

  bool is_loading = false;

  @override
  void initState() {
    fetchcannabiesVendorData();
    // TODO: implement initState
    super.initState();
  }

  Future fetchcannabiesVendorData() async{
    setState(() {
      is_loading = true;
    });

    try {
      CannabiesVendorData = await CannabiesAPIs().getCannabiesVendorData();
      // RentalsAPIs().getRentalVendorData(widget.rentalCityCode, widget.rentalDate, widget.rentalCarName);
    } catch (e) {
      print("Error DATA =====" + e.toString());
    }

    setState(() {
      is_loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBarCart(
          lead: Image.asset(
            "assets/icons/ic_business_services.png",
            color: Colors.white,
          ),
          title: Text(
            "Cannabies Service",
            style: TextStyle(
                fontSize: 20.0,
                fontFamily: 'Helvetica',
                color: Colors.white
            ),
          ),),
        backgroundColor: bgcolor,
        body: is_loading
            ? CustomLoadingScr()
            : SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(10),
            child: Column(
              children: [
                SizedBox(
                  height: 30.0,
                ),
                TextFormField(
                  decoration: kSearchTxtFieldDecoration.copyWith(
                    fillColor: servicecardfillcolor,
                    filled: true,
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                ListView.builder(
                    physics: NeverScrollableScrollPhysics(
                        parent: AlwaysScrollableScrollPhysics()),
                    shrinkWrap: true,
                    itemCount: CannabiesVendorData.length,
                    itemBuilder: (_, index) {

                      return CustomCanneabisVendorCard(
                        shortDisc: CannabiesVendorData[index]
                            .bussinessDetails!
                            .shortDescription!,
                        vendorID: CannabiesVendorData[index].vendorId!,
                        CannabisVendorLogo: CannabiesVendorData[index]
                            .bussinessDetails!
                            .bussinessLogo!,
                        bname: CannabiesVendorData[index]
                            .bussinessDetails!
                            .bussinessName!,
                        price: CannabiesVendorData[index].productPrice!,
                        priceUnit: CannabiesVendorData[index].priceUnit!,
                        CannabisVendorIMG: CannabiesVendorData[index].bussinessDetails!.imagesk!,
                        add1: CannabiesVendorData[index].bussinessDetails!.addressLine1!,
                        add2: CannabiesVendorData[index].bussinessDetails!.addressLine2!,
                        vendrDetail: CannabiesVendorData[index].bussinessDetails!.shortDescription!,
                        photoVendorIMG: CannabiesVendorData[index].photos!,);
                    }),
              ],
            ),
          ),
        ),
      ),
    );
  }
}