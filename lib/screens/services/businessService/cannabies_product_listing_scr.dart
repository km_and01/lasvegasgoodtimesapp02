import 'package:carousel_images/carousel_images.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lasvegas_gts_app/Networking/CannabiesAPIModel/cannabies_api.dart';
import 'package:lasvegas_gts_app/Networking/CannabiesAPIModel/cannabies_product_model.dart';
import 'package:lasvegas_gts_app/Networking/FashionAPI/fashion_api.dart';
import 'package:lasvegas_gts_app/Networking/FashionAPI/fashion_product_model.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar_with_cart.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
// import 'package:carousel_pro/carousel_pro.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:lasvegas_gts_app/screens/services/fashion/customize_now_scr.dart';
import 'package:lasvegas_gts_app/screens/services/fashion/fashion_booking_scr.dart';

bool isProductLoading = false;
bool addingtoCart = false;
String pkgIMGListURL =
    'http://lasvegas.koolmindapps.com/images/business_detail/';
String productImgURL =
    'http://koolmindapps.com/lasvegas/public/images/cannabies/';
List<String> listImagesPkg = [];
List<CannabiesProducts> cannabiesProducts = [];

class CannabiesProductListingScr extends StatefulWidget {
  final String fashionVendorID, bname, add1,add2,vendrDetail;
  final List pkgIMGList;
  final List photoVendorIMG;

  const CannabiesProductListingScr({
    Key? key,
    required this.fashionVendorID,
    required this.bname,
    required this.pkgIMGList,
    required this.photoVendorIMG,
    required this.add1, required this.add2, required this.vendrDetail,
  }) : super(key: key);

  @override
  _CannabiesProductListingScrState createState() =>
      _CannabiesProductListingScrState();
}

class _CannabiesProductListingScrState extends State<CannabiesProductListingScr> {
  //  String bName, address1, address2, pName, pPrice, priceUnit;
  @override
  void initState() {
    int length = widget.pkgIMGList.length;
    listImagesPkg.clear();
    for (var i = 0; i < widget.photoVendorIMG.length; i++) {
      listImagesPkg.add(pkgIMGListURL + widget.photoVendorIMG[i]);
    }
    print("URL ===" + listImagesPkg.toString());
    print(listImagesPkg.toString()); // TODO: implement initState
    fetchCannabiesProducts();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: bgcolor,
        appBar:  CustomAppBarCart(
          lead: Image.asset(
            "assets/icons/ic_business_services.png",
            color: Colors.white,
          ),
          title: Text(
            "Cannabies Service",
            style: TextStyle(
                fontSize: 20.0,
                fontFamily: 'Helvetica',
                color: Colors.white
            ),
          ),),
        /*CustomAppBar(
            lead: Image.asset(
              "assets/icons/ic_fasion_retail.png",
              color: Colors.white,
            ),
            title: Text(
              "Fashion/Retail",
              style: kHeadText,
            )),*/
        // AppBar(
        //   elevation: 0.0,
        //   leading: BackButton(),
        //   title: Column(
        //     crossAxisAlignment: CrossAxisAlignment.start,
        //     children: [
        //       Text(
        //         widget.bname,
        //         // 'name',
        //         // widget.pkgName,
        //         style: kHeadText.copyWith(fontSize: 16),
        //       ),
        //       RatingBar.builder(
        //         glow: false,
        //         itemSize: 20.0,
        //         initialRating: 3,
        //         minRating: 1,
        //         direction: Axis.horizontal,
        //         allowHalfRating: true,
        //         itemCount: 5,
        //         itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
        //         itemBuilder: (context, _) => Icon(
        //           Icons.star,
        //           color: Colors.amber,
        //           //   Image.asset(
        //           // "assets/icons/rating_star.png",
        //           // color: Colors.amber,
        //         ),
        //         // ),
        //         unratedColor: Colors.grey,
        //         onRatingUpdate: (rating) {
        //           print(rating);
        //         },
        //       ),
        //     ],
        //   ),
        //   bottom: PreferredSize(
        //     child: Column(
        //       children: const [
        //         Divider(
        //           thickness: 2,
        //           height: 2,
        //           color: Colors.amber,
        //         ),
        //         SizedBox(
        //           height: 3.0,
        //         ),
        //         DottedLine(
        //           lineThickness: 4.5,
        //           dashRadius: 7,
        //           dashLength: 4.0,
        //           // lineLength:  MediaQuery.of(context).size.width,
        //           dashColor: Colors.amber,
        //         ),
        //       ],
        //     ),
        //     preferredSize: Size.fromHeight(20),
        //   ),
        // ),

        body: isProductLoading
            ? CustomLoadingScr()
            : SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.all(0.01),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 10.0,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment:
                          MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment:
                              CrossAxisAlignment.start,
                              children: [
                                Text(
                                  widget.bname,
                                  // 'name',
                                  // widget.pkgName,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18.0,
                                      fontFamily: 'Raleway-Medium'),
                                ),
                                SizedBox(height: 5),
                                Text(
                                  widget.add1,
                                  // 'name',
                                  // widget.pkgName,
                                  style: TextStyle(
                                      color: greytxtcolor,
                                      fontSize: 13.0,
                                      fontFamily: 'Manrope'),
                                ),
                              ],
                            ),
                            RatingBar.builder(
                              glow: false,
                              itemSize: 15.0,
                              initialRating: 3,
                              minRating: 1,
                              direction: Axis.horizontal,
                              allowHalfRating: true,
                              itemCount: 5,
                              itemPadding:
                              EdgeInsets.symmetric(horizontal: 2.0),
                              itemBuilder: (context, _) => Icon(
                                Icons.star,
                                color: Colors.amber,
                                //   Image.asset(
                                // "assets/icons/rating_star.png",
                                // color: Colors.amber,
                              ),
                              // ),
                              unratedColor: Colors.grey,
                              onRatingUpdate: (rating) {
                                print(rating);
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                    // Image.network(
                    //  imageURL + widget.images,
                    // ),
                    //             Carousel(
                    //   images: listImagesPkg,
                    //   autoplay: true,
                    //   boxFit: BoxFit.fitWidth,
                    //   dotBgColor: Colors.transparent,
                    //   dotSize: 3,
                    //   dotColor: Colors.red,
                    //   dotIncreasedColor: Colors.red,
                    //   autoplayDuration: Duration(seconds: 3),
                    //   animationCurve: Curves.fastOutSlowIn,
                    // ),

                    SizedBox(height: 20),
                    CarouselSlider(
                      items: listImagesPkg
                          .map(
                            (x) => Container(
                          width: double.infinity,
                          decoration: BoxDecoration(
                            //  color: Colors.yellow,
                            borderRadius: BorderRadius.circular(8.0),
                            image: DecorationImage(
                              fit: BoxFit.fill,
                              image: NetworkImage(x, scale: 1),
                            ),
                          ),
                        ),
                      )
                          .toList(),
                      // autoPlay: true,
                      // height: 200.0,
                      options: CarouselOptions(
                        height: 200,
                        aspectRatio: 16 / 9,
                        viewportFraction: 0.9,
                        initialPage: 0,
                        enableInfiniteScroll: true,
                        reverse: false,
                        autoPlay: true,
                        autoPlayInterval: Duration(seconds: 3),
                        autoPlayAnimationDuration:
                        Duration(milliseconds: 800),
                        autoPlayCurve: Curves.fastOutSlowIn,
                        enlargeCenterPage: true,
                        // onPageChanged: callbackFunction,
                        scrollDirection: Axis.horizontal,
                      ),
                    ),

                    // CarouselImages(
                    //   viewportFraction: 0.9,
                    //   scaleFactor: 0.1,
                    //   listImages: listImagesPkg,
                    //   height: 250.0,
                    //   borderRadius: 8.0,
                    //   cachedNetworkImage: true,
                    //   verticalAlignment: Alignment.center,
                    //   onTap: (index) {
                    //     print('Tapped on page $index');
                    //   },
                    // ),
                    SizedBox(
                      height: 30.0,
                    ),
                  ],
                ),
                // Divider(
                //   height: 1.0,
                //   thickness: 1.0,
                //   color: greytxtcolor,
                // ),
                // SizedBox(
                //   height: 25.0,
                // ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    //////Service Name
                    Text(
                      "Our Products",
                      style: TextStyle(
                          fontSize: 20.0,
                          fontFamily: 'Helvetica',
                          color: Colors.white
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    GridView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        gridDelegate:
                        SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            crossAxisSpacing: 15.0,
                            mainAxisExtent: 200,
                            mainAxisSpacing: 15.0),
                        itemCount: cannabiesProducts
                            .length, // ServiceGrid.serviceList.length,
                        itemBuilder: (_, index) {
                          return Container(
                            // padding: EdgeInsets.all(5),
                            height: 145,
                            //width: 80,

                            decoration: BoxDecoration(
                              border:
                              Border.all(color: txtborderbluecolor),
                              color: servicecardfillcolor,
                              borderRadius: BorderRadius.all(
                                  Radius.circular(15.0)),
                            ),

                            child: Column(
                              // mainAxisAlignment:
                              //     MainAxisAlignment.spaceBetween,
                              children: [
                                // Image.asset(
                                //     "assets/images/banner_img_2.png"),
                                ClipRRect(
                                  child: Image.network(
                                    productImgURL +
                                        cannabiesProducts[index]
                                            .photos![0],
                                    fit: BoxFit.fill,
                                    height: 100,
                                    width: 185,
                                  ),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(15.0),
                                    topRight: Radius.circular(15.0),
                                  ),
                                ),
                                Padding(
                                  padding:
                                  const EdgeInsets.only(left: 5),
                                  child: Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    children: [
                                      SizedBox(
                                        width: 120,
                                        child: Text(
                                          cannabiesProducts[index]
                                              .productName!,
                                          // "Product Name"+ index.toString(),
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16.0,
                                              fontFamily: 'Raleway-Medium'),
                                        ),
                                      ),
                                      IconButton(
                                        splashColor: Colors.white,
                                        iconSize: 24,
                                        splashRadius: 10,
                                        padding: EdgeInsets.all(0),
                                        onPressed: () {
                                          insertProductinCart(cannabiesProducts[index].id.toString(),
                                              cannabiesProducts[index].productPrice!);
                                          //  if (addingtoCart) {
                                          // addingtoCart ?
                                          // showDialog(
                                          //     context: context,
                                          //     barrierDismissible: false,
                                          //     builder:
                                          //         (BuildContext context) {
                                          //       return Dialog(
                                          //         backgroundColor:
                                          //             Colors.transparent,
                                          //         insetPadding: EdgeInsets.only(
                                          //             left: MediaQuery.of(
                                          //                         context)
                                          //                     .size
                                          //                     .width *
                                          //                 0.44,
                                          //             right: MediaQuery.of(
                                          //                         context)
                                          //                     .size
                                          //                     .width *
                                          //                 0.44),
                                          //         child: Container(
                                          //           color: Colors
                                          //               .transparent,
                                          //           width:
                                          //               double.infinity,
                                          //           height: 50,
                                          //           child: Column(
                                          //             mainAxisAlignment:
                                          //                 MainAxisAlignment
                                          //                     .center,
                                          //             children: [
                                          //               /*new CircularProgressIndicator(color: const Color(0xff303e9f),),*/
                                          //               // new CircularProgressIndicator(),
                                          //               // ignore: unnecessary_new
                                          //               new CircularProgressIndicator(
                                          //                 backgroundColor:
                                          //                     Colors
                                          //                         .transparent,
                                          //                 color:
                                          //                     btngoldcolor,
                                          //               ),
                                          //             ],
                                          //           ),
                                          //         ),
                                          //       );
                                          //     },
                                          //   )
                                          //  :
                                          //  }
                                          print(
                                              "Add to cart of INDEX =====" +
                                                  index.toString());
                                        },
                                        icon: Icon(Icons
                                            .add_shopping_cart_rounded),
                                        color: Colors.white,
                                        tooltip: "Add to Cart",
                                      ),
                                    ],
                                  ),
                                ),

                                /*Padding(
                                  padding: const EdgeInsets.only(
                                      left: 5, right: 5),
                                  child: Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        cannabiesProducts[index]
                                            .priceUnit! +
                                            cannabiesProducts[index]
                                                .productPrice!,
                                        style: kTxtStyle.copyWith(
                                            fontSize: 16,
                                            fontWeight:
                                            FontWeight.bold),
                                      ),
                                      ElevatedButton(
                                        onPressed: () {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (_) => FashionBookings(
                                                vendorID: widget.fashionVendorID,
                                                pkgIMG: widget.pkgIMGList[0],
                                                productPrice: cannabiesProducts[index].productPrice!,
                                                productType: '', ),
                                            ),
                                          );
                                        },
                                        child: Text(
                                          "Order Now",
                                          style: TextStyle(
                                              fontFamily: 'Raleway-Medium',
                                              fontSize: 12.0,
                                              color: Color(0xff121A31),
                                              letterSpacing: 0.2),
                                        ),
                                        style: ElevatedButton.styleFrom(
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                            BorderRadius.circular(
                                                5.0),
                                          ),
                                          fixedSize: Size(95.0, 30.0),
                                          primary: Color(0xFFFFC71E),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),*/
                              ],
                            ),
                          );
                        }),
                    SizedBox(
                      height: 30.0,
                    ),
                    Text(
                      "Vendor Details :",
                      style: TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    Text(
                      widget.vendrDetail,
                      style: kTxtStyle.copyWith(color: greytxtcolor),
                    ),
                    SizedBox(
                      height: 30.0,
                    ),


                    // Address,
                    Text(
                      "Address :",
                      style: TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    Text(

                      widget.add1 + ' , ' + widget.add2 + ' . ',
                      style: kTxtStyle.copyWith(color: greytxtcolor),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future fetchCannabiesProducts() async {
    setState(() {
      isProductLoading = true;
    });

    try {
      cannabiesProducts =
      await CannabiesAPIs().getCannabiesProductData(widget.fashionVendorID);
    } catch (e) {
      print("Error DATA =====" + e.toString());
    }

    // bName = fashionProducts[1].bussinessDetails!.bussinessName!;

    setState(() {
      isProductLoading = false;
    });
  }

  Future insertProductinCart(String pID, String pAmout) async {
    setState(() {
      isProductLoading = true;
    });

    try {
      await CannabiesAPIs().AddProductToCart(pID, pAmout);
    } catch (e) {
      print("Error DATA =====" + e.toString());
    }

    setState(() {
      isProductLoading = false;
    });
  }
}
