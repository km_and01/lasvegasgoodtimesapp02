import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/custom_stall_card.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';

bool isStallLoading = false;

class TransportStallListingScr extends StatefulWidget {
  final List<String> stallLoc;
  final List<String> stallfees;
  final List<String> stallFor;
  final List<String> stallArea;
  final List<String> stallFeeUnit;
  final String stallImg,eventName, serviceID;
  const TransportStallListingScr(
      {Key? key,
      required this.stallLoc,
      required this.stallfees,
      required this.stallFor,
      required this.stallArea,
      required this.stallFeeUnit, required this.stallImg, required this.eventName, required this.serviceID})
      : super(key: key);

  @override
  State<TransportStallListingScr> createState() => _TransportStallListingScrState();
}

class _TransportStallListingScrState extends State<TransportStallListingScr> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
            lead: Image.asset(
              "assets/icons/ic_transportation.png",
              color: Colors.white,
            ),
            title: Text(
              "Transport",
              style: TextStyle(
                  fontSize: 20.0,
                  fontFamily: 'Helvetica',
                  color: Colors.white
              ),
            )),
        backgroundColor: bgcolor,
        body: isStallLoading
            ? CustomLoadingScr()
            : SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 15.0,
                      ),
                      Text(
                        "Available Stall :",
                        style: TextStyle(
                            fontSize: 20.0,
                            fontFamily: 'Manrope-SemiBold',
                            color: Colors.white
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      ListView.builder(
                          physics: NeverScrollableScrollPhysics(
                              parent: AlwaysScrollableScrollPhysics()),
                          shrinkWrap: true,
                          itemCount: widget.stallLoc.length,
                          itemBuilder: (_, index) {
                            return CustomEventStallCard(
                                vendorID: widget.stallLoc[index],
                                EventVendorIMG: widget.stallImg,
                                stallname: widget.stallLoc[index],
                                price: widget.stallfees[index],
                                priceUnit: widget.stallFeeUnit[index],
                                area: widget.stallArea[index],
                                stallFor: widget.stallFor[index], 
                                eventName: widget.eventName, serviceID: widget.serviceID,);
                          }),
                    ],
                  ),
                ),
              ),
      ),
    );
  }
}
