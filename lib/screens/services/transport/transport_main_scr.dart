// ignore_for_file: deprecated_member_use, avoid_print

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:http/http.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:lasvegas_gts_app/screens/services/transport/transport_vendor_list.dart';

const kGoogleApiKey = "AIzaSyBXfQOd4vA5dxj0VEP-R8LZuGfAgTIeITc";

class TransportHome extends StatefulWidget {
  const TransportHome({Key? key}) : super(key: key);

  @override
  _TransportHomeState createState() => _TransportHomeState();
}

class _TransportHomeState extends State<TransportHome> {
  bool isLoadingcity = false;

  final homeScaffoldKey = GlobalKey<ScaffoldState>();
  final searchScaffoldKey = GlobalKey<ScaffoldState>();
  Mode mode = Mode.overlay;
  DateTime dateTime = DateTime.now();
  String selectedDate = '';
  String pickupAddress = '';
  String pickupCity = '';
  String dropAddress = '';
  String dropCity = '';
  String placeId = '';
  int status = 0;
  final client = Client();
  final place = Place();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: homeScaffoldKey,
      appBar: CustomAppBar(
          lead: Image.asset(
            "assets/icons/ic_transportation.png",
            color: Colors.white,
          ),
          title: Text(
            "Transport",
            style: TextStyle(
                fontSize: 20.0, fontFamily: 'Helvetica', color: Colors.white),
          )),
      bottomNavigationBar: GestureDetector(
        onTap: () async {
          setState(() {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) {
                  return TransportVendorList(
                    pickupLocation: pickupAddress,
                    dropLocation: dropAddress,
                    pickupCity: pickupCity,
                    dropCity: dropCity,
                    pickedDate: selectedDate,
                  );
                },
              ),
            );
          });
        },
        child: Container(
          alignment: Alignment.center,
          child: Text(
            "SEARCH TRANSPORT NOW",
            style: TextStyle(
                fontFamily: 'Helvetica',
                color: Color(0xff121A31),
                fontSize: 18.0,
                letterSpacing: 0.7),
          ),
          height: 57.0,
          color: btngoldcolor,
        ),
      ),
      backgroundColor: bgcolor,
      body: isLoadingcity
          ? CustomLoadingScr()
          : SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(30.0),
                child: Column(
                  children: [
                    SizedBox(
                      height: 10.0,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Image.asset(
                              "assets/icons/ic_stay.png",
                              height: 20.0,
                              width: 20.0,
                              color: Colors.white,
                            ),
                            SizedBox(
                              width: 15.0,
                            ),
                            Text(
                              "PickUp Location",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontSize: 14.0,
                                letterSpacing: 0.8,
                                color: Colors.white,
                                fontFamily: 'Raleway-Bold',
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        InkWell(
                          onTap: () {
                            setState(() {
                              status = 0;
                            });
                            serachAddress();
                          },
                          child: Container(
                              decoration: BoxDecoration(
                                color: servicecardfillcolor,
                                border: Border.all(color: txtborderbluecolor),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                              ),
                              padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  pickupAddress != ''
                                      ? pickupAddress
                                      : 'Search Address',
                                  style: TextStyle(color: Colors.white),
                                ),
                              )),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Row(
                          children: [
                            Image.asset(
                              "assets/icons/ic_stay.png",
                              height: 20.0,
                              width: 20.0,
                              color: Colors.white,
                            ),
                            SizedBox(
                              width: 15.0,
                            ),
                            Text(
                              "Drop Location",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontSize: 14.0,
                                letterSpacing: 0.8,
                                color: Colors.white,
                                fontFamily: 'Raleway-Bold',
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        InkWell(
                          onTap: () {
                            setState(() {
                              status = 1;
                            });
                            serachAddress();
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              color: servicecardfillcolor,
                              border: Border.all(color: txtborderbluecolor),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                            ),
                            padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                dropAddress != ''
                                    ? dropAddress
                                    : 'Search Address',
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Image.asset(
                                  "assets/icons/ic_calendar.png",
                                  height: 20.0,
                                  width: 20.0,
                                  color: Colors.white,
                                ),
                                SizedBox(
                                  width: 10.0,
                                ),
                                Text(
                                  'Select Date',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      fontSize: 14.0,
                                      letterSpacing: 0.8,
                                      color: Colors.white,
                                      fontFamily: 'Raleway-Bold'),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            InkWell(
                              onTap: () async {
                                DateTime? newDate = await showDatePicker(
                                    context: context,
                                    initialDate: dateTime,
                                    firstDate: dateTime,
                                    lastDate: DateTime(2200));
                                if (newDate != null) {
                                  setState(() {
                                    dateTime = newDate;
                                    selectedDate =
                                        '${dateTime.day.toString().padLeft(2, '0')}-${dateTime.month.toString().padLeft(2, '0')}-${dateTime.year}';
                                  });
                                }
                              },
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 10.0),
                                height: 45.0,
                                width: MediaQuery.of(context).size.width,
                                child: Text(
                                  selectedDate,
                                  textAlign: TextAlign.left,
                                  style: kTxtStyle.copyWith(
                                    fontSize: 16.0,
                                    fontFamily: 'Regular',
                                  ),
                                ),
                                decoration: BoxDecoration(
                                  color: servicecardfillcolor,
                                  border: Border.all(color: txtborderbluecolor),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(height: 25.0),
                  ],
                ),
              ),
            ),
    );
  }

  Future serachAddress() async {
    // ignore: unused_local_variable
    Prediction? prediction = await PlacesAutocomplete.show(
        context: context,
        apiKey: kGoogleApiKey,
        types: [],
        strictbounds: false,
        mode: Mode.fullscreen, // Mode.overlay
        language: "en",
        components: [Component(Component.country, "in")]);

    getLatLng(prediction!);
    print("data============> ${prediction.description}");
  }

  Future<PlacesDetailsResponse> getLatLng(Prediction prediction) async {
    GoogleMapsPlaces _places =
        GoogleMapsPlaces(apiKey: kGoogleApiKey); //Same API_KEY as above
    PlacesDetailsResponse detail =
        await _places.getDetailsByPlaceId(prediction.placeId!);
     double latitude = detail.result.geometry!.location.lat;
     double longitude = detail.result.geometry!.location.lng;


    List<Placemark> userLocality = await placemarkFromCoordinates(
        latitude, longitude);

    print('USER_CITY ${userLocality[0].locality}');
    print('USER_CITY ${userLocality[0].subLocality}');
    print('USER_CITY ${userLocality[0].subAdministrativeArea}');

    setState(() {
      placeId = prediction.placeId!;
      status == 0
          ? pickupAddress = prediction.description!
          : dropAddress = prediction.description!;

      status == 0
        ? pickupCity = userLocality[0].locality!
        : dropCity = userLocality[0].locality!;
    });
    getPlaceDetailFromId(placeId);
    return detail;
  }

  Future<Place> getPlaceDetailFromId(String placeId) async {
    var request = Uri.parse(
        'https://maps.googleapis.com/maps/api/place/details/json?place_id=$placeId&fields=address_component&key=$kGoogleApiKey');
    /* final request =
        'https://maps.googleapis.com/maps/api/place/details/json?place_id=$placeId&fields=address_component&key=$kGoogleApiKey'; */
    final response = await client.get(request);

    if (response.statusCode == 200) {
      final result = json.decode(response.body);
      if (result['status'] == 'OK') {
        final components =
            result['result']['address_components'] as List<dynamic>;
        // build result
        for (var c in components) {
          final List type = c['types'];
          if (type.contains('locality')) {
            place.city = c['long_name'];
          }
        }
        print('city====>${place.city}');
        return place;
      }
      throw Exception(result['error_message']);
    } else {
      throw Exception('Failed to fetch suggestion');
    }
  }

  void onError(response) {
    homeScaffoldKey.currentState!.showSnackBar(
      SnackBar(content: Text(response.errorMessage)),
    );
  }
}

class Place {
  String? city;
  Place({
    this.city,
  });

  @override
  String toString() {
    return 'Place(city: $city,)';
  }
}
