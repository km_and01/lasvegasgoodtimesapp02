// ignore_for_file: avoid_print

import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lasvegas_gts_app/Networking/TransportAPI/transport_pkg_model.dart';
import 'package:lasvegas_gts_app/Networking/TransportAPI/transport_service_api.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';

bool isLoading = false;
TransportPKgModel? transportPkgData;
List<String> pkgIMGList = [];
String transportBusinessLOGO =
    "http://koolmindapps.com/lasvegas/public/images/business_detail/";

class TransportPkgListingScr extends StatefulWidget {
  final String transportVendorID;
  final String add1, add2, bname;
  final List eventBIMGS;
  const TransportPkgListingScr(
      {Key? key,
      required this.transportVendorID,
      required this.eventBIMGS,
      required this.add1,
      required this.add2,
      required this.bname})
      : super(key: key);

  @override
  State<TransportPkgListingScr> createState() => _TransportPkgListingScrState();
}

class _TransportPkgListingScrState extends State<TransportPkgListingScr> {
  @override
  void initState() {
    int len = widget.eventBIMGS.length;
    pkgIMGList.clear();
    for (var i = 0; i < len; i++) {
      pkgIMGList.add(transportBusinessLOGO + widget.eventBIMGS[i]);
    }
    fetchtransportPkgData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
            lead: Image.asset(
              "assets/icons/ic_transportation.png",
              color: Colors.white,
            ),
            title: Text(
              "Transport",
              style: TextStyle(
                  fontSize: 20.0, fontFamily: 'Helvetica', color: Colors.white),
            )),
        backgroundColor: bgcolor,
        body: isLoading
            ? CustomLoadingScr()
            : SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 20),
                      CarouselSlider(
                        items: pkgIMGList
                            .map(
                              (x) => Container(
                                width: double.infinity,
                                decoration: BoxDecoration(
                                  //  color: Colors.yellow,
                                  borderRadius: BorderRadius.circular(8.0),
                                  image: DecorationImage(
                                    fit: BoxFit.fill,
                                    image: NetworkImage(x, scale: 1),
                                  ),
                                ),
                              ),
                            )
                            .toList(),
                        // autoPlay: true,
                        // height: 200.0,
                        options: CarouselOptions(
                          height: 200,
                          aspectRatio: 16 / 9,
                          viewportFraction: 0.9,
                          initialPage: 0,
                          enableInfiniteScroll: true,
                          reverse: false,
                          autoPlay: true,
                          autoPlayInterval: Duration(seconds: 3),
                          autoPlayAnimationDuration:
                              Duration(milliseconds: 800),
                          autoPlayCurve: Curves.fastOutSlowIn,
                          enlargeCenterPage: true,
                          // onPageChanged: callbackFunction,
                          scrollDirection: Axis.horizontal,
                        ),
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                      Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.bname,
                              style: TextStyle(
                                  fontSize: 23.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                  fontFamily: 'Manrope-SemiBold'),
                            ),
                            Text(
                              widget.add1, // + ", " + widget.add2,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18.0,
                                  fontFamily: 'Manrope_Medium'),
                            ),
                          ]),
                      SizedBox(
                        height: 20.0,
                      ),
                      Row(
                        children: [
                          RatingBar.builder(
                            glow: false,
                            itemSize: 15.0,
                            initialRating: double.parse(
                                transportPkgData!.review![0].ratings!),
                            minRating: 1,
                            direction: Axis.horizontal,
                            allowHalfRating: true,
                            itemCount: 5,
                            itemBuilder: (context, _) => Icon(
                              Icons.star,
                              color: Colors.amber,
                            ),
                            unratedColor: Colors.grey,
                            onRatingUpdate: (rating) {},
                          ),
                          SizedBox(
                            width: 10.0,
                          ),
                          Text(
                            transportPkgData!.review!.length.toString() +
                                " " +
                                'REVIEWS',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18.0,
                                fontFamily: 'Manrope_Medium'),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Future fetchtransportPkgData() async {
    setState(() {
      isLoading = true;
    });
    try {
      transportPkgData =
          await TransportAPIs().getTransportPkgData(widget.transportVendorID);
      // print("PKG TRANSPORT LENGTH =====" + transportPkgData.length.toString());
    } catch (e) {
      print("Error DATA =====" + e.toString());
    }

    setState(() {
      isLoading = false;
    });
  }
}
