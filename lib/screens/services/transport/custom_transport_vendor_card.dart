// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_hotel_display_card.dart';
import 'package:lasvegas_gts_app/screens/services/transport/transport_pkg_listing_scr.dart';

String vendorTransportID = '';
String serviceTransportID = '';

class CustomTransportVendorCard extends StatefulWidget {
  // final String shortDisc;
  final String vendorID, add1, add2;
  final String transportVendorIMG, bname, price, priceUnit;
  final List vendorIMGs;

  const CustomTransportVendorCard({
    Key? key,
    required this.vendorID,
    required this.transportVendorIMG,
    required this.bname,
    required this.price,
    // required this.eventspsService,
    required this.priceUnit,
    required this.vendorIMGs,
    required this.add1,
    required this.add2,
  }) : super(key: key);

  @override
  State<CustomTransportVendorCard> createState() =>
      _CustomTransportVendorCardState();
}

class _CustomTransportVendorCardState extends State<CustomTransportVendorCard> {
  @override
  void initState() {
    setState(() {
      vendorTransportID = widget.vendorID;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => TransportPkgListingScr(
              transportVendorID: vendorTransportID,
              eventBIMGS: widget.vendorIMGs,
              add1: widget.add1,
              add2: widget.add2,
              bname: widget.bname,
            ),
          ),
        );
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 7.5),
        decoration: BoxDecoration(
          // color: servicecardfillcolor,
          border: Border.all(color: txtborderbluecolor),
          borderRadius: BorderRadius.all(Radius.circular(7.0)),
        ),
        child: Row(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(7.0),
                bottomLeft: Radius.circular(7.0),
              ),
              child: Image.network(
                bLogoUrl + widget.transportVendorIMG,
                fit: BoxFit.fill,
                height: 110,
                width: 130,
              ),
            ),
            SizedBox(
              width: 15.0,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  widget.bname,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18.0,
                      fontFamily: 'Raleway-Medium'),
                ),
                SizedBox(
                  height: 8.0,
                ),
                Text(
                  widget.add1,
                  style: TextStyle(
                      fontFamily: 'Manrope',
                      fontSize: 14.0,
                      color: Colors.white),
                ),
                SizedBox(
                  height: 3.0,
                ),
                RatingBar.builder(
                  glow: false,
                  itemSize: 15.0,
                  initialRating: 3,
                  minRating: 1,
                  direction: Axis.horizontal,
                  allowHalfRating: true,
                  itemCount: 5,
                  itemBuilder: (context, _) => Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  unratedColor: Colors.grey,
                  onRatingUpdate: (rating) {
                    print(rating);
                  },
                ),
                SizedBox(
                  height: 3.0,
                ),
                Text(
                  "Price Starts from " + widget.priceUnit + " " + widget.price,
                  style: TextStyle(
                      fontSize: 16.0,
                      fontFamily: 'Manrope-SemiBold',
                      color: Colors.white),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
