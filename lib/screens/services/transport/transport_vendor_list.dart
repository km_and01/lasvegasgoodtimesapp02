// ignore_for_file: avoid_print, non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/TransportAPI/transport_service_api.dart';
import 'package:lasvegas_gts_app/Networking/TransportAPI/transport_vendor_model.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'package:lasvegas_gts_app/screens/services/evets/event_vendor_listinf_scr.dart';
import 'package:lasvegas_gts_app/screens/services/transport/custom_transport_vendor_card.dart';

class TransportVendorList extends StatefulWidget {
  const TransportVendorList({
    Key? key,
    required this.pickupLocation,
    required this.dropLocation,
    required this.pickupCity,
    required this.dropCity,
    required this.pickedDate,
  }) : super(key: key);

  final String pickupLocation;
  final String dropLocation;
  final String pickupCity;
  final String dropCity;
  final String pickedDate;

  @override
  State<TransportVendorList> createState() => _TransportVendorListState();
}

class _TransportVendorListState extends State<TransportVendorList> {

  bool isTransportVLoading = false;
  List<Datum>? TransportVendorData = [];

  @override
  void initState() {
    fetchTransportVendorData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
            lead: Image.asset(
              "assets/icons/ic_transportation.png",
              color: Colors.white,
            ),
            title: Text(
              "Transport",
              style: TextStyle(
                  fontSize: 20.0, fontFamily: 'Helvetica', color: Colors.white),
            )),
        backgroundColor: bgcolor,
        body: isEventVLoading
            ? CustomLoadingScr()
            : SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 30.0,
                      ),
                      TextFormField(
                        decoration: kSearchTxtFieldDecoration.copyWith(
                          fillColor: servicecardfillcolor,
                          filled: true,
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      ListView.builder(
                          physics: NeverScrollableScrollPhysics(
                              parent: AlwaysScrollableScrollPhysics()),
                          shrinkWrap: true,
                          itemCount: TransportVendorData!.length,
                          itemBuilder: (_, index) {
                            return TransportVendorData == null ?
                             Container() :
                             CustomTransportVendorCard(
                              vendorID: TransportVendorData![index].vendorId ?? '1001',
                              transportVendorIMG: TransportVendorData![index]
                                  .bussinessDetails!
                                  .bussinessLogo!,
                              bname: TransportVendorData![index]
                                  .bussinessDetails!
                                  .bussinessName!,
                              price: TransportVendorData![index].price![0],
                              priceUnit: TransportVendorData![index].priceUnit![0],
                              vendorIMGs: TransportVendorData![index]
                                  .bussinessDetails!
                                  .imagesk!,
                              add1: TransportVendorData![index]
                                  .bussinessDetails!
                                  .addressLine1!,
                              add2: TransportVendorData![index]
                                  .bussinessDetails!
                                  .addressLine2!,
                            );
                          }),  
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Future fetchTransportVendorData() async {
    setState(() {
      isTransportVLoading = true;
    });

    try {
      TransportVendorData = await TransportAPIs().getTransportVendorData(
          widget.pickupLocation,
          widget.dropLocation,
          widget.pickupCity,
          widget.dropCity,
          widget.pickedDate);
      setState(() {

      });
    } catch (e) {
      print("Error DATA =====" + e.toString());
    }
    setState(() {
      isTransportVLoading = false;
    });
  }
}
