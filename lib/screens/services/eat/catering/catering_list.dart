import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/EatAPI/catering_chef_list_model.dart';
import 'package:lasvegas_gts_app/Networking/EatAPI/eat_service_api.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/custom_catering_card.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';

bool isCateringLoading = false;
List<ServiceChef>? CateringData = [];

class CateringList extends StatefulWidget {
  final String sDate, sType, sCity, noPerson;
  const CateringList(
      {Key? key,
      required this.sDate,
      required this.sType,
      required this.sCity,
      required this.noPerson})
      : super(key: key);

  @override
  _CateringListState createState() => _CateringListState();
}

class _CateringListState extends State<CateringList> {
  @override
  void initState() {
    fetchCateringData();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
          lead: Image.asset(
            "assets/icons/ic_eat.png",
            color: Colors.white,
          ),
          title: Text(
            "Eat",
            style: kHeadText,
          ),
        ),
        backgroundColor: bgcolor,
        body: isCateringLoading
            ? CustomLoadingScr()
            : SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 30.0,
                      ),
                      TextFormField(
                        decoration: kSearchTxtFieldDecoration.copyWith(
                          fillColor: servicecardfillcolor,
                          filled: true,
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      CateringData == [] || CateringData == null
                          ? Text(
                              "No Data Available",
                              style: kHeadText,
                            )
                          : ListView.builder(
                              physics: NeverScrollableScrollPhysics(
                                  parent: AlwaysScrollableScrollPhysics()),
                              shrinkWrap: true,
                              itemCount: CateringData!.length,
                              itemBuilder: (_, index) {
                                return CustomCateringCard(
                                  cateIMG: CateringData![index]
                                      .bussinessDetails!
                                      .bussinessLogo!,
                                  bname: CateringData![index]
                                      .bussinessDetails!
                                      .bussinessName!,
                                  price: CateringData![index].minPrice!,
                                  shortDisc: CateringData![index]
                                      .bussinessDetails!
                                      .shortDescription!,
                                   experties: CateringData![index].typesOfFood!,
                                  add1: CateringData![index]
                                      .bussinessDetails!
                                      .addressLine1!,
                                  add2: CateringData![index]
                                      .bussinessDetails!
                                      .addressLine2!,

                                  foodName: CateringData![index].typesOfFood!,
                                  vendorID: CateringData![index].vendorId!,
                                  noPerson: widget.noPerson,
                                  sCity: widget.sCity,
                                  sDate: widget.sDate,
                                  sType: widget.sType, 
                                );
                                //  vendorIDRestaurent = ResaturentData![0].vendorId!;
                              }),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Future fetchCateringData() async {
    setState(() {
      isCateringLoading = true;
    });
    try {
      CateringData = await EatServiceAPIs().getChefData(widget.sDate,
          widget.sType, widget.sCity, widget.noPerson, "Catering");

      print("Cetering DATA =====" +
          CateringData![0].bussinessDetails!.bussinessName!);
    } catch (e) {
      print("Error DATA =====" + e.toString());
    }

    setState(() {
      isCateringLoading = false;
    });
  }
}
