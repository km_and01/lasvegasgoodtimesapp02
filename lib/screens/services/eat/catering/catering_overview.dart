import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/chef_experties_custom_card.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_catering_card.dart';
import 'package:lasvegas_gts_app/customs/custom_hotel_display_card.dart';
import 'package:lasvegas_gts_app/customs/custom_nav_for_restaurant.dart';
import 'package:lasvegas_gts_app/screens/services/eat/chef/chef_booking.dart';
import 'package:lasvegas_gts_app/screens/services/eat/chef/chef_review.dart';

class CateringOverview extends StatefulWidget {
  final String shoertDiscrption, add1, add2, bnamwe, chefIMG;
  final List<String> experties;
  const CateringOverview(
      {Key? key,
      required this.shoertDiscrption,
      required this.add1,
      required this.add2,
      required this.bnamwe,
      required this.chefIMG, required this.experties})
      : super(key: key);

  @override
  _CateringOverviewState createState() => _CateringOverviewState();
}

class _CateringOverviewState extends State<CateringOverview> {
  int selectedItem = 0;
  final screens = [
    CateringOverview(
      shoertDiscrption: '',
      experties: [],
      add1: '',
      add2: '',
      bnamwe: '',
      chefIMG: '',
    ),
    ChefBookings(
      amount: amount,
      foodName: foodNameListCatering,
      vendorID: vendorCateringID,
      noPerson: noPerson,
      sCity: sCity,
      sDate: sDate,
      sType: sType, isChef: false,
    ),
    ChefReview(),
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: bgcolor,
        bottomNavigationBar: CustomNavBarForRestaurents(
          defaultSelectedIndex: 0,
          onChange: (val) {
            setState(() {
              selectedItem = val;
              print(selectedItem);
              screens[val];
            });
          },
        ),
        body: selectedItem == 0
            ? SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    BackButton(
                      color: Colors.white,
                    ),
                    // SizedBox(
                    //   height: 15.0,
                    // ),
                    Container(
                      margin: EdgeInsets.all(15.0),
                      padding: EdgeInsets.all(3.0),
                      alignment: Alignment.topLeft,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              CircleAvatar(
                                  radius: 40.0,
                                  backgroundImage:
                                      NetworkImage(bLogoUrl + widget.chefIMG)

                                  // AssetImage("assets/images/chef1.png"),
                                  ),
                              SizedBox(
                                width: 17.0,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    widget.bnamwe,
                                    // "Chef Name",
                                    style: kHeadText.copyWith(fontSize: 18),
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  RatingBar.builder(
                                    glow: false,
                                    itemSize: 15.0,
                                    initialRating: 3,
                                    minRating: 1,
                                    direction: Axis.horizontal,
                                    allowHalfRating: true,
                                    itemCount: 5,
                                    itemPadding:
                                        EdgeInsets.symmetric(horizontal: 4.0),
                                    itemBuilder: (context, _) => Icon(
                                      Icons.star,
                                      color: Colors.amber,
                                      //   Image.asset(
                                      // "assets/icons/rating_star.png",
                                      // color: Colors.amber,
                                    ),
                                    // ),
                                    unratedColor: Colors.grey,
                                    onRatingUpdate: (rating) {
                                      print(rating);
                                    },
                                  ),
                                  SizedBox(
                                    height: 15.0,
                                  ),
                                  Text(
                                    // for (var i = 0; i < foodNameList.length ; i++) {

                                    // }
                                    foodNameListCatering
                                            .toString()
                                            .replaceAll('[', '')
                                            .replaceAll(']', '') +
                                        " Expert Chef",
                                    style: kTxtStyle,
                                  ),
                                ],
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      height: 1,
                      color: greytxtcolor,
                      thickness: 1.0,
                    ),
                    Container(
                      margin: EdgeInsets.all(15.0),
                      padding: EdgeInsets.all(3.0),
                      alignment: Alignment.topLeft,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Catering Details",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 14.0,
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            widget.shoertDiscrption,
                            // """The Hotel Pennsylvania is conveniently located within one block of the Empire State Building,Madison Square Garden, Penn Station and Macy's flagship store. Located within walking distance are Times Square, the Broadway Theatre District, New York Public Library, Jacob K. Javits Convention Center, and two of New York's most iconic…""",
                            style: kTxtStyle.copyWith(color: greytxtcolor),
                          ),
                          SizedBox(height: 40.0),
                          Text(
                            "Expert in",
                            style: TextStyle(
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          SizedBox(
                            height: 15.0,
                          ),
                          Container(
                            height: 35,
                            padding: EdgeInsets.all(0),
                            child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                physics: BouncingScrollPhysics(
                                    parent: AlwaysScrollableScrollPhysics()),
                                shrinkWrap: true,
                                itemCount:  widget.experties.length,
                                itemBuilder: (_, index) {
                                  return CoustomCard3(title: widget.experties[index]);
                                  // CoustomCard3(
                                  //     title: widget.experties[index]);
                                }),
                          ),
                          // SingleChildScrollView(
                          //   scrollDirection: Axis.horizontal,
                          //   child: Row(
                          //     children: [
                          //       // for (var i = 0; i < widget.experties.length; i++) {
                          //       //   CoustomCard3(title: widget.experties[i]);
                          //       // }

                          //       CoustomCard3(title: 'Chinese'),
                          //       CoustomCard3(title: 'Indian'),
                          //       CoustomCard3(title: 'Italian'),
                          //       CoustomCard3(title: 'Maxican'),
                          //       CoustomCard3(title: 'Hawalian'),
                          //     ],
                          //   ),
                          // ),
                          SizedBox(
                            height: 30.0,
                          ),
                          Text(
                            "Contact",
                            style: TextStyle(
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            widget.add1 + ', ' + widget.add2 + '.',
                            style: kTxtStyle.copyWith(color: greytxtcolor),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Container(
                            child:
                                Image.asset("assets/images/banner_img_2.png"),
                            height: 215.0,
                            width: 380.0,
                            color: bgcolor,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              )
            : screens[selectedItem],
      ),
    );
  }
}
