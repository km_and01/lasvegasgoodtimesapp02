import 'package:autocomplete_textfield_ns/autocomplete_textfield_ns.dart';
import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/StayAPI/stay_search_specil_suits_api.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';
import 'package:lasvegas_gts_app/screens/services/eat/catering/catering_list.dart';
import 'package:lasvegas_gts_app/screens/services/eat/chef/chef_list.dart';
import 'package:lasvegas_gts_app/screens/services/eat/restaurent/restaurent_listing_scr.dart';
import 'package:lasvegas_gts_app/screens/services/stay/city_list_model.dart';
import 'dart:ui' as ui;
String selectedDate = '';
bool isLoadingcity = false;
String selectedType = "BREAKFAST";

class CateringSearchFilter extends StatefulWidget {
  final String? eat_type;
  const CateringSearchFilter({Key? key, this.eat_type}) : super(key: key);

  @override
  _CateringSearchFilterState createState() => _CateringSearchFilterState();
}

List<String> typeList = ["BREAKFAST", "LUNCH", "DINNER"];
DropdownMenuItem<String> buildMenuItem(String item) =>
    DropdownMenuItem(value: item, child: Text(item));

class _CateringSearchFilterState extends State<CateringSearchFilter> {
  List<String> cityList = [];
  List<String> cityCodeList = [];
  String cityCode = '';
  String citytSelected = '';
  TextEditingController cityController = TextEditingController();
  GlobalKey<AutoCompleteTextFieldState<String>> cityKey = GlobalKey();
  TextEditingController nooftableControler = TextEditingController();
  DateTime dateTime = DateTime.now();
  TimeOfDay time = TimeOfDay.now();
  
  // String selectedType = "BREAKFAST";
  bool isCatering = false;
  bool isPvtChef = false;
  String services = '';

  List<DropdownMenuItem<String>> States = [
    DropdownMenuItem(child: Text("MH"), value: "MH"),
    DropdownMenuItem(child: Text("Gj"), value: "gj"),
    DropdownMenuItem(child: Text("Tn"), value: "tn"),
    DropdownMenuItem(child: Text("OD"), value: "Od"),
  ];
  List<DropdownMenuItem<String>> City = [
    DropdownMenuItem(child: Text("c1"), value: "c1"),
    DropdownMenuItem(child: Text("c2"), value: "c2"),
    DropdownMenuItem(child: Text("c3"), value: "c3"),
    DropdownMenuItem(child: Text("c4"), value: "c4"),
  ];
  @override
  void initState() {
    fetchCityList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    bool value = false;
    int val;
    String groupValue = '';
    // return showDialog(
    //                               context: context,
    //                               barrierColor: bgcolor.withOpacity(0.97),
    //                               builder: (BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: bgcolor,
        appBar: AppBar(
          elevation: 0.0,
          leading: BackButton(
            color: Colors.white,
          ),
        ),
        bottomNavigationBar: GestureDetector(
          onTap: () async {
            setState(() {
              print(selectedDate);
              print(selectedType);
              print(isCatering);
              print(nooftableControler.text);
              print(cityCode + citytSelected);
              isCatering ?
              Navigator.push(
                  context, MaterialPageRoute(builder: (_) => CateringList(
                    noPerson:nooftableControler.text, 
                    sCity: cityCode, 
                    sDate: selectedDate,
                    sType: selectedType,
                  )))
                  :
              Navigator.push(
                  context, MaterialPageRoute(builder: (_) => 
                  ChefList(
                    noPerson:nooftableControler.text, 
                    sCity: cityCode, 
                    sDate: selectedDate,
                    sType: selectedType,
                    )
                  ));


              /*Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (_) => RestaurantListing(
                        selectedDate: selectedDate,
                        selectedDish: selectedType,
                        selectedTime: '',
                        city_code: cityCode,
                        noPerson: nooftableControler.text,
                        services: services,
                        eat_type: widget.eat_type == null ? '' : widget.eat_type!,)));*/

            });
          },
          child: Container(
            alignment: Alignment.center,
            child: Text(
              "SEARCH NOW",
              style: TextStyle(fontSize: 18.0, letterSpacing: 0.7),
            ),
            height: 57.0,
            color: btngoldcolor,
          ),
        ),
        body: 
        
        SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(30.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Check Availability",
                  style: kHeadText,
                ),
                SizedBox(
                  height: 15.0,
                ),
                Row(
                  children: [
                    /////Catering
                    Column(
                      children: [
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              services = 'Catering';
                              isCatering = true;
                              isPvtChef = false;
                            });
                          },
                          child: Container(
                            alignment: Alignment.center,
                            child: Image.asset(
                              "assets/icons/5.png",
                              height: 30.0,
                              width: 30.0,
                            ),
                            decoration: BoxDecoration(
                              color: isCatering
                                  ? Colors.white
                                  : servicecardfillcolor,
                              borderRadius: BorderRadius.circular(40),
                              boxShadow: const [
                                BoxShadow(
                                    color: txtborderbluecolor,
                                    spreadRadius: 1.5),
                              ],
                            ),
                            height: 69.0,
                            width: 69.0,

                            //  kTxtFieldDecoration .copyWith(),
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          "Catering",
                          style: kTxtStyle,
                        ),
                      ],
                    ),
                    SizedBox(
                      width: 19.0,
                    ),
                    /////Pvt Chef
                    Column(
                      children: [
                        GestureDetector(
                          onTap: () {
                            setState(() {

                              services = 'Privatechef';
                              isCatering = false;
                              isPvtChef = true;
                            });
                          },
                          child: Container(
                            alignment: Alignment.center,
                            child: Image.asset(
                              "assets/icons/6.png",
                              height: 30.0,
                              width: 30.0,
                            ),
                            decoration: BoxDecoration(
                              color: isPvtChef
                                  ? Colors.white
                                  : servicecardfillcolor,
                              borderRadius: BorderRadius.circular(40),
                              boxShadow: const [
                                BoxShadow(
                                    color: txtborderbluecolor,
                                    spreadRadius: 1.5),
                              ],
                            ),
                            height: 69.0,
                            width: 69.0,

                            //  kTxtFieldDecoration .copyWith(),
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          "Private Chef",
                          style: kTxtStyle,
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: 25.0,
                ),
                //  isCatering
                //         ? CateringContainer()
                //         : PrivateChefContainer(),

                ///////Select City
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Select City",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 14.0,
                        letterSpacing: 0.8,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: servicecardfillcolor,
                        border: Border.all(color: txtborderbluecolor),
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                      padding: EdgeInsets.fromLTRB(10, 10, 0, 10),
                      child: Directionality(
                        textDirection: ui.TextDirection.ltr,
                        child: SimpleAutoCompleteTextField(
                          textSubmitted: (text) {
                            if (cityList.contains(text)) {
                              // cityController.text = text;
                              citytSelected = text;
                              var cityCodeindex =
                                  cityList.indexOf(citytSelected);
                              cityCode = cityCodeList[cityCodeindex];
                              print(cityCodeindex);
                              print(cityCode);
                            }
                          },
                          textChanged: (text) {
                            // setState(() {
                            //   citytSelected = text;
                            // });
                            // for (var i = 0; i < cityList.length; i++) {
                            if (cityList.contains(citytSelected)) {
                              // var cityCodeindex =
                              //     cityCodeList.indexOf(citytSelected);
                              // print(cityCodeindex);
                              // print(cityCode);
                            }
                            // }
                          },
                          clearOnSubmit: false,
                          controller: cityController,
                          key: cityKey,
                          suggestions: cityList,
                          style: kTxtStyle.copyWith(fontSize: 16.0),
                          decoration: InputDecoration.collapsed(
                            hintText: "search city",
                            fillColor: servicecardfillcolor,

                            border: InputBorder.none,
                            // focusedBorder:
                            //     InputBorder.none,
                            // enabledBorder:
                            //     InputBorder.none,
                            // errorBorder:
                            //     InputBorder.none,
                            // disabledBorder:
                            //     InputBorder.none,
                            hintStyle: kTxtStyle,
                            //  TextStyle(
                            //     color: Color(
                            //         ColorConst
                            //             .grayColor),
                            //     fontSize: 11.5.sp),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(height: 25.0),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Image.asset(
                          "assets/icons/ic_calendar.png",
                          height: 20.0,
                          width: 20.0,
                          color: Colors.white,
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        Text(
                          'Select Date',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontSize: 14.0,
                            letterSpacing: 0.8,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    InkWell(
                      onTap: () async {
                        DateTime? newDate = await showDatePicker(
                            context: context,
                            initialDate: dateTime,
                            firstDate: DateTime(2021),
                            lastDate: DateTime(2200));
                        if (newDate != null) {
                          setState(() {
                            dateTime = newDate;
                            selectedDate =
                                '${dateTime.day.toString().padLeft(2, '0')}-${dateTime.month.toString().padLeft(2, '0')}-${dateTime.year}';
                          });
                        }
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        height: 50.0,
                        width: MediaQuery.of(context).size.width,
                        child: Text(
                          selectedDate,
                          textAlign: TextAlign.left,
                          style: kTxtStyle.copyWith(
                            fontSize: 16.0,
                            fontFamily: 'Regular',
                          ),
                        ),
                        decoration: BoxDecoration(
                          color: servicecardfillcolor,
                          border: Border.all(color: txtborderbluecolor),
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        ),
                      ),
                    ),
                    SizedBox(height: 25.0),
                    Row(
                      children: [
                        Icon(
                          Icons.fastfood_outlined,
                          color: Colors.white,
                        ),
                        SizedBox(
                          width: 15.0,
                        ),
                        Text(
                          "Dishes",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontSize: 14.0,
                            letterSpacing: 0.8,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 10.0),
                      height: 50.0,
                      width: MediaQuery.of(context).size.width,
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton<String>(
                            dropdownColor: servicecardfillcolor,
                            style: kTxtStyle.copyWith(fontSize: 16.0),
                            isExpanded: true,
                            icon: Icon(
                              Icons.arrow_drop_down,
                              color: Colors.white,
                            ),
                            value: selectedType,
                            items: typeList.map(buildMenuItem).toList(),
                            onChanged: (value) {
                              setState(() {
                                selectedType = value!;
                              });
                            }),
                      ),
                      decoration: BoxDecoration(
                        color: servicecardfillcolor,
                        border: Border.all(color: txtborderbluecolor),
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                    ),
                  ],
                ),

                SizedBox(height: 25.0),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                  child: Row(
                    children: [
                      Icon(
                        Icons.person,
                        color: Colors.white,
                      ),
                      SizedBox(
                        width: 15.0,
                      ),
                      Text(
                        'No. of Guest',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontSize: 14.0,
                          letterSpacing: 0.8,
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
                TextFormField(
                  style: kTxtStyle.copyWith(
                    fontSize: 16.0,
                    fontFamily: 'Regular',
                  ),
                  keyboardType: TextInputType.number,
                  controller: nooftableControler,
                  decoration: kTxtFieldDecoration,
                ),
              ],
            ),
          ),
        ),
      ),
    );
    // });
  }

  Future fetchCityList() async {
    setState(() {
      isLoadingcity = true;
    });

    print(homeToken);
    String tkn = homeToken;

    List<Datum>? parsedRes = await GetCityList().getCityList(homeToken);
    if (parsedRes != null) {
      for (var i = 0; i < parsedRes.length; i++) {
        if (parsedRes[i].city != null) {
          cityList.add(parsedRes[i].name! + ' - ' + parsedRes[i].city!.name!);
          cityCodeList.add(parsedRes[i].id.toString());
        }
      }
    }
    print(cityList[3]);
    print(cityCodeList[3]);

    setState(() {
      isLoadingcity = false;
    });
  }
}
