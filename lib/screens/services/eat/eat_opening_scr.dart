import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/custom_catering_card.dart';
import 'package:lasvegas_gts_app/customs/custom_chef_card.dart';
import 'package:lasvegas_gts_app/customs/custom_restaurent_card.dart';
import 'package:flutter_awesome_select/flutter_awesome_select.dart';
import 'package:lasvegas_gts_app/screens/services/eat/catering/catering_filter.dart';
import 'package:lasvegas_gts_app/screens/services/eat/restaurent/restaurent_filter.dart';

TextEditingController nooftableControler = TextEditingController();
DateTime dateTime = DateTime.now();
TimeOfDay time = TimeOfDay.now();
String selectedDate = '';
String selectedTime = " ";

bool dish1 = false;
bool dish2 = false;
bool dish3 = false;

List<DropdownMenuItem<String>> States = [
  DropdownMenuItem(child: Text("MH"), value: "MH"),
  DropdownMenuItem(child: Text("Gj"), value: "gj"),
  DropdownMenuItem(child: Text("Tn"), value: "tn"),
  DropdownMenuItem(child: Text("OD"), value: "Od"),
];
List<DropdownMenuItem<String>> City = [
  DropdownMenuItem(child: Text("c1"), value: "c1"),
  DropdownMenuItem(child: Text("c2"), value: "c2"),
  DropdownMenuItem(child: Text("c3"), value: "c3"),
  DropdownMenuItem(child: Text("c4"), value: "c4"),
];

class EatHome extends StatefulWidget {
  const EatHome({Key? key}) : super(key: key);

  @override
  _EatHomeState createState() => _EatHomeState();
}

bool isRestaurants = true;
bool isCatering = false;
bool isPvtChef = false;

class _EatHomeState extends State<EatHome> {
  @override
  void initState() {
    // selectEatType();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    bool value = false;
    int val;
    String groupValue = '';
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
          lead: Image.asset(
            "assets/icons/ic_eat.png",
            color: Colors.white,
            width: 35.0,
            height: 35.0,
          ),
          title: Text(
            "Eat",
            style: TextStyle(
                color: Colors.white,
                fontFamily: 'Raleway-SemiBold',
                fontSize: 20.0
            ),
          ),
        ),
        backgroundColor: bgcolor,
        // bottomNavigationBar: GestureDetector(
        //   onTap: () async {
        //     setState(() {
        //       print(selectedDate);
        //       print(selectedTime);
        //       print("Dishes:" +
        //           dish1.toString() +
        //           dish2.toString() +
        //           dish3.toString());
        //       print(nooftableControler.text);
        //       Navigator.pop(context);
        //     });
        //   },
        //   child: Container(
        //     alignment: Alignment.center,
        //     child: Text(
        //       "SEARCH NOW",
        //       style: TextStyle(fontSize: 18.0, letterSpacing: 0.7),
        //     ),
        //     height: 57.0,
        //     color: btngoldcolor,
        //   ),
        // ),
        body: Container(
          alignment: Alignment.center,
          margin: EdgeInsets.all(10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Select a Category",
                style: TextStyle(
                  fontSize: 25.0,
                  fontFamily: 'Raleway-Bold',
                  color: Colors.white,
                ),
              ),
              SizedBox(
                height: 15.0,
              ),
              Container(
                height: 300,
                decoration: kBorderDecoration,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (_) => RestaurentSearchFilter(
                                  eat_type: "Restaurant",
                                )));
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset('assets/icons/4.png'),
                          SizedBox(
                            width: 15.0,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Restaurants",
                                style: TextStyle(
                                  fontSize: 25.0,
                                  fontFamily: 'Raleway-Bold',
                                  color: Colors.white,
                                ),
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              Text(
                                "Book Tables for your upcoming dinner \nor lunch",
                                style: TextStyle(
                                    fontFamily: 'Manrope',
                                    fontSize: 14.0,
                                    color: Colors.white
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 30.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Divider(
                        height: 1.5,
                        thickness: 1,
                        color: txtborderbluecolor,
                      ),
                    ),
                    SizedBox(
                      height: 30.0,
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (_) => CateringSearchFilter(
                                  eat_type: "Catering",
                                )));
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset('assets/icons/6.png'),
                          SizedBox(
                            width: 15.0,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Private Chef/Catering",
                                style: TextStyle(
                                  fontSize: 25.0,
                                  fontFamily: 'Raleway-Bold',
                                  color: Colors.white,
                                ),
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              Text(
                                "Book Chef/Caterers for your \nupcoming events/function at your place.",
                                style: TextStyle(
                                  fontFamily: 'Manrope',
                                  fontSize: 14.0,
                                  color: Colors.white
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  selectEatType() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          backgroundColor: bgcolor,
          insetPadding: EdgeInsets.only(
              left: MediaQuery.of(context).size.width * 0.44,
              right: MediaQuery.of(context).size.width * 0.44),
          child: Container(
            color: Colors.transparent,
            width: double.infinity,
            height: 50,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Select a Category',
                  style: kHeadText,
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

class RestaurentContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Search Restaurants",
          style: TextStyle(
              color: Colors.white, fontSize: 18.0, fontWeight: FontWeight.bold),
        ),
        SizedBox(
          height: 20.0,
        ),
      ],
    );
  }
}

class CateringContainer extends StatefulWidget {
  @override
  State<CateringContainer> createState() => _CateringContainerState();
}

class _CateringContainerState extends State<CateringContainer> {
  bool value = false;
  late int val;
  String groupValue = '';
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Book Caterings",
            style: kHeadText,
          ),
          SizedBox(height: 25.0),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Select Date',
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 14.0,
                  letterSpacing: 0.8,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              InkWell(
                onTap: () async {
                  DateTime? newDate = await showDatePicker(
                      context: context,
                      initialDate: dateTime,
                      firstDate: DateTime(2021),
                      lastDate: DateTime(2200));
                  if (newDate != null) {
                    setState(() {
                      dateTime = newDate;
                      selectedDate =
                          '${dateTime.day.toString().padLeft(2, '0')}  -  ${dateTime.month.toString().padLeft(2, '0')}  -  ${dateTime.year}';
                    });
                  }
                },
                child: Container(
                  padding:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                  height: 46.0,
                  width: 145.0,
                  child: Text(
                    selectedDate,
                    textAlign: TextAlign.center,
                    style: kTxtStyle.copyWith(
                      fontSize: 16.0,
                      fontFamily: 'Regular',
                    ),
                  ),
                  decoration: BoxDecoration(
                    // color: servicecardfillcolor,
                    border: Border.all(color: txtborderbluecolor),
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height: 25.0),
          Padding(
            padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Select State",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 14.0,
                        letterSpacing: 0.8,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    InkWell(
                      onTap: () async {},
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        height: 46.0,
                        width: 145.0,
                        child: DropdownButton(
                            items: States,
                            onChanged: (val) {
                              setState(() {});
                            }),
                        decoration: BoxDecoration(
                          // color: servicecardfillcolor,
                          border: Border.all(color: txtborderbluecolor),
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        ),
                      ),
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Select City",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 14.0,
                        letterSpacing: 0.8,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    InkWell(
                      onTap: () async {},
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        height: 46.0,
                        width: 145.0,
                        child: DropdownButton(
                            items: City,
                            onChanged: (val) {
                              setState(() {});
                            }),
                        decoration: BoxDecoration(
                          // color: servicecardfillcolor,
                          border: Border.all(color: txtborderbluecolor),
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          SizedBox(height: 25.0),
          Text(
            "Book for",
            textAlign: TextAlign.left,
            style: TextStyle(
              fontSize: 14.0,
              letterSpacing: 0.8,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 8.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                children: [
                  Radio(
                    value: 'Lunch',
                    groupValue: groupValue,
                    activeColor: btngoldcolor,
                    // fillColor: MaterialStateProperty.all(btngoldcolor),
                    onChanged: (value) {
                      setState(() {
                        // fillColor:
                        // MaterialStateProperty.all(btngoldcolor);
                        groupValue = value.toString();
                        print(groupValue);
                      });
                    },

                    // toggleable: true,
                    // autofocus: true,
                    // focusColor: btngoldcolor,

                    // hoverColor: btngoldcolor,
                    // overlayColor: MaterialStateProperty.all(btngoldcolor),
                  ),
                  Text(
                    "Lunch",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 14.0,
                      letterSpacing: 0.8,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Radio(
                    value: 'Dinner',
                    groupValue: groupValue,
                    onChanged: (value) {
                      setState(() {
                        groupValue = value.toString();
                        print(groupValue);
                      });
                    },
                    activeColor: btngoldcolor,
                    toggleable: true,
                  ),
                  Text(
                    "Dinner",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 14.0,
                      letterSpacing: 0.8,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              )
            ],
          ),
          SizedBox(height: 25.0),
          Padding(
            padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
            child: Text(
              'Table Person',
              textAlign: TextAlign.left,
              style: TextStyle(
                fontSize: 14.0,
                letterSpacing: 0.8,
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          TextFormField(
            style: kTxtStyle,
            keyboardType: TextInputType.number,
            controller: nooftableControler,
            decoration: kTxtFieldDecoration,
          ),
          SizedBox(height: 25.0),
        ],
      ),
    );
  }
}

class PrivateChefContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Text(
            "Search Recommended Chef",
            style: TextStyle(
                color: Colors.white,
                fontSize: 18.0,
                fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 20.0,
          ),
        ],
      ),
    );
  }
}
