import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/custom_catering_card.dart';
import 'package:lasvegas_gts_app/customs/custom_chef_card.dart';
import 'package:lasvegas_gts_app/customs/custom_restaurent_card.dart';
import 'package:flutter_awesome_select/flutter_awesome_select.dart';
import 'package:lasvegas_gts_app/screens/services/eat/catering/catering_filter.dart';
import 'package:lasvegas_gts_app/screens/services/eat/restaurent/restaurent_filter.dart';

class EatHome extends StatefulWidget {
  const EatHome({Key? key}) : super(key: key);

  @override
  _EatHomeState createState() => _EatHomeState();
}

bool isRestaurants = true;
bool isCatering = false;
bool isPvtChef = false;

class _EatHomeState extends State<EatHome> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
          lead: Image.asset(
            "assets/icons/ic_eat.png",
            color: Colors.white,
          ),
          title: Text(
            "Eat",
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'Raleway-SemiBold',
              fontSize: 18.0
            ),
          ),
        ),
        backgroundColor: bgcolor,
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(10),
            child: Column(
              children: [
                SizedBox(
                  height: 30.0,
                ),

                TextFormField(
                  decoration: kSearchTxtFieldDecoration.copyWith(
                    fillColor: servicecardfillcolor,
                    filled: true,
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),

                //////Category selection Tab
                Row(
                  children: [
                    /////Restaurants
                    Column(
                      children: [
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              isRestaurants = true;
                              isCatering = false;
                              isPvtChef = false;
                              Navigator.push(context, MaterialPageRoute(builder: (_) =>
                                  RestaurentSearchFilter(
                                    eat_type: "Restaurant",
                                  )));
                             
                            });
                          },
                          child: Container(
                            alignment: Alignment.center,
                            child: Image.asset(
                              "assets/icons/4.png",
                              height: 30.0,
                              width: 30.0,
                            ),
                            decoration: BoxDecoration(
                              color: isRestaurants
                                  ? Colors.white
                                  : servicecardfillcolor,
                              borderRadius: BorderRadius.circular(40),
                              boxShadow: const [
                                BoxShadow(
                                    color: txtborderbluecolor,
                                    spreadRadius: 1.5),
                              ],
                            ),
                            height: 69.0,
                            width: 69.0,

                            //  kTxtFieldDecoration .copyWith(),
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          "Restaurants",
                          style: kTxtStyle,
                        ),
                      ],
                    ),

                    SizedBox(
                      width: 13.0,
                    ),

                    /////Catering
                    Column(
                      children: [
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              isRestaurants = false;
                              isCatering = true;
                              isPvtChef = false;
                               Navigator.push(context, MaterialPageRoute(builder: (_) =>
                                   CateringSearchFilter(
                                     eat_type: "Catering",
                                   )));
                            });
                          },
                          child: Container(
                            alignment: Alignment.center,
                            child: Image.asset(
                              "assets/icons/5.png",
                              height: 30.0,
                              width: 30.0,
                            ),
                            decoration: BoxDecoration(
                              color: isCatering
                                  ? Colors.white
                                  : servicecardfillcolor,
                              borderRadius: BorderRadius.circular(40),
                              boxShadow: const [
                                BoxShadow(
                                    color: txtborderbluecolor,
                                    spreadRadius: 1.5),
                              ],
                            ),
                            height: 69.0,
                            width: 69.0,

                            //  kTxtFieldDecoration .copyWith(),
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          "Catering",
                          style: kTxtStyle,
                        ),
                      ],
                    ),
                    SizedBox(
                      width: 19.0,
                    ),
                    /////Pvt Chef
                    Column(
                      children: [
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              isRestaurants = false;
                              isCatering = false;
                              isPvtChef = true;
                            });
                          },
                          child: Container(
                            alignment: Alignment.center,
                            child: Image.asset(
                              "assets/icons/6.png",
                              height: 30.0,
                              width: 30.0,
                            ),
                            decoration: BoxDecoration(
                              color: isPvtChef
                                  ? Colors.white
                                  : servicecardfillcolor,
                              borderRadius: BorderRadius.circular(40),
                              boxShadow: const [
                                BoxShadow(
                                    color: txtborderbluecolor,
                                    spreadRadius: 1.5),
                              ],
                            ),
                            height: 69.0,
                            width: 69.0,

                            //  kTxtFieldDecoration .copyWith(),
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          "Private Chef",
                          style: kTxtStyle,
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: 20.0,
                ),
                isRestaurants
                    ? RestaurentContainer()
                    : isCatering
                        ? CateringContainer()
                        : isPvtChef
                            ? PrivateChefContainer()
                            : RestaurentContainer(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class RestaurentContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Our Recommended Restaurants",
          style: TextStyle(
              color: Colors.white, fontSize: 18.0, fontWeight: FontWeight.bold),
        ),
        SizedBox(
          height: 20.0,
        ),
        /*CustomRestaurentCard(),
        CustomRestaurentCard(),
        CustomRestaurentCard(),
        CustomRestaurentCard(),
        CustomRestaurentCard(),
        CustomRestaurentCard(),
        CustomRestaurentCard(),
        CustomRestaurentCard(),*/
      ],
    );
  }
}

class CateringContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Text(
            "Our Recommended Catering",
            style: TextStyle(
                color: Colors.white,
                fontSize: 18.0,
                fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 20.0,
          ),
          //CustomCateringCard(),
        ],
      ),
    );
  }
}

class PrivateChefContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Text(
            "Our Recommended Chef",
            style: TextStyle(
                color: Colors.white,
                fontSize: 18.0,
                fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 20.0,
          ),
          /*CustomChefCard(),
          CustomChefCard(),
          CustomChefCard(),
          CustomChefCard(),
          CustomChefCard(),
          CustomChefCard(),
          CustomChefCard(),
          CustomChefCard(),*/
        ],
      ),
    );
  }
}

