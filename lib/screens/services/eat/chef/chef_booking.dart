import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/EatAPI/eat_service_api.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/custom_chef_card.dart';
import 'package:lasvegas_gts_app/screens/services/eat/chef/chef_checkout.dart';
import 'package:dropdown_button2/dropdown_button2.dart';

class ChefBookings extends StatefulWidget {
  final String vendorID, amount;
  final List<String> foodName;
  final bool isChef;

  final String sDate, sType, sCity, noPerson;
  const ChefBookings(
      {Key? key,
      required this.vendorID,
      required this.amount,
      required this.foodName,
      required this.sDate,
      required this.sType,
      required this.sCity,
      required this.noPerson,
      required this.isChef})
      : super(key: key);

  @override
  _ChefBookingsState createState() => _ChefBookingsState();
}

class _ChefBookingsState extends State<ChefBookings> {
  TextEditingController guestCountController = TextEditingController();
  TextEditingController guestAddressController = TextEditingController();
  TextEditingController specialRequestController = TextEditingController();
  TextEditingController AmountController = TextEditingController();
  DateTime dateTime = DateTime.now();
  TimeOfDay time = TimeOfDay.now();
  String selectedDate = '';
  String selectedTime = " ";
  String selectedFood = '';

  @override
  void initState() {
    // TODO: implement initState
    selectedDate = widget.sDate;
    selectedTime = widget.sType;
    guestCountController.text = widget.noPerson;
    selectedFood = widget.foodName.first;
    setState(() {
      AmountController.text =
          (int.parse(amount) * int.parse(widget.noPerson)).toString();
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: bgcolor,
        appBar: CustomAppBar(
          lead: BackButton(),
          title: Text(
            widget.isChef ? "Chef Booking Details" : "Catering Booking Details",
            style: kTxtStyle,
          ),
        ),
        body: SingleChildScrollView(
            reverse: true,
            child: Container(
              margin: EdgeInsets.all(20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 25.0),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Select Date',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontSize: 14.0,
                                letterSpacing: 0.8,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            InkWell(
                              onTap: () async {
                                DateTime? newDate = await showDatePicker(
                                    context: context,
                                    initialDate: dateTime,
                                    firstDate: DateTime(2021),
                                    lastDate: DateTime(2200));
                                if (newDate != null) {
                                  setState(() {
                                    dateTime = newDate;
                                    selectedDate =
                                        '${dateTime.day.toString().padLeft(2, '0')}-${dateTime.month.toString().padLeft(2, '0')}-${dateTime.year}';
                                  });
                                }
                              },
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 10.0),
                                height: 46.0,
                                width: 166.0,
                                child: Text(
                                  selectedDate,
                                  textAlign: TextAlign.center,
                                  style: kTxtStyle.copyWith(
                                    fontSize: 16.0,
                                    fontFamily: 'Regular',
                                  ),
                                ),
                                decoration: BoxDecoration(
                                  color: servicecardfillcolor,
                                  border: Border.all(color: txtborderbluecolor),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Booking for",
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontSize: 14.0,
                                letterSpacing: 0.8,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            InkWell(
                              onTap: () {
                                // TimeOfDay? newTime = await showTimePicker(
                                //   context: context,
                                //   initialTime: time,
                                // );
                                // if (newTime != null) {
                                //   setState(() {
                                //     time = newTime;
                                //     selectedTime =
                                //         '${time.hour.toString().padLeft(2, '0')}  :  ${time.minute.toString().padLeft(2, '0')}  ';
                                //   });
                                // }
                              },
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 10.0),
                                height: 46.0,
                                width: 166.0,
                                child: Text(
                                  selectedTime,
                                  textAlign: TextAlign.center,
                                  style: kTxtStyle.copyWith(
                                    fontSize: 16.0,
                                    fontFamily: 'Regular',
                                  ),
                                ),
                                decoration: BoxDecoration(
                                  // color: servicecardfillcolor,
                                  border: Border.all(color: txtborderbluecolor),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                ),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 25.0),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                    child: Text(
                      'Total number of guests',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 14.0,
                        letterSpacing: 0.8,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  TextFormField(
                    style: kTxtStyle,
                    keyboardType: TextInputType.number,
                    controller: guestCountController,
                    decoration: kTxtFieldDecoration,
                  ),
                  SizedBox(height: 25.0),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                    child: Text(
                      'Food type',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 14.0,
                        letterSpacing: 0.8,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Container(
                    padding:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                    height: 46.0,
                    width: MediaQuery.of(context).size.width,
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton2(
                        dropdownDecoration: BoxDecoration(
                          color: servicecardfillcolor,
                        ),
                        icon: Icon(
                          Icons.arrow_drop_down,
                          color: Colors.white,
                        ),
                        hint: Text(
                          'Select Item',
                          style: TextStyle(
                            fontSize: 14,
                            color: Theme.of(context).hintColor,
                          ),
                        ),
                        items: widget.foodName
                            .map((item) => DropdownMenuItem<String>(
                                  value: item,
                                  child: Text(item, style: kTxtStyle),
                                ))
                            .toList(),
                        value: selectedFood,
                        onChanged: (value) {
                          setState(() {
                            selectedFood = value as String;
                          });
                        },
                        buttonHeight: 40,
                        buttonWidth: 140,
                        itemHeight: 40,
                      ),
                    ),
                    decoration: BoxDecoration(
                      // color: servicecardfillcolor,
                      border: Border.all(color: txtborderbluecolor),
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                  ),

                  // TextFormField(
                  //   style: kTxtStyle,
                  //   keyboardType: TextInputType.number,
                  //   controller: guestCountController,
                  //   decoration: kTxtFieldDecoration,
                  // ),
                  SizedBox(height: 25.0),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                    child: Text(
                      'Total Amount',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 14.0,
                        letterSpacing: 0.8,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  TextFormField(
                    style: kTxtStyle,
                    keyboardType: TextInputType.number,
                    controller: AmountController,
                    decoration: kTxtFieldDecoration,
                  ),
                  SizedBox(height: 25.0),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                    child: Text(
                      'Address',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 14.0,
                        letterSpacing: 0.8,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  TextFormField(
                    style: kTxtStyle,
                    maxLines: 2,
                    controller: guestAddressController,
                    keyboardType: TextInputType.multiline,
                    decoration: kTxtFieldDecoration,
                  ),
                  SizedBox(height: 25.0),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                    child: Text(
                      'Special Request',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 14.0,
                        letterSpacing: 0.8,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  TextFormField(
                    style: kTxtStyle,
                    maxLines: 2,
                    controller: specialRequestController,
                    keyboardType: TextInputType.multiline,
                    decoration: kTxtFieldDecoration,
                  ),
                  SizedBox(height: 25.0),
                  ElevatedButton(
                    onPressed: () async {
                      print(selectedDate);
                      print(selectedTime);
                      print(specialRequestController.text);
                      print(guestCountController.text);
                      print(guestAddressController.text);
                      try {
                        await EatServiceAPIs().bookCateringChef(
                            widget.vendorID,
                            guestAddressController.text,
                            widget.isChef ? 'Private Chef' : 'Catering',
                            guestCountController.text,
                            foodNameList
                                .toString()
                                .replaceAll('[', '')
                                .replaceAll(']', ''),
                            AmountController.text,
                            selectedDate,
                            selectedTime);
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: const Text("Booking Done Successfully")));
                      } catch (e) {
                        ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(content: Text(e.toString())));
                      }
                      Navigator.pop(context);
                      // Navigator.push(
                      //   context,
                      //   MaterialPageRoute(
                      //     builder: (context) => ChefCheckOut(
                      //       address: guestAddressController.text,
                      //       date: selectedDate,
                      //       guestCount: guestCountController.text,
                      //       time: selectedTime,
                      //     ),
                      //   ),
                      // );
                    },
                    child: Text(
                      'BOOK NOW',
                      style: TextStyle(color: Colors.black),
                    ),
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(13.0),
                      ),
                      fixedSize: Size(389.0, 57.0),
                      primary: Color(0xFFFFC71E),
                    ),
                  ),
                ],
              ),
            )),
      ),
    );
  }
}
