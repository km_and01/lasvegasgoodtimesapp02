import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/EatAPI/catering_chef_list_model.dart';
import 'package:lasvegas_gts_app/Networking/EatAPI/eat_service_api.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/custom_chef_card.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';

bool isChefLoading = false;
List<ServiceChef>? ChefData = [];

class ChefList extends StatefulWidget {
  final String sDate, sType, sCity, noPerson;
  const ChefList(
      {Key? key,
      required this.sDate,
      required this.sType,
      required this.sCity,
      required this.noPerson})
      : super(key: key);

  @override
  _ChefListState createState() => _ChefListState();
}

class _ChefListState extends State<ChefList> {
  @override
  void initState() {
    fetchChefData();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print("object");
    print(widget.noPerson + widget.sCity + widget.sDate + widget.sType);
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
          lead: Image.asset(
            "assets/icons/ic_eat.png",
            color: Colors.white,
          ),
          title: Text(
            "Eat",
            style: kHeadText,
          ),
        ),
        backgroundColor: bgcolor,
        body: isChefLoading
            ? CustomLoadingScr()
            : SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 30.0,
                      ),
                      TextFormField(
                        decoration: kSearchTxtFieldDecoration.copyWith(
                          fillColor: servicecardfillcolor,
                          filled: true,
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      ListView.builder(
                          physics: NeverScrollableScrollPhysics(
                              parent: AlwaysScrollableScrollPhysics()),
                          shrinkWrap: true,
                          itemCount: ChefData!.length,
                          itemBuilder: (_, index) {
                            return CustomChefCard(
                              chefIMG: ChefData![index]
                                  .bussinessDetails!
                                  .bussinessLogo!,
                              bname: ChefData![index]
                                  .bussinessDetails!
                                  .bussinessName!,
                              price: ChefData![index].minPrice!, 
                              shortDisc: ChefData![index].bussinessDetails!.shortDescription!,
                               experties: ChefData![index].typesOfFood!, 
                               add1: ChefData![index].bussinessDetails!.addressLine1!,
                                add2: ChefData![index].bussinessDetails!.addressLine2!, 
                              
                                foodName: ChefData![index].typesOfFood!, 
                                vendorID: ChefData![index].vendorId!,
                                 noPerson: widget.noPerson, sCity: widget.sCity, sDate: widget.sDate, sType: widget.sType,
                              
                            );
                            //  vendorIDRestaurent = ResaturentData![0].vendorId!;
                          }),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Future fetchChefData() async {
    setState(() {
      isChefLoading = true;
    });
    try {
      ChefData = await EatServiceAPIs().getChefData(
        widget.sDate,
        widget.sType,
        widget.sCity,
        widget.noPerson,
        "Privatechef",
      );

      print("Chef DATA =====" + ChefData![0].bussinessDetails!.bussinessName!);
    } catch (e) {
      print("Error DATA =====" + e.toString());
    }

    setState(() {
      isChefLoading = false;
    });
  }
}
