import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/screens/services/eat/eat_payment.dart';

class ChefCheckOut extends StatefulWidget {
  final String date;
  final String time;
  final String guestCount;
  final String address;

  const ChefCheckOut(
      {Key? key,
      required this.date,
      required this.time,
      required this.guestCount,
      required this.address})
      : super(key: key);

  @override
  _ChefCheckOutState createState() => _ChefCheckOutState();
}

class _ChefCheckOutState extends State<ChefCheckOut> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: bgcolor,
        appBar: CustomAppBar(
            lead: BackButton(
              color: Colors.white,
            ),
            title: Text(
              "Checkout",
              style: kHeadText,
            )),
        bottomNavigationBar: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.all(18.0),
          height: 75.0,
          color: servicecardfillcolor,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Total",
                    style: TextStyle(color: Colors.white),
                  ),
                  Text(
                    "\$100",
                    style: kTxtStyle,
                  ),
                ],
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (_) => EatPayment()));
                },
                child: Text(
                  "Book Now",
                  style: TextStyle(color: Colors.black),
                ),
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  fixedSize: Size(108.0, 40.0),
                  primary: Color(0xFFFFC71E),
                ),
              ),
            ],
          ),
        ),
        body: Container(
          margin: EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Date & Time",
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                  color: greytxtcolor,
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                widget.date + " | " + widget.time,
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.white,
                ),
              ),
              SizedBox(
                height: 30.0,
              ),
              Text(
                "Total Guest(s)",
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                  color: greytxtcolor,
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                widget.guestCount,
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.white,
                ),
              ),
              SizedBox(
                height: 30.0,
              ),
              Text(
                "Address",
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                  color: greytxtcolor,
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                widget.address,
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.white,
                ),
              ),
              SizedBox(
                height: 30.0,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
