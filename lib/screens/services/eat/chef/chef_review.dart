import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/review_container_custom.dart';

class ChefReview extends StatefulWidget {
  const ChefReview({Key? key}) : super(key: key);

  @override
  _ChefReviewState createState() => _ChefReviewState();
}

class _ChefReviewState extends State<ChefReview> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: bgcolor,
        appBar: CustomAppBar(
          lead: BackButton(),
          title: Text(
            "Review",
            style: kTxtStyle,
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.fromLTRB(0, 20.0, 0, 10.0),
            child: Column(
              children: [
                ReviewCustomContainer(),
                ReviewCustomContainer(),
                ReviewCustomContainer(),
                ReviewCustomContainer(),
                ReviewCustomContainer(),
                ReviewCustomContainer(),
                ReviewCustomContainer(),
                ReviewCustomContainer(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
