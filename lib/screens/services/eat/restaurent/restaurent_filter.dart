import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/screens/services/eat/restaurent/restaurent_listing_scr.dart';
String selectedDateRestau = '';
  String selectedTimeRestau = '';
class RestaurentSearchFilter extends StatefulWidget {
  final String? eat_type;

  const RestaurentSearchFilter({Key? key, this.eat_type}) : super(key: key);

  @override
  _RestaurentSearchFilterState createState() => _RestaurentSearchFilterState();
}

List<String> dishList = ["INDIAN", "CHINESE", "ITALIAN", "THAI", "SEA FOOD"];
DropdownMenuItem<String> buildMenuItem(String item) =>
    DropdownMenuItem(value: item, child: Text(item));

class _RestaurentSearchFilterState extends State<RestaurentSearchFilter> {
  TextEditingController nooftableControler = TextEditingController();
  DateTime dateTime = DateTime.now();
  TimeOfDay time = TimeOfDay.now();
  
  String? selectedDish = 'INDIAN';

  bool dish1 = false;
  bool dish2 = false;
  bool dish3 = false;

  @override
  Widget build(BuildContext context) {
    // return showDialog(
    //                               context: context,
    //                               barrierColor: bgcolor.withOpacity(0.97),
    //                               builder: (BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: bgcolor,
        appBar: AppBar(
          elevation: 0.0,
          leading: BackButton(
            color: Colors.white,
          ),
        ),
        bottomNavigationBar: GestureDetector(
          onTap: () async {
            setState(() {
              print(selectedDateRestau);
              print(selectedTimeRestau);

              print(selectedDish);
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (_) => RestaurantListing(
                          selectedDate: selectedDateRestau,
                          selectedDish: selectedDish!,
                          selectedTime: selectedTimeRestau,
                          eat_type: widget.eat_type == null ? '' : widget.eat_type!,)));
            });
          },
          child: Container(
            alignment: Alignment.center,
            child: Text(
              "SEARCH NOW",
              style: TextStyle(
                  fontFamily: 'Helvetica',
                  fontSize: 18.0,
                  letterSpacing: 0.7,
                  color: Color(0xff121A31)),
            ),
            height: 57.0,
            color: btngoldcolor,
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Search For Restaurant And Book a Table",
                  style: TextStyle(
                    fontSize: 25.0,
                    fontFamily: 'Raleway-Bold',
                    color: Colors.white
                  ),
                ),
                SizedBox(height: 30.0),
                Padding(
                  padding: const EdgeInsets.fromLTRB(10.0, 0, 0, 10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Image.asset(
                            "assets/icons/ic_calendar.png",
                            height: 20.0,
                            width: 20.0,
                            color: Colors.white,
                          ),
                          SizedBox(
                            width: 10.0,
                          ),
                          Text(
                            'Select Date',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 14.0,
                              letterSpacing: 0.8,
                              color: Colors.white,
                              fontFamily: 'Raleway-Bold',
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      InkWell(
                        onTap: () async {
                          DateTime? newDate = await showDatePicker(
                              context: context,
                              initialDate: dateTime,
                              firstDate: DateTime(2021),
                              lastDate: DateTime(2200));
                          if (newDate != null) {
                            setState(() {
                              dateTime = newDate;
                              selectedDateRestau =
                                  '${dateTime.day.toString().padLeft(2, '0')}-${dateTime.month.toString().padLeft(2, '0')}-${dateTime.year}';
                            });
                          }
                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 10.0),
                          height: 46.0,
                          width: MediaQuery.of(context).size.width,
                          child: Text(
                            selectedDateRestau,
                            textAlign: TextAlign.left,
                            style: kTxtStyle.copyWith(
                              fontSize: 16.0,
                              fontFamily: 'Regular',
                            ),
                          ),
                          decoration: BoxDecoration(
                            color: servicecardfillcolor,
                            border: Border.all(color: txtborderbluecolor),
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                     
                     
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children:const [
                              Icon(
                                Icons.access_time,
                                color: Colors.white,
                              ),
                              SizedBox(
                                width: 10.0,
                              ),
                              Text(
                                "Select Time",
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                  fontSize: 14.0,
                                  letterSpacing: 0.8,
                                  color: Colors.white,
                                  fontFamily: 'Raleway-Bold',
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          InkWell(
                            onTap: () async {
                              TimeOfDay? newTime = await showTimePicker(
                                context: context,
                                initialTime: time,
                              );
                              if (newTime != null) {
                                setState(() {
                                  time = newTime;
                                  selectedTimeRestau =
                                      '${time.hour.toString().padLeft(2, '0')}:${time.minute.toString().padLeft(2, '0')}  ';
                                });
                              }
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 10.0),
                              height: 46.0,
                              width: MediaQuery.of(context).size.width,
                              child: Text(
                                selectedTimeRestau,
                                textAlign: TextAlign.left,
                                style: kTxtStyle.copyWith(
                                  fontSize: 16.0,
                                  fontFamily: 'Regular',
                                ),
                              ),
                              decoration: BoxDecoration(
                                color: servicecardfillcolor,
                                border: Border.all(color: txtborderbluecolor),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                              ),
                            ),
                          ),
                        ],
                      ),
                     
                     
                      SizedBox(height: 25.0),
                      Row(
                        children: [
                          Icon(
                            Icons.fastfood_outlined,
                            color: Colors.white,
                          ),
                          SizedBox(
                            width: 10.0,
                          ),
                          Text(
                            "Dishes",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 14.0,
                              letterSpacing: 0.8,
                              color: Colors.white,
                              fontFamily: 'Raleway-Bold',
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 10.0, horizontal: 10.0),
                        height: 46.0,
                        width: MediaQuery.of(context).size.width,
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                              dropdownColor: servicecardfillcolor,
                              style: kTxtStyle.copyWith(fontSize: 16.0),
                              isExpanded: true,
                              alignment: AlignmentDirectional.center,
                              icon: Icon(
                                Icons.arrow_drop_down,
                                color: Colors.white,
                              ),
                              value: selectedDish,
                              items: dishList.map(buildMenuItem).toList(),
                              onChanged: (value) {
                                setState(() {
                                  selectedDish = value!;
                                });
                              }),
                        ),
                        decoration: BoxDecoration(
                          color: servicecardfillcolor,
                          border: Border.all(color: txtborderbluecolor),
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        ),
                      ),
                    ],
                  ),
                ),
                // Column(
                //   children: [
                //     CheckboxListTile(
                //       controlAffinity: ListTileControlAffinity.leading,
                //       activeColor: btngoldcolor,
                //       title: Text(
                //         "data",
                //         style: kTxtStyle,
                //       ),
                //       value: dish1,
                //       onChanged: (value) => setState(() => dish1 = value!),
                //     ),
                //     CheckboxListTile(
                //       controlAffinity: ListTileControlAffinity.leading,
                //       activeColor: btngoldcolor,
                //       title: Text(
                //         "data",
                //         style: kTxtStyle,
                //       ),
                //       value: dish2,
                //       onChanged: (value) => setState(() => dish2 = value!),
                //     ),
                //     CheckboxListTile(
                //       controlAffinity: ListTileControlAffinity.leading,
                //       activeColor: btngoldcolor,
                //       title: Text(
                //         "data",
                //         style: kTxtStyle,
                //       ),
                //       value: dish3,
                //       onChanged: (value) => setState(() => dish3 = value!),
                //     ),
                //   ],
                // ),
                // SizedBox(height: 25.0),
                // Padding(
                //   padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                //   child: Text(
                //     'Table Required',
                //     textAlign: TextAlign.left,
                //     style: TextStyle(
                //       fontSize: 14.0,
                //       letterSpacing: 0.8,
                //       color: Colors.white,
                //       fontWeight: FontWeight.bold,
                //     ),
                //   ),
                // ),
                // TextFormField(
                //   style: kTxtStyle,
                //   keyboardType: TextInputType.number,
                //   controller: nooftableControler,
                //   decoration: kTxtFieldDecoration,
                // ),
                // SizedBox(height: 25.0),
              ],
            ),
          ),
        ),
      ),
    );
    // });
  }
}
