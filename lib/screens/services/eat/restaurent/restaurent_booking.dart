import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/EatAPI/eat_service_api.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/custom_restaurent_card.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';
import 'package:lasvegas_gts_app/screens/services/eat/restaurent/restaurent_filter.dart';
import 'package:lasvegas_gts_app/utils/alert_utils.dart';

String? specialReq;

  TextEditingController guestController = TextEditingController();
  TextEditingController tableController = TextEditingController();
class RestaurentBookings extends StatefulWidget {
  final String vendorID, amount;

  const RestaurentBookings({Key? key, required this.vendorID, required this.amount}) : super(key: key);

  @override
  _RestaurentBookingsState createState() => _RestaurentBookingsState();
}

class _RestaurentBookingsState extends State<RestaurentBookings> {

  DateTime dateTime = DateTime.now();
  TimeOfDay time = TimeOfDay.now();
  String selectedDate = '';
  String selectedTime = '';

  bool isLoading = false;

  @override
  void initState() {
    selectedDate = selectedDateRestau;
    selectedTime = selectedTimeRestau;
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: bgcolor,
        appBar: CustomAppBar(
          lead: BackButton(),
          title: Text(
            "Booking Details",
            style: TextStyle(
              fontFamily: 'Raleway-Medium',
              fontSize: 20.0,
              color: Colors.white
            ),
          ),
        ),
        body: isLoading ? CustomLoadingScr() : SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 25.0),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Select Date',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 14.0,
                              letterSpacing: 0.8,
                              color: Colors.white,
                              fontFamily: 'Raleway-Bold',
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          InkWell(
                            onTap: () async {
                              DateTime? newDate = await showDatePicker(
                                  context: context,
                                  initialDate: dateTime,
                                  firstDate: DateTime(2021),
                                  lastDate: DateTime(2200));
                              if (newDate != null) {
                                setState(() {
                                  dateTime = newDate;
                                  selectedDate =
                                      '${dateTime.day.toString().padLeft(2, '0')}  -  ${dateTime.month.toString().padLeft(2, '0')}  -  ${dateTime.year}';
                                });
                              }
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 10.0),
                              height: 46.0,
                              width: 166.0,
                              child: Text(
                                selectedDate,
                                textAlign: TextAlign.center,
                                style: kTxtStyle.copyWith(
                                  fontSize: 16.0,
                                  fontFamily: 'Regular',
                                ),
                              ),
                              decoration: BoxDecoration(
                                // color: servicecardfillcolor,
                                border: Border.all(color: txtborderbluecolor),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Select Time",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 14.0,
                              letterSpacing: 0.8,
                              color: Colors.white,
                              fontFamily: 'Raleway-Bold',
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          InkWell(
                            onTap: () async {
                              TimeOfDay? newTime = await showTimePicker(
                                context: context,
                                initialTime: time,
                              );
                              if (newTime != null) {
                                setState(() {
                                  time = newTime;
                                  selectedTime =
                                      '${time.hour.toString().padLeft(2, '0')}  :  ${time.minute.toString().padLeft(2, '0')}  ';
                                });
                              }
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 10.0),
                              height: 46.0,
                              width: 166.0,
                              child: Text(
                                selectedTime,
                                textAlign: TextAlign.center,
                                style: kTxtStyle.copyWith(
                                  fontSize: 16.0,
                                  fontFamily: 'Regular',
                                ),
                              ),
                              decoration: BoxDecoration(
                                // color: servicecardfillcolor,
                                border: Border.all(color: txtborderbluecolor),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(height: 25.0),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                  child: Text(
                    'Total number of guests',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 14.0,
                      letterSpacing: 0.8,
                      color: Colors.white,
                      fontFamily: 'Raleway-Bold',
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                TextFormField(
                  controller: guestController,
                  style: kTxtStyle,
                  keyboardType: TextInputType.number,
                  // controller: vendorPhoneNoController,
                  decoration: kTxtFieldDecoration,
                ),
                 SizedBox(height: 25.0),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                  child: Text(
                    'Total number of Tables',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 14.0,
                      letterSpacing: 0.8,
                      color: Colors.white,
                      fontFamily: 'Raleway-Bold',
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                TextFormField(
                  controller: tableController,
                  style: kTxtStyle,
                  keyboardType: TextInputType.number,
                  // controller: vendorPhoneNoController,
                  decoration: kTxtFieldDecoration,
                ),
               
                SizedBox(height: 25.0),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                  child: Text(
                    'Special Request',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 14.0,
                      letterSpacing: 0.8,
                      color: Colors.white,
                      fontFamily: 'Raleway-Bold',
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                TextFormField(
                  style: kTxtStyle,
                  maxLines: 2,
                  // controller: vendorAddressController,
                  keyboardType: TextInputType.multiline,
                  decoration: kTxtFieldDecoration,
                ),
                SizedBox(height: 25.0),
                ElevatedButton(

                  onPressed: () async {

                    checkValidation();

                    // Navigator.push(
                    //   context,
                    //   MaterialPageRoute(
                    //     builder: (context) =>VendorBusinessDetails() ,
                    //   ),
                    // );
                  },
                  child: Text(
                    'CONTINUE',
                    style: TextStyle(
                      fontFamily: 'Raleway-Regular',
                        fontSize: 16.0,
                        color: Colors.black),
                  ),
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(13.0),
                    ),
                    fixedSize: Size(389.0, 57.0),
                    primary: Color(0xFFFFC71E),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future checkValidation() async{

    Map<String, dynamic>? map;

    print('AMOUNT ${widget.amount}');

    if(selectedDate.isEmpty){
      AlertUtils.showAutoCloseDialogue(context, "Please select Date", 2, 'Required field');
    }else if(selectedTime.isEmpty){
      AlertUtils.showAutoCloseDialogue(context, "Please select Time", 2, 'Required field');
    }else if(guestController.text.isEmpty){
      AlertUtils.showAutoCloseDialogue(context, "Please enter total guest", 2, 'Required field');
    }else {
      setState(() {
        isLoading = true;
      });
      map = await EatServiceAPIs().bookRestaurent(
          selectedDate,
          selectedTime,
          tableController.text,
          vendorIDRestaurent,
          homeUserName,
          widget.amount);

      if(map != null){
        if(map['status_code'] == 200){


          int count = 0;
          Navigator.popUntil(context, (route) {
            return count++ == 5;
          });

          AlertUtils.showAutoCloseDialogue(context, "Eat Booking successfully", 3, 'Successful');

        }else {
          AlertUtils.showAutoCloseDialogue(context, map['message'], 3, 'Oops!');
        }
      }else {
        AlertUtils.showAutoCloseDialogue(context, "Try Again", 3, 'Oops!');
      }
      setState(() {
        isLoading = true;
      });
    }
  }
}
