import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/EatAPI/eat_service_api.dart';
import 'package:lasvegas_gts_app/Networking/EatAPI/restaurent_list_model.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/custom_catering_card.dart';
import 'package:lasvegas_gts_app/customs/custom_chef_card.dart';
import 'package:lasvegas_gts_app/customs/custom_restaurent_card.dart';
import 'package:flutter_awesome_select/flutter_awesome_select.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'package:lasvegas_gts_app/screens/services/eat/catering/catering_filter.dart';
import 'package:lasvegas_gts_app/screens/services/eat/restaurent/restaurent_filter.dart';

bool isRestaurantLoading = false;
List<ServiceRes>? ResaturentData = [];

class RestaurantListing extends StatefulWidget {
  final String selectedDate, selectedTime, selectedDish, eat_type;

  //for Catering
  final String? city_code, noPerson, services;
  const RestaurantListing(
      {Key? key,
      required this.selectedDate,
      required this.selectedTime,
      required this.eat_type,
      required this.selectedDish,
      this.city_code,
      this.services,
      this.noPerson})
      : super(key: key);

  @override
  _RestaurantListingState createState() => _RestaurantListingState();
}

bool isRestaurants = true;
bool isCatering = false;
bool isPvtChef = false;

class _RestaurantListingState extends State<RestaurantListing> {
  @override
  void initState() {

    print('TYPE:=== ${widget.eat_type}');

    if(widget.eat_type == 'Restaurant'){
      getRestaurentData();
    }else {
      getCateringData();
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
          lead: Image.asset(
            "assets/icons/ic_eat.png",
            color: Colors.white,
          ),
          title: Text(
            "Eat",
            style: TextStyle(
                color: Colors.white,
                fontFamily: 'Raleway-SemiBold',
                fontSize: 20.0
            ),
          ),
        ),
        backgroundColor: bgcolor,
        body: isRestaurantLoading
            ? CustomLoadingScr()
            : SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 10.0,
                      ),

                      TextFormField(
                        decoration: kSearchTxtFieldDecoration.copyWith(
                          fillColor: servicecardfillcolor,
                          filled: true,
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      RestaurentContainer(
                        selectedDate: widget.selectedDate,
                        selectedTime: widget.selectedTime,
                      ),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Future getCateringData() async{
    setState(() {
      isRestaurantLoading = true;
    });
    try {
      ResaturentData = (await EatServiceAPIs().getChefData01(
          widget.selectedDate, widget.selectedDish, widget.city_code!, widget.noPerson!, widget.services!));

      print("Catering DATA =====" +
          ResaturentData![0].bussinessDetails!.bussinessName!);
    } catch (e) {
      print("Error DATA =====" + e.toString());
    }

    setState(() {
      isRestaurantLoading = false;
    });
  }

  Future getRestaurentData() async {
    setState(() {
      isRestaurantLoading = true;
    });
    try {
      ResaturentData = await EatServiceAPIs().getRestaurentData(
          widget.selectedDate, widget.selectedTime, widget.selectedDish);

      print("Restaurent DATA =====" +
          ResaturentData![0].bussinessDetails!.bussinessName!);
    } catch (e) {
      print("Error DATA =====" + e.toString());
    }

    setState(() {
      isRestaurantLoading = false;
    });
  }
}

class RestaurentContainer extends StatefulWidget {
  final String selectedDate, selectedTime;

  const RestaurentContainer(
      {Key? key, required this.selectedDate, required this.selectedTime})
      : super(key: key);
  @override
  State<RestaurentContainer> createState() => _RestaurentContainerState();
}

class _RestaurentContainerState extends State<RestaurentContainer> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 5.0,
        ),
        Text(
          "Our Recommended Restaurants",
          style: TextStyle(
              color: Colors.white,
              fontSize: 18.0,
              fontFamily: 'Raleway-Bold'),
        ),
        SizedBox(
          height: 5.0,
        ),
        ListView.builder(
            physics: NeverScrollableScrollPhysics(
                parent: AlwaysScrollableScrollPhysics()),
            shrinkWrap: true,
            itemCount: ResaturentData!.length,
            itemBuilder: (_, index) {
              return CustomRestaurentCard(
                add1: ResaturentData![index].bussinessDetails!.addressLine1!,
                restaurentName:
                    ResaturentData![index].bussinessDetails!.bussinessName!,
                tableCapacity: ResaturentData![index].capacity!,
                tablePhoto: ResaturentData![index].tableImages!,
                tablePrice: ResaturentData![index].charges!,
                tableQuantity: ResaturentData![index].quantity!,
                typeOfFood: ResaturentData![index].typesOfFood!,
                // noGuest: widget.noGuest,
                sDate: widget.selectedDate,
                sTime: widget.selectedTime,
                add2: ResaturentData![index].bussinessDetails!.addressLine2!,
                shortDisc:
                    ResaturentData![index].bussinessDetails!.shortDescription!,
                vendorID: ResaturentData![index].vendorId!,
                logo: ResaturentData![index].bussinessDetails!.bussinessLogo!,
              );
              //  vendorIDRestaurent = ResaturentData![0].vendorId!;
            }),
        // ,
        // ,

        // CustomRestaurentCard(),
      ],
    );
  }
}
