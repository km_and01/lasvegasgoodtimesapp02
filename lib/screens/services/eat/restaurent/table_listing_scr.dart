import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/screens/services/eat/restaurent/restaurent_overview.dart';

String tableImgURL = 'http://koolmindapps.com/lasvegas/public/images/eat/';

class RestaurentTables extends StatefulWidget {
  final List<String> tableCapacity;
  final List<String> tableQuantity;
  final List<String> typeOfFood;
  final List<String> tablePrice;
  final List<String> tablePhoto;
  final String selectedDate, selectedTime;
  final String restauName, add1, add2, shortDisc,vendorID;
  const RestaurentTables({
    Key? key,
    required this.tableCapacity,
    required this.tableQuantity,
    required this.typeOfFood,
    required this.tablePrice,
    required this.tablePhoto,
    required this.selectedDate,
    required this.selectedTime,
    required this.restauName,
    required this.add1,
    required this.add2,
    required this.shortDisc, required this.vendorID,
  }) : super(key: key);

  @override
  _RestaurentTablesState createState() => _RestaurentTablesState();
}

class _RestaurentTablesState extends State<RestaurentTables> {
  @override
  Widget build(BuildContext context) {
    print(widget.tableCapacity.length);
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
          lead: BackButton(),
          title: Text(
            "Select Tables",
            style: TextStyle(
              fontFamily: 'Raleway-SemiBold',
              fontSize: 20.0,
              color: Colors.white
            ),
          ),
        ),
        backgroundColor: bgcolor,
        body: SingleChildScrollView(
          child: ListView.builder(
              physics: NeverScrollableScrollPhysics(
                  parent: AlwaysScrollableScrollPhysics()),
              shrinkWrap: true,
              itemCount: widget.tableCapacity.length,
              itemBuilder: (_, index) {
                return CustomTableDisplayCard(
                  tableCapacity: widget.tableCapacity[index],
                  tablePhoto: widget.tablePhoto[0],
                  tablePrice: widget.tablePrice[index],
                  tableQuantity: widget.tableQuantity[index],
                  typeOfFood: widget.typeOfFood[0],
                  add1: widget.add1,
                  add2: widget.add2,
                  // noGuest: widget.noGuest,
                  restauName: widget.restauName,
                  sDate: widget.selectedDate,
                  shoetDisc: widget.shortDisc,
                  sTime: widget.selectedTime, 
                   vendorID: widget.vendorID,
                );
              }),
        ),
      ),
    );
  }
}

class CustomTableDisplayCard extends StatefulWidget {
  final String tableCapacity;
  final String tableQuantity;
  final String typeOfFood;
  final String tablePrice;
  final String tablePhoto;
  final String add1;
  final String add2;
  final String restauName;
  final String shoetDisc;
  // final String noGuest;
  final String sDate;
  final String sTime;
  final String  vendorID;

  const CustomTableDisplayCard({
    Key? key,
    required this.tableCapacity,
    required this.tableQuantity,
    required this.typeOfFood,
    required this.tablePrice,
    required this.tablePhoto,
    required this.add1,
    required this.add2,
    required this.restauName,
    required this.shoetDisc,
    // required this.noGuest,
    required this.sDate,
    required this.sTime,  required this.vendorID,
  }) : super(key: key);

  @override
  State<CustomTableDisplayCard> createState() => _CustomTableDisplayCardState();
}

class _CustomTableDisplayCardState extends State<CustomTableDisplayCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20.0),
      alignment: Alignment.center,
      padding: EdgeInsets.fromLTRB(20.0, 20.0, 15.00, 20),
      child: Column(
        //
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(
                    widget.tableCapacity + ' Seater Table',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0,
                      fontFamily: 'Manrope-SemiBold'
                    ),
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Text(
                    widget.tableQuantity + " Tables Available",
                    style: TextStyle(
                        fontFamily: 'Manrope_Medium',
                        fontSize: 12.0,
                        color: greytxtcolor),
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Text(
                    "Types of Food : " + widget.typeOfFood,
                    style: TextStyle(
                        fontFamily: 'Manrope_Medium',
                        fontSize: 12.0,
                        color: greytxtcolor),
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                ],
              ),
              Container(
                  margin: EdgeInsets.all(5.0),
                  child: Image.network(
                    tableImgURL + widget.tablePhoto,
                    height: 108,
                    width: 108,
                    fit: BoxFit.fill,

                  )),
            ],
          ),
          SizedBox(
            height: 25.0,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "\$ " + widget.tablePrice,
                    style: TextStyle(
                      fontSize: 20.0,
                      fontFamily: 'Manrope-SemiBold',
                      color: Colors.white
                    ),
                  ),
                ],
              ),
              ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) => RestaurentOverview(
                                add1: widget.add1,
                                add2: widget.add2,
                                restaurentName: widget.restauName,
                                shortDiscription: widget.shoetDisc,
                                selectedDate: widget.sDate,
                                selectedTime: widget.sTime,
                                amount: widget.tablePrice,
                                vendorID: widget.vendorID,
                                tablePhoto: widget.tablePhoto,
                              )));
                },
                child: Text(
                  "Book Now",
                  style: TextStyle(
                      fontFamily: 'Raleway-Medium',
                      fontSize: 14.0,
                      color: Color(0xff121A31)),
                ),
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  fixedSize: Size(108.0, 40.0),
                  primary: Color(0xFFFFC71E),
                ),
              ),
            ],
          ),
        ],
      ),
      decoration: BoxDecoration(
        color: Colors.transparent,
        border: Border.all(color: txtborderbluecolor),
        borderRadius: BorderRadius.all(Radius.circular(15.0)),
      ),
    );
  }
}
