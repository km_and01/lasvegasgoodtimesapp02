// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_card_static.dart';
import 'package:lasvegas_gts_app/customs/custom_nav_for_restaurant.dart';
import 'package:lasvegas_gts_app/screens/services/eat/restaurent/restaurent_booking.dart';
import 'package:lasvegas_gts_app/screens/services/eat/restaurent/restaurent_review.dart';
import 'package:lasvegas_gts_app/screens/services/eat/restaurent/table_listing_scr.dart';

String vendor_id = '';
String amount = '';

class RestaurentOverview extends StatefulWidget {
  final String restaurentName, shortDiscription, add1, add2;
  final String selectedDate, selectedTime;
  final String vendorID, amount;
  final String? tablePhoto;
  const RestaurentOverview(
      {Key? key,
      required this.restaurentName,
      required this.shortDiscription,
      required this.add1,
      required this.add2,
      required this.selectedDate,
      required this.selectedTime,
      required this.vendorID,
      required this.amount,
      this.tablePhoto})
      : super(key: key);

  @override
  _RestaurentOverviewState createState() => _RestaurentOverviewState();
}

class _RestaurentOverviewState extends State<RestaurentOverview> {
  int selectedItem = 0;

  var screens = [];

  @override
  void initState() {
    setState(() {
      vendor_id = widget.vendorID;
      amount = widget.amount;
    });

    screens = [
      RestaurentOverview(
        add1: '',
        add2: '',
        restaurentName: '',
        shortDiscription: '',
        selectedDate: '',
        selectedTime: '',
        amount: '',
        vendorID: '',
      ),
      RestaurentBookings(
        amount: amount,
        vendorID: vendor_id,
      ),
      RestaurentReview(),
    ];
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgcolor,
      bottomNavigationBar: CustomNavBarForRestaurents(
        defaultSelectedIndex: 0,
        onChange: (val) {
          setState(() {
            selectedItem = val;
            print(selectedItem);
            screens[val];
          });
        },
      ),
      body: selectedItem == 0
          ? SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                      color: Colors.white,
                      child: Image.network(tableImgURL + widget.tablePhoto!,
                          width: MediaQuery.of(context).size.width,
                          fit: BoxFit.fill,),
                      height: 200.0,
                      width: MediaQuery.of(context).size.width),
                  Container(
                    margin: EdgeInsets.all(15.0),
                    padding: EdgeInsets.all(3.0),
                    alignment: Alignment.topLeft,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.restaurentName,
                          style: TextStyle(
                            fontFamily: 'Raleway-Medium',
                            fontSize: 18.0,
                            color: Colors.white
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        RatingBar.builder(
                          glow: false,
                          itemSize: 15.0,
                          initialRating: 3,
                          minRating: 1,
                          direction: Axis.horizontal,
                          allowHalfRating: true,
                          itemCount: 5,
                          //itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                          itemBuilder: (context, _) => Icon(
                            Icons.star,
                            color: Colors.amber,
                            //   Image.asset(
                            // "assets/icons/rating_star.png",
                            // color: Colors.amber,
                          ),
                          // ),
                          unratedColor: Colors.grey,
                          onRatingUpdate: (rating) {
                            print(rating);
                          },
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          widget.add1 + ', ' + widget.add2,
                          style: TextStyle(
                            fontFamily: 'Manrope',
                            color: Colors.white
                          ),
                        ),
                      ],
                    ),
                  ),
                  Divider(
                    height: 1,
                    color: greytxtcolor,
                    thickness: 1.0,
                  ),
                  Container(
                    margin: EdgeInsets.all(15.0),
                    padding: EdgeInsets.all(3.0),
                    alignment: Alignment.topLeft,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Restaurants details",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18.0,
                              fontFamily: 'Raleway-Bold'),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          widget.shortDiscription,
                          // """The Hotel Pennsylvania is conveniently located within one block of the Empire State Building,Madison Square Garden, Penn Station and Macy's flagship store. Located within walking distance are Times Square, the Broadway Theatre District, New York Public Library, Jacob K. Javits Convention Center, and two of New York's most iconic…""",
                          style: TextStyle(
                            fontFamily: 'Raleway',
                              color: greytxtcolor,
                            fontSize: 14.0
                          ),
                        ),
                        // SizedBox(height: 40.0),
                        // Text(
                        //   "Main Amenities",
                        //   style: TextStyle(
                        //       fontSize: 18.0,
                        //       fontWeight: FontWeight.bold,
                        //       color: Colors.white),
                        // ),
                        // SizedBox(
                        //   height: 15.0,
                        // ),
                        // SingleChildScrollView(
                        //   scrollDirection: Axis.horizontal,
                        //   child: Row(
                        //     children: const [
                        //       CoustomCard2(icon: 'ic_wifi', title: 'Free Wifi'),
                        //       CoustomCard2(
                        //           icon: 'ic_breakfasst', title: 'Breakfast'),
                        //       CoustomCard2(icon: 'ic_spa', title: 'Spa'),
                        //       CoustomCard2(
                        //           icon: 'ic_swiming', title: 'Swimming'),
                        //     ],
                        //   ),
                        // ),
                        SizedBox(
                          height: 30.0,
                        ),
                        Text(
                          "Contact",
                          style: TextStyle(
                              fontSize: 18.0,
                              fontFamily: 'Raleway-SemiBold',
                              color: Colors.white),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          widget.add1 + ', ' + widget.add2,
                          style: kTxtStyle.copyWith(fontFamily: 'Raleway', color: greytxtcolor),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        /*Container(
                          child: Image.asset("assets/images/banner_img_2.png"),
                          height: 215.0,
                          width: 380.0,
                          color: bgcolor,
                        ),*/
                      ],
                    ),
                  )
                ],
              ),
            )
          : screens[selectedItem],
    );
  }
}
