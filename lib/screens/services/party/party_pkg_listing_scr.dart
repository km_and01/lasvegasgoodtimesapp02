
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/EventAPI/event_service_api.dart';
import 'package:lasvegas_gts_app/Networking/PartyAPI/party_api.dart';
import 'package:lasvegas_gts_app/Networking/PartyAPI/party_pkg_model.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/custom_event_pkg_card.dart';
import 'package:lasvegas_gts_app/customs/custom_party_pkg_card.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';

bool isPartyPkgLoading = false;
List<PartPkg> PartyPkgData = [];
List<String> pkgIMGList = [];
String partyBusinessLOGO =
    "http://koolmindapps.com/lasvegas/public/images/business_detail/";

class PartyPkgListingScr extends StatefulWidget {
  final String partyVendorID;
  final String add1, add2, bname;
  final List partyBIMGS;
  const PartyPkgListingScr(
      {Key? key, required this.partyVendorID, required this.partyBIMGS,
       required this.add1, required this.add2, required this.bname})
      : super(key: key);

  @override
  State<PartyPkgListingScr> createState() => _PartyPkgListingScrState();
}

class _PartyPkgListingScrState extends State<PartyPkgListingScr> {
  @override
  void initState() {
    int len = widget.partyBIMGS.length;
    pkgIMGList.clear();
    for (var i = 0; i < len; i++) {
      pkgIMGList.add(partyBusinessLOGO + widget.partyBIMGS[i]);
    }

    // TODO: implement initState
    fetchEventPkgData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
            lead: Image.asset(
              "assets/icons/ic_party.png",
              color: Colors.white,
            ),
            title: Text(
              "Party",
              style: TextStyle(
                  fontSize: 20.0,
                  fontFamily: 'Helvetica',
                  color: Colors.white
              ),
            )),
        backgroundColor: bgcolor,
        body: isPartyPkgLoading
            ? CustomLoadingScr()
            : SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 20),
                      CarouselSlider(
                        items: pkgIMGList
                            .map(
                              (x) => Container(
                                width: double.infinity,
                                decoration: BoxDecoration(
                                  //  color: Colors.yellow,
                                  borderRadius: BorderRadius.circular(8.0),
                                  image: DecorationImage(
                                    fit: BoxFit.fill,
                                    image: NetworkImage(x, scale: 1),
                                  ),
                                ),
                              ),
                            )
                            .toList(),
                        // autoPlay: true,
                        // height: 200.0,
                        options: CarouselOptions(
                          height: 200,
                          aspectRatio: 16 / 9,
                          viewportFraction: 0.9,
                          initialPage: 0,
                          enableInfiniteScroll: true,
                          reverse: false,
                          autoPlay: true,
                          autoPlayInterval: Duration(seconds: 3),
                          autoPlayAnimationDuration:
                              Duration(milliseconds: 800),
                          autoPlayCurve: Curves.fastOutSlowIn,
                          enlargeCenterPage: true,
                          // onPageChanged: callbackFunction,
                          scrollDirection: Axis.horizontal,
                        ),
                      ),
                      SizedBox(
                        height: 25.0,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children:[
                           Text(
                        widget.bname,
                        style: TextStyle(
                          fontFamily: 'Raleway-Bold',
                          fontSize: 22.0,
                          color: Colors.white
                        ),
                      ),
                      Text(
                        widget.add1,// + ", " + widget.add2,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16.0,
                          fontFamily: 'Manrope_Medium',),
                      ),
                        ]
                      ),
                       SizedBox(
                        height: 20.0,
                      ),
                      // Text(
                      //   "Ongoing Events : ",
                      //   style: TextStyle(
                      //       color: Colors.white,
                      //       fontSize: 18.0,
                      //       fontWeight: FontWeight.bold),
                      // ),
                      // SizedBox(
                      //   height: 20.0,
                      // ),
                      ListView.builder(
                          physics: NeverScrollableScrollPhysics(
                              parent: AlwaysScrollableScrollPhysics()),
                          shrinkWrap: true,
                          itemCount: PartyPkgData.length,
                          itemBuilder: (_, index) {
                            return CustomPartyPackageCard(
                              packageIMG: PartyPkgData[index].images![0],
                              packageName: PartyPkgData[index].name!,
                              eventName: PartyPkgData[index].serviceType!,
                              priceUnit: PartyPkgData[index].priceUnit!,
                              packagePrice: PartyPkgData[index].price!,
                              packageDiscPrice: PartyPkgData[index].price!,
                              add1: PartyPkgData[index]
                                  .bussinessDetails!
                                  .addressLine1!,
                              add2: PartyPkgData[index]
                                  .bussinessDetails!
                                  .addressLine2!,
                              fullDetails: PartyPkgData[index]
                                  .bussinessDetails!
                                  .shortDescription!,
                              pkgIMGList: PartyPkgData[index].images!,
                              pkgVendorID: PartyPkgData[index].vendorId!,
                              capacity: PartyPkgData[index].maxPersonAllowed!, 
                              serviceID: PartyPkgData[index].id!.toString(), 
                              date: PartyPkgData[index].showDate!, 
                              pkgShortDetails: PartyPkgData[index].details!, 
                              partyID: PartyPkgData[index].id.toString(), 
                            );
                          }),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Future fetchEventPkgData() async {
    setState(() {
      isPartyPkgLoading = true;
    });
    try {
      PartyPkgData = await PartyAPIs().getPartyPkgData(widget.partyVendorID);
      print("PKG EVENT DATA =====" + PartyPkgData[0].name!);
      print("PKG EVENT LENGTH =====" + PartyPkgData.length.toString());
    } catch (e) {
      print("Error DATA =====" + e.toString());
    }

    setState(() {
      isPartyPkgLoading = false;
    });
  }
}
