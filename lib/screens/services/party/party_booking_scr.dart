import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/PartyAPI/party_api.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/custom_party_pkg_card.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';
import 'package:lasvegas_gts_app/screens/services/fun/fun_main_scr.dart';
import 'package:lasvegas_gts_app/screens/services/party/party_main_scr.dart';
import 'package:lasvegas_gts_app/utils/alert_utils.dart';

String? specialReq;

TextEditingController guestController = TextEditingController();
TextEditingController hrsController = TextEditingController();
TextEditingController uNameController = TextEditingController();
TextEditingController uEmailController = TextEditingController();
TextEditingController uMobController = TextEditingController();
String bookMsg = '';

class PartyBookings extends StatefulWidget {
  final String vendorID, amount, pkgIMG, pkgName, partyID;

  const PartyBookings({
    Key? key,
    required this.vendorID,
    required this.amount,
    required this.pkgIMG,
    required this.pkgName, 
    required this.partyID,
  }) : super(key: key);

  @override
  _PartyBookingsState createState() => _PartyBookingsState();
}

class _PartyBookingsState extends State<PartyBookings> {
  DateTime dateTime = DateTime.now();
  TimeOfDay time = TimeOfDay.now();
  String selectedDate = '';
  String selectedTime = " ";

  bool isLoading = false;
  @override
  void initState() {
    selectedDate = selectedPartyDate;
    uMobController.text = homeUserPhone;
    uEmailController.text = homeUserEmail;
    uNameController.text = homeUserName;
    // selectedTime = selectedTimeES;
    // TODO: implement initState
    super.initState();
  }

  Future validationCheck() async{

    if(selectedDate.isEmpty){
      AlertUtils.showAutoCloseDialogue(context, "Please select date", 2, 'Required field');
    }else if(guestController.text.isEmpty){
      AlertUtils.showAutoCloseDialogue(context, "Please enter number of person", 2, 'Required field');
    }else {

      setState(() {
        isLoading = true;
      });

      bookMsg = await PartyAPIs().bookEvent(
          widget.vendorID,
          partyLogoUrl + widget.pkgIMG,
          widget.partyID,
          guestController.text,
          widget.amount,
          selectedDate);

      if(bookMsg.contains('Party Services Booked successfully.')){
        int count = 0;
        Navigator.popUntil(context, (route) {
          return count++ == 5;
        });

        AlertUtils.showAutoCloseDialogue(context, "Party Booking successfully", 3, 'Successful');
      }else {
        AlertUtils.showAutoCloseDialogue(context, "Try Again", 3, 'Oops!');
      }

      setState(() {
        isLoading = false;
      });
    }

  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar: ElevatedButton(
          onPressed: () async {

            validationCheck();
            
          },
          child: Text(
            'CONTINUE',
            style: TextStyle(
                fontFamily: 'Helvetica',
                color: Color(0xff121A31),
                fontSize: 16.0,
                letterSpacing: 0.7),
          ),
          style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(13.0),
            ),
            fixedSize: Size(389.0, 57.0),
            primary: Color(0xFFFFC71E),
          ),
        ),
        backgroundColor: bgcolor,
        appBar: CustomAppBar(
          lead: BackButton(),
          title: Text(
            widget.pkgName == '' || widget.pkgName == null ? "Booking Details" : widget.pkgName,
            style: TextStyle(
                fontSize: 20.0,
                fontFamily: 'Helvetica',
                color: Colors.white
            ),
          ),
        ),
        body: isLoading ? CustomLoadingScr() : SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 25.0),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      ///// Date
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Select Date',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 14.0,
                              letterSpacing: 0.8,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          InkWell(
                            onTap: () async {
                              DateTime? newDate = await showDatePicker(
                                  context: context,
                                  initialDate: dateTime,
                                  firstDate: DateTime(2021),
                                  lastDate: DateTime(2200));
                              if (newDate != null) {
                                setState(() {
                                  dateTime = newDate;
                                  selectedDate =
                                      '${dateTime.day.toString().padLeft(2, '0')}-${dateTime.month.toString().padLeft(2, '0')}-${dateTime.year}';
                                });
                              }
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 10.0),
                              height: 46.0,
                              width: 166.0,
                              child: Text(
                                selectedDate,
                                textAlign: TextAlign.center,
                                style: kTxtStyle.copyWith(
                                  fontSize: 16.0,
                                  fontFamily: 'Regular',
                                ),
                              ),
                              decoration: BoxDecoration(
                                // color: servicecardfillcolor,
                                border: Border.all(color: txtborderbluecolor),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                              ),
                            ),
                          ),
                        ],
                      ),

                      //////Amount
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Price",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 14.0,
                              letterSpacing: 0.8,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 10.0),
                            height: 46.0,
                            width: 166.0,
                            child: Text(
                              widget.amount,
                              textAlign: TextAlign.center,
                              style: kTxtStyle.copyWith(
                                fontSize: 16.0,
                                fontFamily: 'Regular',
                              ),
                            ),
                            decoration: BoxDecoration(
                              // color: servicecardfillcolor,
                              border: Border.all(color: txtborderbluecolor),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(height: 25.0),

                // //////City
                // Padding(
                //   padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                //   child: Text(
                //     'City',
                //     textAlign: TextAlign.left,
                //     style: TextStyle(
                //       fontSize: 14.0,
                //       letterSpacing: 0.8,
                //       color: Colors.white,
                //       fontWeight: FontWeight.bold,
                //     ),
                //   ),
                // ),
                // Container(
                //   padding:
                //       EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                //   height: 46.0,
                //   // width: 166.0,
                //   child: Text(
                //     FunCitytSelected,
                //     textAlign: TextAlign.center,
                //     style: kTxtStyle.copyWith(
                //       fontSize: 16.0,
                //       fontFamily: 'Regular',
                //     ),
                //   ),
                //   decoration: BoxDecoration(
                //     // color: servicecardfillcolor,
                //     border: Border.all(color: txtborderbluecolor),
                //     borderRadius: BorderRadius.all(Radius.circular(10.0)),
                //   ),
                // ),
                // SizedBox(height: 25.0),

                //////Name user
                // Padding(
                //   padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                //   child: Text(
                //     'Name',
                //     textAlign: TextAlign.left,
                //     style: TextStyle(
                //       fontSize: 14.0,
                //       letterSpacing: 0.8,
                //       color: Colors.white,
                //       fontWeight: FontWeight.bold,
                //     ),
                //   ),
                // ),
                // TextFormField(
                //   controller: uNameController,
                //   style: kTxtStyle,
                //   keyboardType: TextInputType.number,
                //   // controller: vendorPhoneNoController,
                //   decoration: kTxtFieldDecoration,
                // ),
                // SizedBox(height: 25.0),

                // // //////Email
                // // Padding(
                // //   padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                // //   child: Text(
                // //     'Email-ID',
                // //     textAlign: TextAlign.left,
                // //     style: TextStyle(
                // //       fontSize: 14.0,
                // //       letterSpacing: 0.8,
                // //       color: Colors.white,
                // //       fontWeight: FontWeight.bold,
                // //     ),
                // //   ),
                // // ),
                // // TextFormField(
                // //   controller: uEmailController,
                // //   style: kTxtStyle,
                // //   keyboardType: TextInputType.number,
                // //   // controller: vendorPhoneNoController,
                // //   decoration: kTxtFieldDecoration,
                // // ),
                // // SizedBox(height: 25.0),

                // //////Phone
                // Padding(
                //   padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                //   child: Text(
                //     'Mobile Number',
                //     textAlign: TextAlign.left,
                //     style: TextStyle(
                //       fontSize: 14.0,
                //       letterSpacing: 0.8,
                //       color: Colors.white,
                //       fontWeight: FontWeight.bold,
                //     ),
                //   ),
                // ),
                // TextFormField(
                //   controller: uMobController,
                //   style: kTxtStyle,
                //   keyboardType: TextInputType.number,
                //   decoration: kTxtFieldDecoration,
                // ),
                // SizedBox(height: 25.0),

                //////no. person
                Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                  child: Text(
                    'Number of Person',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 14.0,
                      letterSpacing: 0.8,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                TextFormField(
                  controller: guestController,
                  style: kTxtStyle,
                  keyboardType: TextInputType.number,
                  decoration: kTxtFieldDecoration,
                ),
                SizedBox(height: 25.0),
],
            ),
          ),
        ),
      ),
    );
  }
}
