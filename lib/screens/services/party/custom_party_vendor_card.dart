import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_hotel_display_card.dart';
import 'package:lasvegas_gts_app/screens/services/evets/event_pkg_listing_scr.dart';
import 'package:lasvegas_gts_app/screens/services/party/party_pkg_listing_scr.dart';

String vendorPartyID = '';
String servicePartyID = '';

class CustomPartyVendorCard extends StatefulWidget {
  final String shortDisc;
  final String vendorID,add1,add2;
  final String PartyVendorIMG, bname, price, priceUnit;
  final List VendorIMGs;

  const CustomPartyVendorCard({
    Key? key,
    required this.vendorID,
    required this.PartyVendorIMG,
    required this.bname,
    required this.price,
    // required this.eventspsService,
    required this.priceUnit, required this.VendorIMGs, 
    required this.add1, required this.add2,
    required this.shortDisc
  }) : super(key: key);

  @override
  State<CustomPartyVendorCard> createState() => _CustomPartyVendorCardState();
}

class _CustomPartyVendorCardState extends State<CustomPartyVendorCard> {
  @override
  void initState() {
    setState(() {
      vendorPartyID = widget.vendorID;
    });
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => PartyPkgListingScr(
              partyVendorID: vendorPartyID,
              partyBIMGS: widget.VendorIMGs, 
              add1: widget.add1, 
              add2: widget.add2, 
              bname: widget.bname,
            ),
          ),
        );
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 7.5),
        decoration: BoxDecoration(
          // color: servicecardfillcolor,
          border: Border.all(color: txtborderbluecolor),
          borderRadius: BorderRadius.all(Radius.circular(7.0)),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(7.0),
                bottomLeft: Radius.circular(7.0),
              ),
              child: Image.network(
                bLogoUrl + widget.PartyVendorIMG,
                fit: BoxFit.fill,
                height: 110,
                width: 130,
              ),
            ),
            SizedBox(
              width: 15.0,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 5.0,
                ),
                Text(
                  widget.bname,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18.0,
                      fontFamily: 'Raleway-Medium'),
                ),

                RatingBar.builder(
                  glow: false,
                  itemSize: 15.0,
                  initialRating: 3,
                  minRating: 1,
                  direction: Axis.horizontal,
                  allowHalfRating: true,
                  itemCount: 5,
                  //itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
                  itemBuilder: (context, _) => Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  unratedColor: Colors.grey,
                  onRatingUpdate: (rating) {
                    print(rating);
                  },
                ),
                SizedBox(
                  width: 200.0,
                  child: Text(
                    widget.shortDisc,
                    // "Event Name",
                    maxLines: 2,
                    style: TextStyle(
                        color: greytxtcolor,
                        fontSize: 13.0,
                        fontFamily: 'Manrope'),
                  ),
                ),
                SizedBox(
                  height: 3.0,
                ),
                Text(
                  "Price Starts from " + widget.priceUnit + " " + widget.price,
                  style: TextStyle(
                      fontSize: 16.0,
                      fontFamily: 'Manrope-SemiBold',
                      color: Colors.white
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
