import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/EventAPI/event_service_api.dart';
import 'package:lasvegas_gts_app/Networking/EventAPI/event_vendor_model.dart';
import 'package:lasvegas_gts_app/Networking/PartyAPI/party_api.dart';
import 'package:lasvegas_gts_app/Networking/PartyAPI/party_vendor_model.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/custom_fun_vendor_card.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'package:lasvegas_gts_app/screens/services/evets/custom_event_vendor_cart.dart';
import 'package:lasvegas_gts_app/screens/services/party/custom_party_vendor_card.dart';

bool isPartyVLoading = false;
List<PartyV> PartyVendorData = [];

class PartyVendorListingScr extends StatefulWidget {
  final String partyCityName;
  final String partyDate;
  final String partyCityCode;
  const PartyVendorListingScr(
      {Key? key, required this.partyCityName, required this.partyCityCode, required this.partyDate})
      : super(key: key);

  @override
  State<PartyVendorListingScr> createState() => _PartyVendorListingScrState();
}

class _PartyVendorListingScrState extends State<PartyVendorListingScr> {
  @override
  void initState() {
    // TODO: implement initState
    fetchEventVendorData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
            lead: Image.asset(
              "assets/icons/ic_party.png",
              color: Colors.white,
            ),
            title: Text(
              "Party",
              style: TextStyle(
                  fontSize: 20.0,
                  fontFamily: 'Helvetica',
                  color: Colors.white
              ),
            )),
        backgroundColor: bgcolor,
        body: isPartyVLoading
            ? CustomLoadingScr()
            : SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 30.0,
                      ),
                      TextFormField(
                        decoration: kSearchTxtFieldDecoration.copyWith(
                          fillColor: servicecardfillcolor,
                          filled: true,
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      ListView.builder(
                          physics: NeverScrollableScrollPhysics(
                              parent: AlwaysScrollableScrollPhysics()),
                          shrinkWrap: true,
                          itemCount: PartyVendorData.length,
                          itemBuilder: (_, index) {
                            return CustomPartyVendorCard(
                              shortDisc: PartyVendorData[index].bussinessDetails!.shortDescription!,
                              vendorID: PartyVendorData[index].vendorId!,
                              PartyVendorIMG: PartyVendorData[index]
                                  .bussinessDetails!
                                  .bussinessLogo!,
                              bname: PartyVendorData[index]
                                  .bussinessDetails!
                                  .bussinessName!,
                              price: PartyVendorData[index].minPrice!,
                              priceUnit: PartyVendorData[index].priceUnit!,
                              VendorIMGs: PartyVendorData[index]
                                  .bussinessDetails!
                                  .imagesk!,
                              add1: PartyVendorData[index]
                                  .bussinessDetails!
                                  .addressLine1!,
                              add2: PartyVendorData[index]
                                  .bussinessDetails!
                                  .addressLine2!,
                            );
                          }),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Future fetchEventVendorData() async {
    setState(() {
      isPartyVLoading = true;
    });

    try {
      PartyVendorData = await PartyAPIs()
          .getPartyVendorData(widget.partyCityCode, widget.partyDate);
    } catch (e) {
      print("Error DATA =====" + e.toString());
    }

    setState(() {
      isPartyVLoading = false;
    });
  }
}
