import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/WeddingAPI/wedding_api.dart';
import 'package:lasvegas_gts_app/Networking/WeddingAPI/wedding_vendor_model.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/custom_fun_vendor_card.dart';
import 'package:lasvegas_gts_app/customs/custom_wedd_vendor_card.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';

bool isWeddVLoading = false;
List<WeddingVendor> WeddVendorData = [];

class WeddVendorListingScr extends StatefulWidget {
  final String weddDate, weddCityName, weddCityCode;
  const WeddVendorListingScr({
    Key? key,
    required this.weddDate,
    required this.weddCityName,
    required this.weddCityCode,
  }) : super(key: key);

  @override
  _WeddVendorListingScrState createState() => _WeddVendorListingScrState();
}

class _WeddVendorListingScrState extends State<WeddVendorListingScr> {
  @override
  void initState() {
    fetchFunVendorData();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
            lead: Image.asset(
              "assets/icons/ic_wedding_service.png",
              color: Colors.white,
            ),
            title: Text(
              "Wedding Service",
              style: TextStyle(
                  fontSize: 20.0,
                  fontFamily: 'Helvetica',
                  color: Colors.white
              ),
            )),
        backgroundColor: bgcolor,
        body: isWeddVLoading
            ? CustomLoadingScr()
            : SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 30.0,
                      ),
                      TextFormField(
                        decoration: kSearchTxtFieldDecoration.copyWith(
                          fillColor: servicecardfillcolor,
                          filled: true,
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      ListView.builder(
                          physics: NeverScrollableScrollPhysics(
                              parent: AlwaysScrollableScrollPhysics()),
                          shrinkWrap: true,
                          itemCount: WeddVendorData.length,
                          itemBuilder: (_, index) {
                            return CustomWeddVendorCard(
                                shortDisc: WeddVendorData[index]
                                    .bussinessDetails!
                                    .shortDescription!,
                                vendorID: WeddVendorData[index].vendorId!,
                                weddVendorIMG: WeddVendorData[index]
                                    .bussinessDetails!
                                    .bussinessLogo!,
                                bname: WeddVendorData[index]
                                    .bussinessDetails!
                                    .bussinessName!,
                                minPrice: WeddVendorData[index].minPrice!,
                                priceUnit: WeddVendorData[index].priceUnit!);
                          }),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Future fetchFunVendorData() async {
    setState(() {
      isWeddVLoading = true;
    });

    print('WEDDING ${widget.weddCityCode}');
    print('WEDDING ${widget.weddDate}');
    try {
      WeddVendorData = await WeddingAPIs()
          .getWeddingVendorData(widget.weddCityCode, widget.weddDate);
    } catch (e) {
      print("Error DATA =====" + e.toString());
    }

    setState(() {
      isWeddVLoading = false;
    });
  }
}
