import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/FunApi/fun_api.dart';
import 'package:lasvegas_gts_app/Networking/WeddingAPI/wedding_api.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/custom_fun_pkg_card.dart';
import 'package:lasvegas_gts_app/customs/custom_wedd_pkg_card.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'package:lasvegas_gts_app/screens/home/home_scr.dart';
import 'package:lasvegas_gts_app/screens/services/fun/fun_main_scr.dart';
import 'package:lasvegas_gts_app/screens/services/wedding/wedding_main_scr.dart';
import 'package:lasvegas_gts_app/utils/alert_utils.dart';

String? specialReq;

TextEditingController guestController = TextEditingController();
TextEditingController fnNameController = TextEditingController();
TextEditingController uNameController = TextEditingController();
TextEditingController uEmailController = TextEditingController();
TextEditingController uMobController = TextEditingController();
TextEditingController groomController = TextEditingController();
TextEditingController brideController = TextEditingController();

class WeddingBookings extends StatefulWidget {
  final String vendorID, amount, pkgIMG, pkgName, depositAmount;

  const WeddingBookings({
    Key? key,
    required this.vendorID,
    required this.amount,
    // required this.no_of_people_allowed,
    required this.pkgIMG,
    required this.pkgName,
    required this.depositAmount,
    // required this.pkgService
  }) : super(key: key);

  @override
  _WeddingBookingsState createState() => _WeddingBookingsState();
}

class _WeddingBookingsState extends State<WeddingBookings> {
  DateTime dateTime = DateTime.now();
  TimeOfDay time = TimeOfDay.now();
  String selectedDate = '';
  String selectedTime = " ";
  bool isLoading = false;
  @override
  void initState() {
    selectedDate = wddingSelectedDate;
    uMobController.text = homeUserPhone;
    uEmailController.text = homeUserEmail;
    uNameController.text = homeUserName;
    // selectedTime = selectedTimeES;
    // TODO: implement initState
    super.initState();
  }

  Future validationCheck() async{


    Map<String, dynamic>? map;

    if(selectedDate.isEmpty){
      AlertUtils.showAutoCloseDialogue(context, "Please select date", 2, 'Required field');
    }else if(uNameController.text.isEmpty){
      AlertUtils.showAutoCloseDialogue(context, "Please enter name", 2, 'Required field');
    }else if(uMobController.text.isEmpty || uMobController.text.length < 10){
      AlertUtils.showAutoCloseDialogue(context, "Please enter valid mobile number", 2, 'Required field');
    }else if(guestController.text.isEmpty){
      AlertUtils.showAutoCloseDialogue(context, "Please enter number of guest", 2, 'Required field');
    }else {

      setState(() {
        isLoading = true;
      });

      map = await WeddingAPIs().bookWedd(
          widget.vendorID,
          wedLogoUrl + widget.pkgIMG,
          widget.depositAmount,
          widget.amount,
          groomController.text,
          brideController.text,
          uMobController.text,
          guestController.text,
          fnNameController.text,
          widget.pkgName,
          selectedDate);

      if(map != null){
        if(map['status_code'] == 200){

          int count = 0;
          Navigator.popUntil(context, (route) {
            return count++ == 5;
          });

          AlertUtils.showAutoCloseDialogue(context, "Wedding Booking successfully", 3, 'Successful');

        }else {
          AlertUtils.showAutoCloseDialogue(context, map['message'], 3, 'Oops!');
        }
      }else {
        AlertUtils.showAutoCloseDialogue(context, "Try Again", 3, 'Oops!');
      }

      setState(() {
        isLoading = true;
      });

    }

  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar: ElevatedButton(
          onPressed: () async {

            validationCheck();
          },
          child: Text(
            'CONTINUE',
            style: TextStyle(
                fontFamily: 'Helvetica',
                color: Color(0xff121A31),
                fontSize: 16.0,
                letterSpacing: 0.7),
          ),
          style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(13.0),
            ),
            fixedSize: Size(389.0, 57.0),
            primary: Color(0xFFFFC71E),
          ),
        ),
        backgroundColor: bgcolor,
        appBar: CustomAppBar(
          lead: BackButton(),
          title: Text(
            widget.pkgName == '' || widget.pkgName == null ? "Booking Details" : widget.pkgName,
            style: TextStyle(
                fontSize: 20.0,
                fontFamily: 'Helvetica',
                color: Colors.white
            ),
          ),
        ),
        body: isLoading ? CustomLoadingScr() : SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 25.0),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      ///// Date
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Select Date',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 14.0,
                              letterSpacing: 0.8,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          InkWell(
                            onTap: () async {
                              DateTime? newDate = await showDatePicker(
                                  context: context,
                                  initialDate: dateTime,
                                  firstDate: DateTime(2021),
                                  lastDate: DateTime(2200));
                              if (newDate != null) {
                                setState(() {
                                  dateTime = newDate;
                                  selectedDate =
                                      '${dateTime.day.toString().padLeft(2, '0')}  -  ${dateTime.month.toString().padLeft(2, '0')}  -  ${dateTime.year}';
                                });
                              }
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 10.0),
                              height: 46.0,
                              width: 166.0,
                              child: Text(
                                selectedDate,
                                textAlign: TextAlign.center,
                                style: kTxtStyle.copyWith(
                                  fontSize: 16.0,
                                  fontFamily: 'Regular',
                                ),
                              ),
                              decoration: BoxDecoration(
                                // color: servicecardfillcolor,
                                border: Border.all(color: txtborderbluecolor),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0)),
                              ),
                            ),
                          ),
                        ],
                      ),

                      //////Amount
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Amount",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 14.0,
                              letterSpacing: 0.8,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 10.0),
                            height: 46.0,
                            width: 166.0,
                            child: Text(
                              widget.amount,
                              textAlign: TextAlign.center,
                              style: kTxtStyle.copyWith(
                                fontSize: 16.0,
                                fontFamily: 'Regular',
                              ),
                            ),
                            decoration: BoxDecoration(
                              // color: servicecardfillcolor,
                              border: Border.all(color: txtborderbluecolor),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10.0)),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                SizedBox(height: 25.0),
                //////Amount
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Amount to be Deposit",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 14.0,
                        letterSpacing: 0.8,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 10.0),
                      height: 46.0,
                      width: MediaQuery.of(context).size.width,
                      child: Text(
                        widget.depositAmount,
                        textAlign: TextAlign.center,
                        style: kTxtStyle.copyWith(
                          fontSize: 16.0,
                          fontFamily: 'Regular',
                        ),
                      ),
                      decoration: BoxDecoration(
                        // color: servicecardfillcolor,
                        border: Border.all(color: txtborderbluecolor),
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                    ),
                  ],
                ),

                SizedBox(height: 25.0),
                //////City
                Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                  child: Text(
                    'City',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 14.0,
                      letterSpacing: 0.8,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  padding:
                      EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                  height: 46.0,
                  width: MediaQuery.of(context).size.width,
                  child: Text(
                    weddingCitySelected,
                    textAlign: TextAlign.center,
                    style: kTxtStyle.copyWith(
                      fontSize: 16.0,
                      fontFamily: 'Regular',
                    ),
                  ),
                  decoration: BoxDecoration(
                    // color: servicecardfillcolor,
                    border: Border.all(color: txtborderbluecolor),
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                ),
                SizedBox(height: 25.0),
                //////Name user
                Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                  child: Text(
                    'Your Name',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 14.0,
                      letterSpacing: 0.8,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                TextFormField(
                  controller: uNameController,
                  style: kTxtStyle,
                  // keyboardType: TextInputType.number,
                  // controller: vendorPhoneNoController,
                  decoration: kTxtFieldDecoration,
                ),
                SizedBox(height: 25.0),
                //////Groom Name
                Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                  child: Text(
                    'Groom Name',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 14.0,
                      letterSpacing: 0.8,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                TextFormField(
                  controller: groomController,
                  style: kTxtStyle,
                  // keyboardType: TextInputType.number,
                  // controller: vendorPhoneNoController,
                  decoration: kTxtFieldDecoration,
                ),
                SizedBox(height: 25.0),

                //////Bride user
                Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                  child: Text(
                    'Bride Name',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 14.0,
                      letterSpacing: 0.8,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                TextFormField(
                  controller: brideController,
                  style: kTxtStyle,
                  // keyboardType: TextInputType.number,
                  // controller: vendorPhoneNoController,
                  decoration: kTxtFieldDecoration,
                ),
                SizedBox(height: 25.0),

                // //////Email
                // Padding(
                //   padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                //   child: Text(
                //     'Email-ID',
                //     textAlign: TextAlign.left,
                //     style: TextStyle(
                //       fontSize: 14.0,
                //       letterSpacing: 0.8,
                //       color: Colors.white,
                //       fontWeight: FontWeight.bold,
                //     ),
                //   ),
                // ),
                // TextFormField(
                //   controller: uEmailController,
                //   style: kTxtStyle,
                //   keyboardType: TextInputType.number,
                //   // controller: vendorPhoneNoController,
                //   decoration: kTxtFieldDecoration,
                // ),
                // SizedBox(height: 25.0),

                //////Phone
                Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                  child: Text(
                    'Mobile Number',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 14.0,
                      letterSpacing: 0.8,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                TextFormField(
                  controller: uMobController,
                  style: kTxtStyle,
                  keyboardType: TextInputType.number,
                  // controller: vendorPhoneNoController,
                  decoration: kTxtFieldDecoration,
                ),
                SizedBox(height: 25.0),

                //////Total Guest
                Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                  child: Text(
                    'Number of Guest Invited',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 14.0,
                      letterSpacing: 0.8,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                TextFormField(
                  controller: guestController,
                  style: kTxtStyle,
                  keyboardType: TextInputType.number,
                  // controller: vendorPhoneNoController,
                  decoration: kTxtFieldDecoration,
                ),
                SizedBox(height: 25.0),

                //////// Function Nmae
                Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                  child: Text(
                    'Function Name',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 14.0,
                      letterSpacing: 0.8,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                TextFormField(
                  controller: fnNameController,
                  style: kTxtStyle,
                  // keyboardType: TextInputType.number,
                  // controller: vendorPhoneNoController,
                  decoration: kTxtFieldDecoration,
                ),

                SizedBox(height: 25.0),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
