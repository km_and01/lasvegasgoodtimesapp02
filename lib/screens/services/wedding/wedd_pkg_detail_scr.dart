import 'package:carousel_images/carousel_images.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/constants/image_pager.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'package:lasvegas_gts_app/screens/services/fun/fun_booking_scr.dart';
import 'package:lasvegas_gts_app/screens/services/wedding/wedding_booking_scr.dart';

bool isPkgDisLoading = false;
String pkgIMGListURL = 'http://lasvegas.koolmindapps.com/images/wedding/';
List<String> listImagesPkg = [];

class PackageDetailsWedd extends StatefulWidget {
  final String pkgName,
      pkgVendorID,
      pkgIMG,
      priceUnit,
      depositPrice,
      pkgPrice,
      pkgDetails,
      address1,
      address2,
      pkgShortDetails;
  final List<String> pkgIMGList, pkgInclude;
  const PackageDetailsWedd(
      {Key? key,
      required this.pkgName,
      required this.priceUnit,
      required this.depositPrice,
      required this.pkgDetails,
      required this.address1,
      required this.address2,
      required this.pkgIMGList,
      required this.pkgShortDetails,
      required this.pkgVendorID,
      required this.pkgIMG,
      required this.pkgInclude,
      required this.pkgPrice})
      : super(key: key);

  @override
  _PackageDetailsWeddState createState() => _PackageDetailsWeddState();
}

class _PackageDetailsWeddState extends State<PackageDetailsWedd> {
  @override
  void initState() {
    print("URL ===" + pkgIMGListURL);
    int length = widget.pkgIMGList.length;
    listImagesPkg.clear();
    for (var i = 0; i < widget.pkgIMGList.length; i++) {
      listImagesPkg.add(pkgIMGListURL + widget.pkgIMGList[i]);
    }
    print(listImagesPkg.toString()); // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgcolor,
      /*appBar: AppBar(
        elevation: 0.0,
        leading: BackButton(),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              widget.pkgName,
              style: kHeadText.copyWith(fontSize: 16),
            ),
            RatingBar.builder(
              glow: false,
              itemSize: 20.0,
              initialRating: 3,
              minRating: 1,
              direction: Axis.horizontal,
              allowHalfRating: true,
              itemCount: 5,
              itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
              itemBuilder: (context, _) => Icon(
                Icons.star,
                color: Colors.amber,
                //   Image.asset(
                // "assets/icons/rating_star.png",
                // color: Colors.amber,
              ),
              // ),
              unratedColor: Colors.grey,
              onRatingUpdate: (rating) {
                print(rating);
              },
            ),
          ],
        ),
        bottom: PreferredSize(
          child: Column(
            children: const [
              Divider(
                thickness: 2,
                height: 2,
                color: Colors.amber,
              ),
              SizedBox(
                height: 3.0,
              ),
              DottedLine(
                lineThickness: 4.5,
                dashRadius: 7,
                dashLength: 4.0,
                // lineLength:  MediaQuery.of(context).size.width,
                dashColor: Colors.amber,
              ),
            ],
          ),
          preferredSize: Size.fromHeight(20),
        ),
      ),*/
      bottomNavigationBar: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(18.0),
        height: 75.0,
        color: servicecardfillcolor,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Deposit Required " +
                      widget.priceUnit +
                      " " +
                      widget.depositPrice,
                  style: kTxtStyle.copyWith(fontSize: 16),
                ),
                Text(
                  "Price " + widget.priceUnit + " " + widget.pkgPrice,
                  style: kTxtStyle.copyWith(
                      fontSize: 16, fontWeight: FontWeight.bold),
                ),
              ],
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (_) => WeddingBookings(
                              vendorID: widget.pkgVendorID,
                              amount: widget.pkgPrice,
                              pkgIMG: widget.pkgIMG,
                              pkgName: widget.pkgName, depositAmount: widget.depositPrice,
                            )));
              },
              child: Text(
                "Book Now",
                style: TextStyle(color: Colors.black),
              ),
              style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0),
                ),
                fixedSize: Size(108.0, 40.0),
                primary: Color(0xFFFFC71E),
              ),
            ),
          ],
        ),
      ),
      body: isPkgDisLoading
          ? CustomLoadingScr()
          : SingleChildScrollView(
              child: Container(
                /*margin: EdgeInsets.all(15),
                padding: EdgeInsets.all(0.01),*/
                child: Column(
                  children: [

                    ImagePager(
                      viewportFraction: 1.0,
                      scaleFactor: 1.0,
                      listImages: listImagesPkg,
                      height: 200.0,
                      /*borderRadius: 8.0,*/
                      cachedNetworkImage: true,
                      //verticalAlignment: Alignment.center,
                      onTap: (index) {
                        print('Tapped on page $index');
                      },
                    ),

                    Container(
                      margin: EdgeInsets.all(15.0),
                      padding: EdgeInsets.all(3.0),
                      alignment: Alignment.topLeft,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            widget.pkgName,
                            style: TextStyle(
                                fontFamily: 'Raleway-Medium',
                                fontSize: 18.0,
                                color: Colors.white
                            ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          RatingBar.builder(
                            glow: false,
                            itemSize: 15.0,
                            initialRating: 3,
                            minRating: 1,
                            direction: Axis.horizontal,
                            allowHalfRating: true,
                            itemCount: 5,
                            //itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                            itemBuilder: (context, _) => Icon(
                              Icons.star,
                              color: Colors.amber,
                              //   Image.asset(
                              // "assets/icons/rating_star.png",
                              // color: Colors.amber,
                            ),
                            // ),
                            unratedColor: Colors.grey,
                            onRatingUpdate: (rating) {
                              print(rating);
                            },
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            widget.address1 + ', ' + widget.address2,
                            style: TextStyle(
                                fontFamily: 'Manrope',
                                color: Colors.white
                            ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),

                          Text(
                            widget.priceUnit +
                                " " +
                                widget.pkgPrice ,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 16.0,
                              fontFamily: 'Manrope-SemiBold',
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      height: 1,
                      color: greytxtcolor,
                      thickness: 1.0,
                    ),

                    Padding(
                      padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Divider(
                            height: 0,
                            thickness: 0,
                            color: Colors.transparent,
                          ),
                          //////Service Name
                          // Text(
                          //   widget.pkgShortDetails,
                          //   style: kTxtStyle.copyWith(color: greytxtcolor),
                          // ),
                          // SizedBox(
                          //   height: 5.0,
                          // ),
                          // Text(

                          //   style: kTxtStyle.copyWith(fontSize: 18),
                          // ),
                          // SizedBox(
                          //   height: 30.0,
                          // ),

                          widget.pkgDetails != 'null'
                              ? Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Package Description",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18.0,
                                          fontFamily: 'Raleway-Bold'),
                                    ),
                                    SizedBox(
                                      height: 15.0,
                                    ),
                                    Text(
                                      widget.pkgDetails,
                                      // """The Hotel Pennsylvania is conveniently located within one block of the Empire State Building,Madison Square Garden, Penn Station and Macy's flagship store. Located within walking distance are Times Square, the Broadway Theatre District, New York Public Library, Jacob K. Javits Convention Center, and two of New York's most iconic…""",
                                      style: TextStyle(
                                          fontFamily: 'Raleway',
                                          color: greytxtcolor,
                                          fontSize: 14.0
                                      ),
                                    ),
                                    SizedBox(
                                      height: 30.0,
                                    ),
                                  ],
                                )
                              : SizedBox(
                                  height: 0.0,
                                ),

                          Text(
                            "Packge Includes",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18.0,
                                fontFamily: 'Raleway-Bold'),
                          ),
                          SizedBox(
                            height: 15.0,
                          ),
                          Text(
                            widget.pkgInclude
                                .toString()
                                .replaceAll('[', ' ')
                                .replaceAll('\"', '')
                                .replaceAll(']', '')
                                .replaceAll('\,', ',\n'),
                            // """The Hotel Pennsylvania is conveniently located within one block of the Empire State Building,Madison Square Garden, Penn Station and Macy's flagship store. Located within walking distance are Times Square, the Broadway Theatre District, New York Public Library, Jacob K. Javits Convention Center, and two of New York's most iconic…""",
                            style: TextStyle(
                                fontFamily: 'Raleway',
                                color: greytxtcolor,
                                fontSize: 14.0
                            ),
                          ),
                          SizedBox(
                            height: 30.0,
                          ),

                          Text(
                            "Address",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18.0,
                                fontFamily: 'Raleway-Bold'),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Text(
                            widget.address1 + ' , ' + widget.address2 + ' . ',
                            // """T3131 Las Vegas Boulevard South, 89109, Las Vegas""",
                            style: TextStyle(
                                fontFamily: 'Raleway',
                                color: greytxtcolor,
                                fontSize: 14.0
                            ),
                          ),
                          SizedBox(
                            height: 10.0,
                          ),

                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}
