import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/FunApi/fun_api.dart';
import 'package:lasvegas_gts_app/Networking/FunApi/fun_package_model.dart';
import 'package:lasvegas_gts_app/Networking/WeddingAPI/wedding_api.dart';
import 'package:lasvegas_gts_app/Networking/WeddingAPI/wedding_pkg_model.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/custom_fun_pkg_card.dart';
import 'package:lasvegas_gts_app/customs/custom_wedd_pkg_card.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';

bool isWeddPkgLoading = false;
List<WeddindPkg> WeddPkgData = [];

class WeddPkgListingScr extends StatefulWidget {
  final String weddvendorID;
  final String bname;
  const WeddPkgListingScr({Key? key,
    required this.weddvendorID,
    required this.bname
  })
      : super(key: key);

  @override
  _WeddPkgListingScrState createState() => _WeddPkgListingScrState();
}

class _WeddPkgListingScrState extends State<WeddPkgListingScr> {
  @override
  void initState() {
    fetchFunPkgData();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: CustomAppBar(
            lead: Image.asset(
              "assets/icons/ic_wedding_service.png",
              color: Colors.white,
            ),
            title: Text(
              widget.bname == '' || widget.bname == null ? "Wedding Service" : widget.bname,
              style: TextStyle(
                  fontSize: 20.0,
                  fontFamily: 'Helvetica',
                  color: Colors.white
              ),
            )),
        backgroundColor: bgcolor,
        body: isWeddPkgLoading
            ? CustomLoadingScr()
            : SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 20.0,
                      ),
                      Text(
                        "Our Packages",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20.0,
                            fontFamily: 'Raleway-Bold'),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      ListView.builder(
                          physics: NeverScrollableScrollPhysics(
                              parent: AlwaysScrollableScrollPhysics()),
                          shrinkWrap: true,
                          itemCount: WeddPkgData.length,
                          itemBuilder: (_, index) {
                            return CustomWeddPackageCard(
                              packageIMG: WeddPkgData[index].bannerImage!,
                              packageName: WeddPkgData[index].packageName!,
                              priceUnit: WeddPkgData[index].priceUnit!,
                              packagePrice: WeddPkgData[index].price!,
                              packageDiscPrice:
                                  WeddPkgData[index].depositPrice!,
                              add1: WeddPkgData[index]
                                  .bussinessDetails!
                                  .addressLine1!,
                              add2: WeddPkgData[index]
                                  .bussinessDetails!
                                  .addressLine2!,
                              fullDetails: WeddPkgData[index].fullDetails!,
                              pkgIMGList: WeddPkgData[index].moreImages!,
                              pkgVendorID: WeddPkgData[index].vendorId!,
                              shortDetails: WeddPkgData[index].shortDetails!,
                              pkgInclude: WeddPkgData[index].details!,
                            );
                          }),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Future fetchFunPkgData() async {
    setState(() {
      isWeddPkgLoading = true;
    });
    try {
      WeddPkgData = await WeddingAPIs().getWeddingPkgData(widget.weddvendorID);
      print("PKG WeddPkgData DATA =====" + WeddPkgData[0].packageName!);
      print("PKG WeddPkgData LENGTH =====" + WeddPkgData.length.toString());
    } catch (e) {
      print("Error DATA =====" + e.toString());
    }

    setState(() {
      isWeddPkgLoading = false;
    });
  }
}
