// ignore_for_file: avoid_print

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/userlogingAPI/login_api.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/screens/userRegistration/signup_landing_page.dart';
import 'package:lasvegas_gts_app/screens/vendor/vendor_persnal_details_form.dart';

bool isLoading = false;
//  BuildContext dialogContext = BuildContext ;

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

TextEditingController userEmailController = TextEditingController();
TextEditingController userPasswordController = TextEditingController();

class _LoginScreenState extends State<LoginScreen> {
  final _userLoginFormKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.bottomLeft,
            end: Alignment.topRight,
            colors: [
              bgGradientGrey,
              bgGradientBlack,
              bgGradientGrey,
              bgGradientBlack,
            ],
          ),
        ),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: SingleChildScrollView(
            reverse: true,
            child: Form(
              key: _userLoginFormKey,
              child: Padding(
                padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 200.0,
                          width: 200.0,
                          child:
                              Image.asset('assets/images/Exprezzzo_logo.png'),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: const [
                        Text(
                          'Welcome to',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 22.0,
                              letterSpacing: 0.32),
                        ),
                        Text(
                          'Exprezzzo',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 22.0,
                            letterSpacing: 0.32,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 25.0),
                    Text(
                      'Enter your registered email address or phone number to login',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 14.0,
                        letterSpacing: 0.23,
                      ),
                    ),
                    SizedBox(height: 25.0),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                      child: Text(
                        'EMAIL / PHONE',
                        style: TextStyle(
                            fontSize: 14.0,
                            letterSpacing: 0.8,
                            color: Color(0xFF95A0AE),
                            fontFamily: 'Raleway-Regular'),
                      ),
                    ),
                    TextFormField(
                      validator: (email) {
                        if (email!.isEmpty) {
                          return "Please Enter Email Address";
                        }
                      },
                      showCursor: true,
                      cursorColor: Color(0xFF5C75B3),
                      cursorRadius: Radius.circular(0),
                      style: kTxtStyle,
                      controller: userEmailController,
                      decoration: kTxtFieldDecoration,
                    ),
                    SizedBox(height: 25.0),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                      child: Text(
                        'PASSWORD',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            fontSize: 14.0,
                            letterSpacing: 0.8,
                            color: Color(0xFF95A0AE),
                            fontFamily: 'Raleway-Regular'),
                      ),
                    ),
                    TextFormField(
                      validator: (value) {
                        if (value!.isEmpty) {
                          return "Please Enter Your Password";
                        }
                        if (value.length < 6) {
                          return "password must be at least 6 digits long";
                        }
                      },
                      showCursor: true,
                      cursorColor: Color(0xFF5C75B3),
                      cursorRadius: Radius.circular(0),
                      style: kTxtStyle,
                      obscureText: true,
                      controller: userPasswordController,
                      keyboardType: TextInputType.visiblePassword,
                      decoration: kTxtFieldDecoration,
                    ),
                    SizedBox(height: 25.0),
                    ElevatedButton(
                      onPressed: () {
                        var uEmail = userEmailController.text;
                        var uPassword = userPasswordController.text;
                        isLoading = true;
                        if (isLoading == true) {
                          showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (BuildContext context) {
                              return Dialog(
                                backgroundColor: Colors.transparent,
                                insetPadding: EdgeInsets.only(
                                    left: MediaQuery.of(context).size.width *
                                        0.44,
                                    right: MediaQuery.of(context).size.width *
                                        0.44),
                                child: Container(
                                  color: Colors.transparent,
                                  width: double.infinity,
                                  height: 50,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: const [
                                      CircularProgressIndicator(
                                        backgroundColor: Colors.transparent,
                                        color: btngoldcolor,
                                      ),
                                    ],
                                  ),
                                ),
                              );
                            },
                          );
                        }
                        setState(() {
                          isLoading = false;
                        });
                        if (_userLoginFormKey.currentState!.validate()) {
                          setState(() {
                            loginUser(uEmail, uPassword, context);

                            print("login data send");
                            // Navigator.pop(context);
                          });
                        }
                      },
                      child: Text('LOG IN',
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'Raleway-Regular')),
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        fixedSize: Size(420.0, 57.0),
                        alignment: Alignment.center,
                        primary: Color(0xFF6f009d),
                      ),
                    ),
                    SizedBox(height: 25.0),
                    Padding(
                      padding: EdgeInsets.fromLTRB(20.0, 0, 20.0, 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          Expanded(
                            child: Divider(
                                thickness: 1, height: 1, color: Colors.white),
                          ),
                          Padding(
                            padding: EdgeInsets.all(10.0),
                            child: Text(
                              'OR',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14.0,
                                  fontFamily: 'Raleway-Regular'),
                            ),
                          ),
                          Expanded(
                            child: Divider(
                              height: 1,
                              thickness: 1,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 25.0),
                    ElevatedButton(
                      onPressed: () {
                        /* Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => HomeScreen(),
                          ),
                        ); */
                      },
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(15.0, 0, 0, 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: const [
                            Image(
                              height: 30.0,
                              width: 30.0,
                              image: AssetImage("assets/icons/google_icon.png"),
                            ),
                            SizedBox(
                              width: 65.0,
                            ),
                            Text(
                              'LOG IN WITH GOOGLE',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'Raleway-Regular'),
                            ),
                          ],
                        ),
                      ),
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        fixedSize: Size(420.0, 57.0),
                        alignment: Alignment.center,
                        primary: Color(0xFF6f009d),
                      ),
                    ),
                    SizedBox(height: 25.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Don\'t have an account?',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 14.0,
                              letterSpacing: 0.23,
                              fontFamily: 'Raleway-Regular'),
                        ),
                        TextButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => SignupLandingPage(),
                              ),
                            );
                          },
                          child: Text(
                            'Sign up',
                            style: TextStyle(
                                decoration: TextDecoration.underline,
                                color: Color(0xFFc55ff7),
                                fontSize: 14.0,
                                letterSpacing: 0.23,
                                fontFamily: 'Raleway-Regular'),
                          ),
                        ),
                      ],
                    ),
                    Center(
                      child: TextButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      VendorPersonalDetail()));
                        },
                        child: Text(
                          'Get Listed on Exprezzzo',
                          style: TextStyle(
                              decoration: TextDecoration.underline,
                              color: Color(0xFFc55ff7),
                              fontSize: 14.0,
                              letterSpacing: 0.23,
                              fontFamily: 'Raleway-Regular'),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
