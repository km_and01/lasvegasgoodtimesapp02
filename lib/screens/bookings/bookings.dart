import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/BookingHistoryAPI/booking_api.dart';
import 'package:lasvegas_gts_app/Networking/BookingHistoryAPI/booking_history_model.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'dart:developer' as s;

import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';

class Bookings extends StatefulWidget {
  const Bookings({Key? key}) : super(key: key);

  @override
  _BookingsState createState() => _BookingsState();
}

class _BookingsState extends State<Bookings> {

  bool is_loading = false;
  List<Booking> bookingList = [];

  @override
  void initState() {


    fetchBookingsData();
    super.initState();
  }

  Future fetchBookingsData() async{
    setState(() {
      is_loading = true;
    });

    try {
      bookingList = await BookingHistoryAPI().getBookingHistory();
      // RentalsAPIs().getRentalVendorData(widget.rentalCityCode, widget.rentalDate, widget.rentalCarName);

      s.log('R_DATA ${bookingList.first.toJson()}');
    } catch (e) {
      print("Error DATA =====" + e.toString());
    }

    setState(() {
      is_loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgcolor,
      body: is_loading ? CustomLoadingScr() : Container(
        margin: EdgeInsets.all(10.0),
        alignment: Alignment.center,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Search your \nBookings",
                    style: kHeadText,
                    // textAlign: TextAlign.start,
                  ),

                  Row(
                    children: [
                      CircleAvatar(
                        radius: 21.0,
                        backgroundColor: Colors.white,
                        child: CircleAvatar(
                          child: Image(
                            width: 24.0,
                            height: 24.0,
                            image: AssetImage("assets/icons/ic_heart.png"),
                          ),
                        ),
                      ),
                      SizedBox(width: 5.0,),
                      CircleAvatar(
                        radius: 21.0,
                        backgroundColor: Colors.white,
                        // foregroundColor: Colors.white,
                        child: CircleAvatar(
                          child: Image(
                            width: 24.0,
                            height: 24.0,
                            image: AssetImage("assets/icons/ic_notification.png"),
                          ),
                        ),
                      ),
                    ],
                  )

                ],
              ),
            ),
            SizedBox(
              height: 15.0,
            ),
            TextFormField(
              decoration: kSearchTxtFieldDecoration.copyWith(
                fillColor: servicecardfillcolor,
                filled: true,
              ),
            ),
            SizedBox(
              height: 15.0,
            ),
            Text(
              "All Bookings",
              style: TextStyle(
                color: Colors.white,
                fontSize: 18.0,
              ),
            ),

                ListView.builder(
                        physics: AlwaysScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: bookingList.length,
                        itemBuilder: (_, index) {

                          Booking item = bookingList[index];

                          return GestureDetector(
                            onTap: () {

                            },
                            child: Container(
                              margin: EdgeInsets.symmetric(vertical: 7.5),
                              decoration: BoxDecoration(
                                // color: servicecardfillcolor,
                                border: Border.all(color: txtborderbluecolor),
                                borderRadius: BorderRadius.all(Radius.circular(7.0)),
                              ),
                              child: Row(
                                children: [
                                  SizedBox(
                                    width: 130,
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(7.0),
                                        bottomLeft: Radius.circular(7.0),
                                      ),
                                      child: Image.network(
                                        item.imagePath!,
                                        fit: BoxFit.fill,
                                        height: 130,
                                        width: 130,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 15.0,
                                  ),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        item.name != null ? '${item.name}' : '${item.packageId!}',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 18.0,
                                            fontFamily: 'Raleway-Medium'),
                                      ),
                                      SizedBox(
                                        height: 8.0,
                                      ),
                                      Text(
                                        'Booking Date: ${item.checkIn}',
                                        style: TextStyle(
                                            fontFamily: 'Manrope',
                                            fontSize: 14.0,
                                            color: Colors.white
                                        ),
                                      ),
                                      SizedBox(
                                        height: 3.0,
                                      ),
                                      Text(
                                        'No. of Person: ${item.person}',
                                        style: TextStyle(
                                            fontFamily: 'Manrope',
                                            fontSize: 14.0,
                                            color: Colors.white
                                        ),
                                      ),
                                      SizedBox(
                                        height: 3.0,
                                      ),
                                      Text(
                                        'Category Name: ${item.categoryName}',
                                        style: TextStyle(
                                            fontFamily: 'Manrope',
                                            fontSize: 14.0,
                                            color: Colors.white
                                        ),
                                      ),
                                      SizedBox(
                                        height: 3.0,
                                      ),
                                      Text(
                                        'Amount: \$${item.amount}',
                                        style: TextStyle(
                                            fontFamily: 'Manrope',
                                            fontSize: 14.0,
                                            color: Colors.white
                                        ),
                                      ),

                                      SizedBox(
                                        width: 175,
                                        child: Text(
                                          item.status == 0 ?
                                              'YOUR ORDER IS PENDING TO BE APPROVED BY VENDOR.'
                                              :
                                          'YOUR ORDER IS ACCEPTED',
                                          maxLines: 2,
                                          style: TextStyle(
                                              fontFamily: 'Manrope',
                                              fontSize: 12.0,
                                              color: Colors.amberAccent
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          );
                        })
          ],
        ),
      ),
    );
  }
}
