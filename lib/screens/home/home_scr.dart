// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:carousel_images/carousel_images.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:lasvegas_gts_app/customs/custom_card.dart';
import 'package:lasvegas_gts_app/customs/custom_nav_bar.dart';
import 'package:lasvegas_gts_app/screens/bookings/bookings.dart';
import 'package:lasvegas_gts_app/screens/profile/profile.dart';
import 'package:lasvegas_gts_app/screens/services/Auction/auction_service.dart';
import 'package:lasvegas_gts_app/screens/services/air/air_main_src.dart';
import 'package:lasvegas_gts_app/screens/services/bodyguard/bodtguard_main_scr.dart';
import 'package:lasvegas_gts_app/screens/services/businessService/business_service_main_scr.dart';
import 'package:lasvegas_gts_app/screens/services/celebAppearance/celeb_main_scr.dart';
import 'package:lasvegas_gts_app/screens/services/eat/eat_opening_scr.dart';
// import 'package:lasvegas_gts_app/screens/services/eat/eat_main_scr.dart';
import 'package:lasvegas_gts_app/screens/services/eventSpace/event_space_main_scr.dart';
import 'package:lasvegas_gts_app/screens/services/evets/event_main_src.dart';
import 'package:lasvegas_gts_app/screens/services/fashion/fashion_main_scr.dart';
import 'package:lasvegas_gts_app/screens/services/fun/fun_main_scr.dart';
import 'package:lasvegas_gts_app/screens/services/party/party_main_scr.dart';
import 'package:lasvegas_gts_app/screens/services/rental/rental_main_scr.dart';
import 'package:lasvegas_gts_app/screens/services/services.dart';
import 'package:lasvegas_gts_app/screens/services/stay/stay_search_filter_scr.dart';
import 'package:lasvegas_gts_app/screens/services/transport/transport_main_scr.dart';
import 'package:lasvegas_gts_app/screens/services/wedding/wedding_main_scr.dart';
import 'package:shared_preferences/shared_preferences.dart';

final List<String> listImages = [
  "assets/images/banner_img_1.png",
  "assets/images/banner_img_2.png",
  "assets/images/banner_img_3.png",
  "assets/images/banner_img_4.png",
  "assets/images/banner_img_5.png",
  "assets/images/banner_img_6.png",
];
String homeToken = '';
String homeUserName = '';
String homeUserEmail = '';
String homeUserPhone = '';

String homeUserId = '';

class HomeScreen extends StatefulWidget {
  /*final String? userToken;
  final String? userName;
  final String? userEmail;
  final String? userPhone;
  final String? userID;*/

  HomeScreen(
      {Key?
          key /*, this.userToken, this.userName, this.userEmail, this.userPhone, this.userID*/})
      : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    loadData();
    super.initState();
  }

  String _userToken = '';

  Future loadData() async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();

    setState(() {
      homeToken = preferences.getString('userToken')!;
      _userToken = preferences.getString('userToken')!;
      homeUserName = preferences.getString('u_name')!;
      homeUserEmail = preferences.getString('u_email')!;

      homeUserPhone = preferences.getString('u_mob')!;
      homeUserId = preferences.getString('u_id')!;
    });

    print(_userToken + "Token in Home init");
  }

  Future getToken() async {
    final SharedPreferences preferences = await SharedPreferences.getInstance();
    var token = preferences.getString('userToken');

    setState(() {
      _userToken = token!;
    });
    print("Token From Login API in Home By Shared Pref === " + _userToken);
  }

  int selectedItem = 0;

  final screens = [
    HomeScreen(),
    Services(),
    Bookings(),
    Profile(),
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          bottomNavigationBar: CustomNavBar(
            defaultSelectedIndex: 0,
            onChange: (val) {
              setState(() {
                selectedItem = val;
                print(selectedItem);
                screens[val];
              });
            },
          ),
          backgroundColor: Color(0xFF121A31),
          body: selectedItem == 0
              ? SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding:
                            const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Hello \n$homeUserName",
                              style: TextStyle(
                                  fontSize: 25.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                            Row(
                              children: const [
                                CircleAvatar(
                                  radius: 21.0,
                                  backgroundColor: Colors.white,
                                  child: CircleAvatar(
                                    child: Image(
                                      width: 24.0,
                                      height: 24.0,
                                      image: AssetImage(
                                          "assets/icons/ic_heart.png"),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 15.0,
                                ),
                                CircleAvatar(
                                  radius: 21.0,
                                  backgroundColor: Colors.white,
                                  // foregroundColor: Colors.white,
                                  child: CircleAvatar(
                                    child: Image(
                                      width: 24.0,
                                      height: 24.0,
                                      image: AssetImage(
                                          "assets/icons/ic_notification.png"),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 30.0,
                      ),
                      const Divider(
                        thickness: 2,
                        height: 2,
                        color: Colors.amber,
                      ),
                      const SizedBox(
                        height: 3.0,
                      ),
                      const DottedLine(
                        lineThickness: 4.5,
                        dashRadius: 7,
                        dashLength: 4.0,
                        // lineLength:  MediaQuery.of(context).size.width,
                        dashColor: Colors.amber,
                      ),
                      const SizedBox(
                        height: 30.0,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CarouselImages(
                            scaleFactor: 1,
                            listImages: listImages,
                            height: 150.0,
                            borderRadius: 8.0,
                            cachedNetworkImage: true,
                            verticalAlignment: Alignment.center,
                            onTap: (index) {
                              print('Tapped on page $index');
                            },
                          ),
                          const SizedBox(
                            height: 30.0,
                          ),
                          SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Row(
                              children: [
                                /* CoustomCard(
                                  icon: "ic_stay",
                                  title: "Stay",
                                  DestinationWidget: StaySearchFilter(
                                    utoken: homeToken,
                                  ),
                                ), */
                                CoustomCard(
                                  icon: "ic_air_service",
                                  title: "Air Service",
                                  DestinationWidget: AirHome(
                                    uToken: homeToken,
                                  ),
                                ),
                                CoustomCard(
                                  icon: "ic_eat",
                                  title: "Eat",
                                  DestinationWidget: EatHome(),
                                ),
                                CoustomCard(
                                  icon: "ic_event",
                                  title: "Event",
                                  DestinationWidget: EventHome(),
                                ),
                                CoustomCard(
                                  icon: "ic_event_space",
                                  title: "Event Space",
                                  DestinationWidget: EventSpaceHome(),
                                ),
                                CoustomCard(
                                  icon: "ic_transportation",
                                  title: "Transport",
                                  DestinationWidget: TransportHome(),
                                ),
                                CoustomCard(
                                  icon: "ic_bodyguard",
                                  title: "Bodyguards",
                                  DestinationWidget: BodyguardHome(),
                                ),
                                /* CoustomCard(
                                  icon: "ic_celeb_appearence",
                                  title: "Celebrity Appearance",
                                  DestinationWidget: CelebAppearanceHome(),
                                ), */
                                CoustomCard(
                                  icon: "ic_rental",
                                  title: "Rental",
                                  DestinationWidget: RentalHome(),
                                ),
                                CoustomCard(
                                  icon: "ic_party",
                                  title: "Party",
                                  DestinationWidget: PartyHome(),
                                ),
                                CoustomCard(
                                  icon: "ic_fasion_retail",
                                  title: "Fashion / Retails",
                                  DestinationWidget: FashionHome(),
                                ),
                                CoustomCard(
                                  icon: "ic_wedding_service",
                                  title: "Wedding Service",
                                  DestinationWidget: WeddingHome(),
                                ),
                                CoustomCard(
                                  icon: "ic_business_services",
                                  title: "Cannabies Service",
                                  DestinationWidget: BusinessServiceHome(),
                                ),
                                CoustomCard(
                                  icon: "ic_fun",
                                  title: "Fun",
                                  DestinationWidget: FunHome(),
                                ),
                              ],
                            ),
                          ),
                          Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: Text(
                                      "Bidding",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  TextButton(
                                    onPressed: () {},
                                    child: Text(
                                      "",
                                      style: kTxtStyle,
                                    ),
                                  ),
                                ],
                              ),
                              SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Row(
                                  children: [
                                    CoustomCard(
                                      icon: "ic_air_service",
                                      title: "Air Jet",
                                      DestinationWidget: AuctionService(
                                        serviceTitle: 'Air',
                                      ),
                                    ),
                                    CoustomCard(
                                      icon: "ic_eat",
                                      title: "Eat",
                                      DestinationWidget: AuctionService(
                                        serviceTitle: 'Eat',
                                      ),
                                    ),
                                    CoustomCard(
                                      icon: "ic_event_space",
                                      title: "Event Space",
                                      DestinationWidget: AuctionService(
                                        serviceTitle: 'EventSpace',
                                      ),
                                    ),
                                    CoustomCard(
                                      icon: "ic_bodyguard",
                                      title: "Executive Protection",
                                      DestinationWidget: AuctionService(
                                        serviceTitle: 'ExecutiveProtection',
                                      ),
                                    ),
                                    CoustomCard(
                                      icon: "ic_rental",
                                      title: "Rental",
                                      DestinationWidget: AuctionService(
                                        serviceTitle: 'RentalCars',
                                      ),
                                    ),
                                    CoustomCard(
                                      icon: "ic_transportation",
                                      title: "Transport",
                                      DestinationWidget: AuctionService(
                                        serviceTitle: 'Transportation',
                                      ),
                                    ),
                                    CoustomCard(
                                      icon: "ic_fasion_retail",
                                      title: "Fashion / Retails",
                                      DestinationWidget: AuctionService(
                                        serviceTitle: 'Fashion',
                                      ),
                                    ),
                                    CoustomCard(
                                      icon: "ic_event",
                                      title: "Events",
                                      DestinationWidget: AuctionService(
                                        serviceTitle: 'Event',
                                      ),
                                    ),
                                    CoustomCard(
                                      icon: "ic_wedding_service",
                                      title: "Wedding Service",
                                      DestinationWidget: AuctionService(
                                        serviceTitle: 'WeddingService',
                                      ),
                                    ),
                                    CoustomCard(
                                      icon: "ic_business_services",
                                      title: "Cannabies Service",
                                      DestinationWidget: AuctionService(
                                        serviceTitle: 'Cannabies',
                                      ),
                                    ),
                                    CoustomCard(
                                      icon: "ic_fun",
                                      title: "Fun",
                                      DestinationWidget: AuctionService(
                                        serviceTitle: 'Fun',
                                      ),
                                    ),
                                    CoustomCard(
                                      icon: "ic_celeb_appearence",
                                      title: "Celebrity Appearance",
                                      DestinationWidget: AuctionService(
                                        serviceTitle: 'CelebrityAppearance',
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                          const SizedBox(
                            height: 15.0,
                          ),

                          //// night club
                          Container(
                            margin: EdgeInsets.all(10.0),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "Night Clube",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    TextButton(
                                      onPressed: () {},
                                      child: Text(
                                        "See All",
                                        style: kTxtStyle,
                                      ),
                                    ),
                                  ],
                                ),
                                SingleChildScrollView(
                                  scrollDirection: Axis.horizontal,
                                  child: Row(
                                    children: [
                                      Container(
                                        margin: EdgeInsets.all(10.0),
                                        height: 173.0,
                                        width: 133.0,
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  "assets/images/dummy_tumbnail_1.png")),
                                          border: Border.all(
                                              color: txtborderbluecolor),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(15.0)),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.all(10.0),
                                        height: 173.0,
                                        width: 133.0,
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  "assets/images/dummy_tumbnail_1.png")),
                                          border: Border.all(
                                              color: txtborderbluecolor),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(15.0)),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.all(10.0),
                                        height: 173.0,
                                        width: 133.0,
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  "assets/images/dummy_tumbnail_1.png")),
                                          border: Border.all(
                                              color: txtborderbluecolor),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(15.0)),
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 15.0,
                          ),

                          ////pool clun
                          Container(
                            margin: EdgeInsets.all(10.0),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "Pool Clube",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    TextButton(
                                      onPressed: () {},
                                      child: Text(
                                        "See All",
                                        style: kTxtStyle,
                                      ),
                                    ),
                                  ],
                                ),
                                SingleChildScrollView(
                                  scrollDirection: Axis.horizontal,
                                  child: Row(
                                    children: [
                                      Container(
                                        margin: EdgeInsets.all(10.0),
                                        height: 173.0,
                                        width: 133.0,
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  "assets/images/dummy_tumbnail_2.png")),
                                          border: Border.all(
                                              color: txtborderbluecolor),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(15.0)),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.all(10.0),
                                        height: 173.0,
                                        width: 133.0,
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  "assets/images/dummy_tumbnail_2.png")),
                                          border: Border.all(
                                              color: txtborderbluecolor),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(15.0)),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.all(10.0),
                                        height: 173.0,
                                        width: 133.0,
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  "assets/images/dummy_tumbnail_2.png")),
                                          border: Border.all(
                                              color: txtborderbluecolor),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(15.0)),
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 15.0,
                          ),

                          ////Lough / Bar
                          Container(
                            margin: EdgeInsets.all(10.0),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "Lough / Bar",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    TextButton(
                                      onPressed: () {},
                                      child: Text(
                                        "See All",
                                        style: kTxtStyle,
                                      ),
                                    ),
                                  ],
                                ),
                                SingleChildScrollView(
                                  scrollDirection: Axis.horizontal,
                                  child: Row(
                                    children: [
                                      Container(
                                        margin: EdgeInsets.all(10.0),
                                        height: 173.0,
                                        width: 133.0,
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  "assets/images/dummy_tumbnail_3.png")),
                                          border: Border.all(
                                              color: txtborderbluecolor),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(15.0)),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.all(10.0),
                                        height: 173.0,
                                        width: 133.0,
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  "assets/images/dummy_tumbnail_3.png")),
                                          border: Border.all(
                                              color: txtborderbluecolor),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(15.0)),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.all(10.0),
                                        height: 173.0,
                                        width: 133.0,
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  "assets/images/dummy_tumbnail_3.png")),
                                          border: Border.all(
                                              color: txtborderbluecolor),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(15.0)),
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),

                          ////Activity
                          Container(
                            margin: EdgeInsets.all(10.0),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "Activity",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    TextButton(
                                      onPressed: () {},
                                      child: Text(
                                        "See All",
                                        style: kTxtStyle,
                                      ),
                                    ),
                                  ],
                                ),
                                SingleChildScrollView(
                                  scrollDirection: Axis.horizontal,
                                  child: Row(
                                    children: [
                                      Container(
                                        margin: EdgeInsets.all(10.0),
                                        height: 173.0,
                                        width: 133.0,
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  "assets/images/dummy_tumbnail_1.png")),
                                          border: Border.all(
                                              color: txtborderbluecolor),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(15.0)),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.all(10.0),
                                        height: 173.0,
                                        width: 133.0,
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  "assets/images/dummy_tumbnail_2.png")),
                                          border: Border.all(
                                              color: txtborderbluecolor),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(15.0)),
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.all(10.0),
                                        height: 173.0,
                                        width: 133.0,
                                        decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  "assets/images/dummy_tumbnail_3.png")),
                                          border: Border.all(
                                              color: txtborderbluecolor),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(15.0)),
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                )
              : screens[selectedItem]),
    );
  }
}
