import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/screens/login/login_src.dart';
import 'package:lasvegas_gts_app/screens/userRegistration/registration_scr.dart';

class SignupLandingPage extends StatefulWidget {
  const SignupLandingPage({Key? key}) : super(key: key);

  @override
  State<SignupLandingPage> createState() => _SignupLandingPageState();
}

class _SignupLandingPageState extends State<SignupLandingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF121A31),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(
                left: 12.0, top: 30.0, right: 12.0, bottom: 0.0),
            child: Image.asset(
              'assets/images/Exprezzzo_logo_icon.png',
              height: 250,
              width: 250,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Discover local vendors and book now. \n Let the good times scroll.',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontSize: 16.0,
                letterSpacing: 0.32,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Image.asset(
                  "assets/icons/ic_stay.png",
                  height: 50,
                  width: 50,
                  color: Colors.blue,
                ),
                Image.asset(
                  "assets/icons/ic_rental.png",
                  height: 50,
                  width: 50,
                  color: Colors.blue,
                ),
                Image.asset(
                  "assets/icons/ic_air_service.png",
                  height: 50,
                  width: 50,
                  color: Colors.blue,
                ),
                Image.asset(
                  "assets/icons/ic_eat.png",
                  height: 50,
                  width: 50,
                  color: Colors.blue,
                ),
                Image.asset(
                  "assets/icons/ic_event.png",
                  height: 50,
                  width: 50,
                  color: Colors.blue,
                ),
              ],
            ),
          ),
          SizedBox(
            height: 170,
          ),
          ElevatedButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => RegistrationScreen()));
            },
            child: Text('SIGN UP',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.0,
                  fontFamily: 'Raleway-Regular',
                )),
            style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
              fixedSize: Size(389.0, 57.0),
              primary: Color(0xFF6f009d),
            ),
          ),
          SizedBox(
            height: 50,
          ),
          Divider(
            color: Colors.grey,
            height: 1,
          ),
          SizedBox(
            height: 20,
          ),
          TextButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => LoginScreen()));
            },
            child: Text(
              'LOG IN',
              style: TextStyle(
                color: Colors.white,
                fontSize: 18.0,
                letterSpacing: 0.23,
                fontFamily: 'Raleway-Regular',
              ),
            ),
          ),
        ],
      ),
    );
  }
}
