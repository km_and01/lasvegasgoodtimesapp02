// import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/userregAPI/ureg_api.dart';

import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/screens/userRegistration/otp_verification_screen.dart';

bool isLoading = false;

class RegistrationScreen extends StatefulWidget {
  const RegistrationScreen({Key? key}) : super(key: key);

  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

TextEditingController userEmailController = TextEditingController();
TextEditingController userPasswordController = TextEditingController();
TextEditingController userNameController = TextEditingController();
TextEditingController userPhoneNoController = TextEditingController();

class _RegistrationScreenState extends State<RegistrationScreen> {
  var _userRegFormKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFF121A31),
        // appBar: AppBar(
        //   elevation: 0.0,
        //   leading: Icon(Icons.arrow_back),
        //   backgroundColor: Color(0xFF121A31),
        // ),
        body: SingleChildScrollView(
          reverse: true,
          child: Form(
            key: _userRegFormKey,
            // alignment: Alignment.centerLeft,
            // padding: EdgeInsets.all(0),
            // color: Color(0xFF121A31),
            child: Padding(
              padding: EdgeInsets.fromLTRB(16.0, 0, 16.0, 0),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    // crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      ////back btn for exit the app
                      /* BackButton(
                        color: Colors.white,
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ), */
                      /* SizedBox(
                        width: 100.0,
                      ), */
                      SizedBox(
                        height: 200.0,
                        width: 200.0,
                        child: Image.asset('assets/images/Exprezzzo_logo.png'),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: const [
                      Text(
                        'Create your account',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 22.0,
                            letterSpacing: 0.32),
                      ),
                    ],
                  ),
                  SizedBox(height: 25.0),
                  Text(
                    'Please enter your details to create account',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14.0,
                      letterSpacing: 0.23,
                    ),
                  ),
                  SizedBox(height: 25.0),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                    child: Text(
                      'FULL NAME',
                      style: TextStyle(
                        fontSize: 12.0,
                        letterSpacing: 0.8,
                        color: Color(0xFF95A0AE),
                        fontFamily: 'Raleway-Regular',
                      ),
                    ),
                  ),
                  TextFormField(
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please Enter Your Name";
                      }
                    },
                    showCursor: true,
                    cursorColor: Color(0xFF5C75B3),
                    cursorRadius: Radius.circular(0),
                    style: kTxtStyle,
                    controller: userNameController,
                    decoration: kTxtFieldDecoration,
                  ),
                  SizedBox(height: 25.0),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                    child: Text(
                      'PHONE NUMBER',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 12.0,
                        letterSpacing: 0.8,
                        color: Color(0xFF95A0AE),
                        fontFamily: 'Raleway-Regular',
                      ),
                    ),
                  ),
                  TextFormField(
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please Enter Your Mobile Number";
                      }
                      if (value.length > 10) {
                        return "Enter Valid Number";
                      }
                      if (value.length < 10) {
                        return "Enter Valid Number";
                      }
                    },
                    showCursor: true,
                    cursorColor: Color(0xFF5C75B3),
                    cursorRadius: Radius.circular(0),
                    style: kTxtStyle,
                    keyboardType: TextInputType.phone,
                    controller: userPhoneNoController,
                    decoration: kTxtFieldDecoration,
                  ),
                  SizedBox(height: 25.0),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                    child: Text(
                      'EMAIL ADDRESS',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 12.0,
                        letterSpacing: 0.8,
                        color: Color(0xFF95A0AE),
                        fontFamily: 'Raleway-Regular',
                      ),
                    ),
                  ),
                  TextFormField(
                    validator: (email) {
                      if (email!.isEmpty) {
                        return "Please Enter Email Address";
                      }
                    },
                    showCursor: true,
                    cursorColor: Color(0xFF5C75B3),
                    cursorRadius: Radius.circular(0),
                    style: kTxtStyle,
                    keyboardType: TextInputType.emailAddress,
                    controller: userEmailController,
                    decoration: kTxtFieldDecoration,
                  ),
                  SizedBox(height: 25.0),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0.0, 0, 0, 10.0),
                    child: Text(
                      'PASSWORD',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 12.0,
                        letterSpacing: 0.8,
                        color: Color(0xFF95A0AE),
                        fontFamily: 'Raleway-Regular',
                      ),
                    ),
                  ),
                  TextFormField(
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please Enter Your Password";
                      }
                      if (value.length < 6) {
                        return "password must be at least 6 digits long";
                      }
                    },
                    showCursor: true,
                    cursorColor: Color(0xFF5C75B3),
                    cursorRadius: Radius.circular(0),
                    style: kTxtStyle,
                    obscureText: true,
                    controller: userPasswordController,
                    keyboardType: TextInputType.visiblePassword,
                    decoration: kTxtFieldDecoration,
                  ),
                  SizedBox(height: 25.0),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 12.0),
                    child: ElevatedButton(
                      onPressed: () {
                        if (_userRegFormKey.currentState!.validate()) {
                          setState(() {
                            isLoading = true;
                            var uName = userNameController.text;
                            var uEmail = userEmailController.text;
                            var uPassword = userPasswordController.text;
                            var uPhnone = userPhoneNoController.text;
                            if (isLoading == true) {
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return Dialog(
                                      backgroundColor: Colors.transparent,
                                      insetPadding: EdgeInsets.only(
                                          left: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.44,
                                          right: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.44),
                                      child: Container(
                                        color: Colors.transparent,
                                        width: double.infinity,
                                        height: 50,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: const [
                                            CircularProgressIndicator(
                                              backgroundColor:
                                                  Colors.transparent,
                                              color: btngoldcolor,
                                            ),
                                          ],
                                        ),
                                      ));
                                },
                              );
                            }
                            regUserData(uName, uEmail, uPhnone, uPassword);
                            isLoading = false;
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => OTPVerificationScreen(
                                  isLoading: false,
                                  uPhone: uPhnone,
                                ),
                              ),
                            );
                          });
                        }
                      },
                      child: Text(
                        'CONTINUE',
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'Raleway-Regular',
                        ),
                      ),
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        fixedSize: Size(420.0, 57.0),
                        primary: Color(0xFF6f009d),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
