import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/Networking/userregAPI/otp_api.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/screens/login/login_src.dart';

late String uotp;

class OTPVerificationScreen extends StatefulWidget {
  final bool isLoading;
  final String uPhone;

  const OTPVerificationScreen(
      {Key? key, required this.isLoading, required this.uPhone})
      : super(key: key);

  @override
  _OTPVerificationScreenState createState() => _OTPVerificationScreenState();
}

class _OTPVerificationScreenState extends State<OTPVerificationScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          elevation: 0.0,
        ),
        backgroundColor: Color(0xFF121A31),
        body: Container(
          margin: EdgeInsets.all(32.0),
          alignment: Alignment.center,
          child: Column(
            children: [
              Image(
                image: AssetImage("assets/images/otp_src_img.png"),
              ),
              SizedBox(height: 24.0),
              Text(
                "Verify your Mobile",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 22.0,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 20.0),
              Text(
                "Enter OTP code here",
                style: TextStyle(color: Colors.white, fontSize: 14.0),
              ),
              SizedBox(height: 30.0),
              OTPForm(
                uPhone: widget.uPhone,
              )
            ],
          ),
        ),
      ),
    );
  }
}

class OTPForm extends StatefulWidget {
  final String uPhone;
  const OTPForm({Key? key, required this.uPhone}) : super(key: key);

  @override
  _OTPFormState createState() => _OTPFormState();
}

class _OTPFormState extends State<OTPForm> {
  late FocusNode pin1;
  late FocusNode pin2;
  late FocusNode pin3;
  late FocusNode pin4;

  TextEditingController p1 = TextEditingController();
  TextEditingController p2 = TextEditingController();
  TextEditingController p3 = TextEditingController();
  TextEditingController p4 = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    pin2 = FocusNode();
    pin3 = FocusNode();
    pin4 = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    print(p1.text + p2.text + p3.text + p4.text);
    pin2.dispose();
    pin3.dispose();
    pin4.dispose();

    // TODO: implement dispose
    super.dispose();
  }

  nextPin({required String value, required FocusNode focusNode}) {
    if (value.length == 1) {
      focusNode.requestFocus();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      child: Padding(
        padding: const EdgeInsets.all(18.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              width: 60.0,
              child: TextFormField(
                controller: p1,
                textAlign: TextAlign.center,
                autofocus: true,
                // obscureText: true,
                onChanged: (value) {
                  nextPin(value: value, focusNode: pin2);
                },

                keyboardType: TextInputType.number,
                style: kOTPStyle,
                decoration: kOTPDecoration,
              ),
            ),
            SizedBox(
              width: 60.0,
              child: TextFormField(
                controller: p2,
                textAlign: TextAlign.center,
                // obscureText: true,
                onChanged: (value) {
                  nextPin(value: value, focusNode: pin3);
                },
                focusNode: pin2,
                keyboardType: TextInputType.number,
                style: kOTPStyle,
                decoration: kOTPDecoration,
              ),
            ),
            SizedBox(
              width: 60.0,
              child: TextFormField(
                controller: p3,
                textAlign: TextAlign.center,
                // obscureText: true,
                onChanged: (value) {
                  nextPin(value: value, focusNode: pin4);
                },
                focusNode: pin3,
                keyboardType: TextInputType.number,
                style: kOTPStyle,
                decoration: kOTPDecoration,
              ),
            ),
            SizedBox(
              width: 60.0,
              child: TextFormField(
                textAlign: TextAlign.center,
                controller: p4,
                // obscureText: true,
                onChanged: (value) async {
                  pin4.unfocus();
                  showDialog(
                    context: context,
                    //barrierDismissible: false,
                    builder: (BuildContext context) {
                      return Dialog(
                          backgroundColor: Colors.transparent,
                          insetPadding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * 0.44,
                              right: MediaQuery.of(context).size.width * 0.44),
                          child: Container(
                            color: Colors.transparent,
                            width: double.infinity,
                            height: 50,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                /*new CircularProgressIndicator(color: const Color(0xff303e9f),),*/
                                // new CircularProgressIndicator(),
                                // ignore: unnecessary_new
                                new CircularProgressIndicator(
                                  backgroundColor: Colors.transparent,
                                  color: btngoldcolor,
                                ),
                              ],
                            ),
                          ));
                    },
                  );

                  uotp = p1.text + p2.text + p3.text + p4.text;
                 await verifyOtp(widget.uPhone, uotp);
                  // Navigator.popUntil(context, (route) => false);
                  Navigator.push(context,
                      MaterialPageRoute(builder: (_) => LoginScreen()));
                },
                focusNode: pin4,
                keyboardType: TextInputType.number,
                style: kOTPStyle,
                decoration: kOTPDecoration,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
