import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Text(
        "Profile",
        style: kHeadText,
      ),
    );
  }
}
