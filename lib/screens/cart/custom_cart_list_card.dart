import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:counter_button/counter_button.dart';
import 'package:lasvegas_gts_app/screens/cart/cart_list.dart';
import 'package:lasvegas_gts_app/screens/cart/cart_list_model.dart';
import 'package:lasvegas_gts_app/screens/cart/custom_counter_btn.dart';
import 'package:lasvegas_gts_app/screens/services/fashion/fashion_booking_scr.dart';
import 'package:lasvegas_gts_app/screens/services/fashion/fashon_product_listing_scr.dart';

String cartFIMG = "http://lasvegas.koolmindapps.com/images/fashion/";
String cartCanaIMG = "http://lasvegas.koolmindapps.com/images/cannabies/";

String sDate = '', sType = '', sCity = '', noPerson = '';
int _counterValue = 1;
int totalPriceinCart = 0;
int totalPriceIndividuals = 0;

class CustomCartListCard extends StatefulWidget {
  final String shortDisc;
  // final String vendorID, add1, add2, vendrDetail;
  // final String sDate, sType, sCity;
  final String bname, price, priceUnit, fashionVendorLogo;
  final List FashionVendorIMG;
  final int index,quantity,productQuantity;

  FashionCart? fashionCart;

  CustomCartListCard({
    Key? key,
    required this.shortDisc,
    required this.FashionVendorIMG,
    required this.bname,
    required this.price,
    required this.priceUnit,
    required this.fashionVendorLogo,
    required this.index,
    required this.quantity,
    required this.productQuantity,
    this.fashionCart
  }) : super(key: key);

  State<CustomCartListCard> createState() => _CustomCartListCardState();
}

class _CustomCartListCardState extends State<CustomCartListCard> {
  @override
  void initState() {
    // print("Vendor fashion id though widget ========" + widget.vendorID);
    // print("Vendor fashion Name though widget ========" + widget.bname);
    // amount = widget.price;
    // vendorFashionID = widget.vendorID;
    // serviceEventSpaceID = widget.eventspsService;
    // foodNameListCatering = widget.foodName;
    setState(() {
      totalPriceIndividuals = int.parse(widget.price);
      widget.fashionCart!.quntity = 1;
      widget.fashionCart!.itemTotal = int.parse(widget.price);
      // sDate = widget.sDate;
      // sCity = widget.sCity;
      // sType = widget.sType;
      // noPerson = widget.noPerson;
    });
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      margin: EdgeInsets.symmetric(vertical: 7.5),
      decoration: BoxDecoration(
        // color: servicecardfillcolor,
        border: Border.all(color: txtborderbluecolor),
        borderRadius: BorderRadius.all(Radius.circular(7.0)),
      ),
      child: Row(
        children: [
          Container(
            height: 150,
            width: 150,
            child: ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(7.0),
                bottomLeft: Radius.circular(7.0),
              ),
              child: Image.network(
                cartFIMG + widget.fashionVendorLogo,
                fit: BoxFit.fill,
                height: 150,
                width: 130,
              ),
              //     Image.asset(
              //   "assets/images/restaurent1.png",
              // ),
            ),
          ),
          SizedBox(
            width: 15.0,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // SizedBox(
                  //   height: 10.0,
                  // ),
                  SizedBox(
                    width: 120,
                    child: Text(
                      widget.bname,
                      // "Event Name",
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18.0,
                          fontFamily: 'Raleway-Medium'),
                    ),
                  ),
                  RatingBar.builder(
                    glow: false,
                    itemSize: 15.0,
                    initialRating: 3,
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    itemCount: 5,
                   // itemPadding: EdgeInsets.symmetric(horizontal: 2.0),
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: Colors.amber,
                      //   Image.asset(
                      // "assets/icons/rating_star.png",
                      // color: Colors.amber,
                    ),
                    // ),
                    unratedColor: Colors.grey,
                    onRatingUpdate: (rating) {
                      print(rating);
                    },
                  ),
                  Text(
                    widget.priceUnit + " " + widget.price,
                    // "456 - Lorem ipsum dolor sit amet,\nconsectetur adipiscing elit.",
                    style: TextStyle(
                        fontSize: 16.0,
                        fontFamily: 'Manrope-SemiBold',
                        color: Colors.white
                    ),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      // print("Vendor fashion id though Btn ========" +
                      //     widget.vendorID);
                      print("Vendor fashion name though Btn ========" +
                          widget.bname);
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => FashionBookings(
                            vendorID: widget.fashionCart!.data!.vendorId!,
                            pkgIMG: cartFIMG + widget.fashionVendorLogo,
                            productPrice: widget.fashionCart!.itemTotal!.toString(),
                            productType: widget.fashionCart!.data!.productType!, ),
                        ),
                      );
                    },
                    child: Text(
                      "Shop Now",
                      style: kTxtStyle.copyWith(color: Colors.black),
                    ),
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.amber)),
                  ),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Quantity",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 18.0,
                        fontFamily: 'Raleway-Medium'),
                  ),
                  CustomCounterButton(
                    count: widget.fashionCart!.quntity,
                    onChange: (int val) {

                      if(val != 0){
                        setState(() {
                          _counterValue = val;

                          widget.fashionCart!.quntity = val;

                          widget.fashionCart!.itemTotal = int.parse(widget.price) * val;
                          totalPriceIndividuals = int.parse(widget.price) * val;
                          totalPriceinCart += totalPriceIndividuals;
                          print("ALL COST =====" + totalPriceinCart.toString());
                          totalProductPrice = totalPriceinCart;
                        });
                      }

                    },
                    loading: false,
                    countColor: Colors.white,
                    buttonColor: btngoldcolor,
                    progressColor: Colors.white, index: widget.index, qty: widget.quantity,
                  ),
                  Text('${widget.priceUnit} ${widget.fashionCart!.itemTotal}',
                    /*widget.priceUnit + totalPriceIndividuals.toString(),*/
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 14.0,
                        fontFamily: 'Raleway-Medium'),
                  ),
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}
