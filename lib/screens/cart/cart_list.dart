import 'package:flutter/material.dart';
import 'package:lasvegas_gts_app/constants/decoration_const.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';
import 'package:lasvegas_gts_app/customs/custom_appbar.dart';
import 'package:lasvegas_gts_app/customs/custom_fashion_vendor_card.dart';
import 'package:lasvegas_gts_app/customs/loading_scr.dart';
import 'package:lasvegas_gts_app/screens/cart/cart_list_api.dart';
import 'package:lasvegas_gts_app/screens/cart/cart_list_model.dart';
import 'package:lasvegas_gts_app/screens/cart/custom_cart_list_card.dart';

bool isCartLoading = false;
List<CannabisCart> cannabiesCart = [];
List<FashionCart> fashionCartData = [];
CartListModel cartListModelData = CartListModel();
 int totalProduct = 0;
int totalProductPrice = 0;



class CartListScr extends StatefulWidget {
  const CartListScr({Key? key}) : super(key: key);

  @override
  State<CartListScr> createState() => _CartListScrState();
}

class _CartListScrState extends State<CartListScr> {


  @override
  void initState() {
    getCartList();
    

    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: bgcolor,
        appBar: CustomAppBar(
          lead: Icon(Icons.shopping_cart_rounded),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "My Cart",
                style: TextStyle(
                    fontSize: 20.0,
                    fontFamily: 'Helvetica',
                    color: Colors.white
                ),
              ),
              Text(
                "Total Poduct: $totalProduct",
                style: TextStyle(
                    fontFamily: 'Raleway-Medium',
                    fontSize: 16.0,
                    color: Colors.white,
                    letterSpacing: 0.2),
              ),
            ],
          ),
        ),
        bottomNavigationBar: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.all(18.0),
          height: 75.0,
          color: servicecardfillcolor,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Total",
                    style: TextStyle(color: Colors.white),
                  ),
                  Text(
                    "\$ "+ totalProductPrice.toString(),
                    style: kTxtStyle,
                  ),
                ],
              ),
              ElevatedButton(
                onPressed: () {
                  setState(() {
                    
                  });
                  // Navigator.push(
                  //     context,
                  //     MaterialPageRoute(
                  //       builder: (_) => AirReviewBooking(
                  //         dDate: widget.DepartureDate,
                  //       fromAP: widget.fromAP,
                  //       fromAPC: widget.fromAPC,
                  //       toAP: widget.toAP,
                  //       toAPC: widget.toAPC,
                  //       tPass: widget.totalGuest,
                  //       helijet: widget.helijet,
                  //       planeType: widget.flightName,
                  //       price: widget.discPrice,
                  //       rDate: widget.returnDate,
                  //       vendorID:widget.vendorID,)
                  //       // StayCheckout(
                  //       //   discPrise: widget.discPrice,
                  //       //   roomName: widget.flightName,
                  //       //   checkInDate: widget.DepartureDate,
                  //       //   checkOutDate: widget.returnDate,
                  //       //   totalGuest: widget.totalGuest,
                  //         // totalRooms: widget.totalRooms,
                  //       // ),
                  //     ));
                },
                child: Text(
                  "Book Now",
                  style: TextStyle(color: Colors.black),
                ),
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  fixedSize: Size(150.0, 50.0),
                  primary: Color(0xFFFFC71E),
                ),
              ),
            ],
          ),
        ),
        body: isCartLoading
            ? CustomLoadingScr()
            : SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 20.0,
                      ),
                      ListView.builder(
                          physics: NeverScrollableScrollPhysics(
                              parent: AlwaysScrollableScrollPhysics()),
                          shrinkWrap: true,
                          itemCount: fashionCartData.length,
                          itemBuilder: (_, index) {
                            return CustomCartListCard(
                              fashionCart: fashionCartData[index],
                              shortDisc:
                                  fashionCartData[index].data!.description!,
                              // vendorID:
                              //     fashionCartData[index].data!.description!,
                              fashionVendorLogo:
                                  fashionCartData[index].data!.photos![0],
                              bname: fashionCartData[index].data!.productName!,
                              price: fashionCartData[index].data!.productPrice!,
                              priceUnit:
                                  fashionCartData[index].data!.priceUnit!,
                              FashionVendorIMG:
                                  fashionCartData[index].data!.photos!, 
                                  index: index, 
                                  productQuantity:int.parse(fashionCartData[index].data!.productQty!.toString()), 
                                  quantity: fashionCartData[index].quntity,
                              // add1: fashionCartData[index].data!.description!,
                              // add2: fashionCartData[index].data!.description!,
                              // vendrDetail:
                              //     fashionCartData[index].data!.description!,
                            );
                          }),
                      // ListView.builder(
                      //     physics: NeverScrollableScrollPhysics(
                      //         parent: AlwaysScrollableScrollPhysics()),
                      //     shrinkWrap: true,
                      //     itemCount: cannabiesCart.length,
                      //     itemBuilder: (_, index) {
                      //       return CustomCartListCard(
                      //         shortDisc: cannabiesCart[index]
                      //             .data!
                      //             .productDetails!, // .description!,
                      //         // vendorID:
                      //         //     cannabiesCart[index].data!.productDetails!,
                      //         fashionVendorLogo:
                      //             cannabiesCart[index].data!.photos![0],
                      //         bname: cannabiesCart[index].data!.productName!,
                      //         price: cannabiesCart[index].data!.productPrice!,
                      //         priceUnit: cannabiesCart[index].data!.priceUnit!,
                      //         FashionVendorIMG:
                      //             cannabiesCart[index].data!.photos!,
                      //         // add1: cannabiesCart[index].data!.serviceName,
                      //         // add2: cannabiesCart[index].data!.serviceName,
                      //         // vendrDetail:
                      //         //     cannabiesCart[index].data!.productDetails!,
                      //       );
                      //     }),
                    ],
                  ),
                ),
              ),
      ),
    );
  }

  Future getCartList() async {
    setState(() {
      isCartLoading = true;
    });

    try {
      cartListModelData = await CartListApis().getCartList();
      // RentalsAPIs().getRentalVendorData(widget.rentalCityCode, widget.rentalDate, widget.rentalCarName);
      cannabiesCart = cartListModelData.data!;
      fashionCartData = cartListModelData.demo!;

      setState(() {
      totalProduct = fashionCartData.length + cannabiesCart.length;

    });
    } catch (e) {
      print("Error DATA =====" + e.toString());
    }

    setState(() {
      isCartLoading = false;
    });
  }
}
