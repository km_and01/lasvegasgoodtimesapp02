// To parse this JSON data, do
//
//     final cartListModel = cartListModelFromJson(jsonString);

import 'dart:convert';

CartListModel cartListModelFromJson(String str) =>
    CartListModel.fromJson(json.decode(str));

String cartListModelToJson(CartListModel data) => json.encode(data.toJson());

class CartListModel {
  CartListModel({
    this.message,
    this.statusCode,
    this.data,
    this.demo,
  });

  String? message;
  int? statusCode;
  List<CannabisCart>? data;
  List<FashionCart>? demo;

  factory CartListModel.fromJson(Map<String, dynamic> json) => CartListModel(
        message: json["message"],
        statusCode: json["status_code"],
        data: List<CannabisCart>.from(
            json["data"].map((x) => CannabisCart.fromJson(x))),
        demo: List<FashionCart>.from(
            json["demo"].map((x) => FashionCart.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "status_code": statusCode,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
        "demo": List<dynamic>.from(demo!.map((x) => x.toJson())),
      };
}

class CannabisCart {
  CannabisCart({
    this.id,
    this.userId,
    this.productId,
    this.cartTotalPrice,
    this.createdAt,
    this.updatedAt,
    this.data,
  });

  int? id;
  String? userId;
  String? productId;
  String? cartTotalPrice;
  DateTime? createdAt;
  DateTime? updatedAt;
  CannabisCartData? data;

  factory CannabisCart.fromJson(Map<String, dynamic> json) => CannabisCart(
        id: json["id"],
        userId: json["user_id"],
        productId: json["product_id"],
        cartTotalPrice: json["cart_total_price"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        data: CannabisCartData.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "product_id": productId,
        "cart_total_price": cartTotalPrice,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
        "data": data!.toJson(),
      };
}

class CannabisCartData {
  CannabisCartData({
    this.id,
    this.vendorId,
    this.serviceName,
    this.productName,
    this.productQty,
    this.productQtyUnit,
    this.productPrice,
    this.priceUnit,
    this.productDetails,
    this.photos,
    this.status,
    this.createdAt,
    this.updatedAt,
  });

  int? id;
  String? vendorId;
  dynamic serviceName;
  String? productName;
  String? productQty;
  String? productQtyUnit;
  String? productPrice;
  String? priceUnit;
  String? productDetails;
  List<String>? photos;
  int? status;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory CannabisCartData.fromJson(Map<String, dynamic> json) =>
      CannabisCartData(
        id: json["id"],
        vendorId: json["vendor_id"],
        serviceName: json["service_name"],
        productName: json["product_name"],
        productQty: json["product_qty"],
        productQtyUnit: json["product_qty_unit"],
        productPrice: json["product_price"],
        priceUnit: json["price_unit"],
        productDetails: json["product_details"],
        photos: List<String>.from(json["photos"].map((x) => x)),
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "vendor_id": vendorId,
        "service_name": serviceName,
        "product_name": productName,
        "product_qty": productQty,
        "product_qty_unit": productQtyUnit,
        "product_price": productPrice,
        "price_unit": priceUnit,
        "product_details": productDetails,
        "photos": List<dynamic>.from(photos!.map((x) => x)),
        "status": status,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
      };
}

class FashionCart {
  FashionCart({
    this.id,
    this.userId,
    this.productId,
    this.cartTotalPrice,
    this.createdAt,
    this.updatedAt,
    this.data,
    required this.quntity,
    this.itemTotal,
  });

  int? id;
  String? userId;
  String? productId;
  String? cartTotalPrice;
  DateTime? createdAt;
  DateTime? updatedAt;
  DemoData? data;
  ////custom vars
  int quntity = 0;
  int? itemTotal;

  factory FashionCart.fromJson(Map<String, dynamic> json) => FashionCart(
        id: json["id"],
        userId: json["user_id"],
        productId: json["product_id"],
        cartTotalPrice: json["cart_total_price"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
        data: DemoData.fromJson(json["data"]), 
        quntity:0,// quntity,
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "product_id": productId,
        "cart_total_price": cartTotalPrice,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
        "data": data!.toJson(),
      };
}

class DemoData {
  DemoData({
    this.id,
    this.vendorId,
    this.productType,
    this.productName,
    this.productQty,
    this.productQtyUnit,
    this.productPrice,
    this.priceUnit,
    this.description,
    this.photos,
    this.parameterName,
    this.parameterValue,
    this.status,
    this.createdAt,
    this.updatedAt,
  });

  int? id;
  String? vendorId;
  String? productType;
  String? productName;
  String? productQty;
  String? productQtyUnit;
  String? productPrice;
  String? priceUnit;
  String? description;
  List<String>? photos;
  List<ParameterName>? parameterName;
  List<String>? parameterValue;
  int? status;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory DemoData.fromJson(Map<String, dynamic> json) => DemoData(
        id: json["id"],
        vendorId: json["vendor_id"],
        productType: json["product_type"],
        productName: json["product_name"],
        productQty: json["product_qty"],
        productQtyUnit: json["product_qty_unit"] == null
            ? 'null'
            : json["product_qty_unit"],
        productPrice: json["product_price"],
        priceUnit: json["price_unit"],
        description: json["description"],
        photos: List<String>.from(json["photos"].map((x) => x)),
        parameterName: List<ParameterName>.from(
            json["parameter_name"].map((x) => parameterNameValues.map![x])),
        parameterValue:
            List<String>.from(json["parameter_value"].map((x) => x)),
        status: json["status"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "vendor_id": vendorId,
        "product_type": productType,
        "product_name": productName,
        "product_qty": productQty,
        "product_qty_unit": productQtyUnit == null ? null : productQtyUnit,
        "product_price": productPrice,
        "price_unit": priceUnit,
        "description": description,
        "photos": List<dynamic>.from(photos!.map((x) => x)),
        "parameter_name": List<dynamic>.from(
            parameterName!.map((x) => parameterNameValues.reverse![x])),
        "parameter_value": List<dynamic>.from(parameterValue!.map((x) => x)),
        "status": status,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
      };
}

enum ParameterName { COLOR, SIZE, HEIGHT, WIDTH }

final parameterNameValues = EnumValues({
  "Color": ParameterName.COLOR,
  "Height": ParameterName.HEIGHT,
  "Size": ParameterName.SIZE,
  "Width": ParameterName.WIDTH
});

class EnumValues<T> {
  Map<String, T>? map;
  Map<T, String>? reverseMap;

  EnumValues(this.map);

  Map<T, String>? get reverse {
    if (reverseMap == null) {
      reverseMap = map!.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
