import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:lasvegas_gts_app/customs/colors.dart';

class CustomCounterButton extends StatefulWidget {
  CustomCounterButton(
      {Key? key,
      required this.count,
      required this.onChange,
      required this.loading,
      this.countColor = Colors.black,
      this.addIcon = const Icon(Icons.add),
      this.removeIcon = const Icon(Icons.remove),
      this.buttonColor = Colors.black,
      this.progressColor = Colors.black,
      required this.index,
      required this.qty})
      : super(key: key);
  int qty;
  final int count, index;

  final Color countColor;

  final ValueChanged<int> onChange;

  final bool loading;

  final Color progressColor;

  final Color buttonColor;

  final Icon addIcon;

  final Icon removeIcon;

  @override
  _CustomCounterButtonState createState() => _CustomCounterButtonState();
}

class _CustomCounterButtonState extends State<CustomCounterButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: servicecardfillcolor,
        border: Border.all(width: 0.5),
        borderRadius: BorderRadius.circular(7.0),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4.0),
        child: IntrinsicWidth(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              if (widget.loading)
                LinearProgressIndicator(
                  valueColor:
                      AlwaysStoppedAnimation<Color>(widget.progressColor),
                  backgroundColor: Colors.transparent,
                ),
              Padding(
                padding: const EdgeInsets.all(8),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    SizedBox(
                      width: 25.0,
                      height: 25.0,
                      child: IconButton(
                        onPressed: widget.loading
                            ? null
                            : () {
                                widget.onChange(widget.count - 1);
                                setState(() {
                                  widget.qty = widget.qty - 1;
                                });
                              },
                        icon: widget.removeIcon,
                        padding: EdgeInsets.zero,
                        color: widget.buttonColor,
                      ),
                    ),
                    AnimatedSwitcher(
                      duration: const Duration(milliseconds: 250),
                      layoutBuilder: (Widget? currentChild,
                          List<Widget> previousChildren) {
                        return currentChild!;
                      },
                      transitionBuilder:
                          (Widget child, Animation<double> animation) {
                        final Animation<Offset> inAnimation = Tween<Offset>(
                                begin: const Offset(1.0, 0.0), end: Offset.zero)
                            .animate(animation);
                        final Animation<Offset> outAnimation = Tween<Offset>(
                                begin: const Offset(-1.0, 0.0),
                                end: Offset.zero)
                            .animate(animation);

                        if (child.key.toString() == widget.count.toString()) {
                          return ClipRect(
                            child: SlideTransition(
                                position: inAnimation, child: child),
                          );
                        } else {
                          return ClipRect(
                            child: SlideTransition(
                                position: outAnimation, child: child),
                          );
                        }
                      },

                      //////counter value
                      child: SizedBox(
                        key: Key(widget.count.toString()),
                        width: 25.0,
                        height: 25.0,
                        child: Center(
                          child: Text(
                            widget.count.toString(),
                            style: TextStyle(
                                fontSize: 18.0,
                                fontWeight: FontWeight.w600,
                                color: widget.countColor),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 25.0,
                      height: 25.0,
                      child: IconButton(
                        onPressed: widget.loading
                            ? null
                            : () {
                                widget.onChange(widget.count + 1);
                                 setState(() {
                                  widget.qty = widget.qty + 1;
                                });
                              },
                        icon: widget.addIcon,
                        padding: EdgeInsets.zero,
                        color: widget.buttonColor,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
